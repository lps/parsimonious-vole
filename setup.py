from setuptools import setup, find_packages


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name="parsimonious_vole",
    version="0.4",
    author="Eugeniu Costetchi",
    author_email="costezki.eugen@gmail.com",
    description='A systemic functional Parser based on dependecy parse',
    license="GNU GPLv3",
    keywords="syntax parser nlp natural language processing systemic functional grammar",
    url="https://bitbucket.org/lps/parsimonious-vole",

    install_requires=[
        "nltk", "xmltodict", "lxml", "beautifulsoup4", "click", "networkx", "matplotlib", "graphviz", "pygraphviz",
        "pydot",
        "pyyaml", "chardet"
    ],

    # packages=['magic', 'uam_corpus_tool_interface', "usage", "evaluation", "test"],
    packages=find_packages(),
    include_package_data=True,
    long_description=readme(),

    package_data={"": ["*.txt"]},
    # data_files={"": ["*.txt"]},

    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Information Technology",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Human Machine Interfaces",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Software Development :: Libraries",
        "Topic :: Text Processing :: Linguistic"
    ],
    entry_points="""
        [console_scripts]
        vole=parsimonious_vole.usage.vole:cli
    """
)
