#!/bin/bash
# Created 18 Feb 2014
# Author Eugeniu Costetchi <costezki.eugen@gmail.com>

function overwriteLexicalizedParserCommand {
    echo "--------------------------------------------------------------";
    echo "Copying custom command line scripts for ${1}";
    echo "--------------------------------------------------------------";

    cp ./resources/lexparser.sh ./third-party/${1}/lexparser_.sh;
    chmod +x ./third-party/${1}/lexparser_.sh

    cp ./resources/lexparser.bat ./third-party/${1}/lexparser_.bat;
    chmod +x ./third-party/${1}/lexparser_.bat
}

function downloadStanfordParser {
    echo "--------------------------------------------------------------";
    echo "Downloading Stanford Parser as a third party library";
    echo "Version: ${1}";
    echo "--------------------------------------------------------------";
    mkdir third-party;
    wget --directory-prefix=./third-party/ http://nlp.stanford.edu/software/${1};
    unzip ./third-party/${1} -d ./third-party/;
}

function downloadPunct {
    echo "--------------------------------------------------------------";
    echo "Please make sure that *punkt* model in models section is downloaded";
    echo "--------------------------------------------------------------";
    python -c "import nltk; nltk.download()";
}

function installDependencies {
    echo "--------------------------------------------------------------";
    echo "Updating repositories";
    echo "--------------------------------------------------------------";
    sudo apt-get update;

    echo "--------------------------------------------------------------";
    echo "Installing Basic APT libraries";
    echo "--------------------------------------------------------------";
    sudo apt-get -y install openjdk-8-jdk git unzip;

    echo "--------------------------------------------------------------";
    echo "Installing APT libraries";
    echo "--------------------------------------------------------------";
    sudo apt-get -y install python-matplotlib python-pip libgraphviz-dev pkg-config graphviz python-pygraphviz python-networkx; #qt4-default qt4-dev-tools pyqt4-dev-tools ;

    echo "--------------------------------------------------------------";
    echo "Installing PIP libraries";
    echo "--------------------------------------------------------------";
    sudo pip install --upgrade nltk xmltodict lxml beautifulsoup4 click
}

function makeEnvironmentScript {
    echo "--------------------------------------------------------------";
    echo "Make the environment setup script";
    echo "--------------------------------------------------------------";
}

function makeApplications {
    echo "--------------------------------------------------------------";
    echo "Make the *apps* executables ";
    echo "--------------------------------------------------------------";
}


#####################################################################
# main script
#####################################################################
FILE_NAME_DEV="stanford-parser-full-2014-06-16.zip";
DIR_NAME_DEV="stanford-parser-full-2014-06-16";
FILE_NAME_LATEST="stanford-parser-full-2016-10-31.zip";
DIR_NAME_LATEST="stanford-parser-full-2016-10-31";

#installDependencies
downloadStanfordParser ${FILE_NAME_DEV}
downloadStanfordParser ${FILE_NAME_LATEST}
overwriteLexicalizedParserCommand ${DIR_NAME_DEV}
overwriteLexicalizedParserCommand ${DIR_NAME_LATEST}
downloadPunct