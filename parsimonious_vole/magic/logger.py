'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Apr 2, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import logging

from parsimonious_vole.configuration import LOG_FILE


def getLogger():
    # create logger with 'spam_application'
    logger = logging.getLogger('magic_parser')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler(LOG_FILE)
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger

def logInfo(message):
    getLogger().info(message)

def logDebug(message):
    getLogger().debug(message)

def logError(message):
    getLogger().error(message)

def logWarning(message):
    getLogger().warning(message)

def logException(message):
    getLogger().exception(message)
    
if __name__ == '__main__':
    pass