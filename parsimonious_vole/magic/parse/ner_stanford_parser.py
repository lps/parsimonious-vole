'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 12, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import ner

def parse_ner(text):
    """
        a client getting NER
    """
    tagger = ner.SocketNER(host='localhost', port=8312, output_format="slashTags")
    print tagger.tag_text(text)
    entities = tagger.get_entities(text) 
    print entities

if __name__ == '__main__':
    parse_ner("University of California is located in California, United States")
    pass