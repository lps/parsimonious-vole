'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 26, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

reads the Process Type Data Base and provides and interface to it
'''
import cPickle as pickle
import itertools

from parsimonious_vole.magic.guess.fs_constants import MANDATORY, OPTIONAL
from parsimonious_vole.magic.utils import singleton, strip, flatten

from parsimonious_vole.configuration import PICKLE_SOURCE
from parsimonious_vole.magic.parse.VerbForm import FORM_STRIPPED, PROCESS_TYPE, PARTIC, PARTICIPANT_CANONICAL,\
    PREP_MANDT, PREP_OPT


@singleton
class PTDB:
    """ reads the Process Type Data Base and provides and interface to it"""
    _db = []
    _indexed_db = {}
    _flat_indexed_db = {}

    def __init__(self):
        file = open(PICKLE_SOURCE, "rb")
        self._db = pickle.load(file)
        self.__index_db()
        self.__flaten_indexed_db()
   
    def __flaten_indexed_db(self):
        for i in self._indexed_db:
            self._flat_indexed_db[i] = self.__get_flatened_entries(i)  
        
    def __index_db(self):
        """ indexes the verbs by their forms regardless of prepositions """
        for i in self._db:
            if i:
                if i.form in self._indexed_db:
                    self._indexed_db[i.form].append(i)
                else: 
                    self._indexed_db[i.form] = [i]
    
    def __get_flatened_entries(self, lex):
        """ flatens the VerbForm into an dict array for each sense"""
        if lex not in self._indexed_db:
            return None
        res = []
        for i in self._indexed_db[lex]:
            for j in i.entries:
                v = j.copy()
                v.update({PREP_OPT:i.prep_optional, PREP_MANDT:i.prep_mandatory, FORM_STRIPPED:i.form})
                res.append(v)
        return res 
    
    def get_all_entries(self, lex=None):
        if lex is None: return self._flat_indexed_db 
        if lex in self._flat_indexed_db:
            return self._flat_indexed_db[lex]
        return []
    
    def print_all(self):
        print "len of indexed db is ", len(self._indexed_db)
        print "len of flat db is ", len(self._flat_indexed_db) 
        count = 0
        for i in self._flat_indexed_db:
            count += len(i)
        print "total number of entries is ", count
        # print self._flat_indexed_db

#utils
db = PTDB()

def entry_canonic_form(entry):
    """
        cannonic form after PTDB v.3.2
        ignores the optional roles and treats them all as mandatory
        
        test_data: {'participants': [{'mandatory': 'Ag'},  
                            {'optional': 'Ca'}, {'mandatory': 'Af-Ca'}, 
                            {'mandatory': 'At'}, {'optional': 'Ca'}, 
                            {'canonical': ('Ag', 'Af-Ca', 'At')}], 'prep_mandatory': [], 
        'process_type': 'attributive', 'sense': 'be of opinion (they do not consider a child as important)', 'example_and_synonym': 'be of opinion (they do not consider a child as 
 important)', 'cardiff_feature': 'attributive', 'form_stripped': 'consider', 'levin_feature': '', 
 'prep_optional': []}
    """
    lemma = entry[FORM_STRIPPED]
    process_type = strip(entry[PROCESS_TYPE])
    configuration = filter(lambda x: PARTICIPANT_CANONICAL in x, entry[PARTIC])[0][PARTICIPANT_CANONICAL]
    prepositions = tuple(entry[PREP_MANDT])
    return (lemma,prepositions,process_type,configuration)

def entry_canonic_form_with_variations(entry):
    """
        cannonic form after PTDB v.3.2
        same as entry_canonic_form(entry) but returns a complete set of variations for participant roles by taking 
        into consideration optional roles
    """
    def strip_split(x):
        """ 
            split that handles AND/OR just instead of strip split below that handles only OR
            #strip_split = lambda x: [strip(y) for y in x.split("/") if strip(y)]
            #test set : [('Ag', 'Af'), ('Ag', 'So/Des')]
        """
        content = filter(None, [strip(y) for y in x.split("/")])
        result = []
        for i in range(len(content)+1): result.extend((itertools.permutations(content,i)))
        return [x for x in result if x]
    
    lemma = entry[FORM_STRIPPED]
    process_type = strip(entry[PROCESS_TYPE])
    prepositions = tuple(entry[PREP_MANDT])
    configurations = list(role_variation_generator(entry[PARTIC]))
    ext_conf = []
    for y in configurations: ext_conf.extend(list(itertools.product(* map(strip_split,y))))
    ext_conf = [tuple(flatten(x)) for x in ext_conf]
    return itertools.product([lemma],[prepositions],[process_type],ext_conf)

def role_variation_generator(partitipants_entry_list,result=()):
    """
        returns exhaustive combinatoric variations of participant roles that include/exclude optional roles 
        # [{'mandatory': 'Ag'}, {'mandatory': 'Af'}, {'optional': 'Ca'}, {'canonical': ('Ag', 'Af','Ca')}]
        # [{'mandatory': 'Ag'}, {'optional': 'Ca'}, {'mandatory': 'Af'}, {'optional': 'Ca'}, {'canonical': ('Ag', 'Af','Ca')}]
    """
    if partitipants_entry_list:
        if MANDATORY in partitipants_entry_list[0]:
            for i in role_variation_generator(partitipants_entry_list[1:], result + tuple([partitipants_entry_list[0][MANDATORY]])): yield i
        elif OPTIONAL in partitipants_entry_list[0]:
            for i in role_variation_generator(partitipants_entry_list[1:], result): yield i 
            for i in role_variation_generator(partitipants_entry_list[1:], result + tuple([partitipants_entry_list[0][OPTIONAL]])): yield i
        else: 
            for i in role_variation_generator(partitipants_entry_list[1:], result): yield i
    
    if not partitipants_entry_list:
        yield result
    
#end utils
if __name__ == '__main__':
    pass
