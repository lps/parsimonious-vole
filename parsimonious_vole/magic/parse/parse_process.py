'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 28, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

responsible for :
* reading XML output of stanford NLP parser

* transforming it into a parse graph where the 
    nodes are the word tokens (Token class) and 
    edges the dependency relations
    
* process graph in order to extract main verbs 
    (acting as processes), nouns dependent on verbs
    (process participants) etc. (to be specified latter)
    
    TODO: add reached description   
'''

import json
import logging
import random
import re
import uuid

import networkx as nx
import nltk
import xmltodict
from parsimonious_vole.magic.parse.parse_constants import ROOT, DOCUMENT, SENTENCE, SENTENCES, \
    TOKEN, TOKENS, DEP, DDEPENDENCIES, BASIC_DEPENDENCIES, WORD, TYPE, \
    COLLAPSED_CCPROCESSED_DEPENDENCIES, GOVERNOR, IDX, ID, LEMMA, NER, POS, \
    DEPENDENT, TEXT
from parsimonious_vole.magic.utils import natsort
from nltk.featstruct import FeatDict

from parsimonious_vole.configuration import DUKE_AUNT_TEAPOT_XML
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors


class Token(object):
    """ 
        this class is the data structure attached to the graph nodes
        it contains the elements from teh parser like POS, word, NER
        but also the Feature Structure which is a consequence of 
        further processing of the token
    """
    _id = None
    word = None
    lemma = None
    pos = None
    ner = None
    synset = None
    
    __uuid = None 
    
    _constituency_stack = None
    """ 
        keeps all the FS attached to this token, the first FS is the richest 
        one , the following ones are FS that could not be unified with the first one
    """
    _feature_structures = None
        
    def __init__(self, ids, word, lemma, pos, ner):
        try:
            self._id = int(ids)
        except ValueError:
            self._id = -1

        self.word = word
        self.lemma = lemma
        self.pos = pos
        self.ner = ner
        self._feature_structures = [FeatDict()]
        self._constituency_stack = []
        self.__uuid = uuid.uuid4()
    
    def __str__(self):
        return "(" + str(self._id) + ")" + str(self.word) + "/" + str(self.pos)
    
    def __repr__(self):
        return self.__str__()
    
    def feature_structure(self):
        """returns the head feature structure that is supposed to be the
         richest and most complete FS
         """
        return self._feature_structures[0]
    
    def all_feature_structures(self):
        """
        returns all the FS attached to this token, the first FS is the richest 
        one , the following ones are FS that could not be unified with the first one
        """
        return self._feature_structures
    
    #TODO: schedule remove
    def unify_or_append(self, new_feature_structure):
        
        if new_feature_structure is None:
            logging.warn("no feature structure to append/unify") 
            return
        ufs = self.feature_structure().unify(new_feature_structure)
        if ufs:
            self._feature_structures[0] = ufs
        else:
            self._feature_structures.append(new_feature_structure)
    #Unudes
    def fs_append(self, new_feature_structure):
        self._feature_structures.append(new_feature_structure)
        
    def id(self):
        try:return int(self._id)
        except ValueError: return -1
    
    def add_constituent(self, constituent):
        if constituent not in self._constituency_stack: 
            self._constituency_stack.append(constituent)
    
    def pop_constituent(self):
        return self._constituency_stack.pop()
        
    def current_constituent(self):
        return self._constituency_stack[-1]
    
    def constituents(self):
        return self._constituency_stack
    
    def __eq__(self, other):
        if other:
            return hash(self) == hash(other)
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __cmp__(self, other):
        if other:
            if  self._id < other._id: return -1
            elif self._id > other._id: return +1
            elif self._id == other._id: return 0
        return -1
    
    def __hash__(self): 
        return hash(self.__uuid)
    
    def erase_features_and_constituency_stack(self):
        del self._constituency_stack
        del self._feature_structures

class ParseResult(object):
    """ TODO: """
    sentence_data = None
    sentence_graphs = None
    
    def __init__(self, source_file):
        raise NotImplementedError
    
    def tokens(self, sentenceId=0):
        """ returns the list of tokens for a sentence"""
        raise NotImplementedError
    
    def words(self, sentenceId=0):
        """ return the list of words of a sentence"""
        raise NotImplementedError
    
    def dependencies(self, sentenceId=0):
        raise NotImplementedError
    
    def generate_dep_graph(self, sentenceId=0):
        raise NotImplementedError
    
    def graphs(self):
        return self.sentence_graphs
    
class ParseResult_XML(ParseResult):
    """ 
    reads the XML file that is output of stanfordNLP parser and transform it into a networkX graph
    
    TENSE_TEST_XML | PARSE_XML | DUKE_AUNT_TEAPOT_XML | POLARITY_TEST | POTATOES_TEST_EXAMPLE
     | PHRASAL_TWO | PHRASAL_THREE | REL_DIRECTIONAL_EXAMPLES | ONE_ROLE_EXAMPLES | TWO_ROLE_EXAMPLES
    """
    
    
    # sentence_text = None
    
    def __init__(self, test_file=DUKE_AUNT_TEAPOT_XML):
        """
        TENSE_TEST_XML | PARSE_XML | DUKE_AUNT_TEAPOT_XML | POLARITY_TEST
        TODO: to be extended to receive parses on the fly 
        """
        logging.info("Reading dependency parse result from file:"+str(test_file))
        text = open(test_file).read()
        doc = json.loads(json.dumps(xmltodict.parse(text)))
        self.sentence_data = doc[ROOT][DOCUMENT][SENTENCES][SENTENCE]
        self.sentence_graphs = self.__generate_sentence_graphs()
        enrich_with_word_senses(self)
        
    def __generate_sentence_graphs(self):
        return [self.generate_dep_graph(i) for i in range(len(self.sentence_data))]
    
    def tokens(self, sentenceId=0):
        """ returns the set of tokens for a sentence,indexed by id """
        return self.sentence_data[sentenceId][TOKENS][TOKEN]
    
    def words(self, sentenceId=0):
        """ """
        return map(lambda i: i[WORD], self.tokens(sentenceId))
    
    def dependencies(self, sentenceId=0, cc_collapsed=True):
        """ returns the set of dependencies for a Sentence, indexed by id """
        result = None
        if cc_collapsed:
            try:
                result = self.sentence_data[sentenceId][COLLAPSED_CCPROCESSED_DEPENDENCIES][DEP]
            except KeyError:
                for i in self.sentence_data[sentenceId][DDEPENDENCIES]:
                    if i[TYPE] == COLLAPSED_CCPROCESSED_DEPENDENCIES: 
                        result = i[DEP] 
        else:
            try:
                result = self.sentence_data[sentenceId][BASIC_DEPENDENCIES][DEP]
            except KeyError:
                for i in self.sentence_data[sentenceId][DDEPENDENCIES]:
                    if i[TYPE] == self.BASIC_DEPENDENCIES: 
                        result = i[DEP]
        return result if isinstance(result, list) else [result] 
    
    def generate_dep_graph(self, sentenceId=0):
        """ parses the dictionary structure obtained from 
        XML (parse result file) file and transforms it into 
        a networkX graph with nodes of type Token, and edges 
        containing data the relation type
         """
        dx = nx.DiGraph()
        nodes = {}
        for t in self.tokens(sentenceId):
            nodes[t[ID]] = Token(t[ID], t[WORD], t[LEMMA], t[POS], t[NER])
            dx.add_node(nodes[t[ID]])
        
        for d in self.dependencies(sentenceId):
            type_dep = d[TYPE]
            if type_dep != "root":
                dx.add_edge(nodes[d[GOVERNOR][IDX]], nodes[d[DEPENDENT][IDX]], {TYPE:type_dep})
        
        # creating in each node, an attribute #text which contains the text of the node
        for n in dx.nodes():
            dx.node[n] = {TEXT:str(n), TOKEN:n}
        return dx
    
class ParseResult_txt(ParseResult):
    """ reads the text file that is output of stanfordNLP parser and transform it into a networkX graph  
        ( Stanford parser parameters: -outputFormat "words,wordsAndTags, typedDependenciesCollapsed" 
          -outputFormatOptions "stem" )
    """
    punctuation_tags = ["$", "``", "''", ")", "(", ",", "--","-", ".", ":"]
    
    def __init__(self, text_file):
        with open(text_file, "r") as fl:
            self.__read_file_into_sentence_data(fl.read())
            #self.sentence_data = self.__merge_sentence_data()
        self.sentence_graphs = self.__generate_sentence_graphs()
        enrich_with_word_senses(self)
        #print "PR Sentence Graphs: ",[x/1024 for x in get_recursive_size(self.sentence_graphs)]
        #print "PR Sentence Data: ",[x/1024 for x in get_recursive_size(self.sentence_data)]
   
    def __generate_sentence_graphs(self):
        sentence_graphs = []
        for i in self.sentence_data.keys():
            try:
                sentence_graphs.append(self.generate_dep_graph(i))
            except Exception, e:
                print e
        return sentence_graphs 
   
    def __read_file_into_sentence_data(self, f):
        """ reads the STP file into a data structure"""
        self.sentence_data = {}
        index = 0

        def parse_dependency_line(line):
            """ re based """
            mo = re.match(u"^(\w*)\((.*)-(\w*),\s(.*)-(\w*)\)", line, re.M|re.U|re.I)
            if not mo or not mo.groups(): logging.error("Could not parse the dependency line:"+str(line))
            return mo.groups()[0],mo.groups()[1],int(mo.groups()[2]),mo.groups()[3],int(mo.groups()[4])
        
        def get_parse_groups(text):
            """ return the file as a list of groups of lines"""
            p = "(.*\n{2}){1}(.*/.*\n{2}){1}((\w*\(.*-\w*,\s.*-\w*\)\n{0,2})*)"
            return [(x[0].replace("\n",""),x[1].replace("\n",""),filter(None,x[2].split("\n"))) for x in re.findall(p,text)]
        
        for line1, line2, lines in get_parse_groups(f):#groups_of_n(lsplit(list(f), delim=["\n", " \n", "", " "]), 3):
            self.sentence_data[index] = {}
            self.sentence_data[index][WORD] = nltk.word_tokenize(line1)
            #self.sentence_data[index][TOKEN] = line2.split(" ")
            
            # merge Tokens
            #tokens [['Four', 'four', 'CD', 0, 1]]
            words = [x for x in line1.split(" ") if x]
            tokens = [x for x in line2.split(" ") if x]
            if len(words) != len(tokens):
                logging.error("sentence indexing problem. Mismatch is:" +str(len(words))+" words and "+str(len(tokens))+" tokens in \n "+str(line1))
            else: # ok 
                self.sentence_data[index][TOKEN] = []
                for idx in range(len(words)):
                    if "/" not in tokens[idx]:
                        logging.error("incorrectly specified token ",tokens[idx],". It needs to be in form Lemma/POS.")
                    else:
                        self.sentence_data[index][TOKEN].append(
                                                                (words[idx],
                                                                 tokens[idx].split("/")[0],
                                                                 tokens[idx].split("/")[1],
                                                                 idx+1)
                                                                )
            self.sentence_data[index][COLLAPSED_CCPROCESSED_DEPENDENCIES] = [parse_dependency_line(l) for l in lines if parse_dependency_line(l)]
            #deps [('num', 'week', 2, 'four', 1)]
            self.sentence_data[index][COLLAPSED_CCPROCESSED_DEPENDENCIES] = [x for x in self.sentence_data[index][COLLAPSED_CCPROCESSED_DEPENDENCIES] if x[0].lower() != "root"]
            index += 1
    
    def words(self, sentenceId=0):
        return self.sentence_data[sentenceId][WORD]
    
    def tokens(self, sentenceId=0):
        return self.sentence_data[sentenceId][TOKEN]
    
    def dependencies(self, sentenceId=0):
        if COLLAPSED_CCPROCESSED_DEPENDENCIES in self.sentence_data[sentenceId]:  
            return self.sentence_data[sentenceId][COLLAPSED_CCPROCESSED_DEPENDENCIES]
        return []
    
    def generate_dep_graph(self, sentenceId=0):
        """ """
        #print "sent Data I: %s "%sentenceId, [x/1024 for x in get_recursive_size(self.sentence_data[sentenceId])]
        dx = nx.DiGraph()
        #print "empty graph: %s "%sentenceId, [x/1024 for x in get_recursive_size(dx)]
        #print hpy().heap()
        
        nodes = {}
        for i in self.tokens(sentenceId):
            nodes[i[3]] = Token(i[3], i[0], i[1], i[2], None)
            dx.add_node(nodes[i[3]])
        #print "nodes: %s "%sentenceId, [x/1024 for x in get_recursive_size(nodes)]
        
        for dep in self.dependencies(sentenceId):
            try:
                node1 = nodes[dep[2]]
                node2 = nodes[dep[4]]
                depp = {TYPE:dep[0]}
                dx.add_edge(node1, node2, depp)
            except Exception,e:
                logging.warn("Could not create the complete dependency graph. skipped "+str(dep))
        
        #print "nodes: %s "%sentenceId, [x/1024 for x in get_recursive_size(nodes)]
        
        # creating in each node, an attribute #text which contains the text of the node
        for n in dx.nodes():
            dx.node[n] = {TEXT:str(n), TOKEN:n}
        #print "nodes: %s "%sentenceId, [x/1024 for x in get_recursive_size(nodes)]
        return dx

def enrich_with_word_senses(parse_result):
    """ use PyWSD, if installed, to add word senses """
    try:
        import pywsd
    except:
        logging.warn("PyWSD not installed, word sense disambiguation task failed") 
        return
    poses = ['NN','VB','JJ']
    for graph in parse_result.graphs():
        sent = " ".join([n.word for n in natsort(graph.nodes())])
        for w in graph.nodes():
            if w.pos[:2] in poses:
                w.synset = pywsd.lesk.cosine_lesk(sent, w.word)
        
def get_parse_result(file_name):
    if file_name[-4:] == ".xml":
        return  ParseResult_XML(file_name)
    elif file_name[-4:] == ".stp":
        return  ParseResult_txt(file_name)
    else: 
        raise IOError("The filename is not an .xml or .stp file type")

def get_sentence_dependecy(sentence_id, bundle_file, corrected):
    """ returns the sentence from the bundle file.
        if sentence Id is not specified or higher than available
        then a random sentence is chosen 
        from the test bed 
    """
    result = None
    p = get_parse_result(bundle_file)
    if not p: return result
    
    if sentence_id is not None:  
        if len(p.sentence_graphs) > sentence_id: 
            result = p.sentence_graphs[sentence_id]
    else: result = p.sentence_graphs[ random.randint(0, len(p.sentence_graphs) - 1)]
    
    # Corrections 
    if result and corrected: return correct_dependecy_parse_errors(result)
    return result 

def test_sentence_graph(sentence_id=None, test_bed=DUKE_AUNT_TEAPOT_XML, corrected=False):
    """ 
    @Obsolete: TODO: schedule for removal 
    returns the sentence from the TEST_BED, if not specified returns sentence from AUNT_DUKE file.
    if sentence Id is not specified or higher available sentences then a random sentence is chosen 
    from the test bed
    """
    return get_sentence_dependecy(sentence_id, test_bed, corrected)

if __name__ == '__main__':
    pass


