'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 12, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

# stanfordNL parser structure tags of XML output
ROOT = "root"
DOCUMENT = "document"
SENTENCES = "sentences"
SENTENCE = "sentence"
TOKENS = "tokens"
TOKEN = "token"
COLLAPSED_CCPROCESSED_DEPENDENCIES = "collapsed-ccprocessed-dependencies"
BASIC_DEPENDENCIES = "basic-dependencies"
DDEPENDENCIES = "dependencies"
POS = "POS"
LEMMA = "lemma"
WORD = "word"
DEP = "dep"
GOVERNOR = "governor"
DEPENDENT = "dependent"
TEXT = "#text"
TYPE = "@type"
ID = "@id"
IDX = "@idx"
NER = "NER"

if __name__ == '__main__':
    pass