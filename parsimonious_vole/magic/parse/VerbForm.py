'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 4, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

# constants
VERB_FORM = "form"
CARDIFF_FEATURE = "cardiff_feature"
LEVIN_FEATURE = "levin_feature"
SENSE = "sense"
PARTIC = "participants"
FORM_STRIPPED = "form_stripped"
PREP_OPT = "prep_optional"
PREP_MANDT = "prep_mandatory"
PARTICIPANT_MANDATORY = "mandatory" 
PARTICIPANT_OPTIONAL = "optional"
PARTICIPANT_MISSING_FREQUENCY = "freq_unrealised" # how frequent the participant is unrealized
PARTICIPANT_CANONICAL = "canonical" # swap to this field lately, abandon "OPTIONAL/MANDATORY" interpretation

PROCESS_TYPE = "process_type"
MAJOR_PROCESS_TYPE = "major_process_type"
MINOR_PROCESS_TYPES = "minor_process_types"

EXAMPLE_AND_SYNONYM = "example_and_synonym"


class VerbForm(object):
    '''
    classdocs
    '''
    # fields
    form = ""
    prep_mandatory = []
    prep_optional = []
    entries = []
        
    def __init__(self, form, pm, po, entries):
        self.form = form
        self.entries = entries
        self.prep_mandatory = pm
        self.prep_optional = po
    
    def __str__(self):
        res = self.form + " "
        if self.prep_mandatory:
            res += ", ".join(self.prep_mandatory)
        if self.prep_optional:
            res+= ", ".join(self.prep_optional)
        return res
    
    def __repr__(self):
        return self.__str__()

if __name__ == '__main__':
    pass
