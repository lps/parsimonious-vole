'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Sep 19, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

#TODO: schedule move to guess package

performs a series of changes in dependecy graph in order to conform with 
requirements for mood parsing. This is a preliminary normalization procedure
 before creating the Mood Constituency Tree  
'''
import itertools

from parsimonious_vole.magic.guess.pattern_matching import get_dependent_tokens, \
    get_dominant_tokens
from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.utils import natsort, duplicates_only
from networkx.algorithms.cycles import simple_cycles
from networkx.classes.digraph import DiGraph

from parsimonious_vole.magic.parse.dep_graph_utils import find_prep_node, get_isolated_nodes


def correct_dependecy_parse_errors(dg): 
    """ 
    #TODO: links that lead to cicles and are removed in the gependecy graph
    would be nice to keep them somewhere and pass them to MCG builder for references
     known links are: rcmod, xsubj, partmod(maybe) 
    """
    
    def __the_dg_corrections(dg):
        """ the bunch of corrections on a single DG"""
        result = dg
        result = modify_conjunctions(result)
        result = un_copulate_dep_graph(result)
        result = remove_reverse_link_of_rcmod(result)
        result = correct_interrogative_ccomp_to_aux(result)
        result = correct_pos_of_1st_auxiliary(result)
        result = correct_verb_particles(result)
        result = correct_xcomp_with_subjects(result)
        result = correct_pepc_without_verb(result)
        result = correct_imperative_non_infinitives(result)
        result = correct_pos_of_predicate(result)
        result = correct_misplaced_ccomp_on_partmod_node(result) 
        result = correct_DT_prep_NN(result)
        result = correct_two_subjects(result)
        return result
     
    if isinstance(dg, DiGraph): # handling single graph
        return __the_dg_corrections(dg)
    elif isinstance(dg, (list,tuple,set)): # handling dg bundle
        return [__the_dg_corrections(x) for x in dg if x]

###############
###### Modifying functions for dependency graph 
##############

def modify_conjunctions(dependecy_graph):
    """
    disentangle cycles created by conjunctions in the dependecy graph.
    the result is the dependecy tree.
    
    1 treat NN conj NN and subj
    2 treat NN conj NN and obj
    3 treat PP conj PP and obj
    4 treat VB conj VB 
    
    5 treat copula JJ conj JJ subj
    6 treat copula NN conj NN subj
    """
    # 5treat copula JJ conj JJ subj
    # 6treat copula NN conj NN subj
    for croot in filter(lambda x: "cop" in x[2][TYPE], dependecy_graph.edges(None, True)):
        predicator = croot[1]
        complement = croot[0]
        
        # if there is a conjuncted copula (subj,JJ/NN1,NN) (subj,JJ/NN2,NN) (conj,JJ/NN1,JJ/NN2)
        # thenk break the direct link and leave the second subj through a cc "transitive"   
        conjunctions = filter(lambda x: "conj" in x[2][TYPE] and "preconj" not in x[2][TYPE], dependecy_graph.out_edges([complement], True))
        subject = filter(lambda x: "subj" in x[2][TYPE], dependecy_graph.out_edges([complement], True))
        for conj in conjunctions:
            if subject:
                #print [(conj[2], subject[0][1])]
                dependecy_graph.remove_edges_from([(conj[1], subject[0][1])])
    # 
    conjunctions = filter(lambda x: "conj" in x[2][TYPE] and "preconj" not in x[2][TYPE], dependecy_graph.edges(data=True))   
    for conj in conjunctions:
        # in case of subj/obj constituents
        for hc_edge in dependecy_graph.in_edges([conj[0]]):
            dependecy_graph.remove_edges_from([(hc_edge[0], conj[1])])
            
        # in case of VB_conj_VB shared subjects
        hc_subjects = filter(lambda x: "subj" in x[2][TYPE], dependecy_graph.out_edges([conj[0]], True))
        dc_subjects = filter(lambda x: "subj" in x[2][TYPE], dependecy_graph.out_edges([conj[1]], True))
        if hc_subjects and dc_subjects:
            if hc_subjects[0][1] == dc_subjects[0][1]: 
                dependecy_graph.remove_edges_from(dc_subjects)
    return dependecy_graph


###############
###### Correcting functions for dependency graph 
##############

def remove_reverse_link_of_rcmod(dep_graph):
    """ 
    Bell, a telecommunication company, which is based in Los Angeles, makes and distributes electronic, computer and building products.
    
    (8)based/VBN, (0)Bell/NNP, {'@type': 'nsubjpass'} 
    (0)Bell/NNP, (8)based/VBN, {'@type': 'rcmod'}
    
    Need to leave only rcmod, subj/subjpass shall be removed
    """
    rcmod_links = filter(lambda x: x[2][TYPE] == "rcmod", dep_graph.edges(None, True))
    if rcmod_links:
        for link in rcmod_links:
            # find cycles where rcmod link is involved
            idd = link[0] # the parent of rcmod link
            last_incoming_edges = [(x[x.index(idd)-1],x[x.index(idd)]) for x in [y[1:] for y in simple_cycles(dep_graph)] if idd in x]
            dep_graph.remove_edges_from(last_incoming_edges)
            
            # The code commented below might be useful (especially prep part),  
            #it got updated by cycle detection code above
            
#             reverse_edges = filter(lambda x: x[0] == link[1]  , dep_graph.in_edges([link[0]], True))
#             dep_graph.remove_edges_from([(link[1], link[0])])
#             for redge in reverse_edges:
#                 if "prep" in redge[2][TYPE]:
#                     # find prep
#                     preps = map(lambda x: (abs(x.id() - redge[0].id()), x) , get_isolated_nodes(dep_graph, ["IN"]))
#                     preps = sorted(preps, key=lambda x: x[0])
#                     if preps:
#                         dep_graph.add_edge(redge[0], preps[0][1], {TYPE:"prep"})
            
            # add wh element, if any, as the subject or dobj
            lb = min([link[1].id(), link[0].id()]) - 1
            ub = max([link[1].id(), link[0].id()]) + 1
            isolated = get_isolated_nodes(dep_graph, ["WP", "WP$", "WRB", "WDT"], lb, ub)
            subj_edges = filter(lambda x: x[2][TYPE] in ["nsubj","nsubjpass","csubj","csubjpass"] and x[0]==link[1], dep_graph.edges(None, True))
            if isolated: dep_graph.add_edge(link[1], isolated[0], {TYPE: "dobj" if subj_edges else "nsubj"}) # if a subject exists then it is dobj else it is nsubj
    return dep_graph


def remove_reverse_link_of_rcmod1(dep_graph):
    """
    @backup
    @obsolete
    TODO: schedule remove
     
    Bell, a telecommunication company, which is based in Los Angeles, makes and distributes electronic, computer and building products.
    
    (8)based/VBN, (0)Bell/NNP, {'@type': 'nsubjpass'} 
    (0)Bell/NNP, (8)based/VBN, {'@type': 'rcmod'}
    
    Need to leave only rcmod, subj/subjpass shall be removed
    """
    rcmod_links = filter(lambda x: x[2][TYPE] == "rcmod", dep_graph.edges(None, True))
    if rcmod_links: 
        for link in rcmod_links:
            reverse_edges = filter(lambda x: x[0] == link[1]  , dep_graph.in_edges([link[0]], True))
            dep_graph.remove_edges_from([(link[1], link[0])])
            
            for redge in reverse_edges:
                if "prep" in redge[2][TYPE]:
                    # find prep
                    preps = map(lambda x: (abs(x.id() - redge[0].id()), x) , get_isolated_nodes(dep_graph, ["IN"]))
                    preps = sorted(preps, key=lambda x: x[0])
                    if preps:
                        dep_graph.add_edge(redge[0], preps[0][1], {TYPE:"prep"})
            
            # add wh element, if any, as the subject or dobj
            lb = min([link[1].id(), link[0].id()]) - 1
            ub = max([link[1].id(), link[0].id()]) + 1
            isolated = get_isolated_nodes(dep_graph, ["WP", "WP$", "WRB", "WDT"], lb, ub)
            subj_edges = filter(lambda x: x[2][TYPE] in ["nsubj","nsubjpass","csubj","csubjpass"] and x[0]==link[1], dep_graph.edges(None, True))
            
            if isolated:
                dep_graph.add_edge(link[1], isolated[0], {TYPE: "dobj" if subj_edges else "nsubj"}) # if a subject exists then it is dobj else it is nsubj
                
    return dep_graph

# def add_which_determiner_as_marker_for_rcmod_links(dep_graph):
#     """ """
#     rcmod_links = filter(lambda x: x[2][TYPE]=="rcmod", dep_graph.edges(None,True))
#     if rcmod_links: 
#         for link in rcmod_links:
#             isolated = get_isolated_nodes(dep_graph,["WDT"],link[0].id(),link[1].id())
#             if isolated:
#                 dep_graph.add_edge(link[1],isolated[0],{TYPE:"mark"})
#     return dep_graph

def un_copulate_dep_graph(dep_graph):
    """ 
     if the dependency graph contain a copular clause, 
     it will make the predicator the root of the graph
     instead of direct object
     
     the direct object is an adjective or noun  
     """
    dependecy_graph = dep_graph
    # link that can be on NN/JJ and not on VB
    nn_links = ["dep", "poss", "possesive",
                 "amod", "appos", "conj",
                 "mwe", "infmod", "nn", "num",
                 "number", "partmod", "preconj"
                 "predet", "quantmod", "rcmod",
                 "ref", "det"]
    jj_links = ["advmod", "amod", "conj"]
    
    # 0 for every cop link
    # 1 find cop root, 
    # 2 find all the outgoing links, filter them and assign to verb all except nn/jj
    # 3 find all the incoming links, put  them on verb
    # 4 create dobj link between verb and cop
    for croot in filter(lambda x: "cop" in x[2][TYPE], dependecy_graph.edges(None, True)):
        predicator = croot[1]
        complement = croot[0]
        
        all_out_except_pred = filter(lambda x: "cop" not in x[2][TYPE] and "conj" not in x[2][TYPE] , dependecy_graph.out_edges([croot[0]], True))
        all_in = dependecy_graph.in_edges([croot[0]], True)
        
        dependecy_graph.remove_edges_from(all_in)
        dependecy_graph.remove_edges_from(all_out_except_pred)
        dependecy_graph.remove_edge(complement, predicator)
        dependecy_graph.add_edge(predicator, complement, {TYPE:"dobj"})
        
        for edge in all_out_except_pred:
            if "JJ" in edge[0].pos:
                if edge[2][TYPE] in jj_links:
                    dependecy_graph.add_edge(complement, edge[1], edge[2])
                else: dependecy_graph.add_edge(predicator, edge[1], edge[2])
                
            # elif "NN" in edge[0].pos:
            else:  # "NN", "PR" ...
                if "prepc" in edge[2][TYPE]:
                    dependecy_graph.add_edge(predicator, edge[1], edge[2])
                elif "prep" in edge[2][TYPE]:
                    dependecy_graph.add_edge(complement, edge[1], edge[2])
                elif edge[2][TYPE] in nn_links:
                    dependecy_graph.add_edge(complement, edge[1], edge[2])
                else: dependecy_graph.add_edge(predicator, edge[1], edge[2])

        for edge in all_in:
            if edge[2][TYPE] in ["nsubj", "dobj", "nsubjpass", "iobj"] :  # the strange case of rcmod and nsubj/dobj reciproc links
                # the prep was additional condition , nowe removed: |or "prep" in edge[2][TYPE]|
                dependecy_graph.add_edge(edge[0], complement, edge[2])
            else:
                dependecy_graph.add_edge(edge[0], predicator, edge[2]) 
    return dependecy_graph

def correct_verb_particles(dg):
    """ 
        if prep (not prep_XXX) then it is a prt 
    """
    # some time ago desired feature, now unclear
    
    # FIXME: verb-[advmod,]-xx can be transformed into verb-prt-xx,if the PTDB contains such an expression
    # consider 'acomp' link as well but not sure see Example 14/27:ComplexClauses
    # all descending links moved from 'prt' onto the dominant/parent verb
    
    ##############
    for prep in filter(lambda x: "prep" == x[2][TYPE], dg.in_edges(None, data=True)):
        prep[2][TYPE] = "prt"
    return dg

def correct_xcomp_with_subjects(result):
    """ 
    it seems possible in SD but we don't want it. 
    for a clause to be xcomped and have a subject. 
     even if the xcomped clause is usually nonfinite, it still has subject.
     ccomped clauses are expected to be finite and have subject.
     we rurn the xcomps with subj into ccomps with subj, even if they 
     are nonfinite
      
      # if (VB-xcomp-VB1, VB1-[csubj,nsubj,nsubjpass]) then transform into VB-ccompl-VB1
     """
    xcomps = filter(lambda x: result.edge[x[0]][x[1]][TYPE] == "xcomp", result.edges())
    for link in xcomps:
        subjects = filter(lambda x:  x[2][TYPE] in ["nsubj", "nsubjpass", "csubj"], result.out_edges(link[1], True))
        if subjects:
            result.edge[link[0]][link[1]][TYPE] = "ccomp"
        else:
            xsubjs = filter(lambda x:  x[2][TYPE] in ["xsubj"], result.out_edges(link[1], True))
            for xs in xsubjs:
                result.remove_edges_from([(xs[0], xs[1])]) 
    return result

modal_vb = ['shall', 'should', 'must', 'may', 'might', 'can', 'could', 'will', 'would']
modal_2word = ["ought to", "used to", " used not to", "ought not to"]
aux_lemma = ["do", "have", "be"]
aux_present_vb = ["do", "have", "am", "are"]
aux_present_3rd_vb = ["has", "is"]
aux_past_vb = ["did", "had", "was", "were"]

def correct_interrogative_ccomp_to_aux(result):
    """ VB1-ccomp-VB2, VB2-[nsubj,csubj]-[PR,NN]; 
        VB1<[PR,NN]<VB2; VB1 is modal-VB or aux-VB;
        VB1 has no children 
        then ccomp->aux
    """
    modal_and_aux_vb = modal_vb + aux_lemma
    ccomp_end_links = filter(lambda x: result.edge[x[0]][x[1]][TYPE] == "ccomp" and len(result.out_edges(x[0])) == 1, result.edges())

    for link in ccomp_end_links:
        # test if aux or mod verb, and if so change the link
        if link[0].lemma in modal_and_aux_vb and link[0].pos in ["MD", "VBZ", "VBP", "VBN", "VBD"]:
            result.remove_edge(link[0], link[1])
            result.add_edge(link[1], link[0], {TYPE:"aux"})
    return result
    
def correct_pos_of_1st_auxiliary(result):
    """ if the predicate has several aux verbs, the 1st one is finite, 
    hence shall have correct POS ["VBD"] = past, ["VBP","VBZ"]= present, ["MD"]- modal 
    """
    # get all non_aux verbs
    verbs = filter(lambda x: ("vb" in x.pos.lower() or "md" in x.pos.lower()) , result.nodes())
    main_verbs = filter(lambda x: "aux" not in map(lambda y: y[2][TYPE], result.in_edges(x, True)) , verbs)
    # get 1st aux of each of the main verbs
    for vb in main_verbs:
        aux = filter(lambda x: "aux" in x[2][TYPE], result.out_edges(vb, True))
        first_aux = natsort(map(lambda x: x[1] , aux))[0] if aux else None 
        # check and correct pos
        if first_aux:
            # because this operation leads to changing of hash value of the node, It has to be recreated in the graph
            # in_edges = result.in_edges(vb, True)
            # out_edges = result.out_edges(vb, True)
            # result.remove_node(first_aux)
            # changing 
            if first_aux.word in modal_vb: first_aux.pos = "MD"
            elif first_aux.word in aux_present_vb: first_aux.pos = "VBP"
            elif first_aux.word in aux_present_3rd_vb: first_aux.pos = "VBZ"
            elif first_aux.word in aux_past_vb: first_aux.pos = "VBD"
            # reinserting node into the graph
            # result.add_node(first_aux)
            # for i in in_edges: result.add_edge(i[0], first_aux, i[2])
            # for i in out_edges:  result.add_edge(first_aux, i[1], i[2])
    return result

def correct_pepc_without_verb(result):
    """ 
        assuming that the copulas are already changes,
      the successor node of the prepc link shall be a verb,
       otherwise it is not a prepc but a prep link
    """
    prepc_links = filter (lambda x: "prepc" in result.edge[x[0]][x[1]][TYPE], result.edges())
    for link in prepc_links:
        if "RB" in link[1].pos and "VB" in link[0].pos:
            current_prep_type = result.edge[link[0]][link[1]][TYPE]
            result.edge[link[0]][link[1]][TYPE] = "advmod"
            prep_node = find_prep_node(result, link)
            if prep_node: result.add_edge(link[0], prep_node, {TYPE:"prt"})
        elif "VB" not in link[1].pos:
            current_prep_type = result.edge[link[0]][link[1]][TYPE]
            new_prep_type = current_prep_type.replace("prepc_", "prep_")
            result.edge[link[0]][link[1]][TYPE] = new_prep_type
    return result 

def correct_imperative_non_infinitives(result):
    """ 
    if there is a VB pos that does not have an 'aux' dependecy  to preposition 'to'
     then it is rather present simple than infinitive so the verb POs is VBP 
    """
    # verbs = filter(lambda x: ("vb" in x.pos.lower() or "md" in x.pos.lower()) , result.nodes())
    # main_verbs = filter(lambda x: "aux" not in map(lambda y: y[2][TYPE], result.in_edges(x, True)) , verbs)
    
    infinitive_verbs = filter(lambda x: ("VB" == x.pos) , result.nodes())
    for i in infinitive_verbs:
        to_aux_preposition = filter(lambda y: y[2][TYPE] == "aux" and y[1].lemma == "to", result.out_edges(i, True))
        if not to_aux_preposition:
            i.pos = "VBP"
    return result

def correct_pos_of_predicate(dg):
    """
        if a node has a subject then it is a verb
        eg: That    movie    upset/JJ    Ivy    .
    """
    for subj in filter(lambda x: "subj" in x[2][TYPE], dg.out_edges(None, data=True)):
        if "JJ" in subj[0].pos:  # if the pos is adjective then switch it to past form veb
            subj[0].pos = "VBD"
            
    return dg

def correct_misplaced_ccomp_on_partmod_node(dep_graph):
    """
        IF the partmod child node contains a ccomp link, then 
        the ccomp link shall be placed on the parent  
        
        Eg: Tell the boy playing the piano that he is good. 
    """
    
    partmod_edge_continued_with_ccomp = filter(lambda x: "partmod" in x[2][TYPE] and get_dependent_tokens(dep_graph, x[1], ["ccomp"], True), dep_graph.edges(None, data=True)) 
    
    for pmod_edge in partmod_edge_continued_with_ccomp:
        receiving_verbs = filter(lambda x: "VB" in x[0].pos, get_dominant_tokens(dep_graph, pmod_edge[0], ["iobj", "dobj", "prep"], False))
        ccomp_nodes = get_dependent_tokens(dep_graph, pmod_edge[1], ["ccomp"], True)
        if receiving_verbs and ccomp_nodes:
            receiving_verb = receiving_verbs[0][0]
            ccomp_node = ccomp_nodes[0][1]
            dep_graph.remove_edges_from([ccomp_nodes[0]])
            dep_graph.add_edge(receiving_verb, ccomp_node, {TYPE:"ccomp"})
    return dep_graph

def correct_DT_prep_NN(dep_graph):
    """
     if there is a DT(determiner) connected 
     to a NN via preposition, then the NN is 
     part of a PP that is a sibling of DT
     
     EG: ... and put that[DT] in the pan[NN]
     NN shall de compl/adjunct of "put" and not a qualifier of DT
    """
    prep_links = filter (lambda x: "prep" in dep_graph.edge[x[0]][x[1]][TYPE], dep_graph.edges(data=True))
    for link in prep_links:
        if "DT" in link[0].pos and "NN" in link[1].pos:
            # get parent of "DT"
            parents = dep_graph.predecessors(link[0])
            if not parents: return dep_graph # very strange situation to have
            # remove link from DT-NN
            dep_graph.remove_edge(link[0],link[1])
            # insert the link DT.parent-NN
            dep_graph.add_edge(parents[0],link[1],link[2])
    return dep_graph

def correct_two_subjects(dep_graph):
    """
        If there are two subjects then:
        * give priority to the one closer to the verb 
        * give priority to PR ovr NN OR
        
        current implementation takes only proximity to the verb into consideration 
        
        Ex: Any vitamins I could be lacking ?
        det(vitamin-2, any-1)
        nsubj(lack-6, vitamin-2)
        nsubj(lack-6, I-3)
        aux(lack-6, could-4)
        aux(lack-6, be-5)
        root(ROOT-0, lack-6)
    """
    #subj_edges = filter(lambda x: x[2][TYPE] in ["nsubj","nsubjpass","csubj","csubjpass"] and x[0]==link[1], dep_graph.edges(None, True))
    subjs = [x for x in dep_graph.edges(data=True) if x[2][TYPE] in ["nsubj","nsubjpass","csubj","csubjpass"]]
    key_fn = lambda x: x[0]
    subjs = duplicates_only(subjs, key_fn) # duplicates are only by the parent node so x[0] of the edge
    if subjs:
        #iterate over sets of duplicates
        for subj_set in [(list(y)) for x,y in itertools.groupby(duplicates_only(subjs,key_fn),key_fn)]:
            proximity_sorted = sorted(subj_set, key = lambda x: abs(x[0].id()-x[1].id()))
            for dobj in proximity_sorted[1:]:
                dobj[2][TYPE] = "dobj"
    return dep_graph
    
# def add_punctuation_nodes(dep_graph):
#     """ """
#     dep_graph.add_edges_from(punctuation_new_edges(dep_graph))
#     return dep_graph

if __name__ == '__main__':
    pass
