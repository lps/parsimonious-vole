'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 16, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import os
import pprint

from bs4 import BeautifulSoup
from parsimonious_vole.magic.utils import natsort

from parsimonious_vole.configuration import TEMPORARY_PATH, PATH_TO_TARQUI_CODE, TEMPORAL_SENTENCES
from parsimonious_vole.magic.parse.parse_process import test_sentence_graph

TEMP_INPUT_FILE = TEMPORARY_PATH +  "/gutime.in.xml"
TEMP_OUTPUT_FILE = TEMPORARY_PATH +  "/gutime.out.xml"
TARSQUI_PATH = PATH_TO_TARQUI_CODE 

class TimexObject(object):
    type = None
    words = []
    def __init__(self, _type, _words):
        self.type = _type
        self.words = _words
    
    def __str__(self, *args, **kwargs):
        return "<"+self.type+"'"+" ".join(map(lambda x: ""+str(x[0])+":"+x[1], self.words))+"'>"
    
    def __repr__(self, *args, **kwargs):
        return self.__str__()

def tarsqui_parse_dg(dependecy_graph):
    """ 
        returns a dictionary of { timex3 tags:[(id,lex),], }
        TODO: to be extended to other tags
    """
    write_input_file(dependecy_graph)
    call_tarsqui()
    timex_tag_labels = read_taggs()
    timex_objects = tags_to_timex_objects(timex_tag_labels)
    pprint.pprint(timex_tag_labels)
    pprint.pprint(timex_objects)
    return timex_objects

def write_input_file(dependecy_graph):
    assert dependecy_graph," Requiring a dependency graph "
    assert dependecy_graph," Requiring a non empty dependency graph"
    
    soup = BeautifulSoup(features='xml')
    doc = soup.new_tag("DOC")
    text = soup.new_tag("TEXT")
    text.string = " ".join(map(lambda x: x.word, natsort(dependecy_graph.nodes()))) 
    
    doc.append(soup.new_tag("DOCID"))
    doc.append(text)
    soup.append(doc)
    with open(TEMP_INPUT_FILE,"wb+") as ff:
        ff.write(str(soup))
        ff.close()

def call_tarsqui():
    print "looking for Tarsqi in", TARSQUI_PATH
    os.chdir(TARSQUI_PATH)
    try: # cleanup
        os.remove(TEMP_OUTPUT_FILE)
    except: pass
    command = "python tarsqi.py simple-xml %s %s trap_errors=True pipeline=PREPROCESSOR, GUTIME, EVITA, SLINKET, S2T, BLINKER, CLASSIFIER" % (TEMP_INPUT_FILE, TEMP_OUTPUT_FILE)
    (fh_in, fh_out, fh_errors) = os.popen3(command)
    if fh_errors:
        print "DEBUG: Tarqsi script result"
        for line in fh_errors:
            print line

def read_taggs():
    """ return a dictionarry { timex3 tags:[(id,lex),], }"""
    soup = BeautifulSoup(open(TEMP_OUTPUT_FILE), "xml")
    #for sentence in filter(lambda x: isinstance(x, bs4.element.Tag) and x.name == "s", soup.DOC.TEXT):
    lex_index = soup.find_all("lex")
    timex_tags = {k:[] for k in soup.find_all("TIMEX3")}
    for i in lex_index:
        parent = i.find_parent("TIMEX3")
        if parent:timex_tags[parent].append((lex_index.index(i),i.string))
            #print lex_index.index(i),parent
    #cleanup
    try: # cleanup
        os.remove(TEMP_OUTPUT_FILE)
        os.remove(TEMP_INPUT_FILE)
    except: pass
    return timex_tags

def tags_to_timex_objects(timex_tag_labels):
    """ """
    result = []
    for tt in timex_tag_labels.keys():
        if "TYPE" not in tt.attrs:
            print "WARNING: Timex3 tag without TYPE attribute", tt
            continue
        else: 
            result.append(TimexObject(tt.attrs["TYPE"],timex_tag_labels[tt]))
    return result

if __name__ == '__main__':
    tarsqui_parse_dg(test_sentence_graph(3, TEMPORAL_SENTENCES, True))