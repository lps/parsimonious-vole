'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Sep 19, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import os
import stat
import subprocess
from shutil import copyfile
from sys import platform as _platform

from parsimonious_vole.magic.utils import translate_text

from parsimonious_vole.configuration import PATH_TO_PROJECT, PATH_TO_STANFORD_PARSER, TEMPORARY_PATH
from parsimonious_vole.magic.parse.parse_process import ParseResult_txt


def STP_word_translation(word_list):
    """ translates the STP conventions 
        * braket signs and 
    """
    STANFORD_BRACKETS_MAPPING = {"-LRB-":"(","-RRB-":")",
                                 "-LSB-":"[","-RSB-":"]",
                                 "-LCB-":"{","-RCB-":"}", 
                                 "``":'"',"''":'"',
                                 '\xc2\xa0' : ' ', # nbsp
                                 u'\xc2\xa0' : ' ', # nbsp
                                 }
    if word_list is None: return ""
    if isinstance(word_list, basestring):
        return translate_text(word_list, STANFORD_BRACKETS_MAPPING)
    elif isinstance(word_list, (list,tuple,set)):
        return [translate_text(w,STANFORD_BRACKETS_MAPPING) for w in word_list]
    return word_list

def STP_word_join(word_list):
    """
        translates the STP conventions into a plain text. and merges the list of words
        * the space before apostroph (')  
    """
    REMOVE_SPACES_BEFORE_PUNCT = {" '":"'",
                              " !":"!",
                              " :":":",
                              " ,":",",
                              " ?":"?",
                              " .":".",
                              " n't":"n't",
                              "\n":"",
                              "\t":"",
                              "\r":""}
    res = " ".join([STP_word_translation(word_list) for w in word_list])
    for pppct in REMOVE_SPACES_BEFORE_PUNCT: res = res.replace(pppct, REMOVE_SPACES_BEFORE_PUNCT[pppct])
    return res

def copy_the_bach_scipts():
    """
    TODO: obsolete, this functionality has been moved to setup.sh
    copies the scripts from project/resource folder to stanford_parser 
    TODO: put this procedure in the install script"""
    copyfile(PATH_TO_PROJECT+"/resources/lexparser.sh",PATH_TO_STANFORD_PARSER+"/lexparser_.sh")
    copyfile(PATH_TO_PROJECT+"/resources/lexparser.bat",PATH_TO_STANFORD_PARSER+"/lexparser_.bat")
    os.chmod(PATH_TO_STANFORD_PARSER+"/lexparser_.sh",stat.S_IRWXO| stat.S_IRWXG | stat.S_IRWXU)
    os.chmod(PATH_TO_STANFORD_PARSER+"/lexparser_.bat",stat.S_IRWXO| stat.S_IRWXG | stat.S_IRWXU)

def call_stanford_parser_text(text):
    copy_the_bach_scipts()
    command_line = ""
    if _platform == "linux" or _platform == "linux2":
        command_line = PATH_TO_STANFORD_PARSER+"/lexparser_.sh"
    elif _platform == "win32":
        command_line = PATH_TO_STANFORD_PARSER+"/lexparser_.bat"
    file_path = TEMPORARY_PATH + "/parser.input"
    f = open(file_path,"wb+")
    f.write(text)
    f.close()
    subprocess.call([os.path.abspath(command_line),  os.path.abspath(file_path)]) 


def call_stanford_parser_file(file_path, path_to_stanford=PATH_TO_STANFORD_PARSER):
    copy_the_bach_scipts()
    command_line = ""
    if _platform == "win32":
        command_line = PATH_TO_STANFORD_PARSER+"/lexparser_.bat"
    else: #_platform == "linux" or _platform == "linux2":
        command_line = PATH_TO_STANFORD_PARSER+"/lexparser_.sh"
    cur_dir = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(command_line)))
    subprocess.call([os.path.abspath(command_line),  os.path.abspath(file_path)])
    os.chdir(cur_dir) 

# def parse_result_text(text):
#     call_stanford_parser_text(text)
#     return ParseResult_txt(TEMPORARY_PATH + "/parser.input.stp")

def parse_result_file(file_path):
    call_stanford_parser_file(file_path)
    return ParseResult_txt(file_path+".stp")

if __name__ == '__main__':
    pass
