'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Sep 19, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
from parsimonious_vole.magic.utils import natsort

from parsimonious_vole.magic.parse.parse_constants import TYPE


##############
###### Utility functions
##############
 
def get_isolated_nodes(dependecy_graph, pos_filterr=None,lower_bound=0,upper_bound=-1):
    """
        return the isolated nodes from a dependency graph
    """
    nodes = []
    if pos_filterr:
        nodes = filter(lambda x: not dependecy_graph.in_edges(x) and x.pos in pos_filterr, dependecy_graph.nodes())
    else:
        nodes = filter(lambda x: not dependecy_graph.in_edges(x), dependecy_graph.nodes())
    
    # filtering by boundaries
    if upper_bound == -1: upper_bound = len(dependecy_graph.nodes())
    return filter(lambda x: x.id()>=lower_bound and x.id()<=upper_bound, nodes)

def get_node_by_id(dep_graph, node_id=0):
    for i in dep_graph.nodes():
        if i.id() == node_id:
            return i
    return None

def find_genitive_indicator_node(dependecy_graph, edge):
    """
     given a \poss\ or \possessed\ edge, find the \'s\ node 
    """
    if dependecy_graph[edge[0]][edge[1]][TYPE] in ["poss","possessive"]:
        lb = min([edge[1].id(),edge[0].id()])
        ub = max([edge[1].id(),edge[0].id()])
        isolated = get_isolated_nodes(dependecy_graph, ["POS"],lb,ub)
        if isolated: return isolated[0]
    return None

def find_prep_node(dependecy_graph, edge):
    """"
    given a preposition edge returns the preposition node originally detached from the graph
    
    return the preposition node that is in between the two nodes of the edge
    
    this function to handles: 
     1. agent link, 
     2. situation when preposition is in front of leftmost 1st node
     
     this function does not handle situations:  
     3. when preposition is of more than one word
    """
    # get all disconnected nodes
    preposition = None
    lb = min([edge[1].id(),edge[0].id()])-1
    ub = max([edge[1].id(),edge[0].id()])+1
    isolated = get_isolated_nodes(dependecy_graph, ["IN","TO", "CC", ","],lb,ub)

    if dependecy_graph[edge[0]][edge[1]][TYPE] == "agent":
        # find by in front of the word
        preposition = ["by"]
        #
        lb = min([edge[1].id(),edge[0].id()])-1
        ub = max([edge[1].id(),edge[0].id()])
        isolated = get_isolated_nodes(dependecy_graph, ["IN"],lb,ub)
    elif "conj" in dependecy_graph[edge[0]][edge[1]][TYPE]:
        # find "comma" or "and" or "or"
        preposition =  [dependecy_graph[edge[0]][edge[1]][TYPE].replace("conj_", "")]
        preposition.append(",")
        #
        lb = min([edge[1].id(),edge[0].id()])
        ub = max([edge[1].id(),edge[0].id()])
        isolated = get_isolated_nodes(dependecy_graph, ["CC", ","],lb,ub)

    else: # preposition
        preposition = [dependecy_graph[edge[0]][edge[1]][TYPE].replace("prepc_", "").replace("prep_", "")]
    # print "edge: ", str(edge), " preposition: ", str(preposition)
    
    the_preps = filter(lambda x: x.word in preposition or x.lemma in preposition, isolated)
    the_preps = natsort(filter(lambda x: not (x.pos =="TO" and x.id() ==ub), the_preps)) # exclude "TO"s that are after the verb
    
    if the_preps: 
        if len(the_preps) == 1: return the_preps[0]
        else:
            return the_preps[-1]
    return None 

preceding_pos = ["$", "(", "``", ",", ":", "--", ]
succeding_pos = [")", ".", "''"] 
def get_punctuation_marks(dep_graph,nodes_bunch):
    nodes = natsort(nodes_bunch)
    isolated_nodes_prec = get_isolated_nodes(dep_graph, preceding_pos, nodes[0].id()-1,nodes[-1].id())
    isolated_nodes_succ = get_isolated_nodes(dep_graph, succeding_pos,nodes[0].id(),nodes[-1].id()+1)
    return isolated_nodes_prec+isolated_nodes_succ

def punctuation_new_edges(dep_graph):
    """
     returns a set of links 
    """
    isolated_nodes_prec = get_isolated_nodes(dep_graph, preceding_pos)
    isolated_nodes_succ = get_isolated_nodes(dep_graph, succeding_pos)
    
    result = []
    for punct_node in isolated_nodes_prec:
        node = get_node_by_id(dep_graph, punct_node.id() + 1) 
        if node: result.append((node, punct_node, {TYPE:"punct"}))
    for punct_node in isolated_nodes_succ:
        node = get_node_by_id(dep_graph, punct_node.id() - 1) 
        if node: result.append((node, punct_node, {TYPE:"punct"}))
    return result

if __name__ == '__main__':
    pass
