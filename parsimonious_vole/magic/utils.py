'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 11, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import collections
import csv
import fnmatch
import itertools
import logging
import os
import re
import unicodedata
from pprint import pprint as pprint

import matplotlib.pyplot as plt
import networkx as nx
from networkx.classes.digraph import DiGraph

from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.parse import parse_constants


def strip(text):
#     if isinstance(text, basestring):
#         return ' '.join(word_tokenize(text))
#     try: return ' '.join(word_tokenize(str(text))) 
#     except: return ""
    #return re.sub( '\s+', ' ', text ).strip()
    return ' '.join(text.split())

def index_words(raw_text, los, start_offset=0, return_not_found=False):
    results, missing_words, idx, untested_precc = [], [], start_offset, None
    couple_iter = zip(*[los[i::1] for i in range(2)])
    accepted_successor = None
    for couple in couple_iter:
        target_word_index = raw_text[idx:].find(couple[0])
        successor_word_index = raw_text[idx:].find(couple[1]) if len(couple)>1 else None
        #print "recaluclating succ"
        if successor_word_index is not None and successor_word_index==target_word_index: # for identical consecutive words
            successor_word_index = raw_text[target_word_index+len(couple[0])+idx:].find(couple[1])+target_word_index+len(couple[0]) if len(couple)>1 else None
        #print couple, target_word_index, successor_word_index, idx, raw_text[idx+target_word_index:idx+target_word_index+len(couple[0])]
        if target_word_index>-1:
            if successor_word_index is not None:
                # testing precc
                if untested_precc:
                    untested_precc_index = raw_text[idx:].find(untested_precc)
                    if successor_word_index>untested_precc_index :
                        #print "untested precc normal"
                        results.append((untested_precc,
                                        idx+untested_precc_index,
                                        idx+untested_precc_index+len(untested_precc),
                                        raw_text[idx+untested_precc_index:idx+untested_precc_index+len(untested_precc)]))                    
                    elif -1<successor_word_index<untested_precc_index: # <
                        #print "untested precc misplaced"
                        missing_words.append(untested_precc)
                    untested_precc = None
    
                # testing target
                if successor_word_index>target_word_index:
                    #print "noraml t<s"
                    results.append((couple[0],idx+target_word_index,idx+target_word_index+len(couple[0]),raw_text[idx+target_word_index:idx+target_word_index+len(couple[0])]))
                    accepted_successor = (couple[1],idx+successor_word_index,idx+successor_word_index+len(couple[1]),raw_text[idx+successor_word_index:idx+successor_word_index+len(couple[1])])
                    idx=idx+target_word_index+len(couple[0])
                    #print "accepted successor: ", accepted_successor
                elif -1<successor_word_index<target_word_index: # <
                    #print " misplaced/missing s<t"
                    missing_words.append(couple[0])
                else: # succ==-1
                    #print "succ missing"
                    missing_words.append(couple[1])
                    untested_precc = couple[0]
            else:
                #print "no succ, last element"
                if target_word_index>-1:
                    results.append((couple[0],idx+target_word_index,idx+target_word_index+len(couple[0]),raw_text[idx+target_word_index:idx+target_word_index+len(couple[0])]))
        else:
            missing_words.append(couple[0])
        # test if the text is different from the found word
    if successor_word_index is not None:
        if accepted_successor and accepted_successor[0]==couple_iter[-1][1]:
            results.append(accepted_successor)
    return results, uniquify(missing_words)

def translate_text(text, dic):
    """ translates the text according to dic"""
    for i, j in dic.iteritems():text = text.replace(i, j)
    return text

def eattr(stype):
    """ 
     an utility function for graph patterns to generate edge attributes, 
    """
    return {TYPE:stype}

####
def equal_dict_in_list(item_dict, list_of_dicts):
    """ determines if the list_of_dicts of dictionarries already contains the items"""
    item = set(item_dict.items()) 
    for i in list_of_dicts:
        iitem = set(i.items())
        if not (item - iitem or iitem - item) : return True 
    return False

def unique_dictionaries_in_list(list_of_dicts):
    """ removes duplicate dictionaries in list_of_dicts """
    new_list = []
    for i in list_of_dicts:
        if not equal_dict_in_list(i, new_list): new_list.append(i)
    return new_list

def uniquify(seq, idfun=repr):
    """
        anaxagramma' comment from 
        http://code.activestate.com/recipes/52560-remove-duplicates-from-a-sequence/ 
    """
    seen = {}
    return [seen.setdefault(idfun(e), e) for e in seq if idfun(e) not in seen]

def duplicates_only(seq, selector_fn=lambda x: x):
    """ returns the list of elements that are duplicated in the list and 
        fulfill selector lambda function """
    duplicates = [x for x, y in collections.Counter([selector_fn(x) for x in seq]).items() if y > 1]
    return [x for x in seq if selector_fn(x) in duplicates] 

def any_los_in_los(los1,los2,ignore_caps=True):
    """
        returns true if any item of los1 is among items of los2
    """
    if not los2 and los1: return False
    if not los1: return True
    
    if not ignore_caps:
        ilos1 = [los1] if isinstance(los1, basestring) else filter(None, los1)
        ilos2 = [los2] if isinstance(los2, basestring) else filter(None, los2)
    else:
        ilos1 = [los1.lower()] if isinstance(los1, basestring) else [x.lower() for x in los1 if x] 
        ilos2 = [los2.lower()] if isinstance(los2, basestring) else [x.lower() for x in los2 if x]
    
    return True if set(ilos1) & set(ilos2) else False
    #return any([x in ilos2 for x in ilos1])
    
def los_in_los(los1, los2, substrings_allowed=True, ignore_caps=True):
    """ 
        checks whether los1 is in los2, 
        los can be list_of_sets string or list/tuple/set of strings
        if substrings_allowed is True then tries to match 
        also substrings of los2 in to los1(items) otherwise 
        searches for strict match between los1(items) and los2(items)
    """
    if not los2 and los1: return False
    if not los1: return True
    
    ilos1 = None
    ilos2 = None
    if not ignore_caps:
        ilos1 = [los1] if isinstance(los1, basestring) else filter(None, los1)
        ilos2 = [los2] if isinstance(los2, basestring) else filter(None, los2)
    else:
        ilos1 = [los1.lower()] if isinstance(los1, basestring) else map(lambda x: x.lower(), filter(None, los1))
        ilos2 = [los2.lower()] if isinstance(los2, basestring) else map(lambda x: x.lower(), filter(None, los2))
#     # something cannot be found in nothing
#     if not ilos2 and ilos1: return False
#     # empty strings are always found in any other string
#     if not ilos1 : return True
    
    for i1 in ilos1:
        if substrings_allowed:
            for i2 in ilos2:
                if i1 in i2: return True
        elif i1 in ilos2: return True
    return False

def los_sublist_of_los(los1, los2, ignore_caps=True, return_boundaries=False):
    """ similar to list_sublist_of_list but allows list_of_sets single string 
    parameter and ignore caps and preserves the order of items"""
    ilos1 = None
    ilos2 = None
    if not ignore_caps:
        ilos1 = [los1] if isinstance(los1, basestring) else filter(None, los1)
        ilos2 = [los2] if isinstance(los2, basestring) else filter(None, los2)
    else:
        ilos1 = [los1.lower()] if isinstance(los1, basestring) else map(lambda x: x.lower(), filter(None, los1))
        ilos2 = [los2.lower()] if isinstance(los2, basestring) else map(lambda x: x.lower(), filter(None, los2))
    
    boundaries = list_sublist_of_list(ilos1, ilos2)
    if boundaries: 
        return True if not return_boundaries else boundaries
    else: return False if not return_boundaries else boundaries

def list_sublist_of_list(sl, l):
    """ find starting and ending indices of list_of_sets sublist in list_of_sets 
    list and preserves the order of items """
    results = []
    sll = len(sl)
    for ind in (i for i, e in enumerate(l) if e == sl[0]):
        if l[ind:ind + sll] == sl:
            results.append((ind, ind + sll - 1))
    return results

def singleton(cls):
    """ class annotation to make it Singleton """
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

def draw_graph(dx, save_file=None):
    pos = None
    # insert_extra attributes into nodes for drawing them
    l = {}
    for n in dx.nodes():
        l[n] = str(n)
    # 
    try:
        from networkx import graphviz_layout
        pos = nx.graphviz_layout(dx, prog="dot", root=0)  # dot, neato, fdp, sfdp, twopi, circo
    except ImportError:
        logging.error("could not find Graphviz and either PyGraphviz or Pydot, drawing without them.")
        pos = nx.spring_layout(dx)
        
    nx.draw(dx, pos, labels=l, font_size=10)
    try:
        edge_labels = dict([((u, v,), d[parse_constants.TYPE]) for u, v, d in dx.edges(data=True)])
        nx.draw_networkx_edge_labels(nx, pos, edge_labels=edge_labels, labelfloat=True)
    except Exception:
        pass
    if save_file and isinstance(save_file, basestring) :
        plt.savefig(save_file)
    else: plt.show() 
    plt.close()


def draw_taxis_graph(dx, save_file=None):
    """ draws a taxis graph """
    
    def __generate_appendixes_for_edge(edge_with_data):
        """ returns a tuple of (n1_app, n2_app, edge) """
        left = []
        right = []
        edge = []
        for i in edge_with_data[2][TYPE]:
            if i[0] == "parataxis": 
                left += ['1']
                right += ['2']
            elif i[0] == "hypotaxis":
                left += ['a']
                right += ['b']
            else:
                left += ['?A?']
                right += ['?B?']
            edge += [i[1]]
        return (uniquify(left), uniquify(right), uniquify(edge))
        
    pos = None
    
    # the the taxis appendix
    node_appendinx = {}
    for edge in dx.edges(data=True):
        if edge[0] not in node_appendinx: node_appendinx[edge[0]] = []
        if edge[1] not in node_appendinx: node_appendinx[edge[1]] = []
        app = __generate_appendixes_for_edge(edge)
        node_appendinx[edge[0]].append(app[0])
        node_appendinx[edge[1]].append(app[1])
        edge[2][TYPE] = uniquify(app[2])
    # insert_extra attributes into nodes for drawing them
    l = {}
    for n in dx.nodes():
        l[n] = str(n.name() + str(uniquify(node_appendinx[n])))
    # 
    try:
        from networkx import graphviz_layout
        pos = nx.graphviz_layout(dx, prog="dot", root=0)  # dot, neato, fdp, sfdp, twopi, circo
    except ImportError:
        logging.error("could not find Graphviz and either PyGraphviz or Pydot, drawing without them.")
        pos = nx.spring_layout(dx)
        
    nx.draw(dx, pos, labels=l, font_size=10)
    try:
        edge_labels = dict([((u, v,), d[parse_constants.TYPE]) for u, v, d in dx.edges(data=True)])
        nx.draw_networkx_edge_labels(nx, pos, edge_labels=edge_labels, labelfloat=True)
    except Exception:
        pass
    if save_file and isinstance(save_file, basestring) :
        plt.savefig(save_file)
    else: plt.show() 
    plt.close()

def print_graph_taxis(txg):
    """ prints node names and edge relation between them """
    for edge in txg.edges(data=True):
        print edge[0].name() + " - " + edge[1].name() + " - " + str(edge[2][TYPE])
        
    # a = [('hypotaxis', ['exposition']), ('hypotaxis', ['exemplification']), ('hypotaxis', ['clarification']), ('hypotaxis', ['same-time']), ('hypotaxis', ['reason-ce']), ('hypotaxis', ['purpose']), ('hypotaxis', ['result'])]
    
def tokenize_by_space(text):
    if not text: return []
    result = text.split(" ")
    return filter(None, result) 

def print_graph(dependecy_graph):
    result = None
    try:
        result = str(natsort(dependecy_graph.nodes()))
    except Exception:
        result = dependecy_graph.nodes()
    pprint(result)

def print_graph_and_attributes(dependecy_graph):
    result = None
    try:
        result = str(natsort(dependecy_graph.nodes(True)))
    except Exception:
        result = dependecy_graph.nodes(True)
    pprint(result)
    
def print_mcg_graph(mcg):
    result = None
    try:
        result = natsort(mcg.nodes(True))
    except Exception:
        result = mcg.nodes(True)
    result = map(lambda x: (x[0],
                            x[0].feature_structure(),
                            x[1],
                            x[1]["constituent"].feature_structure() if "constituent" in x[1] else None), result)
    pprint(result)

def natsort(list_):
    """ natural Sorting of list_of_sets list of strings 
      ref: http://code.activestate.com/recipes/285264-natural-string-sorting/
     """
    # decorate
    if not list_: return []
    tmp = []
    tmp = [ (int(re.search('\d+', str(i)).group(0) if re.search('\d+', str(i)) else 0), i) for i in list_ ]
    tmp.sort()
    # undecorate
    return [ i[1] for i in tmp ]
 

def root_node(graph):
    for i in graph.nodes():
        if i and not graph.predecessors(i) and graph.successors(i):
            return i
    if graph.nodes():
        return graph.nodes()[0]

def lowest_index_after_the_gap(l):
    """ list is list_of_sets list of numbers ascending numbers , returns the lowest index"""
    idx = 0
    for i in range(1, len(l)):
        if l[i] - l[i - 1] > 1: 
            idx = i
    return idx

def get_dict_keys_by_value(dict_, value_):
    return [key for key, value in dict_.iteritems() if value == value_]

def list_minus(a, b):
    """ subtracts list b from list list_of_sets """
    d = [b] if not isinstance(b, (list, set, tuple)) else b
    c = a[:]
    for i in d:
        while i in c:
            c.remove(i)
    return c

def mmsplit(strg, seps):
    """
    splits the string by separators from seps.
    seps is list_of_sets list of strings, which can contain multiple char separators
    (sep1|sep2|sep3)
    """
    return filter(lambda x:x not in seps, re.split("(" + "|".join(map(re.escape, seps)) + ")+", strg))

def unique_maximal_set(list_of_sets):
    """ """
    result = [x for x in list_of_sets if not [ y for y in list_minus(list_of_sets, [x]) if x.issubset(y)]] 
    return uniquify(result) 

def flatten(l):
    """
        a generator to recursively flatten a list of lists
        source: http://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists-in-python 
    """
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, basestring):
            for sub in flatten(el):
                yield sub
        else:
            yield el

def filter_out_keys(fs, keys=["words", "lemmas", "possible_process_types", "dependecy_relation"]):
    """ returns the FeatStruct without the indicated keys """
    assert isinstance(fs, dict)
    _keys = [keys] if not isinstance(keys, (list, tuple, set)) else list(keys)
    return {i[0]:i[1] for i in fs.items() if i[0] not in _keys} 

def multi_filter(lst, lambda_filters):
    """
        filters a list according to a set of filters defined as lambda functions
    """
    assert lambda_filters, " lambda_filters required, received (%s)" % (lambda_filters)
    if not lst and lst is not None: return lst
    _lambda_filters = lambda_filters if isinstance(lambda_filters, (list, set, set)) else [lambda_filters]
    return [x for x in lst if all([y(x) for y in _lambda_filters])]

def lsplit(lst, delim=[""]):
    """ splits a list into sublists according to delimiters """
    return [list(group) for k, group in itertools.groupby(lst, lambda x: x in delim) if not k]

def groups_of_n(lst, n):
    """ [1,2,3,4,5,6,7,] --> [(1,2),(3,4),(5,6),(7,)] """
    return list(itertools.izip(*[iter(lst)] * n))

def strip_punctuation(text):
    """
    >>> strip_punctuation(u'something')
    u'something'

    >>> strip_punctuation(u'something.,:else really')
    u'somethingelse really'
    """
    
    punctutation_cats = set(['Pc', 'Pd', 'Ps', 'Pe', 'Pi', 'Pf', 'Po'])
    try:
        return ''.join(x for x in text if unicodedata.category(x) not in punctutation_cats)
    except:
        return ''.join(x for x in unicode(text) if unicodedata.category(x) not in punctutation_cats)

def safe_cast_to_number(input_string, default):
    """ casts the string into int/float else defalut
    """
    try:
        if isinstance(input_string, int): return input_string
        if isinstance(input_string, float): return input_string 
        if isinstance(input_string, basestring):
            return float(input_string) if '.' in input_string else int(input_string)
        else: return int(input_string)
    except Exception:
        return default

def safe_cast_to_boolean(input_string, default):
    """ casts the string into boolean else defalut
    """    
    if isinstance(input_string, bool): return input_string
    if isinstance(input_string, basestring):
        if "true" in input_string.lower(): return True
        if "false" in input_string.lower(): return False
    return default

def conditional_traversal(graph, start_node, lambda_condition_list=[], only_forward=False, visited=[]):
    """
        *starting from a start node, traverses a digraph forward and backwards until a border node is reached.
        the norder nodes are defiened by the constraints of lambda functions
        *visited, is a list of visited nodes, and can be ignored
        *only_forward indicates how the graph shall be traversed. if True then takes into consideration only 
        successors otehrwise(False) takes into the consideration also the predecessors   
    """
    assert graph and isinstance(graph, DiGraph) and start_node
    yield start_node
    succ = [x for x in graph.successors(start_node) if all([y(x) for y in lambda_condition_list]) and x not in visited]
    predd = [x for x in graph.predecessors(start_node) if all([y(x) for y in lambda_condition_list]) and x not in visited] if not only_forward else []
    unseen_neighbours = set(visited + succ + predd) - set(visited)
        
    for s in unseen_neighbours:
        for i in conditional_traversal(graph, s, lambda_condition_list, only_forward, visited + list(unseen_neighbours)):
            yield i

def write_to_csv(file_name, data, headder=[]):
    """ 
        writes to CSV file the headder (list) and data (list of tuples) 
    """
    assert file_name and data
    f_name = file_name + ".csv" if not file_name.endswith(".csv") else file_name 
    
    with open(f_name, 'wb') as f:
        writer = csv.writer(f)
        if headder: writer.writerows([headder])  # wrire headder if any 
        writer.writerows(data)  # write data
        f.close()

def find_files(directory, pattern):
    """ returns the files matching a file pattern resursively starting from a base dir"""
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                yield filename

def longest_sublist_condittioned(lst,compare=lambda x,y: x==y-1):
    """
        returns the longest sublist of consecutive elements that fulfil the comparison condition 
    """
    max_index, max_len = 0, 0
    for i in range(len(lst)):
        temp_index, temp_len=i,1
        for j in range(i,len(lst)-1):
            if compare(lst[j],lst[j+1]): temp_len +=1
            else: break
        if temp_len>max_len: max_len, max_index= temp_len,temp_index
        if len(lst)-i < max_len: break
    return lst[max_index:max_index+max_len]

def clean_nasty_unicode_chars(string):
    """ """
    chars = {
                        '\xc2\x82' : ',',        # High code comma
#                         '\xc2\x84' : ',,',       # High code double comma
#                         '\xc2\x85' : '...',      # Tripple dot
                        '\xc2\x88' : '^',        # High carat
                        '\xc2\x91' : '\x27',     # Forward single quote
                        '\xc2\x92' : '\x27',     # Reverse single quote
                        '\xc2\x93' : '\x22',     # Forward double quote
                        '\xc2\x94' : '\x22',     # Reverse double quote
                        '\xc2\x95' : ' ',
                        '\xc2\x96' : '-',        # High hyphen
#                         '\xc2\x97' : '--',       # Double hyphen
                        '\xc2\x99' : ' ',
                        '\xc2\xa0' : ' ',
                        '\xc2\xa6' : '|',        # Split vertical bar
#                         '\xc2\xab' : '<<',       # Double less than
#                         '\xc2\xbb' : '>>',       # Double greater than
#                         '\xc2\xbc' : '1/4',      # one quarter
#                         '\xc2\xbd' : '1/2',      # one half
#                         '\xc2\xbe' : '3/4',      # three quarters
                        '\xca\xbf' : '\x27',     # c-single quote
                        '\xcc\xa8' : '',         # modifier - under curve
                        '\xcc\xb1' : '',          # modifier - under line
                        '\xc2\xb4' : "'",
                        }
    replace_chars = lambda match: chars[match.group(0)]
    #return re.sub('(' + '|'.join(chars.keys()) + ')', replace_chars, codecs.decode(string))
    return translate_text(string, chars)
    #return string

if __name__ == '__main__':
    
    pass
