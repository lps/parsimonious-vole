'''
Created on Jan 18, 2013

@author: lps
'''
import re
"""
# non embeded patterns
(\(?"?[a-zA-Z,;\-\s]"?\)?)*\s*(1|a|b){1}(\[[\w\+\-]*\])?\s*(\(?"?[a-zA-Z,;\-\s]"?\)?)*\s*(2|a|b){1}(\[[\w\+\-]*\])?\s*

Examples
"if" @a[+negative] (,)("then") @b
@1 "and" @2
@b, @a 
@1 (,) "or" @2
"not only" @1 "but also" @2
"both" @1 "and" @2
@a , @b[-Subj]
@a , @b 


# embeded patterns


Examples
a[+bub] <marker b>
"add" a <b[-Subj]> 
a <b>


---
(\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*\s*(@1|@a|@b){1}(\[[\+\-\w]+\])?\s*(\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*\s*(@2|@a|@b){1}(\[[\+\-\w]+\])?

"""
text0 = "@b, @a " 
text = '"if and oly if"(,)(mrkr) @a[+negative] (,)("then") @b'
text1 = 'abcaaccbb'
text2 = '"if"(,)(mrkr) '
expr = '((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@1|@a|@b){1}(\[[\+\-\w]+\])?\s*((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@2|@a|@b){1}(\[[\+\-\w]+\])?'
expr1 = '((?:a|b|c)*)'
expr2 = '([\w\s\,\-;]+|["\(\)])'

# __repattern = re.compile(expr2, re.IGNORECASE)
# grps = __repattern.split(text2)
# print grps

MANDATORY = "mandatory"
OPTIONAL = "optional"
PLUS = "plus"
MINUS = "minus"

def pattern_parse(pattern_text):
    expr = '((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@1|@a|@b){1}(\[[\+\-\w]+\])?\s*((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@2|@a|@b){1}(\[[\+\-\w]+\])?'
    __repattern = re.compile(expr, re.IGNORECASE)
    grps = __repattern.match(pattern_text)
    print grps.groups()
    print "1st marker"
    # print _marker_parse(grps.group(1))
    print "1st clause"
    print _clause_feature_parse(grps.group(3))
    print "2nd marker"
    # print _marker_parse(grps.group(4))
    print "2nd clause"

def _marker_parse(marker_group_text):
    result = {MANDATORY:[], OPTIONAL:[]}
    expr = '([\w\s\,\-;]+|["\(\)])'
    __repattern = re.compile(expr, re.IGNORECASE)
    grps = __repattern.split(marker_group_text)
    # strip and reduce empty strings
    filtered = filter(None, map(lambda x: x.lstrip().rstrip(), grps))
    print filtered
    # extract all optional elements
    i = 0
    is_mandatory = True
    while i < len(filtered):
        if not filtered[i] in ["(", ")", "\""]:
            result[MANDATORY].append(filtered[i]) if is_mandatory else result[OPTIONAL].append(filtered[i]) 
        else:
            if filtered[i] == "(": 
                is_mandatory = False
            elif filtered[i] == ")":
                is_mandatory = True
        i += 1
    return result
    
def _clause_feature_parse(clause_feature_text):
    result = {PLUS:[], MINUS:[]}
    if None: return None
    else:
        expr = '([\w\s\,\-;]+|[\+\-])'
        __repattern = re.compile(expr, re.IGNORECASE)
        grps = __repattern.split(clause_feature_text.replace(" ", "").replace("[", "").replace("]", ""))
        # strip and reduce empty strings
        filtered = filter(None, map(lambda x: x.lstrip().rstrip(), grps))
        i = 0
        while i < len(filtered):
            if filtered[i] == "+":
                result[PLUS].append(filtered[i+1])
            elif filtered[i] == "-":
                result[MINUS].append(filtered[i+1])
            i=i+1
        print result 
        
#pattern_parse(text)

node = ('no .', 7)
list_of_tokens = [['It', 'it', 'PRP', 0, 1], ['was', 'be', 'VBD', 1, 2], ['Haydn', 'Haydn', 'NNP', 2], ["'s", "'s", 'POS', 3], ['Piano', 'Piano', 'NNP', 4], ['Sonata', 'Sonata', 'NNP', 5], ['No.', 'no.', 'NN', 6], ['48', '48', 'CD', 7, 8], ['.', '.', '.', 8]] 
candidates = filter(lambda x: x[1] == node[0] or x[0] == node[0], list_of_tokens)
print candidates

