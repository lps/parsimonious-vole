'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 14, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import logging
import re
import traceback
from operator import itemgetter

from parsimonious_vole.magic.guess.fs_constants import MANDATORY, MINUS, PLUS, FINITE, HYPOTAXIS, NON_FINITE, PARATAXIS, OPTIONAL, \
    PREDICATOR, PREDICATOR_FINITE
from parsimonious_vole.magic.utils import get_dict_keys_by_value, tokenize_by_space

from parsimonious_vole.magic.guess.mcg_utils import is_feature_selected, is_clause


class TaxisPattern(object):
    '''
        the pattern to be identified in the mood constituency graph
    '''
    relation_type = None
    
    markers = None
    pattern_string = None

    left_clause_node = None
    left_clause_node_features = None
    left_marker = None
    
    right_clause_node = None
    right_clause_node_features = None
    right_marker = None
    
    db_finitness_condition = None
    db_taxis_condition = None
    
    def __init__(self, _markers, _pattern_string, _relation_type, _db_taxis_condition=None, _db_finitness_condition=None):
        """ creates the """
        assert _pattern_string and _relation_type, "pattern string is " + (_pattern_string) + " relation type is " + (_relation_type)
        assert isinstance(_markers, list)
        assert isinstance(_pattern_string, basestring)
        assert isinstance(_relation_type, (basestring, list))
        
        self.markers = _markers
        self.pattern_string = _pattern_string
        self.relation_type = _relation_type if isinstance(_relation_type, list) else [_relation_type]
        
        self.db_finitness_condition = _db_finitness_condition
        self.db_taxis_condition = _db_taxis_condition
        
        try:
            self.__pattern_parse(self.pattern_string)
            self.__validate()
            self.__accomodate_markers()
            self.__accomodate_finitness_as_feature()
        except:
            logging.error("could not parse the pattern " + self.pattern_string)
            print traceback.format_exc()
    
    def __pattern_parse(self, pattern_text):
        expr = '((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@1|@a|@b){1}(\[[\+\-\w]+\])?\s*((?:\(?mrkr\)?|\(?[,;\-]\)?|\(?"[\w\s]+"\)?|\s*)*)\s*(@2|@a|@b){1}(\[[\+\-\w]+\])?'
        __repattern = re.compile(expr, re.IGNORECASE)
        grps = __repattern.match(pattern_text)
        if grps.group(1):
            self.left_marker = self.__marker_parse(grps.group(1))
        if grps.group(2):
            self.left_clause_node = grps.group(2)
        if grps.group(3):
            self.left_clause_node_features = self.__clause_feature_parse(grps.group(3))
        
        if grps.group(4):
            self.right_marker = self.__marker_parse(grps.group(4))
        if grps.group(5):
            self.right_clause_node = grps.group(5)
        if grps.group(6):
            self.right_clause_node_features = self.__clause_feature_parse(grps.group(6))
            
    def __marker_parse(self, marker_group_text):
        result = [] 
                    # {MANDATORY:[], OPTIONAL:[]}
        expr = '([\w\s\,\-;]+|["\(\)])'
        __repattern = re.compile(expr, re.IGNORECASE)
        grps = __repattern.split(marker_group_text)
        # strip and reduce empty strings
        filtered = filter(None, map(lambda x: x.lstrip().rstrip(), grps))
        # print filtered
        # extract all optional elements
        i = 0
        is_mandatory = True
        while i < len(filtered):
            if not filtered[i] in ["(", ")", "\""]:
                result.append({MANDATORY:filtered[i]}) if is_mandatory else result.append({OPTIONAL: filtered[i]}) 
            else:
                if filtered[i] == "(": 
                    is_mandatory = False
                elif filtered[i] == ")":
                    is_mandatory = True
            i += 1
        return result
    
    def __clause_feature_parse(self, clause_feature_text):
        result = {PLUS:[], MINUS:[]}
        if None: return None
        else:
            expr = '([\w\s\,\-;]+|[\+\-])'
            __repattern = re.compile(expr, re.IGNORECASE)
            grps = __repattern.split(clause_feature_text.replace(" ", "").replace("[", "").replace("]", ""))
            # strip and reduce empty strings
            filtered = filter(None, map(lambda x: x.lstrip().rstrip(), grps))
            i = 0
            while i < len(filtered):
                if filtered[i] == "+":
                    result[PLUS].append(filtered[i + 1])
                elif filtered[i] == "-":
                    result[MINUS].append(filtered[i + 1])
                i = i + 1
            # print result 
            return result
    
    def __accomodate_markers(self):
        """ sets the markers """
        if self.left_marker:
            for m in self.left_marker:
                k = get_dict_keys_by_value(m, "mrkr")
                if k: m[k[0]] = self.markers
        if self.right_marker:
            for m in self.right_marker:
                k = get_dict_keys_by_value(m, "mrkr")
                if k: m[k[0]] = self.markers
    
    def __accomodate_finitness_as_feature(self):
        """
                check the finitness of secodary clause whether it is as expected by the pattern.
                this check is due to pattern classification, to avoid writing +finite/-finite feature 
                for each pattern in the class, hence avoinding some errors.
                
                Adendum: the finitness shall be checked only for hypotactic relations as for
                paratactic ones the finitness of sencondary clause shall be the same as the one of the 
                secondary one.
                Adendum:  primary clause should be finite
                
                        
        inserts clause finitness properties
        # if paratactic template:
            #1 and 2 need have same finitness
        # if hypotactic template
            # a is finite and b is finite or non finite depending on the db requirement
            
        """
        # create clause pattern feature structures first
        if self.left_clause_node_features is None: self.left_clause_node_features = {PLUS:[], MINUS:[]}
        if self.right_clause_node_features is None: self.right_clause_node_features = {PLUS:[], MINUS:[]}
        
        if self.db_taxis_condition == HYPOTAXIS:
            # seting the primary
            if self.left_clause_node == "@a": 
                self.left_clause_node_features[PLUS].append("finite")
                if self.db_finitness_condition is FINITE:
                    self.right_clause_node_features[PLUS].append("finite")
                elif self.db_finitness_condition is NON_FINITE:
                    self.right_clause_node_features[PLUS].append("non-finite")
                else: 
                    logging.warn("could not initiate finitness features")
            elif self.right_clause_node == "@a":
                self.right_clause_node_features[PLUS].append("finite")
                if self.db_finitness_condition is FINITE:
                    self.left_clause_node_features[PLUS].append("finite")
                elif self.db_finitness_condition is NON_FINITE:
                    self.left_clause_node_features[PLUS].append("non-finite")
                else: 
                    logging.warn("could not initiate finitness features")
            else: pass
        
    def __validate(self):
        """  """
        valid_nodes = ["@1", "@2", "@a", "@b"]
        if not self.left_clause_node: raise SyntaxError("missing the left clause node")
        if not self.right_clause_node: raise SyntaxError("missing the right clause node")
        if not self.left_clause_node in valid_nodes:
            logging.warn("the left clause node(" + self.left_clause_node + ") is invalid, valid pattern clause nodes are " + str(valid_nodes) + " ")
        if not self.right_clause_node in valid_nodes:
            logging.warn("the right clause node(" + self.left_clause_node + ") is invalid, valid pattern clause nodes are " + str(valid_nodes) + " ")
        if self.db_taxis_condition:
            if self.is_hypotactic_notation() and self.db_taxis_condition is not HYPOTAXIS:
                logging.warn("the pattern is marked in db as hypotactic but the pattern notation is not")
            elif self.is_paratactic_notation() and self.db_taxis_condition is not PARATAXIS:
                logging.warn("the pattern is marked in db as paratactic but the pattern notation is not")

    def is_paratactic_notation(self):
        """ returns true if left and right nodes are @1 and @2"""
        if self.db_taxis_condition is not None and self.db_taxis_condition is PARATAXIS: return True
        para = ["@1", "@2"]
        if self.left_clause_node in para and self.right_clause_node in para:
            return True
        return False
            
    def is_hypotactic_notation(self):
        """ returns true if left and right nodes are @a and @b"""
        if self.db_taxis_condition is not None and self.db_taxis_condition is HYPOTAXIS: return True
        hypo = ["@a", "@b"]
        if self.left_clause_node in hypo and self.right_clause_node in hypo:
            return True
        return False
        
    def score_pattern_match(self, mcg, cl_node1, cl_node2):  # get_matched_markers=False
        """ 
            applies current pattern to clause1 and clause2 and returns a score of how well they match.
            clause1 is the MCG parent(direct or indirect) of clause2   
            
            Scoring:
            * mandatory_marker +20 * word count # -100 if no mandatory marker found 
            * optional_marker +10 * word count 
            * order of clauses respected +30 # otherwise -100
            * if there are features: -100 for each feature that does not match
            * if hypotactic pattern and the clauses are of different finitness -100
            * if paratactic pattern beta cannot be finite if alfa is not_finte -100
        """
        cl1_words = self.__taxis_relevant_words(mcg, cl_node1)
        cl2_words = self.__taxis_relevant_words(mcg, cl_node2)
        
        def __score_marker(clause_words_list, mrk, score_per_word=10):
            """ 
                if at least one marker is found in the clause words then 
                the one with highest value is selected
                otherwise score is 0 (zero)
                
                markers can be multiple words string, while clause_words single word list 
            """
            marker_list = mrk if isinstance(mrk, list) else [mrk]
            clw = " ".join(clause_words_list)
            qsc = [(m, score_per_word * len(tokenize_by_space(m))) for m in marker_list if m in clw]  # [(marker,score)]
            #logging.debug("Marker&Scores:" + str(qsc))
            if qsc:
                qsc = sorted(qsc, key=itemgetter(1), reverse=True)
                return qsc[0][1]  # the second tuple-item of the first list item
            return 0
        
        def __score_clause_markers(clause_words, clause_markers, score_madatory=20, mandatory_penalty=1000, score_optional=10):
            """ scores all the markers for clause words and selects the maximum scores """
            if not clause_markers: return 0
            score = 0
            for mmm in clause_markers:
                if MANDATORY in mmm:
                    s = __score_marker(clause_words, mmm[MANDATORY], score_madatory)
                    if s > 0: score += s
                    else: score -= mandatory_penalty 
                elif OPTIONAL in mmm:
                    score += __score_marker(clause_words, mmm[OPTIONAL], score_optional)
            return score
                    
        def __score_clause_predicate_order(correct_order_score=50, penalty=1000): 
            """ if the predicates of cl1 and cl2 are as @1/a and @2/b or otherwise """
            predicates = filter(lambda x: x.type() in [PREDICATOR, PREDICATOR_FINITE], mcg.successors(cl_node1))
            pred1 = predicates[0]
            predicates = filter(lambda x: x.type() in [PREDICATOR, PREDICATOR_FINITE], mcg.successors(cl_node2))
            pred2 = predicates[0]
            
            # minimal check
            if self.left_clause_node in ["@1", "@a"] and pred1.lowest_token_index().id() < pred2.lowest_token_index().id():
                return correct_order_score
            # minimal check
            if self.right_clause_node in ["@1", "@a"] and pred1.lowest_token_index().id() > pred2.lowest_token_index().id():
                return correct_order_score
            return -penalty

        def __score_pattern_features(clause_node, pattern_features, present_score=25, missing_penalty=1000):
            """ checks features of clauses and returns the score """
            if pattern_features is None or not pattern_features.keys(): return 0
            scr = 0
            for feature in pattern_features[PLUS]:
                if is_feature_selected(clause_node, feature): scr += present_score
                else: scr -= missing_penalty
            
            for feature in pattern_features[MINUS]:
                if not is_feature_selected(clause_node, feature): scr += present_score
                else: scr -= missing_penalty
            return scr
        
        def __score_paratactic_features(correct_score=25, penalty=1000):
            """" if parataxis then clauses should have the same finiteness """
            if self.is_paratactic_notation():
                if is_feature_selected(cl_node1, "finite") and not is_feature_selected(cl_node2, "finite"):
                        return -penalty
                elif is_feature_selected(cl_node1, "non-finite") and not is_feature_selected(cl_node2, "non-finite"):
                        return -penalty
            return correct_score
        
        # ## 
        score = 0
        # scoring left mandatory markers #scoring left optional  markers
        score += __score_clause_markers(cl1_words, self.left_marker, 20, 1000, 10)
        #logging.debug("score after evaluating Left mandatory and optional " + str(score))
        # scoring right mandatory markers #scoring right optional  markers
        score += __score_clause_markers(cl2_words, self.right_marker, 20, 1000, 10)
        #logging.debug("score after evaluating Right mandatory and optional " + str(score))
        # order of clauses respected +30 # otherwise -100
        score += __score_clause_predicate_order(correct_order_score=50, penalty=1000)
        #logging.debug("score after evaluating clause order " + str(score))
         
        # if paratactic pattern and the clauses are of different finitness -100
        score += __score_paratactic_features(correct_score=25, penalty=1000)
        #logging.debug("score after evaluating paratactic finitness constraint " + str(score))
         
        # check pattern-clause features, whether are matched in the target clauses
        score += __score_pattern_features(cl_node1, self.left_clause_node_features, present_score=25, missing_penalty=1000)
        #logging.debug("score after evaluating left pattern features of  " + str(score))
        score += __score_pattern_features(cl_node2, self.right_clause_node_features, present_score=25, missing_penalty=1000)
        #logging.debug("score after evaluating right pattern features of  " + str(score))
        
        return score
    
    def __taxis_relevant_words(self, mcg, clause_node):
        """ 
            returns the words that are relevant for performing a taxis matching.
            the relevant words and punctuation markers are the ones preceding the predicator.
            returns the list of words preceding the predicator. 
        """
        assert clause_node and mcg
        assert is_clause(clause_node, mcg)
        predicates = filter(lambda x: x.type() in [PREDICATOR, PREDICATOR_FINITE], mcg.successors(clause_node))
        if predicates: pred = predicates[0]
        assert pred
        return clause_node.words()[:pred.lowest_token_index().id() - clause_node.lowest_token_index().id()]
    
    def __str__(self):
        return self.__repr__()
    
    def __repr__(self):
        return str(self.relation_type) + "\t"\
             + " " + str(self.db_taxis_condition)\
             + " " + (str(self.left_marker) if self.left_marker else "") \
             + " " + str(self.left_clause_node) + "" + (str(self.left_clause_node_features) if self.left_clause_node_features else " ")\
             + " " + (str(self.right_marker) if self.right_marker else "")\
             + " " + str(self.right_clause_node) + "" + (str(self.right_clause_node_features) if self.right_clause_node_features else "")

if __name__ == '__main__':
    pass 

