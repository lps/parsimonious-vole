'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 19, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

'''
import csv
import itertools
import pickle
import pprint

from parsimonious_vole.magic.guess.fs_constants import CLAUSE, C_TYPE, COMPLEMENT, COMPLEMENT_ADJUNCT, PARTICIPANT_ROLE, SUBJECT,\
    QUALIFIER, FINITE, PARATAXIS, HYPOTAXIS, NON_FINITE
from parsimonious_vole.magic.parse.VerbForm import PROCESS_TYPE
from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.taxis.taxis_pattern import TaxisPattern
from parsimonious_vole.magic.utils import uniquify, list_minus, get_dict_keys_by_value, singleton
from networkx.classes.digraph import DiGraph

from parsimonious_vole.configuration import PICKLE_DESTINATION_TAXIS_PATTERNS, CSV_SOURCE_TAXIS_PATTERNS
from parsimonious_vole.magic.guess.mood_pattern_matching import NODES, EDGES, contains_pattern


@singleton
class TaxisDB(object):
    """
        use it to access TaxisPattern library
    """
    __db = None
    
    def __init__(self):
        self.__db = pickle.load(open(PICKLE_DESTINATION_TAXIS_PATTERNS, "rb"))

    def get_entries(self):
        return self.__db
    
    def score_clause_pair(self, mcg, clause_node1, clause_node2):
        """
            apply all the patterns to two clause nodes and return the list of possible relations and clause orders
        """
        result = []
        for pattern in self.get_entries():
            s = pattern.score_pattern_match(mcg, clause_node1, clause_node2) 
            if s > 0: result.append((s, pattern, clause_node1, clause_node2))
        
        # take only the top score elements
        result = sorted(uniquify(result), key=lambda x:x[0], reverse=True)
        if result:
            top = result[0][0]
            result = [x for x in result if x[0] == top]
        return  result
    
    def print_all(self):
        pprint.pprint(self.__db)
        print "-------------------------------------------\n\t\t\t\t\t", len(self.__db)
        
######################################################################################
# generate the cPickle from CSV file
######################################################################################
def read_and_transform(source_csv=CSV_SOURCE_TAXIS_PATTERNS, has_header=True):
    """ 
        Generate the pattern database from CSV file. 
        Read the csv and transform it into a pattern database .pickle 
    """
    def _split_markers(marker_text):
        return  filter(None, [x.lstrip().rstrip() for x in marker_text.split(",") if x])
    
    def _split_patterns(pattern_text):
        return  filter(None, [x.lstrip().rstrip() for x in pattern_text.split("//")])
    
    headerline = None
    patterns = []
    print "reading and processing CSV file: ", source_csv
    with open(source_csv, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        if has_header: headerline = reader.next()
        for row in reader:
            sys_net_fs = row[2]
            # parataxis 
            _markers = _split_markers(row[4])
            for _pattern_string in _split_patterns(row[5]):
                patterns.append(TaxisPattern(_markers, _pattern_string, sys_net_fs, PARATAXIS, FINITE))

            # hypotaxis finite
            _markers = _split_markers(row[6])
            for _pattern_string in _split_patterns(row[7]):
                patterns.append(TaxisPattern(_markers, _pattern_string, sys_net_fs, HYPOTAXIS, FINITE))

            # hypotaxis non-finite 1
            _markers = _split_markers(row[8])
            for _pattern_string in _split_patterns(row[9]):
                patterns.append(TaxisPattern(_markers, _pattern_string, sys_net_fs, HYPOTAXIS, NON_FINITE))

            # hypotaxis non-finite 2
            _markers = _split_markers(row[10])
            for _pattern_string in _split_patterns(row[11]):
                patterns.append(TaxisPattern(_markers, _pattern_string, sys_net_fs, HYPOTAXIS, NON_FINITE))

            # hypotaxis non-finite 3
            _markers = _split_markers(row[12])
            for _pattern_string in _split_patterns(row[13]):
                patterns.append(TaxisPattern(_markers, _pattern_string, sys_net_fs, HYPOTAXIS, NON_FINITE))
    
    # write pattern DB
    print "writing Pickle file file: ", PICKLE_DESTINATION_TAXIS_PATTERNS
    pickle.dump(patterns, open(PICKLE_DESTINATION_TAXIS_PATTERNS, "wb"))
    print "Done."


######################################################################################
# taxis graph building
######################################################################################
def build_caluse_complex_graph(mcg):
    """  """
    txg = DiGraph()
    if not len(filter(lambda x: x.type() == CLAUSE, mcg.nodes()))>1: return None
    
    # nodes get clause id + tactic labels
    # edges get rel type 
    
    def _compress_taxis_rel_list(rels):
        """ [('a',[1]),('a',[2]),('a',[4])] --> {"a":[1,2,4]} """
        return {key:[x[1][0] for x in group] for key,group in itertools.groupby(rels,lambda x:x[0])}
    
    # taxis
    for pair in get_clause_pairs_for_taxis_matching(mcg):
        edge_rel=[ (x[1].db_taxis_condition,x[1].relation_type) for x in TaxisDB().score_clause_pair(mcg, pair[0], pair[1])]
        txg.add_edge(pair[0], pair[1], {TYPE:_compress_taxis_rel_list(edge_rel)})
    # embeded - all 
    embeded = get_clause_pairs_for_embediment_matching(mcg)
    for pair in embeded[0][1]:
        if txg.has_edge(pair[0],  pair[1]): txg[pair[0]][pair[1]][TYPE]["embedded"].append("influential")
        else:txg.add_edge(pair[0],  pair[1], {TYPE:{"embedded":["influential"]}})
    for pair in embeded[1][1]:
        if txg.has_edge(pair[0],  pair[1]): txg[pair[0]][pair[1]][TYPE]["embedded"].append("projection")
        else:txg.add_edge(pair[0],  pair[1], {TYPE:{"embedded":["projection"]}})
    for pair in embeded[2][1]:
        if txg.has_edge(pair[0],  pair[1]): txg[pair[0]][pair[1]][TYPE]["embedded"].append("qualifier")
        else: txg.add_edge(pair[0],  pair[1], {TYPE:{"embedded":["qualifier"]}})
    for pair in embeded[3][1]:
        if txg.has_edge(pair[0],  pair[1]): txg[pair[0]][pair[1]][TYPE]["embedded"].append("full")
        else:txg.add_edge(pair[0],  pair[1], {TYPE:{"embedded":["full"]}})
    return txg 

######################################################################################
direct_clause_pattern = {NODES:{"cl":({C_TYPE:CLAUSE, }, None),
                              "cl2":({C_TYPE:CLAUSE, }, None), },
                         EDGES:[  ("cl", "cl2", None), ]}

def get_clause_pairs_for_taxis_matching(mcg):
    """ returns a list of clause pairs to be tested for taxis relations """
    # only direct clause relations are taken into consideration
    # the indirect parenting via participant constituent are embedement relations
    match_result = contains_pattern(mcg, direct_clause_pattern, True)
    return __get_clause_pairs_from_match_result(match_result, 'cl', 'cl2')


def get_clause_pairs_for_embediment_matching(mcg, truncate_embded=True):
    """ 
            
         get pairs of clauses that are embedded as full participants or qualifiers. 
         they reveal either embedement(influential),  projection (cognitive, emotional, verbal), 
         or something else (to be checked)
         
         result =  (("influential",[...]), 
                    ("projected",[...]),
                    ("embeded_qualifiers",[...]),
                    ("embeded_full_participants",[...]) )
         
          the last two patterns are very generic, therefore, the result can include them stripped from 
          the first two results (i.e. influential and projection) 
    """
    result = [] # [(cl1,cl2),(xl3,cl4)...]
    influential = __get_influencial_embeded_clause_pairs(mcg)
    projected = __get_projection_embedded_clause_pairs(mcg)
    # here should be all the embeded clauses that are not influential and not projections unless all are useful, t.b.s.
    # the following 2 patterns are the most generic ones and "eventually" include the previous two patterns
    # 
    embeded_qualifier = list_minus(__get_relative_embeded__qualifier_clause_pairs(mcg),(influential+projected)) \
            if truncate_embded else  __get_relative_embeded__qualifier_clause_pairs(mcg)
    embeded_full_participant = list_minus(__get_relative_embeded_full_clause_pairs(mcg),(influential+projected)) \
            if truncate_embded else __get_relative_embeded_full_clause_pairs(mcg)
    return (("influential",uniquify(influential)), ("projected",uniquify(projected)), \
            ("embeded_qualifiers",uniquify(embeded_qualifier)), \
            ("embeded_full_participants",uniquify(embeded_full_participant)))

######################################################################################
# Notes on influential patterns
# p1:
# cl[+finite](causative,directional(causative interpret) )-compl-qualif-clause[+perfective,+imperfective(for directional, stopping)]
#
# p2:
# cl[+finite](preventive,delaying,starting)-compl1- compl2-(mrkr)
#                                                         - cl2[+imperfective]
# p3:
# cl[+finite](causative,enabling,tentative)-compl-cl[+finite,+perfective]
#
# p4: nomials and grammatical metaphors
# cl[+finite](start)-subj(event noun WN check)
#
# p5:
# cl[+finite](start,stop,continue,delay, cease)-compl-thing(event WN check) 
# #
# p1
cla1_compl_qualif_cla2 = {NODES:{"cl":({C_TYPE:CLAUSE, u'RANK':u'clause', u"FINITNESS":u'finite', }, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:['causative']}, None),
                                "compl":({u'INTERPERSONAL-FUNCTION':u'complement-direct'}, None),
                                "qualif":({C_TYPE:QUALIFIER,u'NOMINAL-GROUP-FUNCTION':u'qualifier', u'RANK':u'clause'}, None),
                              "cl2":({C_TYPE:CLAUSE,u'RANK':u'clause', "NON-FINITE-TYPE":[u'perfective']}, None), },
                         EDGES:[("cl","pr",None),  ("cl", "compl", None), ("compl", "qualif", None), ("qualif", "cl2", None), ]}
# p2
cla1_compl_mrk_cla2 = {NODES:{"cl":({C_TYPE:CLAUSE,u'RANK':u'clause', u"FINITNESS":u'finite',}, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:['preventive']}, None),
                                "compl":({u'INTERPERSONAL-FUNCTION':u'complement-direct'}, None),
                                "mrk":({"RANK":u'marker'}, None),
                              "cl2":({C_TYPE:CLAUSE,u'RANK':u'clause', "NON-FINITE-TYPE":[u'imperfective']}, None), 
                              "cl3!":({u'RANK':u'clause'}, None), },
                         EDGES:[("cl","pr",None),  ("cl", "compl", None), ("compl", "mrk", None), ("compl", "cl2", None),("cl2", "cl3", None), ]}

cla1_compl_cla2_imperfective = {NODES:{"cl":({C_TYPE:CLAUSE,u'RANK':u'clause', u"FINITNESS":u'finite',}, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:['delaying', 'preventive', 'ceasing', 'starting']}, None),
                                "compl":({u'INTERPERSONAL-FUNCTION':u'complement-direct'}, None),
                              "cl2":({C_TYPE:CLAUSE,u'RANK':u'clause', "NON-FINITE-TYPE":[u'imperfective']}, None), 
                                "cl3!":({u'RANK':u'clause'}, None), },
                         EDGES:[("cl","pr",None),  ("cl", "compl", None), ("compl", "cl2", None),("cl2", "cl3", None), ]}

# p3
cla1_compl_cla2_finite = {NODES:{"cl":({C_TYPE:CLAUSE,u'RANK':u'clause', u"FINITNESS":u'finite',}, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:['enabling', 'tentative', 'causative','permisive','failing','succeeding']}, None),
                                "compl":({u'INTERPERSONAL-FUNCTION':u'complement-direct'}, None),
                              "cl2":({C_TYPE:CLAUSE,u'RANK':u'clause', "FINITNESS":[u'finite']}, None), 
                              "cl3!":({u'RANK':u'clause'}, None), },
                         EDGES:[("cl","pr",None),  ("cl", "compl", None), ("compl", "cl2", None),("cl2", "cl3", None), ]}

cla1_compl_cla2_perfective = {NODES:{"cl":({C_TYPE:CLAUSE,u'RANK':u'clause', u"FINITNESS":u'finite',}, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:['enabling', 'tentative', 'causative','permisive','failing','succeeding']}, None),
                                "compl":({u'INTERPERSONAL-FUNCTION':u'complement-direct'}, None),
                              "cl2":({C_TYPE:CLAUSE,u'RANK':u'clause', "NON-FINITE-TYPE":[u'perfective']}, None), 
                              "cl3!":({u'RANK':u'clause'}, None), },
                         EDGES:[("cl","pr",None),  ("cl", "compl", None), ("compl", "cl2", None),("cl2", "cl3", None), ]}

def __get_influencial_embeded_clause_pairs(mcg):
    """
        returns influential embedded clause pairs;
    """
    result = []
    match_result = contains_pattern(mcg, cla1_compl_qualif_cla2, True) # cla1_compl_qualif_cla2
    result += __get_clause_pairs_from_match_result(match_result,"cl","cl2")
    
    match_result = contains_pattern(mcg, cla1_compl_mrk_cla2, True) # cla1_compl_mrk_cla2
    result += __get_clause_pairs_from_match_result(match_result,"cl","cl2")
    
    match_result = contains_pattern(mcg, cla1_compl_cla2_imperfective, True)# cla1_compl_cla2_imperfective
    result += __get_clause_pairs_from_match_result(match_result,"cl","cl2")
     
    match_result = contains_pattern(mcg, cla1_compl_cla2_finite, True) # cla1_compl_cla2_finite
    result += __get_clause_pairs_from_match_result(match_result,"cl","cl2")
     
    match_result = contains_pattern(mcg, cla1_compl_cla2_perfective, True) # cla1_compl_cla2_perfective
    result += __get_clause_pairs_from_match_result(match_result,"cl","cl2")

    return result
######################################################################################
cla1_compl_subj_cla2_projection = {NODES:{"cl":({C_TYPE:CLAUSE,u"FINITNESS":u'finite',}, None),
                                 "pr":({u'EXPERIENTIAL-FUNCTION':u'process', PROCESS_TYPE:[u'two role cognition',u'three role cognition']}, None),
                                "compl":({C_TYPE:[SUBJECT,COMPLEMENT,COMPLEMENT_ADJUNCT],PARTICIPANT_ROLE:['Ph','Cre-Ph']}, None),
                                "cl2":({C_TYPE:CLAUSE,}, None),}, 
                         EDGES:[("cl","pr",None), ("cl", "compl", None), ("compl", "cl2", None) ]}

def __get_projection_embedded_clause_pairs(mcg):
    """
        returns projection embedded clause pairs;
        TODO: out of scope in current project
        the result is an estimated/untested clause pair   
    """
    match_result = contains_pattern(mcg, cla1_compl_subj_cla2_projection , True) 
    return __get_clause_pairs_from_match_result(match_result,"cl","cl2")

######################################################################################
cla1_compl_subj_qualifier_cla2 = {NODES:{"cl":({C_TYPE:CLAUSE,}, None),
                                "compl":({C_TYPE:[SUBJECT,COMPLEMENT,COMPLEMENT_ADJUNCT]}, None),
                                "qualifier":({C_TYPE:QUALIFIER}, None),
                              "cl2":({C_TYPE:CLAUSE,}, None),}, 
                         EDGES:[  ("cl", "compl", None), ("compl", "qualifier", None),("qualifier", "cl2", None) ]}

def __get_relative_embeded__qualifier_clause_pairs(mcg):
    """ returns the embeded clauses that are qualifiers of a nominal group. 
    """
    match_result = contains_pattern(mcg, cla1_compl_subj_qualifier_cla2, True) 
    return __get_clause_pairs_from_match_result(match_result,"cl","cl2")

######################################################################################
cla1_compl_subj_cla2 = {NODES:{"cl":({C_TYPE:CLAUSE,}, None),
                                "compl":({C_TYPE:[SUBJECT,COMPLEMENT,COMPLEMENT_ADJUNCT]}, None),
                              "cl2":({C_TYPE:CLAUSE,}, None),}, 
                         EDGES:[  ("cl", "compl", None), ("compl", "cl2", None), ]}
                   
def __get_relative_embeded_full_clause_pairs(mcg):
    """ 
    returns the embeded clauses that are full participants. 
    """
    match_result = contains_pattern(mcg, cla1_compl_subj_cla2, True) 
    return __get_clause_pairs_from_match_result(match_result,"cl","cl2")

######################################################################################
def __get_clause_pairs_from_match_result(pattern_check_result,clause_label1="cl",clause_label2="cl2"):
    """ 
        an utility function for getting clause pairs
        returns the clause1c clause2 pairs 
    """
    assert pattern_check_result and clause_label1 and clause_label2 and isinstance(pattern_check_result, tuple)
    found, match = pattern_check_result
    # found, match2 = contains_pattern(mcg, direct_clause_pattern_non_finite, True)
    # match ==  [{mcgn1:pn1, mcgn2:pn2},{{mcgn1:pn1, mcgn2:pn2}}]
    # -> [(mcgn1,mcgn2),(mcgn1,mcgn2)]
    result = []
    if found:
        for m in match:
            cl = get_dict_keys_by_value(m, "cl")[0]  # the label of dominant clause in the pattern above
            cl2 = get_dict_keys_by_value(m, "cl2")[0]  # the label of dominated clause in the pattern above
            result.append((cl, cl2))
    return result

if __name__ == '__main__':
    read_and_transform()
    pass
