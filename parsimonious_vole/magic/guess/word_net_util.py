'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 19, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

from nltk.corpus import wordnet as wn

from parsimonious_vole.magic.utils import uniquify


def get_borders_from_noun_path(path):
    """ """
    assert path
    top = None
    bottom = None
    for i in path:
        if i.lexname == "noun.Tops": top = i
        else: 
            bottom = i
            break
    return bottom,top

def get_unique_lex_names_in_path(path):
    return uniquify([x.lexname for x in path])

def get_top_borders_of_noun(word,include_lex=False):
    """ get the last synset that is a top noun and 
        the immediate next synset that is a non-top noun"""
    lower = []
    upper = []
    ss = {k:k.hypernym_paths() for k in wn.synsets(word,pos="n")}
    for i in ss.itervalues():
        for j in i:
            l,u = get_borders_from_noun_path(j)
            lower.append(l)
            upper.append(u)
    if include_lex:
        return lower,upper,[x.lexname for x in lower if x]
    return lower,upper

def sysnset_by_parent_path(parent_path):
    """ returns a list of synsets that have the parent hierarchy as given in parent_path """
    assert parent_path
    assert isinstance(parent_path, list), "Lexical parent path shall be a list"
    ss = {k:k.hypernym_paths() for k in wn.synsets(parent_path[0])}
    result = []
    for k,v in dict(ss).iteritems():
        for j in v:
            skip = False
            for i in parent_path:
                print "",i,parent_path.index(i),j[ -(parent_path.index(i)+1)]
                if i not in str(j[ -(parent_path.index(i)+1)]):
                    skip = True
            if not skip: result.append(j)
    return result

def get_verb_borders(word,include_lex=False):
    """ returns the verb top synset and the bottom synset from each synset path """
    ss = {k:k.hypernym_paths() for k in wn.synsets(word,pos="v")}
    tops = []
    bottoms = []
    for i in ss.itervalues():
        for j in i:
            tops.append(j[0])
            bottoms.append(j[-1])
    if include_lex:
        return bottoms,tops,[x.lexname for x in bottoms]
    return bottoms,tops

def get_adjective_synsets(word):
    """
        return the adjective synsets
    """
    return wn.synsets(word,pos=["a"])

def get_adverb_synsets(word):
    """ """
    return wn.synsets(word,pos=["r"])

def print_wn_noun_top(item=wn.synset('entity.n.01'),ident=0): 
    """ """
    sss = set()
    for i in item.hyponyms():
        if i.lexname == "noun.Tops":
            print "|"*ident+"->"+str(i)
            print_wn_noun_top(i, ident+1)
        else:
            #print "|"+i.lexname+"-"*ident+">"+str(i)
            sss.add(i.lexname)
    if sss: print sss

if __name__ == '__main__':
    pass
