'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Oct 26, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

'''
from parsimonious_vole.magic.utils import strip

from parsimonious_vole.configuration import CONSTITUENT_SYSTEM, PERSON_SYSTEM, DETERMINANT_SYSTEM, \
    MOOD_SYSTEM, AGREEMENT_SYSTEM, TRANSITIVITY_SYSTEM
from parsimonious_vole.uam_corpus_tool_interface.io.network_scheme import read_systemic_network_from_file, \
    feature_path_to_string_list


def generate_dictionary_from_feature_glosses(path):
    """ 
     generate a path of feature selections for each gloss in the sn
    """
    sn = read_systemic_network_from_file(path)
    d = {}
    for path in sn.feature_paths():
        if path[-1].comment:
            if "," in path[-1].comment:
                words = path[-1].comment.lower().split(",")
                for word in words:
                    if word not in d:  d[strip(word)] = set([feature_path_to_string_list(path)[-1]])
                    else: d[strip(word)].union(set([feature_path_to_string_list(path)[-1]]))
            else:
                if strip(path[-1].comment) not in d:
                    d[strip(path[-1].comment)] = set([feature_path_to_string_list(path)[-1]])
                else: d[strip(path[-1].comment)].union(set([feature_path_to_string_list(path)[-1]]))
    for k in d.keys():
        d[k] = list(d[k]) 
    return d

###############################################################################################
# generated with generate_dictionary_from_feature_glosses
###############################################################################################


VOCABULARY_DETERMINER = {
 u'a': [u'partial-non-selective-singular'],
 u'all': [u'positive-plural'],
 u'an': [u'partial-non-selective-singular'],
 u'any': [u'partial-non-singular'],
 u'anybody': [u'partial-non-selective-singular'],
 u'anyone': [u'partial-non-selective-singular'],
 u'anything': [u'partial-non-selective-singular'],
 u'both': [u'positive-plural'],
 u'each': [u'positive-singular'],
 u'either': [u'partial-singular'],
 u'every': [u'positive-singular'],
 u'everybody': [u'positive-plural'],
 u'everyone': [u'positive-plural'],
 u'everything': [u'positive-plural'],
 u'few': [u'partial-non-singular'],
 u'her': [u'female'],
 u'his': [u'male'],
 u'its': [u'non-conscious'],
 u'my': [u'speaker'],
 u'neither': [u'negative'],
 u'no': [u'negative'],
 u'nobody': [u'negative'],
 u'none': [u'negative'],
 u'noone': [u'negative'],
 u'not any': [u'negative'],
 u'not either': [u'negative'],
 u'nothing': [u'negative'],
 u'one': [u'partial-singular'],
 u'our': [u'speaker-plus'],
 u'several': [u'partial-non-singular'],
 u'some': [u'partial-non-singular'],
 u'somebody': [u'partial-singular'],
 u'someone': [u'partial-singular'],
 u'something': [u'partial-singular'],
 u'that': [u'non-plural'],
 u'the': [u'non-selective'],
 u'their': [u'plural-referent'],
 u'these': [u'near'],
 u'this': [u'near'],
 u'those': [u'plural'],
 u'what': [u'interrogative'],
 u'which': [u'interrogative'],
 u'who': [u'interrogative'],
 u'whose': [u'interrogative'],
 u'your': [u'addressee']}

###############################################################################################
VOCABULARY_PERSON = {
 u'i': [u'speaker'],
 u'he': [u'male'],
 u'her': [u'female'],
 u'herself': [u'female'],
 u'him': [u'male'],
 u'himself': [u'male'],
 u'it': [u'non-conscious'],
 u'itself': [u'non-conscious'],
 u'me': [u'speaker'],
 u'myself': [u'speaker'],
 u'one': [u'generalized'],
 u'oneself': [u'generalized'],
 u'ourself': [u'speaker-plus'],
 u'ourselves': [u'speaker-plus'],
 u'she': [u'female'],
 u'them': [u'plural-referent'],
 u'themself': [u'plural-referent'],
 u'themselves': [u'plural-referent'],
 u'they': [u'plural-referent'],
 u'us': [u'speaker-plus'],
 u'we': [u'speaker-plus'],
 u'you': [u'addressee'],
 u'yourself': [u'addressee'],
 u'yourselves': [u'addressee']}

VOCABULARY_ANIMATE= [u'somebody', u'someone', u'one', u'oneself', u'everybody', u'everyone', u'anybody', 
                     u'anyone', u'who', u'whose', u'noone', u'nobody', u'themselves', u'themself',]
FUNCTIONS_ANIMATE = ["speaker","speaker-plus","addressee","male","female"]

# modal verbs with selections from Systemic_network_MOOD sytemic network
VOCABULARY_MODALS_POSITIVE = { # modal verb possible systemic selection when combined with positive polarity
                              u'can':[u'probability',u'obligation-permission',u'inclination-volition',u'ability'],
                              u'could':[u'probability',u'obligation-permission',u'inclination-volition',u'ability'],
                              u'may':[u'probability',u'obligation-permission',u'inclination-volition'],
                              u'might':[u'probability',u'obligation-permission'],
                              u'must':[u'probability',u'obligation-permission'],
                              u'shall':[u'inclination-volition'],
                              u'should':[u'probability',u'obligation-permission',u'inclination-volition'],
                              u'would':[u'probability',u'usuality',u'inclination-volition'],
                              u'able to':[u'probability',u'ability'],
                              u'ought to':[u'obligation-permission'],
                              u'have to':[u'obligation-permission'],
                              u'used to':[u'usuality'],}

VOCABULARY_MODALS_NEGATIVE = {  # modal verb possible systemic selection when combined with negative polarity
                              u'can':[u'probability',u'obligation-permission',u'ability'],
                              u'ca':[u'probability',u'obligation-permission',u'ability'],# can't
                              u'could':[u'probability',u'obligation-permission',u'inclination-volition',u'ability'], # couldn't
                              u'may':[u'obligation-permission',u'inclination-volition'],
                              u'might':[u'probability'],
                              u'must':[u'obligation-permission'],
                              u'mus':[u'obligation-permission'], # musn't
                              u'shall':[u'obligation-permission'],#
                              u'should':[u'probability',u'obligation-permission',u'inclination-volition'], # shouldn't
                              u'would':[u'usuality',u'inclination-volition'], # wouldn't
                              u'able to':[u'probability',u'ability'],
                              u'ought to':[u'obligation-permission'],
                              u'have to':[u'obligation-permission'],
                              u'used to':[u'usuality'],}

#TODO: analyze the influence of subject person function on the selection of modality type
#NOTE: the directionality of speakr or adresee influences the force being applied and 
#varies between obligation/perm and inclination volition
#NOTE: the inanimate subjects are likely to select probability/usuality
#NOTE: the speaker subjects are likely to select ability/probability/volition
#NOTE: the listener subjects are likely to select from obligation-permission/ability/...  

# adjuncts marked with choices from  MOOD_CONSTITUENCY systemic network
# NOT Implemented
MOOD_ADJUNCTS = {"eventually":["now-relative"],
                 "soon":["now-relative"],
                 "once":["now-relative"],
                 "just":["now-relative", "expectancy-limiting"],
                 "still":["now-relative"],
                 "already":["now-relative"],
                 "not yet":["now-relative"],
                 "no longer":["now-relative"],
                 "usually":["usuality"],
                 "always":["usuality"],
                 "never":["usuality"],
                 "sometimes":["usuality"],
                 "occasionally":["usuality"],
                 "seldom":["usuality"],
                 "rarely":["usuality"],
                 "probably":["probability"],
                 "certainly":["probability"],
                 "definitely":["probability"],
                 "no way":["probability"],
                 "possibly":["probability"],
                 "perhaps":["probability"],
                 "maybe":["probability"],
                 "hardly":["probability"],
                 "totally":["total"],
                 "utterly":["total"],
                 "entirely":["total"],
                 "completely":["total"],
                 "throughly":["total"],
                 "downright":["total"],
                 "quite":["high"],
                 "almost":["high"],
                 "nearly":["high"],
                 "scarcely":["low"],
                 "hardly":["low"],
                 "even":["expectancy-exceeding"],
                 "actually":["expectancy-exceeding"],
                 "really":["expectancy-exceeding"],
                 "in fact":["expectancy-exceeding"],
                 "indeed":["expectancy-exceeding"],
                 "simply":["expectancy-limiting"],
                 "merely":["expectancy-limiting"],
                 "only":["expectancy-limiting"],} 

# adjuncts marked with the features from MOOD_ELEMENTS/constituency system network
# not implemented,  
MOOD_CONJUNCTIVE_ADJUNCTS = {'actually': ['conjunctive-adjunct'],
 'also': ['conjunctive-adjunct'],
 'alternatively': ['conjunctive-adjunct'],
 'anyway': ['conjunctive-adjunct'],
 'as a matter of fact': ['conjunctive-adjunct'],
 'as a result': ['conjunctive-adjunct'],
 "as far as that's concerned": ['conjunctive-adjunct'],
 'at least': ['conjunctive-adjunct'],
 'before that': ['conjunctive-adjunct'],
 'besides': ['conjunctive-adjunct'],
 'briefly': ['conjunctive-adjunct'],
 'conversely': ['conjunctive-adjunct'],
 'despite that': ['conjunctive-adjunct'],
 'finally': ['conjunctive-adjunct'],
 'for instance': ['conjunctive-adjunct'],
 'for this reason': ['conjunctive-adjunct'],
 'however': ['conjunctive-adjunct'],
 'in addition': ['conjunctive-adjunct'],
 'in any case': ['conjunctive-adjunct'],
 'in conclusion': ['conjunctive-adjunct'],
 'in fact': ['conjunctive-adjunct'],
 'in other words': ['conjunctive-adjunct'],
 'in that case': ['conjunctive-adjunct'],
 'in the same way': ['conjunctive-adjunct'],
 'in this respect': ['conjunctive-adjunct'],
 'instead': ['conjunctive-adjunct'],
 'later on': ['conjunctive-adjunct'],
 'leaving that aside': ['conjunctive-adjunct'],
 'likewise': ['conjunctive-adjunct'],
 'meanwhile': ['conjunctive-adjunct'],
 'moreover': ['conjunctive-adjunct'],
 'nevertheless': ['conjunctive-adjunct'],
 'next': ['conjunctive-adjunct'],
 'on the other hand': ['conjunctive-adjunct'],
 'or rather': ['conjunctive-adjunct'],
 'otherwise': ['conjunctive-adjunct'],
 'soon': ['conjunctive-adjunct'],
 'starting a new idea': ['conjunctive-adjunct'],
 'that is': ['conjunctive-adjunct'],
 'therefore': ['conjunctive-adjunct'],
 'to be precise': ['conjunctive-adjunct'],
 'to sum up': ['conjunctive-adjunct'],
 'under the circumstances': ['conjunctive-adjunct'],
 'with this in mind': ['conjunctive-adjunct']}

###############################################################################################
# MOOD_CONSTITUENT type mapping
###############################################################################################

CONSTITUENT_TYPE_MAPPING = {
"finite" :["finite", "verbal-group"],
"predicator_finite" :["finite", "process", "verbal-group","predicator"],
"predicator" :["verbal-group", "process","predicator"],
"adjunct" :["adjunct", "circumstance", "adverbial-group"],
"complement" :["complement-direct"],
"complement_agent" :["complement-direct","nominal-group"],
"complement_dative" :["complement-indirect","nominal-group"],
"complement_or_adjunct" :["complement-direct", "adjunct"],
"subject" :["subject"],
"expletive_marker" :["expletive"],
"marker" :["marker"],
"clause" :["clause"],
"root" : ["clause"], # not sentence
"discourse" : [],
#######################
"deictic": ["deictic"],
"predeictic": ["pre-deictic"],
"cardinal_numerative": ["cardinal-numerative"],
"epithet_classifier_or_ordinal": ["epithet", "classifier", "ordinal-numerative"],
"posessor": ["posessor"],
"apposition": ["apposition"],
"qualifier": ["qualifier"],
"thing": ["thing"],
}


###############################################################################################
SYSTEMIC_NETWORK_CONSTITUENCY = read_systemic_network_from_file(CONSTITUENT_SYSTEM)
SYSTEMIC_NETWORK_PERSON = read_systemic_network_from_file(PERSON_SYSTEM)
SYSTEMIC_NETWORK_DETERMINERS = read_systemic_network_from_file(DETERMINANT_SYSTEM)
SYSTEMIC_NETWORK_MOOD = read_systemic_network_from_file(MOOD_SYSTEM)
SYSTEMIC_NETWORK_AGREEMENT = read_systemic_network_from_file(AGREEMENT_SYSTEM)
SYSTEMIC_NETWORK_TRANSITIVITY = read_systemic_network_from_file(TRANSITIVITY_SYSTEM)

if __name__ == '__main__':
    pass
