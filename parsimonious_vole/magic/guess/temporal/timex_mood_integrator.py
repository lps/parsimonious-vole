'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 18, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
from parsimonious_vole.magic.guess.fs_constants import THING, QUALIFIER
from parsimonious_vole.magic.guess.mcg_utils import is_feature_selected, remove_feature_shallow, smallest_constituent_with_words
from parsimonious_vole.magic.parse.tarsqui_parser import tarsqui_parse_dg

from parsimonious_vole.configuration import TEMPORAL_SENTENCES
from parsimonious_vole.magic.guess.mood import test_mood_graph


def integrate_time_to_mood(mcg, dg):
    """ """
    timex_objs = tarsqui_parse_dg(dg)
    if not timex_objs: return
    for t in timex_objs:
        print smallest_constituent_with_words(mcg, t.words)

def enforce_temporal_adjuncts(mcg, dg):
    """ 
        if the temporal expression is the Thing of a prepositional group  
        then select Adjunct only Interpersonal function, and remove 
        Complement function
    """
    timex_objs = tarsqui_parse_dg(dg)
    if not timex_objs: return
    for t in timex_objs:
        constit, confidence = smallest_constituent_with_words(mcg, t.words,True)
        if confidence==1: # whole constit is a temp expression
            if is_feature_selected(constit, [THING]): # it is a thing
                if is_feature_selected(constit.parent(), ["adjunct"]) and \
                    is_feature_selected(constit.parent(), ["complement-direct"]):
                    remove_feature_shallow(constit.parent(),"complement-direct") # remove complement function
                elif is_feature_selected(constit.parent(), [QUALIFIER]):
                    # do we do anything it id qualifier?
                    pass 
            else:
                # do we do anything it it is not a Thing but anything else?
                pass

if __name__ == '__main__':
    mcg, dg = test_mood_graph(2, TEMPORAL_SENTENCES, True, True)
    print mcg.nodes()[0]
    #integrate_time_to_mood(mcg, dg)
    enforce_temporal_adjuncts(mcg, dg)
