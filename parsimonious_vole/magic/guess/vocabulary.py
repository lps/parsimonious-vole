'''
Created on Oct 29, 2013

@author: lps

'''
import mmap
import pkg_resources

from parsimonious_vole.configuration import MALE_FIRST_NAME_LIST, FEMALE_FIRST_NAME_LIST, \
    FEMALE_NOUN_LIST, MALE_NOUN_LIST, PERSON_TITLE_NOUN_LIST, ADVERB_INTENSITY_LIST, ADVERB_TEMPORALITY_LIST, \
    ADVERB_MODALITY_LIST


def __is_in_dictionary_file1(word, dictionary_file):
    with open(dictionary_file, "r") as f:
        mm = mmap.mmap(f.fileno(), 0)
        if mm.find("\n" + word.lower() + "\n") > 0:
            mm.close()
            return True
        mm.close()
    return False


def __is_in_dictionary_file(word, dictionary_file):
    with open(dictionary_file, "r") as f:
        s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
        if s.find(word) != -1:
            return True
    return False


###################################################################################################
# Nouns
###################################################################################################
def is_male_name(word):
    return __is_in_dictionary_file(word, MALE_FIRST_NAME_LIST)


def is_female_name(word):
    return __is_in_dictionary_file(word, FEMALE_FIRST_NAME_LIST)


def is_male_noun(word):
    return __is_in_dictionary_file(word, MALE_NOUN_LIST)


def is_female_noun(word):
    return __is_in_dictionary_file(word, FEMALE_NOUN_LIST)


def is_conscious_noun(word):
    return __is_in_dictionary_file(word, PERSON_TITLE_NOUN_LIST)


###################################################################################################
# Adverbs
###################################################################################################

def is_adverb_of_intensity(word):
    return __is_in_dictionary_file(word, ADVERB_INTENSITY_LIST)


def is_adverb_of_temporality(word):
    return __is_in_dictionary_file(word, ADVERB_TEMPORALITY_LIST)


def is_adverb_of_modality(word):
    return __is_in_dictionary_file(word, ADVERB_MODALITY_LIST)


if __name__ == '__main__':
    pass
