'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Mar 14, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

creates a mood structure from the dependency parse graph

'''
import collections
import copy
import itertools
import logging
import os
import uuid
from traceback import print_stack

from parsimonious_vole.magic.guess.fs_constants import C_TYPE, MARKER, DEPENDECY_RELATION, SUBJECT, ADJUNCT, COMPLEMENT_DATIVE, CLAUSE, \
    CARDINAL_NUMERATIVE, QUALIFIER, POSESSOR, PREDEICTIC, COMPLEMENT_ADJUNCT, COMPLEMENT, DISCOURSE, EXPLETIVE_MARKER, \
    DEICTIC, ROOT, DEPENDECY_CONTEXT, COMPLEMENT_AGENT, EPITHET_CLASSIFIER_OR_ORDINAL, APPOSITION, PREDICATOR, \
    PREDICATOR_FINITE, THING, FINITE
from parsimonious_vole.magic.guess.mcg_utils import get_child_constituents, enrich_with_systemic_selections
from parsimonious_vole.magic.guess.pattern_matching import get_dependent_tokens
from parsimonious_vole.magic.guess.rules_modality import enrich_with_modality_features
from parsimonious_vole.magic.guess.rules_tense import get_tense_or_modality_feature_selection
from parsimonious_vole.magic.guess.system_network_mappings import SYSTEMIC_NETWORK_CONSTITUENCY, \
    CONSTITUENT_TYPE_MAPPING, VOCABULARY_DETERMINER, \
    SYSTEMIC_NETWORK_DETERMINERS, VOCABULARY_PERSON, SYSTEMIC_NETWORK_PERSON, \
    SYSTEMIC_NETWORK_MOOD, SYSTEMIC_NETWORK_AGREEMENT, VOCABULARY_ANIMATE, FUNCTIONS_ANIMATE
from parsimonious_vole.magic.guess.vocabulary import is_female_noun, is_male_noun, is_conscious_noun, is_adverb_of_intensity, \
    is_adverb_of_temporality, is_adverb_of_modality
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.parse.dep_graph_utils import find_prep_node, get_punctuation_marks, \
    find_genitive_indicator_node
from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.parse.parse_process import test_sentence_graph, get_parse_result
from parsimonious_vole.magic.parse.stanford_parser import call_stanford_parser_file
from parsimonious_vole.magic.utils import natsort, uniquify, root_node, draw_graph, list_minus, los_in_los
from networkx.algorithms.traversal.breadth_first_search import bfs_edges, \
    bfs_successors
from networkx.classes.digraph import DiGraph
from nltk.featstruct import FeatDict

from parsimonious_vole.configuration import COGNITIVE_EXAMPLES
from parsimonious_vole.magic.guess.rules_mood import get_finite_and_feature_selection, get_voice_feature_selection, \
    get_polarity_feature_selection_by_marker


class Constituent(object):
    """ Mood constituent """
    index = 0
    _name = None
    _feature_structure = None
    _tokens = None
    _parent = None
    _head_dependecy_node = None
    _possible_process_types = None 
    
    __uuid = None
    __semantic_correspondents = []
    
    def __init__(self, name, parent=None, head_dependecy_node=None):
        self._name = name + str(Constituent.index)
        Constituent.index += 1
        self._head_dependecy_node = head_dependecy_node
        self._parent = parent
        self._feature_structure = FeatDict({C_TYPE:name})
        self._tokens = []
        self.__uuid = uuid.uuid4()
    
    def add_nodes(self, nodes):
        for i in nodes:
            self.add_node(i)
        
    def add_node(self, node):
        if node not in self._tokens: 
            self._tokens.append(node)
            if self._parent:
                self._parent.add_node(node)
    
    def get_process_type_list(self):
        return self._possible_process_types    
        
    def remove_node(self, node):
        if node in self._tokens:
            self._tokens.remove(node)
    
    def parent(self):
        return self._parent
    
    def head_dependecy_node(self):
        return self._head_dependecy_node if self._head_dependecy_node else None
    
    def parents(self):
        if not self._parent: return None 
        res = []
        parent = self._parent
        while parent:
            res.append(parent)
            parent = parent.parent()
        return res
    
    def nodes(self):
        return natsort(self._tokens)
    
    def words(self):
        return map(lambda x: x.word, natsort(self._tokens))

    def append_feature_structure(self, fs):
        self._feature_structure.update(fs)
    
    def update_feature_structure(self, fs):
        """
             rewrites the FS
        """
        self._feature_structure.update(fs)
    
    def extend_feature_structure_values1(self, fs):
        """ if feature exists but has a different value, 
        then the feature type becomes a list containing two 
        elements, the old and the new values. If the feature
        type is already a list then the new value is appended 
        to the list. 
        """
        for new_key in fs.keys():
            if new_key in self._feature_structure.keys():  # the key exists
                if isinstance(self._feature_structure[new_key], collections.Iterable) \
                and not isinstance(self._feature_structure[new_key], basestring) :
                    if isinstance(fs[new_key], collections.Iterable)\
                        and not isinstance(fs[new_key], basestring) :
                        self._feature_structure[new_key].extend(fs[new_key])
                    else: self._feature_structure[new_key].append(fs[new_key])
                else:
                    temp = self._feature_structure[new_key]
                    self._feature_structure[new_key] = [temp]
                    if isinstance(fs[new_key], collections.Iterable)\
                        and not isinstance(fs[new_key], basestring) :
                        self._feature_structure[new_key].extend(fs[new_key])
                    else: self._feature_structure[new_key].append(fs[new_key])
            else:  # the key does not exist
                self.append_feature_structure(fs)

    def extend_feature_structure_values(self, fs):
        """ if feature exists but has a different value, 
        then the feature type becomes a list containing two 
        elements, the old and the new values. If the feature
        type is already a list then the new value is appended 
        to the list.
        
        NOTE: potential memory leak BUG solved by using copy.copy(x) 
        """
        for new_key in fs.keys():
            if new_key in self._feature_structure.keys():  # the key exists
                if isinstance(self._feature_structure[new_key], basestring):
                    temp = self._feature_structure[new_key]
                    self._feature_structure[new_key] = [temp]
                if isinstance(fs[new_key], basestring): # it is a simple element
                    self._feature_structure[new_key].append(fs[new_key])
                else: # it is a list
                    self._feature_structure[new_key].extend(copy.copy(fs[new_key]))
            else:  # the key does not exist
                self._feature_structure[new_key] = copy.copy(fs[new_key])
    
    def extend_feature_structure_unique_values(self, fs):
        """ if feature exists but has a different value, 
        then the feature type becomes a list containing two 
        elements, the old and the new values. If the feature
        type is already a list then the new value is appended 
        to the list. 
        """
        for new_key in fs.keys():
            if new_key in self._feature_structure.keys():  # the key exists
                if isinstance(self._feature_structure[new_key], collections.Iterable) \
                and not isinstance(self._feature_structure[new_key], basestring) :
                    if isinstance(fs[new_key], collections.Iterable)\
                        and not isinstance(fs[new_key], basestring) :
                        self._feature_structure[new_key].extend(fs[new_key])
                    else: self._feature_structure[new_key].append(fs[new_key])
                else:
                    temp = self._feature_structure[new_key]
                    self._feature_structure[new_key] = [temp]
                    if isinstance(fs[new_key], collections.Iterable)\
                        and not isinstance(fs[new_key], basestring) :
                        self._feature_structure[new_key].extend(fs[new_key])
                    else: self._feature_structure[new_key].append(fs[new_key])
                self._feature_structure[new_key] = uniquify(self._feature_structure[new_key])
            else:  # the key does not exist
                self.append_feature_structure(fs)
    
    def feature_structure(self):
        return self._feature_structure
    
    def type(self):
        return self._feature_structure[C_TYPE]
    
    def name(self):
        return self._name
    
    def __str__(self):
        return self._name + str(self.nodes())
    
    def __repr__(self):
        return self.__str__()
    
    def __eq__(self, other):
        if other:
            return hash(self) == hash(other)
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __hash__(self): 
        return hash(self.__uuid)    

    def lowest_token_index(self, ignore_reference_constituents=True):
        """  return lowest index token """
        if ignore_reference_constituents: min([x for x in self.nodes() if x is not isinstance(x, ReferenceConstituent)],
                                               key=lambda x: x.id())
        return min (self.nodes(), key=lambda x: x.id())
    def highest_token_index(self, ignore_reference_constituents=True):
        """ return highest index token"""
        if ignore_reference_constituents: max([x for x in self.nodes() if x is not isinstance(x, ReferenceConstituent)],
                                               key=lambda x: x.id())
        return max (self.nodes(), key=lambda x: x.id())
            
    def boundaries(self, ignore_reference_constituents=True):
        """ returns the boundaries of a constituent """
#         if ignore_reference_constituents:
#             return self.lowest_token_index(ignore_reference_constituents), \
#                     max([x for x in self.nodes() if x is not isinstance(x, ReferenceConstituent)])
#         return (self.lowest_token_index(ignore_reference_constituents), max(self.nodes()))
        return self.lowest_token_index(ignore_reference_constituents), self.highest_token_index(ignore_reference_constituents)

    def add_semantic_description(self, description_object):
        """ """
        self.__semantic_correspondents.append(description_object)
        
    def get_semantic_correspondents(self):
        """ """
        return self.__semantic_correspondents
    
class ReferenceConstituent(Constituent):
    """ reference constituent is conceived for empty/covert constituents of the clause, 
        they are empty but contains a link to another overt constituent """
    _referenced_constituent = None
    
    def __init__(self, referenced_constituent, parent):  # , exclude_children=False
        Constituent.__init__(self, "ref_" + referenced_constituent.name(),
                             parent,
                             referenced_constituent.head_dependecy_node())
        self._referenced_constituent = referenced_constituent
        self._feature_structure = referenced_constituent.feature_structure().copy(deep=True)
        
        # self._tokens = __assign_tokens(self._referenced_constituent.nodes(),exclude_children)
    
    def __str__(self):
        return self._name + str(self.nodes())
    
    def nodes(self):
        return self._referenced_constituent.nodes()
    
    def words(self):
        return self._referenced_constituent.words()
    
#### parsing functions ############################

def extend_dependent(**kwargs):
    """ nothing really to do except for adding the prepositions if any """
    child_nodes = kwargs["child_nodes"]
    current_constituent = kwargs["current_constituent"]
    preposition = kwargs["preposition"] if "preposition" in kwargs else None
    if preposition:
        if current_constituent.parents():
            for i in reversed(current_constituent.parents()):
                preposition.add_constituent(i)
                i.add_node(preposition)
        child_nodes.append(preposition)
    
    current_constituent.add_nodes(child_nodes)
    for i in child_nodes:
        i.add_constituent(current_constituent)
        

def start_new_constituent(**kwargs):
    name = kwargs["name"]
    current_dep_node = kwargs["current_dep_node"] 
    current_dep_edge = kwargs["current_dep_edge"]
    current_dep_edge_type = kwargs["current_dep_edge_type"]
    child_nodes = kwargs["child_nodes"]
    current_constituent = kwargs["current_constituent"]
    constituency_graph = kwargs["constituency_graph"]
    preposition = kwargs["preposition"] if "preposition" in kwargs else None
    if preposition:
        if current_constituent.parents():
            for i in reversed(current_constituent.parents()):
                preposition.add_constituent(i)
                i.add_node(preposition)
        child_nodes.append(preposition)

    # add preposition as marker
    con = Constituent(name, current_constituent, current_dep_node)
    if current_constituent:
        constituency_graph.add_edge(current_constituent, con)
    
    con.add_nodes(child_nodes)
    if current_dep_node: 
        con.add_node(current_dep_node)
        current_dep_node.add_constituent(con)
    
    for i in child_nodes:
        i.add_constituent(con)
        
    # adding the feature: the dependecy context that created the constituent
    if current_dep_edge:
        con.append_feature_structure({DEPENDECY_CONTEXT:
                                      (current_dep_edge[0].pos, current_dep_edge_type, current_dep_edge[1].pos),
                                      DEPENDECY_RELATION:current_dep_edge_type})
    elif current_dep_edge_type: 
        con.append_feature_structure({DEPENDECY_RELATION:current_dep_edge_type})
    
    if preposition:  # and current_dep_edge_type not in ["prep","prepc","prt"]
        # adding a Marker constituent
        mrk = Constituent(MARKER, con, preposition)
        constituency_graph.add_edge(con, mrk)
        mrk.add_node(preposition)
    return con

def start_sibling_constituent(**kwargs):
    # print "start sibling"
    newargs = kwargs.copy()
    newargs["current_constituent"] = kwargs["current_constituent"].parent()
    remove_from_current_constituent(**kwargs)
    return start_new_constituent(**newargs)

def __is_compliment_or_adjunct(**kwargs):
    """ 
        make a guess/decide based on number 'prep', and 'verb type(s)' whether
        the component introduced by preposition phrase starting with 'prep'
        is an adjunct or complement 
        #FIXME: need to make more detailed research Haliday 2006 pp278
    """
#    kwargs["current_constituent"].parent()
#    prep_that_can_introcude_participant = ['by', 'to', 'for', 'on', 'in', 'as']
#    preposition = kwargs["preposition"] if "preposition" in kwargs else None
#    if preposition: 
#        if preposition.word in prep_that_can_introcude_participant:
#            return COMPLEMENT
#        else: return ADJUNCT
    return COMPLEMENT_ADJUNCT 
        
def start_subordinated_clause_constituent(**kwargs):
    # print "start new clause constituent"
    c = start_new_constituent(**kwargs)
    newargs = kwargs.copy()
    newargs["current_constituent"] = c
    newargs["name"] = CLAUSE
    
    if "preposition" in newargs:  # if there is a preposition, do not include it in the child clause
        if newargs["preposition"] in newargs["child_nodes"]:
            newargs["child_nodes"].remove(newargs["preposition"])
        newargs["preposition"] = None
    
    return  start_new_constituent(**newargs)

def remove_from_current_constituent(**kwargs):
    """
    @obsolete
    extended by default, now just need to remove it from constituent, and 
    remove the constituent from the node;s stack"""
    current_dep_node = kwargs["current_dep_node"] 
    current_constituent = kwargs["current_constituent"]
    c = current_dep_node.pop_constituent()
    current_constituent.remove_node(current_dep_node)
    # FIXME: maybe also all children of current dep node not only current dep node
    if c != current_constituent: 
        logging.error("the node did not belong to current constituent")
        return False
    return True

rules = { 
          "acomp":(start_new_constituent, COMPLEMENT),
          "advcl":(start_subordinated_clause_constituent, ADJUNCT),
          "advmod":(extend_dependent, None),
          "amod":(start_new_constituent, EPITHET_CLASSIFIER_OR_ORDINAL),
          "agent": (start_new_constituent, COMPLEMENT_AGENT),
          "appos":(start_new_constituent, APPOSITION),
          # "attr":(start_new_constituent, COMPLEMENT),  # obsolete after Stanford Parser V3.3, 
          "aux":(extend_dependent, None),
          "auxpass":(extend_dependent, None),
          # "cc":(extend_dependent, None),  # unused ?
          # "ccomp":(start_subordinated_clause_constituent, COMPLEMENT),  # meed to distinguish between NP-ccomp-VB, and VB-ccomp-VB 
          "complm":(start_new_constituent, MARKER),  # obsolete after Stanford Parser V3.3,
          "conj":(extend_dependent, None),
          # "cop":(extend_dependent, None), # shall never be used as a normal rule, resolve aproiry copular conenctions
          "csubj":(start_subordinated_clause_constituent, SUBJECT),
          "csubjpass":(start_subordinated_clause_constituent, SUBJECT),
          # "dep":(extend_dependent, None), 
          "det":(start_new_constituent, DEICTIC),
          "dobj":(start_new_constituent, COMPLEMENT),
          "expl":(start_new_constituent, EXPLETIVE_MARKER),
          "infmod":(start_subordinated_clause_constituent, QUALIFIER),  # obsolete after Stanford Parser V3.3, see vmod
          "iobj":(start_new_constituent, COMPLEMENT_DATIVE),
          "mark":(start_new_constituent, MARKER),
          "mwe":(extend_dependent, None),
          "neg":(extend_dependent, None),
          "nn":(extend_dependent, None),
          "npadvmod":(start_new_constituent, ADJUNCT),  # TODO: research how it changed in V3.3 fo Stanford Parser, need special rules
          "nsubj":(start_new_constituent, SUBJECT),
          "nsubjpass":(start_new_constituent, SUBJECT),
          "num":(start_new_constituent, CARDINAL_NUMERATIVE),
          "number":(extend_dependent, None),
          "parataxis": (start_new_constituent, CLAUSE),
          "partmod": (start_subordinated_clause_constituent, QUALIFIER),  # obsolete after Stanford Parser V3.3 , see vmod
          "vmod": (start_subordinated_clause_constituent, QUALIFIER),
          "pobj": (extend_dependent, None),  # unused?
          "poss":(start_new_constituent, POSESSOR),
          "possessive":(start_new_constituent, POSESSOR),  # unused?
          "preconj":(extend_dependent, None),
          "predet":(start_new_constituent, PREDEICTIC),
          # "prep":(start_new_constituent, MARKER),    # there is an internal handling of prepositions when constituents are created
          "prepc":(start_subordinated_clause_constituent, COMPLEMENT_ADJUNCT),
          "prt":(start_new_constituent, MARKER),
          "punct":(extend_dependent, None),
          "purpcl":(start_new_constituent, CLAUSE),  # obsolete after Stanford Parser V3.3,
          "quantmod":(extend_dependent, None),
          "rcmod":(start_subordinated_clause_constituent, QUALIFIER),
          "ref":(extend_dependent, None),  # Stanford Parser V3.3, unused?
          "rel":(start_new_constituent, CLAUSE),  # obsolete after Stanford Parser V3.3,
          # "root":(extend_dependent, None),  # unused?
          "tmod":(start_new_constituent, ADJUNCT),
          "xcomp":(start_subordinated_clause_constituent, COMPLEMENT),
          "xsubj":(start_new_constituent, SUBJECT),
          
          # new edge types
          "discourse":(start_new_constituent, DISCOURSE),
          "goeswith": (extend_dependent, None),
        }

# 2 caps letters pos - dependecy - 2 caps letters pos
ambiguous_dep_rules = {
                        "JJ-dep-IN": (start_sibling_constituent, MARKER),
                        "VB-dep-IN": (start_new_constituent, MARKER),
                        "VB-dep-VB": (start_new_constituent, CLAUSE),
                        "NN-dep-NN": (extend_dependent, None),
                        "NN-dep-VB": (start_new_constituent, CLAUSE), # TODO, shall it be create subordinate clause qualifier?
                        "VB-dep-WP": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        "VB-dep-NN": (start_new_constituent, ADJUNCT),  # I do n't understand how easily restricting is coming back to me this time around
                        "RB-dep-IN": (extend_dependent, None),  # Maybe because school 's about to end and there 's little stress around -LRB- except about finals -RRB- , and school-related stress is my main binge-trigger .
                        "WR-dep-JJ": (extend_dependent, None),  # NOT SURE, "It's so odd how full I've been feeling"
                        "VB-dep-JJ": (start_new_constituent, ADJUNCT),
                        
                        
                        "VB-conj-VB": (start_new_constituent, CLAUSE),
#                        "NN-conj-NN": (extend_dependent, None),
#                        "JJ-conj-JJ": (extend_dependent, None),
#                        "RB-conj-RB": (extend_dependent, None),

                        "VB-cc-CC":(start_new_constituent, MARKER),
                        "NN-cc-CC":(extend_dependent, None),
                        
                        "VB-prep-NN": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        "VB-prep-JJ": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        "VB-prep-PR": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        "VB-prep-WP": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        "VB-prep-CD": (start_new_constituent, COMPLEMENT_ADJUNCT),
                        
                        "NN-prep-NN": (start_new_constituent, QUALIFIER),
                        "NN-prep-PR": (start_new_constituent, QUALIFIER),
                        
                        "RB-npadvmod-NN": (extend_dependent, None),  # three weeks ago
                        "NN-npadvmod-NN": (extend_dependent, None),
                        "VB-npadvmod-NN": (start_new_constituent, ADJUNCT),  # untested
                        "JJ-npadvmod-RB": (extend_dependent, None),  # "(I felt) a lot better"
                        
                        "VB-advmod-RB":(start_new_constituent, ADJUNCT),
                        "VB-advmod-JJ":(start_new_constituent, ADJUNCT),
                        "VB-advmod-WR":(start_new_constituent, COMPLEMENT),  #
                        "NN-advmod-RB":(start_new_constituent, PREDEICTIC),
                        
                        "VB-ccomp-NN":(start_new_constituent, COMPLEMENT),
                        "VB-ccomp-VB":(start_subordinated_clause_constituent, COMPLEMENT),
                        
                        "IN-pcomp-IN":(start_sibling_constituent, COMPLEMENT_ADJUNCT),
                        "IN-pcomp-NN":(start_sibling_constituent, COMPLEMENT_ADJUNCT),
                        "IN-pcomp-CD":(start_sibling_constituent, COMPLEMENT_ADJUNCT),
                        "IN-pcomp-JJ":(start_sibling_constituent, COMPLEMENT_ADJUNCT),
                        
                        "NN-amod-CD":(start_new_constituent, CARDINAL_NUMERATIVE),
                        "NN-infmod-VB":(start_subordinated_clause_constituent, QUALIFIER),  # made the branch to bend # errorneous qualifier
                        "CD-prep-NN": (start_new_constituent, QUALIFIER),
                        "CD-dep-#": (extend_dependent, None),
                        # new link 
                        "NN-vmod-VB": (start_subordinated_clause_constituent, QUALIFIER),  # untested, intuitive rules with vmod, then new link in v3.3
                        "NN-prep-JJ": (start_new_constituent, QUALIFIER),  # restaurant next to my flat - flat mislabelled as JJ
                        
                        # crazy workarounds, or not so crazy after all Eg: for some of the boys
                        "DT-prep-NN": (start_new_constituent, QUALIFIER),
                        "JJ-prep-NN": (extend_dependent, None),
                        }

def parse1(dependecy_graph):
    """
    S 
    parse the dependecy graph and create constituency tree without predicates and finites
    """
    root = root_node(dependecy_graph)
    if not root:
        logging.warn("no root node in the graph")
        draw_graph(dependecy_graph)
        
    constit_graph = DiGraph()
    
    # test wtheter it is a clause or a noun phrase or something else
    if "VB" in root.pos:  
        start_subordinated_clause_constituent(name=ROOT,
                              current_dep_node=root,
                              child_nodes=dependecy_graph.nodes(),
                              current_constituent=None,
                              constituency_graph=constit_graph,
                              current_dep_edge=None,
                              current_dep_edge_type=None)
    else:
        start_new_constituent(name=ROOT,
                              current_dep_node=root,
                              child_nodes=dependecy_graph.nodes(),
                              current_constituent=None,
                              constituency_graph=constit_graph,
                              current_dep_edge=None,
                              current_dep_edge_type=None)
    
    if not dependecy_graph.edges():
        pass
        # TODO: treat the case when there are not edges, just one node: "No?", "Uh!", "Wow"
        # create a one word clause
    else:
        for edge in bfs_edges(dependecy_graph, root):
            edge_type = dependecy_graph.edge[edge[0]][edge[1]][TYPE]
        
            # process prep(c) dependecy
            prep = None
            if "prepc" in edge_type:
                prep = find_prep_node(dependecy_graph, edge)
                edge_type = "prepc"
            elif "prep" in edge_type:
                prep = find_prep_node(dependecy_graph, edge)
                edge_type = "prep"
            elif "agent" in edge_type:
                prep = find_prep_node(dependecy_graph, edge)
                edge_type = "agent"
            elif "conj" in edge_type and "preconj" not in edge_type :
                prep = find_prep_node(dependecy_graph, edge)
                edge_type = "conj"
            # end process prep

            ss = bfs_successors(dependecy_graph, edge[1])
            children = list(itertools.chain.from_iterable(ss.values()))
            children.extend(get_punctuation_marks(dependecy_graph, children[:] + [edge[1]]))
            if find_genitive_indicator_node(dependecy_graph, edge):
                children.append(find_genitive_indicator_node(dependecy_graph, edge))
            
            #
            key_in_rule_dict = str("" + edge[0].pos[:2] + "-" + edge_type + "-" + edge[1].pos[:2])
            
            # if key_in_rule_dict in ambiguous_dep_rules:
            try:
                ambiguous_dep_rules[key_in_rule_dict][0](name=ambiguous_dep_rules[key_in_rule_dict][1],
                                current_dep_node=edge[1],
                                child_nodes=children,
                                current_constituent=edge[0].current_constituent(),
                                constituency_graph=constit_graph,
                                preposition=prep,
                                current_dep_edge=edge,
                                current_dep_edge_type=edge_type)
            except KeyError:
                try:
                    rules[edge_type][0](name=rules[edge_type][1],
                                    current_dep_node=edge[1],
                                    child_nodes=children,
                                    current_constituent=edge[0].current_constituent(),
                                    constituency_graph=constit_graph,
                                    preposition=prep,
                                    current_dep_edge=edge,
                                    current_dep_edge_type=edge_type)
                except KeyError:
                    logging.warn("could not find key [" + str(key_in_rule_dict) + "] in ambiguous_dep_rules AND neither [" + str(edge_type) + "] in rules.")
                except Exception:
                    print_stack()
            except Exception:
                    print_stack()

    return constit_graph

def parse21(dependecy_graph, const_graph):
    """ 
        create predicators and finites, and enrich clause nodes with 
        polatiry, voice, finitness and tense features
    """
    predicator_links = ["aux", "neg", "auxpass"]
    for clause in filter(lambda x: x.type() is CLAUSE, const_graph.nodes()):
        all_child_nodes = []
        for c in const_graph.successors(clause): all_child_nodes.extend(c.nodes())
        predicator_nodes = map(lambda x: x[1], get_dependent_tokens(dependecy_graph,
                                                                   clause.head_dependecy_node(), predicator_links, strict_filter=True))
        # free_nodes = list_minus(clause.nodes(), all_child_nodes)
        free_nodes = predicator_nodes
        free_nodes.append(clause.head_dependecy_node())
        
        clause_dg_subgraph = dependecy_graph.subgraph(clause.nodes())
        
        #####
        finiteness_decider, finitness_feature_selection = \
            get_finite_and_feature_selection(clause_dg_subgraph, clause.head_dependecy_node())
        if not finiteness_decider: 
            continue  # skip current clause
        #
            
        is_finite = "finite" in finitness_feature_selection
        is_finte_same_as_predicator = is_finite and finiteness_decider == clause.head_dependecy_node()
        
        dr = clause.feature_structure()[DEPENDECY_RELATION] if \
                    DEPENDECY_RELATION in clause.feature_structure() else None
                    
        if is_finite and not is_finte_same_as_predicator:
            start_new_constituent(name=FINITE,
                        current_dep_node=finiteness_decider,
                        child_nodes=[],
                        current_constituent=clause,
                        constituency_graph=const_graph,
                        preposition=None,
                        current_dep_edge=None,
                        current_dep_edge_type=None)
            
            predicate_nodes = list_minus(free_nodes, finiteness_decider)
            start_new_constituent(name=PREDICATOR,
                                    current_dep_node=clause.head_dependecy_node(),
                                    child_nodes=predicate_nodes,
                                    current_constituent=clause,
                                    constituency_graph=const_graph,
                                    preposition=None,
                                    current_dep_edge=None,
                                    current_dep_edge_type=dr)
        elif is_finite and is_finte_same_as_predicator:
            predicate_nodes = free_nodes
            start_new_constituent(name=PREDICATOR_FINITE,
                                    current_dep_node=clause.head_dependecy_node(),
                                    child_nodes=predicate_nodes,
                                    current_constituent=clause,
                                    constituency_graph=const_graph,
                                    preposition=None,
                                    current_dep_edge=None,
                                    current_dep_edge_type=dr)
        elif not is_finite:
            start_new_constituent(name=PREDICATOR,
                                    current_dep_node=clause.head_dependecy_node(),
                                    child_nodes=free_nodes,
                                    current_constituent=clause,
                                    constituency_graph=const_graph,
                                    preposition=None,
                                    current_dep_edge=None,
                                    current_dep_edge_type=dr)

        # polarity checking bt marker and by deixis of constituents
        pm = get_polarity_feature_selection_by_marker(clause_dg_subgraph, clause.head_dependecy_node())
        pd = get_polarity_feature_selection_by_constituent_deixis(const_graph, clause)
        polarity_feature_selection = ["negative"] if "negative" in pm + pd else ["positive"]
        # 
        mood_feature_selection = []
        if is_finite: 
            mood_feature_selection.extend(get_mood_feature_selection(clause, dependecy_graph, const_graph))
            mood_feature_selection.extend(get_tense_or_modality_feature_selection(clause_dg_subgraph, clause.head_dependecy_node()))
        voice_feature_selection = get_voice_feature_selection(clause_dg_subgraph, clause.head_dependecy_node())
        
        enrich_with_systemic_selections(clause, SYSTEMIC_NETWORK_MOOD, \
                                         finitness_feature_selection + voice_feature_selection + \
                                         mood_feature_selection + polarity_feature_selection)
    return const_graph

def get_polarity_feature_selection_by_constituent_deixis(mcg, clause):
    """ 
        given a clause search for negative polarity in subject and complemntclausecg
        search for "TOTAL-TYPPE:negative" feature in subj/compl/det/predet/thing 
    """
    _f = FeatDict({"TOTAL-TYPE":"negative"})
    def _has_negation_in_fs(fs):
        return _f.subsumes(fs)
    
    for constit in filter(lambda x: x.type() in [SUBJECT, COMPLEMENT, COMPLEMENT_ADJUNCT, COMPLEMENT_DATIVE], mcg.successors(clause)):
        if _has_negation_in_fs(constit.feature_structure()): return ["negative"]
        for sub_constit in filter(lambda x:x.type() in [THING, DEICTIC, PREDEICTIC, POSESSOR], mcg.successors(constit)):
            if _has_negation_in_fs(sub_constit.feature_structure()): return ["negative"]
    return ["positive"]


def get_mood_feature_selection(clause_node, sentence_dependecy_graph, constituency_graph):
    """
    TODO: reimplement 
    """
    feature_selection = []
    wh_pos = ["WP", "WP$", "WRB", "WDT"]
    # search for Wh element
    wh_tokens = filter(lambda x: los_in_los(x.pos, wh_pos, substrings_allowed=False), clause_node.nodes())
    wh_constituent = None
    if wh_tokens:
#         if len(wh_tokens) > 1:
#             print "WARNING: A clause with more than one Wh_ Token? check that out please."
        # clause_constit_with_wh = filter(lambda x: wh_tokens[0] in x.nodes(), get_child_constituents(clause_node,constituency_graph,[SUBJECT,COMPLEMENT,COMPLEMENT_ADJUNCT,COMPLEMENT_AGENT,COMPLEMENT_DATIVE,ADJUNCT]) )
        clause_constit_with_wh = [x for x in get_child_constituents(clause_node, constituency_graph, [SUBJECT, COMPLEMENT, COMPLEMENT_ADJUNCT, COMPLEMENT_AGENT, COMPLEMENT_DATIVE, ADJUNCT]) \
                                  if any(wt in x.nodes() for wt in wh_tokens) and x.parent() == clause_node]
        # wh_constituent = wh_tokens[0].current_constituent() if wh_tokens[0].current_constituent().parent() == clause_node else None
        wh_constituent = clause_constit_with_wh[0] if clause_constit_with_wh else None 
#         if not wh_constituent:
#             # print "WARNING: the WH token belongs to a constituent(" + str(wh_tokens[0].current_constituent()) + " )that is not child of current clause:", str(clause_node)
#             print "WARNING: the Wh Token does not belong to any clause constituent"
    # retrieving finite, predicator and subject
    finite_constituents = filter(lambda x: x.type() in [FINITE, PREDICATOR_FINITE], constituency_graph.successors(clause_node))
    predicator_constituents = filter(lambda x: x.type() in [PREDICATOR_FINITE, PREDICATOR], constituency_graph.successors(clause_node))
    subject_constituents = filter(lambda x: x.type() in [SUBJECT], constituency_graph.successors(clause_node))
    # assertions
    if len (finite_constituents) > 1:
        logging.warn("There is more than one finite(" + str(finite_constituents) + ") in the clause(" + str(clause_node) + ")")
    if len (predicator_constituents) > 1:
        logging.warn("There is more than one predicator(" + str(predicator_constituents) + ") in the clause(" + str(clause_node) + ")")
    if len (subject_constituents) > 1:
        logging.warn("There is more than one subject(" + str(subject_constituents) + ") in the clause(" + str(clause_node) + ")")
    
    finite_constituent = finite_constituents[0] if finite_constituents else None
    predicator_constituent = predicator_constituents[0] if predicator_constituents else None
    subject_constituent = subject_constituents[0] if subject_constituents else None
    
    # assesing mood
    if not predicator_constituent:
        feature_selection.append("minor")
        return feature_selection
    
    if not finite_constituent:
        feature_selection.append("non-finite")
        return feature_selection
    
    if not subject_constituent and finite_constituent:
        feature_selection.append("imperative")
        return feature_selection
    
    if subject_constituent and predicator_constituent and finite_constituent:
        if not wh_constituent:
            if subject_constituent.head_dependecy_node() < finite_constituent.head_dependecy_node():  # S^F
                feature_selection.append("declarative")
                return feature_selection
            elif subject_constituent.head_dependecy_node() > finite_constituent.head_dependecy_node():  # F^S
                feature_selection.append("yes-no")
                return feature_selection
            else:
                logging.warn("strange situation, Subject and Finite cannot have the same index")
                return []
        else:  # wh constituent
                feature_selection.append("wh")
                if wh_constituent.type() is SUBJECT: feature_selection.append("wh-subject")
                elif wh_constituent.type() is COMPLEMENT: feature_selection.append("wh-complement")
                elif wh_constituent.type() is ADJUNCT: feature_selection.append("wh-adjunct")
                elif wh_constituent.type() is COMPLEMENT_ADJUNCT: feature_selection.extend(["wh-complement", "wh-adjunct"])
                else: logging.warn("what kind of interrogative is it?")
                return feature_selection
    logging.warn("Opps Unknown mood ") 
    return []

def parse41_noun_phrase_head_creation(sentence_dependecy_graph, cg):
    """ 
     takes all the words of a subject or complement that are not in any of it's 
     children and creates the NP_head constituent with them 
    """
    soa_constituents = filter(lambda x: x.type() in [SUBJECT, COMPLEMENT,
                                                     COMPLEMENT_AGENT, COMPLEMENT_ADJUNCT,
                                                     COMPLEMENT_DATIVE, ADJUNCT, QUALIFIER], cg.nodes())
    for constit in soa_constituents:
        children = cg.successors(constit)
        clause_children = filter(lambda x: x.type() is CLAUSE, children)
        # if it is a single word then no need to create the NP_head
        # if not children: continue
        
        # if it is a clause and not noun phrase so skip this constituent
        if clause_children and len(children) == 1: continue
        # if the head is not a DET ot NN
        if constit.head_dependecy_node().pos not in ["NN", "NNS", "NNP", "NNPS", "PDT", "DT", "PRP", "PRP$", "CD"]: continue
        # creating the NP head 
        noun_head_nodes = constit.nodes()[:]
        for child in children:
            noun_head_nodes = list_minus(noun_head_nodes, child.nodes())
        # minus punctuation marks
        noun_head_nodes = list_minus(noun_head_nodes, filter(lambda x: x.pos in [".", ":", ","], noun_head_nodes))
        # if anything left, then create the THING constituent
        
        if noun_head_nodes:
            start_new_constituent(name=THING,
                                    current_dep_node=constit.head_dependecy_node(),
                                    child_nodes=noun_head_nodes,
                                    current_constituent=constit,
                                    constituency_graph=cg,
                                    preposition=None,
                                    current_dep_edge=None,
                                    current_dep_edge_type=constit.feature_structure()[DEPENDECY_RELATION])
        
    
    # correct if there is a reciprocal complement "each other" or "one another" 
    # make it one head
    # if there is the reciprocal, remove all child nodes except markers and create one thing with reciprocal tokens
    for constit in filter(lambda x: x.type() in [SUBJECT, COMPLEMENT,
                                                     COMPLEMENT_AGENT, COMPLEMENT_ADJUNCT,
                                                     COMPLEMENT_DATIVE, ADJUNCT, QUALIFIER, POSESSOR], cg.nodes()):
        constit_words = " ".join(constit.words())
        if ("each other" in constit_words or "one another" in constit_words.lower()):
            if constit.head_dependecy_node().word in ["one", "other"]:
                children = filter(lambda x: x.type() != MARKER , cg.successors(constit))
                cg.remove_nodes_from(children)
                # get all the tokens except "." punctuation and the marker tokens
                noun_head_nodes = constit.nodes()[:]
                marker = filter(lambda x: x.type() == MARKER , cg.successors(constit))
                if marker:
                    noun_head_nodes = list_minus(noun_head_nodes, marker[0].nodes())
                # minus punctuation marks
                noun_head_nodes = list_minus(noun_head_nodes, filter(lambda x: x.pos in ["."], noun_head_nodes))
                
                start_new_constituent(name=THING,
                                        current_dep_node=constit.head_dependecy_node(),
                                        child_nodes=noun_head_nodes,
                                        current_constituent=constit,
                                        constituency_graph=cg,
                                        preposition=None,
                                        current_dep_edge=None,
                                        current_dep_edge_type=constit.feature_structure()[DEPENDECY_RELATION])
    # TODO: check adjuncts that are prepositional phrases: noun phrase structure not present for some reason
    return cg
        
def parse5_enrich_from_systemic_network(sentence_dep_gr, constit_gr):
    """ """
    for con in constit_gr.nodes():
        head = con.head_dependecy_node()
        wordd = con.head_dependecy_node().word.lower()
        c_type = con.type()
        # enriching by proto_constituent constituent type
        enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, CONSTITUENT_TYPE_MAPPING[c_type])
        
        # adjust the rank: if the child clause is of the same boundaries as current constit
        _child_clauses = get_child_constituents(con, constit_gr, [CLAUSE])
        if _child_clauses and con.boundaries() == _child_clauses[0].boundaries():
            con.feature_structure().pop("RANK")
            enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["clause"])
        
        # specify the syntactic group type
        if "RANK" in con.feature_structure():  # each constit should have rank by now
            if "group" in con.feature_structure()["RANK"]:  # and if it is undrspecified group
                if con.head_dependecy_node().pos in ["JJ", "JJR", "JJS"]:
                    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["adjectival-group"])
                elif con.head_dependecy_node().pos in ["NN", "NNS", "NNP", "NNPS", "PRP", "PRP$"]:
                    if con.feature_structure()[DEPENDECY_RELATION] == "prep":
                        enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["prepositional-group"])
                    else: enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["nominal-group"])
                elif con.head_dependecy_node().pos in ["RB", "RBR", "RBS"]:
                    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["adverbial-group"])
                # elif con.head_dependecy_node().pos in ["VB","VBD","VBG","VBN","VBP","VBZ"]: # may be incorrect
                #    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["verbal-group"])

        # set grouptype for reflexives
        # change the group type from adjectival to nominal
        # GROUP-TYPE=u'adjectival-group'
        if "each other" in " ".join(con.words()) or "one another" in " ".join(con.words()): 
            if "GROUP-TYPE" in con.feature_structure():
                del con.feature_structure()["GROUP-TYPE"]
                enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["nominal-group"])
            
        # Set the Noun_phrase functional parts
        if c_type in [DEICTIC, PREDEICTIC]:
            # is determiner in vocabulary?
            if wordd in VOCABULARY_DETERMINER:
                enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_DETERMINERS, VOCABULARY_DETERMINER[wordd])
            # else:
            #    print "WARNING: The determiner '%s' is not in the vocabulary" % con.head_dependecy_node()
        
        if c_type in [THING, POSESSOR]:
            # is it any of the pronouns? check in both vocabularies, determiner and person
            if wordd in VOCABULARY_DETERMINER: 
                enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_DETERMINERS, VOCABULARY_DETERMINER[wordd])
            elif wordd in VOCABULARY_PERSON:
                enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_PERSON, VOCABULARY_PERSON[wordd])
            
            # adding agreement features
            
            # animacy
            animate = False
            if wordd not in VOCABULARY_DETERMINER and wordd not in VOCABULARY_PERSON:
                for w in con.nodes():
                    if is_conscious_noun(w.lemma): 
                        animate = True
                        break
                if animate: enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["animate"])
                else: enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["inanimate"])
            else:  # the word is in in VOCABULARY_DETERMINER and wordd not in VOCABULARY_PERSON, add agreement features?
                if los_in_los(wordd, VOCABULARY_ANIMATE, False, True) or\
                        los_in_los(FUNCTIONS_ANIMATE, VOCABULARY_PERSON[wordd] if wordd in VOCABULARY_PERSON else None, False, True) or\
                        los_in_los(FUNCTIONS_ANIMATE, VOCABULARY_DETERMINER[wordd] if wordd in VOCABULARY_DETERMINER else None, False, True):
                        enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["animate"])
                else: enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["inanimate"])
            #
            if (len(con.nodes()) == 1 or \
                    (len(con.nodes()) > 1 and not los_in_los(["and"], con.words(), False, True))) or\
                     "or" in con.words():  # if it is a one word thing or it is disjunction of more words
                
                # adding gender            
                if "NN" in head.pos and animate:
                    if is_female_noun(head.lemma): enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["female"])
                    elif is_male_noun(head.lemma): enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["male"])
                
                # adding plurality
                if head.pos in ["NN", "NNP"]:
                    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["non-plural"])
                elif head.pos in ["NNS", "NNPS"]:
                    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["plural"])
            elif len(con.nodes()) > 1:  # check if the thing is composed of more words, i.e. conjunction, disjunction
                if "and" in con.words():
                    enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_AGREEMENT, ["plural"])
                    if ["i", "me", "myself", "we", "us", "ourself", "ourselves"] in con.words():
                        enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_PERSON, ["speaker-plus"])
                    elif ["you", "yourself", "yourselves"] in con.words():
                        enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_PERSON, ["addressee"])
    
    # enriching mood adjuncts
        if c_type in [ADJUNCT]:
            w = " ".join(con.words())
            if is_adverb_of_intensity(w): enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["intensity"])
            if is_adverb_of_temporality(w): enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["temporality"])
            if is_adverb_of_modality(w): enrich_with_systemic_selections(con, SYSTEMIC_NETWORK_CONSTITUENCY, ["modality"])
    return constit_gr

def parse6_modality_parse(sentence_dep_gr, constit_gr):
    """
        parse modality and mood/comment assesment of the clause
    """
    # enriching with more delicate modality selection
    for clause in filter(lambda x: x.type() is CLAUSE, constit_gr.nodes()):
        enrich_with_modality_features(clause, constit_gr)

def clean_the_sentence_graph(dg):
    """ remove all the references to constituents from Tokens """
    for n in dg.nodes(): n.erase_features_and_constituency_stack()
   
###################################################################################################################
# External Use Functions
###################################################################################################################

#@profile
def parse_mood(sentence_dependecy_graph):
    """ parse mood for a dependecy graph """
    if not sentence_dependecy_graph: return None
    logging.info("*** MOOD-Parsing ***")
    logging.info("*** " + " ".join([x.word for x in natsort(sentence_dependecy_graph.nodes())]))

    #print "Sentence DG0: ",[x/1024 for x in get_recursive_size(sentence_dependecy_graph)]
    
    # cannot parse a graph that is not a tree
    # TODO: need to handle shor answers/statements i.e. chunks of texts that are less than a clause 
    if not root_node(sentence_dependecy_graph): 
        return None
    cg = parse1(sentence_dependecy_graph)
    parse21(sentence_dependecy_graph, cg)
    parse41_noun_phrase_head_creation(sentence_dependecy_graph, cg)
    parse5_enrich_from_systemic_network(sentence_dependecy_graph, cg)
    parse6_modality_parse(sentence_dependecy_graph, cg)
    clean_the_sentence_graph(sentence_dependecy_graph)

    logging.info("*** END MOOD-Parsing ***")
    return cg 

def parse_mood_txt_file(txt_file, return_dep_graph=False):
    """ return a MCG bundle for a .txt file"""
    if not os.path.isfile(txt_file):
        raise IOError("The specified path of .txt file does not exist or it is not a file")
    if not os.path.isfile(txt_file + ".stp"):
        call_stanford_parser_file(txt_file)
    if not os.path.isfile(txt_file + ".stp"):
        raise Exception("The .stp file not found afetr running Stanford parser.")
    return parse_mood_stp_file(txt_file + ".stp", return_dep_graph)
    
def parse_mood_stp_file(stp_file, return_dep_graph=False):
    """ returns an MCG bundle for a .stp file """
    result = []
    parse_result = get_parse_result(stp_file)
    for sentence in parse_result.sentence_graphs:
            sentence = correct_dependecy_parse_errors(sentence)
            result.append(parse_mood(sentence))
    return result if not return_dep_graph else (result, parse_result.sentence_graphs)

def test_mood_graph(sentence_id=19, test_bed=COGNITIVE_EXAMPLES, corrected=True, return_dg=False):
    the_sentence = test_sentence_graph(sentence_id, test_bed , corrected)
    res = parse_mood(the_sentence)
    # if dependecy graph si needed, return it
    if return_dg: return (res, the_sentence)
    return res

if __name__ == '__main__':
    #print str(rules).replace('),',"\n")
    pass
