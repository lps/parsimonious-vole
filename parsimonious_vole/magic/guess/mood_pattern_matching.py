'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jul 7, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

'''
import logging
import traceback

from parsimonious_vole.magic.utils import los_in_los, get_dict_keys_by_value, list_minus
from networkx.algorithms.isomorphism.vf2userfunc import DiGraphMatcher
from networkx.classes.digraph import DiGraph
from nltk.featstruct import FeatDict, subsumes, conflicts

from parsimonious_vole.magic.guess.mood import Constituent, C_TYPE, ReferenceConstituent

PARAMETERS = "parameters"
ACQUIRED = "acquired"
CONSTITUENT = "constituent"

NODES = "nodes"
EDGES = "edges"

INSERT_REFERENCE = "insert_reference"
INSERT_NEW = "insert_new"
#INSERT_DUPLICATE = "insert_duplicate"
# EXCLUDE_CHILDREN = "exclude_children"
WORDS = "words"
LEMMAS = "lemmas"


####### Pattern as pyton data structure ###############################################################

def pattern_to_graph(pattern, include_negations=True, include_functional=True):
    """ transforms the pattern structure into pattern graph
    The pattern is a dictionary with two keys: NODES and EDGES.
    
    * include_operational - specifies whether the edge attributes shall be included or not. the default is False.
    * exclude_negations - specifies whether the negated nodes to be included or not. a node is negated if it ends with "!"
    
    * Pattern Specs:
    NODES = dictionary of node names with values a binary tuple. 
    the first tuple element -  is the dictionary with properties that shall match the graph
    the second tuple element - is the dictionary with parameters that shall be added
    EDGES =  list of triple tuples
    the first tuple element is the source node
    the second tuple element is the destination node
    the third tuple element is the the dictionary of edge attributes: operational attributes
     - allowed operational attributes are: insert_reference, insert_new
    
    Example:
    
    test_pattern = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({"type":"predicator"},None),
                         "subj":({"type":"subject"},None),
                         "compl":({"type":"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2":({"type":"predicator"},None),
                         "compl2":({"type":"complement"},{"special_feature":"matching_works"})},
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None)]
                }
    """
    def reference_by_label(node):
        """ ignores the mistake in pattern specification. 
            only the node labels decide if a node is negated or not
            even if it is referenced as negated in edges, it will be referenced as regular
        """
        if node in pattern[NODES]: return node
        elif str(node) + "!" in pattern[NODES]: return str(node) + "!"
        elif node.replace("!", "") in pattern[NODES]: return node.replace("!", "") 
        return node
    
    # creating the digraph
    dg = DiGraph()
    for i in pattern[EDGES]:
        dg.add_edge(reference_by_label(i[0]), 
                    reference_by_label(i[1]), 
                    i[2] if i[2] else None)
            
    for i in pattern[NODES].keys():
        dg.node[i] = FeatDict({PARAMETERS:FeatDict(pattern[NODES][i][0]), ACQUIRED:FeatDict(pattern[NODES][i][1])})

    # initial set of nodex/edges to be removed if 
    functional_edges = filter(lambda x: x[2], dg.edges(data=True))  
    funct_nodes = map(lambda y:y[1],functional_edges) # auxiliary list
    negated_nodes =  filter(lambda x: x[-1]=="!" ,dg.nodes(data=False))
    negated_and_functional = filter(lambda x: x in funct_nodes, negated_nodes) 
    
#     print "functional            :", funct_nodes 
#     print "negated               :", negated_nodes
#     print "negated and functional:", negated_and_functional
    
    if include_negations:
        if include_functional:
            # all nodes 
            pass
        else:            
            # A - (F-NF)
#             print "F-NF",list_minus(funct_nodes,negated_and_functional)
            dg.remove_nodes_from(list_minus(funct_nodes,negated_and_functional))
    else:
        if include_functional:
            # A - (N-NF)
#             print  "N - NF",list_minus(negated_nodes,negated_and_functional)
            dg.remove_nodes_from(list_minus(negated_nodes,negated_and_functional))
        else:
#             print "-N - F"
            dg.remove_nodes_from(negated_nodes)
            dg.remove_nodes_from(funct_nodes)
    
    # if there are free nodes in the graph, then the pattern is badly specified, 
    # remove the free nodes    
    
    free_nodes = filter(lambda x: not dg.successors(x) and not dg.predecessors(x) , dg.nodes()) 
    if free_nodes: 
        logging.warn("there are free nodes in the pattern graph and are ignored. The nodes are %s"%str(free_nodes))
        dg.remove_nodes_from(free_nodes)
        
    return dg
    
def __expose_constituents(mood_graph):
    """ 
        in order to apply isomorphism test, the constituents needs to be exposed as graph_node attribute 
    """
    if CONSTITUENT not in mood_graph.nodes(True)[0][1]:
        for i in mood_graph.nodes():
            # adding words and lemmas as features in order to enable pattern matching over lexicon
            i.feature_structure()[WORDS] = [x.word for x in i.nodes()]
            i.feature_structure()[LEMMAS] = [x.lemma for x in i.nodes()]
            # exposing
            mood_graph.node[i] = {CONSTITUENT:i}

############# Pattern Matching ##############################################################
# class MCGMatcher(DiGraphMatcher):
#     """ """
#     
#     @property
#     def state(self):
#         return self.state1()
#     
#     def __init__(self, G1, G2, node_match=None, edge_match=None):
#         """ """
#         super(MCGMatcher,self).__init__(G1,G2,node_match,edge_match)
#     
#     
#     def initialize(self):
#         """Reinitializes the state of the algorithm.
# 
#         This method should be redefined if using something other than GMState.
#         If only subclassing GraphMatcher, a redefinition is not necessary.
# 
#         """
# 
#         # core_1[n] contains the index of the node paired with n, which is m,
#         #           provided n is in the mapping.
#         # core_2[m] contains the index of the node paired with m, which is n,
#         #           provided m is in the mapping.
#         self.core_1 = {}
#         self.core_2 = {}
# 
#         # See the paper for definitions of M_x and T_x^{y}
# 
#         # inout_1[n]  is non-zero if n is in M_1 or in T_1^{inout}
#         # inout_2[m]  is non-zero if m is in M_2 or in T_2^{inout}
#         #
#         # The value stored is the depth of the SSR tree when the node became
#         # part of the corresponding set.
#         self.inout_1 = {}
#         self.inout_2 = {}
#         # Practically, these sets simply store the nodes in the subgraph.
# 
#         self.state1 = weakref.ref(DiGMState(self))
# 
#         # Provide a convienient way to access the isomorphism mapping.
#         self.mapping = self.core_1.copy()
#     
#     def clean(self):
#         """
#             reset the recursion limit and 
#             break the strong ref cycle between GM state and GMatcher 
#         """
#         self.reset_recursion_limit()
#         del self.state
# 
#     def match(self):
#         """Extends the isomorphism mapping.
# 
#         This function is called recursively to determine if a complete
#         isomorphism can be found between G1 and G2.  It cleans up the class
#         variables after each recursive call. If an isomorphism is found,
#         we yield the mapping.
# 
#         """
#         if len(self.core_1) == len(self.G2):
#             # Save the final mapping, otherwise garbage collection deletes it.
#             self.mapping = self.core_1.copy()
#             # The mapping is complete.
#             yield self.mapping
#         else:
#             for G1_node, G2_node in self.candidate_pairs_iter():
#                 if self.syntactic_feasibility(G1_node, G2_node):
#                     if self.semantic_feasibility(G1_node, G2_node):
#                         # Recursive call, adding the feasible state.
#                         newstate = weakref.ref(DiGMState(self, G1_node, G2_node)) 
#                         #self.state.__class__(self, G1_node, G2_node)
#                         for mapping in self.match():
#                             yield mapping
# 
#                         # restore data structures
#                         newstate().restore()
# 
# class MCGMatcherState(DiGMState):
#     """ """
#     
#     @property
#     def GM(self): 
#         return self.GM1()
#     @property
#     def G1_node(self):
#         return self.self.G1_node1()
# 
#     @property
#     def G2_node(self):
#         return self.self.G2_node1()
# 
#     def __init__(self, GM, G1_node=None, G2_node=None):
#         """Initializes DiGMState object.
# 
#         Pass in the DiGraphMatcher to which this DiGMState belongs and the
#         new node pair that will be added to the GraphMatcher's current
#         isomorphism mapping.
#         """
#         self.GM1 = weakref.ref(GM)
# 
#         # Initialize the last stored node pair.
#         self.G1_node1 = None
#         self.G2_node1 = None
#         self.depth = len(GM.core_1)
# 
#         if G1_node is None or G2_node is None:
#             # Then we reset the class variables
#             GM.core_1 = weakref.WeakKeyDictionary()
#             GM.core_2 = weakref.WeakKeyDictionary()
#             GM.in_1 = weakref.WeakKeyDictionary()
#             GM.in_2 = weakref.WeakKeyDictionary()
#             GM.out_1 = weakref.WeakKeyDictionary()
#             GM.out_2 = weakref.WeakKeyDictionary()
# 
#         # Watch out! G1_node == 0 should evaluate to True.
#         if G1_node is not None and G2_node is not None:
#             # Add the node pair to the isomorphism mapping.
#             GM.core_1[G1_node] = G2_node
#             GM.core_2[G2_node] = G1_node
# 
#             # Store the node that was added last.
#             self.G1_node1 = weakref.ref(G1_node)
#             self.G2_node1 = weakref.ref(G2_node)
# 
#             # Now we must update the other four vectors.
#             # We will add only if it is not in there already!
#             self.depth = len(GM.core_1)
# 
#             # First we add the new nodes...
#             for vector in (GM.in_1, GM.out_1):
#                 if G1_node not in vector:
#                     vector[G1_node] = self.depth
#             for vector in (GM.in_2, GM.out_2):
#                 if G2_node not in vector:
#                     vector[G2_node] = self.depth
# 
#             # Now we add every other node...
# 
#             # Updates for T_1^{in}
#             new_nodes = weakref.WeakSet()
#             for node in GM.core_1:
#                 new_nodes.update([predecessor for predecessor in GM.G1.predecessors(node) if predecessor not in GM.core_1])
#             for node in new_nodes:
#                 if node not in GM.in_1:
#                     GM.in_1[node] = self.depth
# 
#             # Updates for T_2^{in}
#             new_nodes = weakref.WeakSet()
#             for node in GM.core_2:
#                 new_nodes.update([predecessor for predecessor in GM.G2.predecessors(node) if predecessor not in GM.core_2])
#             for node in new_nodes:
#                 if node not in GM.in_2:
#                     GM.in_2[node] = self.depth
# 
#             # Updates for T_1^{out}
#             new_nodes = weakref.WeakSet()
#             for node in GM.core_1:
#                 new_nodes.update([successor for successor in GM.G1.successors(node) if successor not in GM.core_1])
#             for node in new_nodes:
#                 if node not in GM.out_1:
#                     GM.out_1[node] = self.depth
# 
#             # Updates for T_2^{out}
#             new_nodes = weakref.WeakSet()
#             for node in GM.core_2:
#                 new_nodes.update([successor for successor in GM.G2.successors(node) if successor not in GM.core_2])
#             for node in new_nodes:
#                 if node not in GM.out_2:
#                     GM.out_2[node] = self.depth
#     
#     def restore(self):
#         """Deletes the DiGMState object and restores the class variables."""
# 
#         # First we remove the node that was added from the core vectors.
#         # Watch out! G1_node == 0 should evaluate to True.
#         if self.G1_node is not None and self.G2_node is not None:
#             del self.GM.core_1[self.G1_node]
#             del self.GM.core_2[self.G2_node]
# 
#         # Now we revert the other four vectors.
#         # Thus, we delete all entries which have this depth level.
#         for vector in (self.GM.in_1, self.GM.in_2, self.GM.out_1, self.GM.out_2):
#             for node in list(vector.keys()):
#                 if vector[node] == self.depth:
#                     del vector[node]
    
def _isomorphism_mood_node_compare(node1, node2):
    """
    A function that returns True iff node n1 in G1 and n2 in G2 
    should be considered equal during the isomorphism test. 
    The function will be called like:  node_match(G1.node[n1], G2.node[n2])
    That is, the function will receive the node attribute dictionaries 
    of the nodes under consideration. If None, then no attributes are 
    considered when testing for an isomorphism.
    
    node1 - mood_graph node
    node2 - pattern graph node
     """
    #DEBUG
#     print "-----------" 
#     print node1[CONSTITUENT].feature_structure()
#     print "" 
#     print node2[PARAMETERS] 
#     print subsumes(node2[PARAMETERS],node1[CONSTITUENT].feature_structure()), "*"
    
    #    
    #print "->",node1,"->", node2
    # same type
    if C_TYPE in node2[PARAMETERS]:
        los1 = node1[CONSTITUENT].feature_structure()[C_TYPE]
        los2 = node2[PARAMETERS][C_TYPE]
        if not los_in_los(los1, los2, False, True):
            return False
    
    if not subsumes(node2[PARAMETERS], node1[CONSTITUENT].feature_structure()):
        # check for conflicts and try compare conflicting keys, one level depth
        _conflicts = conflicts(node2[PARAMETERS], node1[CONSTITUENT].feature_structure())
        for conflict in _conflicts:
            los1 = node2[PARAMETERS][conflict[0]]
            los2 = node1[CONSTITUENT].feature_structure()[conflict[0]]
            if not los_in_los(los1, los2, substrings_allowed=False, ignore_caps=True):
                return False
    return True

def _isomorphism_mood_edge_compare(edge1, edge2):
    """
    A function that returns True iff the edge attribute dictionary 
    for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should 
    be considered equal during the isomorphism test. The function 
    will be called like: edge_match(G1[u1][v1], G2[u2][v2])
    That is, the function will receive the edge attribute dictionaries 
    of the edges under consideration. If None, then no attributes are 
    considered when testing for an isomorphism.
    """
    return True

def contains_pattern(mood_graph, pattern=None, return_matching_subraphs=False, return_graph_pattern=False):
    """ 
        checks whether the mood graph is isomorphic with the pattern.
        
        to express negation/absence of a NODE add "!" at the end of it's name.
    """
    ###
    def check_negated_node_by_parent(n,isomorphism_dictionary):
        """ return True if the negated node is in the MCG else False"""
        for pattern_parent in graph_pattern.predecessors(n):
            mcg_parents = get_dict_keys_by_value(isomorphism_dictionary, pattern_parent) # mcg nodes, should be just one
            for mcg_parent in mcg_parents:
                # should exclude the ones that are already in isomorphism
                un_mapped_nodes =  list_minus(mood_graph.successors(mcg_parent), isomorphism_dictionary.keys())
                for hyp_neg_node in un_mapped_nodes: 
                    if _isomorphism_mood_node_compare(mood_graph.node[hyp_neg_node],graph_pattern.node[n]):
                        return mood_graph.node[hyp_neg_node]
        return None
    ###
    
    only_positive_nodes_graph_pattern = None
    graph_pattern = None
    negated_nodes = filter(lambda x: x[-1] == '!' , pattern[NODES].keys())
    
    try:
        graph_pattern = pattern_to_graph(pattern,include_functional=False,include_negations=True)
        if negated_nodes:
            only_positive_nodes_graph_pattern = pattern_to_graph(pattern, include_negations=False,include_functional=False)
        else:
            only_positive_nodes_graph_pattern = graph_pattern
    except Exception, e:
        traceback.print_exc()
        raise Exception("The graph pattern is not correctly specified. \n\
                        " + str(pattern)+"\n"+str(e))
        
    # expose the constituents as node attributes
    __expose_constituents(mood_graph)
    
    # check isomorphism
    dgm = DiGraphMatcher(mood_graph, only_positive_nodes_graph_pattern,
                                 node_match=_isomorphism_mood_node_compare,
                                 edge_match=_isomorphism_mood_edge_compare)
    subgraph_isomorphisms= list(dgm.subgraph_isomorphisms_iter()) #  [{mcg_node:pattern_node, ...},{...}]
    
    # cleaning up
    dgm.reset_recursion_limit()
    
    accepted_isomorphisms = []
    if negated_nodes:
        for sisomorphism in subgraph_isomorphisms:
            negated_node_detected = False
            for nnode in negated_nodes:
                if check_negated_node_by_parent(nnode, sisomorphism): negated_node_detected=True
            if not negated_node_detected: accepted_isomorphisms.append(sisomorphism)
    else: accepted_isomorphisms=subgraph_isomorphisms
    
    
    is_isom=True if accepted_isomorphisms else False
    #return result 
    if return_matching_subraphs: 
        if return_graph_pattern: return is_isom, subgraph_isomorphisms, graph_pattern
        else: return is_isom, subgraph_isomorphisms
    else:
        if return_graph_pattern: return is_isom, graph_pattern
        else: return is_isom
            
def enrich_from_pattern(mood_graph, pattern=None,condition_functions=[], append=True, full_mcg_graph=None):
    """ 
        enriches the mood_graph with features from the pattern.
        condition_functions  are a list of functions that shall return a boolean value 
        testing whether the acquired features are NOT INVALID. 
        the condition function takes 2 parameters: 
            * constituent node and 
            * the dictionary of features to be acquired
            * the mood graph
        Note: if feature exists but has a different value, 
            then the feature type becomes a list containing two 
            elements, the old and the new values. If the feature
            type is already a list then the new value is appended 
            to the list.
        Note: the condition function needs to evaluate first the input parameters and decide
             if they are the target of the function, if not then return True. ONLY when it is 
             sure that the input parameters are the targeted ones then pass through      
    
    """
    is_matching, subgraph_isomorphisms, graph_pattern = contains_pattern(mood_graph, pattern, return_matching_subraphs=True, return_graph_pattern=True)
    if not is_matching: return False
    if not full_mcg_graph: full_mcg_graph = mood_graph 
    
    #executes the condition functions over all the nodes before enriching them with extra features
    condition_proofed_subgraph_isomorphisms = []
    if condition_functions:
        for subgraph in subgraph_isomorphisms:
            failed_conditions=False
            for constituent in subgraph.keys():
                pattern_node = subgraph[constituent]
                enrichment_fs = graph_pattern.node[pattern_node][ACQUIRED]
                # if the constituent fails the condition tests then it will not be added to safe set 
                for condition in condition_functions:
                    if not condition(constituent,enrichment_fs,full_mcg_graph): 
                        failed_conditions = True
            # if the subgraph did not fail the tests then add it to safe set
            if not failed_conditions: 
                condition_proofed_subgraph_isomorphisms.append(subgraph)
    else: 
        condition_proofed_subgraph_isomorphisms = subgraph_isomorphisms # no testing 
    
    if not condition_proofed_subgraph_isomorphisms: return False
    
    #enriches the graph from the pattern
    for subgraph in condition_proofed_subgraph_isomorphisms:
        for constituent in subgraph.keys():
            pattern_node = subgraph[constituent]
            enrichment_fs = graph_pattern.node[pattern_node][ACQUIRED]
#             print "enrichment_fs"
#             print enrichment_fs
            constituent.extend_feature_structure_values(enrichment_fs)
#             print "constituent_fs"
#             print constituent.feature_structure() 
    return True

def insert_from_pattern(mood_graph, pattern):
    """ 
    matched unmarked nodes to the graph, and in case of success inserts the marked nodes 
    operations: insert_reference, insert_new, insert_duplicate
    insert_reference - value the node to be referenced
    insert_new - value a dictionary with feature structures
    """
    match, matching_subgraphs = contains_pattern(mood_graph, pattern, return_matching_subraphs=True, )
    if not match: return False
    
    # remove the edges to be inserted:
    operational_edges = filter(lambda x: x[2], pattern_to_graph(pattern).edges(data=True))
    for edge in operational_edges:
        # findout all graph_nodes that match the edge[0]
        for subgraph in matching_subgraphs:
            insertion_parents = []  
            insertion_parents.extend(filter(lambda x: subgraph[x] == edge[0] , subgraph.keys()))
            if INSERT_REFERENCE in edge[2]:
                insertion_referents = []
                insertion_referents.extend(filter(lambda x: subgraph[x] == edge[2][INSERT_REFERENCE] , subgraph.keys()))
                if not insertion_referents:
                    logging.warn("no match found for pattern node"+str(edge[2][INSERT_REFERENCE])+", how is that possible? ")
                    return False
                if len(insertion_referents) > 1:
                    logging.warn("more than one match found for pattern node"+str(edge[2][INSERT_REFERENCE])+", Inserted references for all of them.")
                # inserting references
                for parent in insertion_parents:
                    for referent in insertion_referents:
                        new_constit = ReferenceConstituent(referent, parent)
                        if PARAMETERS in edge[2] and edge[2][PARAMETERS]: # updating parameters of the constituent
                            new_constit.update_feature_structure(edge[2][PARAMETERS])
                        mood_graph.add_edge(parent, new_constit)
                        mood_graph.node[new_constit] = {CONSTITUENT:new_constit} # __expose_constituent()
            elif INSERT_NEW in edge[2]:
                if not C_TYPE in edge[2][INSERT_NEW]:
                    logging.warn("component type must be specified "+str( edge[2]))
                for parent in insertion_parents:
                    new_constit = Constituent(edge[2][INSERT_NEW][C_TYPE], parent)
                    if PARAMETERS in edge[2] and edge[2][PARAMETERS]: # updating parameters of the constituent
                        new_constit.update_feature_structure(edge[2][PARAMETERS])
                    mood_graph.add_edge(parent, new_constit)  # add and 
                    mood_graph.node[new_constit] = {CONSTITUENT:new_constit} # __expose_constituent()
            else:
                logging.warn("no INSERT operation specified")
                return False
    return True

if __name__ == '__main__':
    pass
