'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Oct 18, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import itertools
import logging

from parsimonious_vole.magic.guess.fs_constants import CLAUSE, SUBJECT, PREDICATOR, PREDICATOR_FINITE, COMPLEMENT, COMPLEMENT_ADJUNCT, \
    COMPLEMENT_AGENT, COMPLEMENT_DATIVE, THING, DEICTIC, MARKER, ROOT, CONFIGURATION
from parsimonious_vole.magic.guess.pattern_matching import get_dependent_tokens
from parsimonious_vole.magic.guess.system_network_mappings import VOCABULARY_PERSON, VOCABULARY_DETERMINER
from parsimonious_vole.magic.parse.VerbForm import PROCESS_TYPE
from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.parse.reader import db, entry_canonic_form_with_variations
from parsimonious_vole.magic.utils import uniquify, los_in_los, root_node, filter_out_keys, flatten, natsort, conditional_traversal, \
    list_minus, longest_sublist_condittioned
from networkx.algorithms.components.weakly_connected import weakly_connected_components
from networkx.classes.function import subgraph
from nltk.featstruct import FeatStruct

from parsimonious_vole.uam_corpus_tool_interface.io.network_scheme import feature_path_to_feature_structure


def get_list_of_clause_graphs(mood_constituency_graph):
    """ 
    for each clause node get the immediate children 
    and form a new graph with them
    """
    res = []
    clause_nodes = filter(lambda x: x.type() == CLAUSE, mood_constituency_graph.nodes())
    for i in clause_nodes:
        nodes = [i]
        nodes.extend(mood_constituency_graph.successors(i))
        sg = subgraph(mood_constituency_graph, nodes)
        # removing components that do not contain the target clause graph
        sg.remove_nodes_from(flatten([x for x in weakly_connected_components(sg) if i not in x])) 
        res.append(sg)
    return res

def clause_graf_for_node(mcg_node, mcg):
    """ 
        given a mcg node returns the parents, siblings and child nodes that are part of the clause
        1. go up until a clause parent is found and ignore root node
        2. from there traverse all the children nodes
        2.1 stop when another clause node is encountered 
    """
    nodes = conditional_traversal(mcg, mcg_node, [lambda x: x.type() not in [CLAUSE, ROOT], ], True)\
            if mcg_node.type() in [CLAUSE, ROOT] else\
            conditional_traversal(mcg, mcg_node, [lambda x: x.type() not in [CLAUSE, ROOT], ], False)  
    result = mcg.subgraph(list(nodes)) 
    return result

def get_subject_from_mcg_clause_graph(mood_constituency_graph):
    r = root_node(mood_constituency_graph)
    subjects = filter(lambda x: x.type() == SUBJECT, mood_constituency_graph.successors(r))
    if subjects: 
        return subjects[0]
    return None

def get_predicate_from_mcg_clause_graph(mood_constituency_graph):
    r = root_node(mood_constituency_graph)
    predicates = filter(lambda x: x.type() in [PREDICATOR, PREDICATOR_FINITE], mood_constituency_graph.successors(r))
    if predicates: 
        return predicates[0]
    return None

def get_complements_from_mcg_clause_graph(mood_constituency_graph):
    r = root_node(mood_constituency_graph)
    return filter(lambda x: x.type() in [COMPLEMENT, COMPLEMENT_ADJUNCT, COMPLEMENT_AGENT, COMPLEMENT_DATIVE], mood_constituency_graph.successors(r))

def get_markers_of_mcg_constituent(constituent, mcg_graph):
    return filter(lambda x: x.type() in [MARKER], mcg_graph.successors(constituent))

def list_of_possible_configurations(dependecy_graph, verb_node):
    """ 
        return list of possible process types for a given the VERB node.
         * take into consideration whether there are any "prt" or "prep" dependencies
         TODO: * take into consideration the participants number
     """
    entries = db.get_all_entries(verb_node.lemma)
    
    # switch from entry_canonic_form to vation including and also to unpaking config roles (So/Des)  
    # lst = uniquify(map(lambda x: entry_canonic_form(x), entries))
    emp = [z for z in [list(y) for y in [list(entry_canonic_form_with_variations(x)) for x in entries]]]
    lst = []
    for i in emp: lst.extend(i)
    lst = uniquify(lst)
    
    prt = get_dependent_tokens(dependecy_graph, verb_node, ["prt"])
    prt = map(lambda x: x[1].lemma, prt) 
    
    prep = get_dependent_tokens(dependecy_graph, verb_node, ["prep"])
    prep = map(lambda x: x[2][TYPE].replace("prepc_", "").replace("prep_", ""), prep)
    
    # the search space for prt is between verb_node and the next verb_node or the end of the sentence.
    token_list = natsort(dependecy_graph.nodes())
    lb = token_list.index(verb_node) + 1 if len(token_list) >= token_list.index(verb_node) + 1 else token_list.index(verb_node)  
    vb_list = [x for x in token_list[lb:] if "VB" in x.pos]
    ub = token_list.index(vb_list[0]) - 1 if vb_list else None 
    token_list = token_list[lb:ub] if ub else token_list[lb:]
    prt_search_space = " ".join([x.lemma for x in token_list])

    reduced_list = None
    if prt or prep: 
        reduced_list = [x for x in lst if x[1] and\
                        ((los_in_los(prt, x[1], False, True) and prt) or \
                        (los_in_los(prep, x[1], False, True) and prep) or \
                        (los_in_los(x[1], [prt_search_space], False, True)and prt_search_space))] 
    else:
        reduced_list = [x for x in lst if x[1] and\
                los_in_los(x[1], prt_search_space)] 

    # if the reduced list is empty then take only forms without extensions
    if not reduced_list: reduced_list = filter(lambda x:  not x[1] , lst)
    
    # filter out the configurations that are incomplete, i.e. contain "?" in their specification 
    # filter(lambda x: x[2] and x[3] and x[3][0] ,possible_configurations)
    reduced_list = filter(lambda x: x[2] and x[3] and x[3][0] and "?" not in x[2], reduced_list)
    
    return uniquify(reduced_list, lambda x: x[2:])

def print_mcg_borders(mcg):
    print mcg
    
    for i in mcg.nodes():
        bdrl = i.boundaries()[0].id()
        bdrh = i.boundaries()[1].id()
        print "[" + str(bdrl) + "," + str(bdrh) + "]" + str(i)

def is_feature_selected(constit, feature_selection):
    """ checks in the feature_structure of the constituent
         whether feature_selection are among the values
     """
    if not constit or not feature_selection: return False
    # check
    values = []
    for i in constit.feature_structure().itervalues():
        if isinstance(i, basestring): values.append(i)
        elif isinstance(i, (list, tuple, set)): values.extend(flatten(i))
        else: logging.warn("WARNING: feature structure value type. Expecting string or iterable found %s " % str(type(i)))
    if los_in_los(feature_selection, values, False , True):
            return True 
    return False

def enrich_with_systemic_selections(constituent, sys_net, fname_or_list_of_feature_names):
    """ enriches the constituent with features selected from systemic network """
    _fnl = []
    if isinstance(fname_or_list_of_feature_names, list):
        _fnl = fname_or_list_of_feature_names
    else: _fnl = [fname_or_list_of_feature_names]
    #
    for fname in _fnl:
        f = feature_path_to_feature_structure(sys_net.select_feature(fname, True))
        if f: constituent.extend_feature_structure_unique_values(f)
        else:
            logging.error("could not find feature '%s' in systemic network" % fname)

def remove_feature_shallow(constit, feature):
    """
        removes the feature from the feature structure without checking the systemic paths and redundancy
    """
    keys_to_delete = [] 
    for k, i in constit.feature_structure().iteritems():
        if isinstance(i, (basestring, unicode)) and feature == i: 
            keys_to_delete.append(k)
        elif isinstance(i, (list, tuple, set)) and feature in i:
            if isinstance(i, list): constit.feature_structure()[k] = [ x for x in i if x != feature]
            elif isinstance(i, list): constit.feature_structure()[k] = {x for x in i if x != feature}
            elif isinstance(i, list): constit.feature_structure()[k] = tuple(y for y in i if y != feature)
            # additional check
            if len(constit.feature_structure()[k]) == 1:
                # type change from iterable to simple value
                constit.feature_structure()[k] = constit.feature_structure()[k][0]
            elif len(constit.feature_structure()[k]) == 0:
                # if empty remove it
                keys_to_delete.append(k)
        else: 
            # print "DEBUG: feature structure value type. Expecting string or iterable found %s "%str(type(i))
            pass
    if keys_to_delete:
        for k in keys_to_delete: del constit.feature_structure()[k] 

def remove_feature_deep(constituent, sys_net, fname_or_list_of_feature_names):
    """
        removes the feature from the feature structure and checks other features and the paths in the systemic network
    """


def is_contained(p, q, mcg_graph):
    """ returns true if p is a descendant/successor of q """    
    parent = p.parent()
    while parent:
        if parent == q: return True
        parent = parent.parent()
    return False

def get_lowest_common_parent(n1, n2, mcg_graph, include_immediate_children=False):
    """ 
        returns the lowest common parent of two nodes 
    """
    if not (mcg_graph.has_node(n1) and mcg_graph.has_node(n2)): 
        return (None, None, None) if include_immediate_children else None
    
    n1_parents = [n1]
    n2_parents = [n2]
    
    # getting n1 parents
    parent = n1.parent()
    while parent:
        n1_parents.append(parent)
        parent = parent.parent()
        
    # getting n2 parents
    parent = n2.parent()
    while parent:
        n2_parents.append(parent)
        parent = parent.parent()
    
    # searching
    for i in n1_parents:
        for j in n2_parents:
            if i == j: 
                if not include_immediate_children: return i
                else:
                    n1_cp = n1_parents.index(i) - 1 if n1_parents.index(i) > 0 else n1_parents.index(i)
                    n2_cp = n2_parents.index(i) - 1 if n2_parents.index(i) > 0 else n2_parents.index(i) 
                    return i, n1_parents[n1_cp], n2_parents[n2_cp]
    
    return (None, None, None) if include_immediate_children else None

def get_child_constituents(node, mcg, child_type):
    """ 
        for a given node in MCG graph returns the child nodes of a particular type/type_list 
    """
    assert mcg
    if node is None: node = root_node(mcg)
    _ct = []
    if child_type:
        if isinstance(child_type, (list, tuple, set)): _ct = list(child_type)
    return filter(lambda x: x.type() in _ct , mcg.successors(node)) if child_type else list(mcg.successors(node)) 

def clause_less_boundaries(constit, mcg, return_tokens=False):
    """ returns the boundaries of a constituent without the direct-child-clause 
    if the constituent is a clause, else (if pr or root then take it as whole)"""
    direct_clause_tokens = natsort(list(flatten([x.nodes() for x in get_child_constituents(constit, mcg, [CLAUSE])])))
    clause_less_tokens = list_minus(natsort(constit.nodes()), direct_clause_tokens) if constit.type()==CLAUSE else natsort(constit.nodes()) 
    continuous_tokens = longest_sublist_condittioned(clause_less_tokens, lambda x, y:x.id() == y.id() - 1)
    if return_tokens: min(continuous_tokens, key=lambda x:x.id()), max(continuous_tokens, key=lambda x: x.id()), continuous_tokens
    return min(continuous_tokens, key=lambda x:x.id()), max(continuous_tokens, key=lambda x: x.id()) 
    
##############################################################
# type checkers
##############################################################
REFLEXIVE_PRONOUNS = ["myself", "yourself", "himself", "herself", "itself", "ourselves", "yourselves", "themselves"]
RECIPROCAL_PRONOUNS = ["one another", "each other"]   

def is_reflexive_pronoun(mcg_node):
    """ return true if the node is reflexive pronoun """
    if los_in_los(REFLEXIVE_PRONOUNS, mcg_node.words()): return True
    return False

def is_reciprocal_pronoun(mcg_node):
    """ return true if the node contains a reciprocal pronoun """
    if los_in_los(RECIPROCAL_PRONOUNS, " ".join(mcg_node.words())): 
        return True
    return False    

def is_3rd_person_pronoun(node):
    """ return true if the node is third person """
    return is_feature_selected(node, "non-interactant")
                
def is_pronoun(mcg_node):
    """ return strue if the MCG node is a pronoun, 
        be it a clause level constit, or a noun phrase lvl constit """
    if mcg_node.head_dependecy_node().pos in ["PRP", "PRP$"]: return True
    if mcg_node.head_dependecy_node().lemma in (VOCABULARY_PERSON.keys() + VOCABULARY_DETERMINER.keys())  : 
        return True  # might be a bit redundant check
    if is_3rd_person_pronoun(mcg_node): return True 
    if is_reciprocal_pronoun(mcg_node): return True
    return False

def is_clause(mcg_node, mcg_graph):
    """ retun true if mcg node is a clause or it has a child
         whose boundaries are exactly the same and is a clause 
     """
    if is_feature_selected(mcg_node, "clause"): return True
    if get_child_constituents(mcg_node, mcg_graph, [CLAUSE]):
        if get_child_constituents(mcg_node, mcg_graph, [CLAUSE])[0].boundaries() == mcg_node.boundaries():
            return True
    return False

def is_noun_phrase(mcg_node, mcg_graph):
    """ return if the constituent is nominal-phrase """
    if is_feature_selected(mcg_node, "nominal-group") and not is_clause(mcg_node, mcg_graph): return True
    return False

def is_prepositional_phrase(mcg_node, mcg_graph):
    """ return if the constituent is nominal-phrase """
    if is_feature_selected(mcg_node, "prepositional-group") and not is_clause(mcg_node, mcg_graph): return True
    return False

def is_prepositional_clause(mcg_node, mcg_graph):
    """ return if the constituent is nominal-phrase """
    if is_feature_selected(mcg_node, "prepositional-group") and is_clause(mcg_node, mcg_graph): return True
    return False

##############################################################
# RAP Salience-relates tests
##############################################################
def is_nomial_constituent(mcg_node):
    """ return True if the current constituent is a constituent part of a nominal group """
    if "NOMINAL-GROUP-FUNCTION" in mcg_node.feature_structure(): return True
    return False

def is_clausal_constituent(mcg_node):
    """ returns true if the current constituent is a clause constituent, 
        i.e. has an interpersonal function """
    if "INTERPERSONAL-FUNCTION" in mcg_node.feature_structure(): return True
    return False

def get_first_clausal_parent(mcg_node):
    """ finds first parent that is a clausal constituent"""
    if not is_nomial_constituent(mcg_node): return None
    node = mcg_node
    while node and not is_clausal_constituent(node):
        node = node.parent()
    return node

def get_deictic(mcg_node, mcg_graph):
    """ returns the deictic of a noun phrase if any"""
    assert is_noun_phrase(mcg_node, mcg_graph) or is_prepositional_phrase(mcg_node, mcg_graph), \
                 "The MCG Node needs to be noun or prepositional phrase and not " + str(mcg_node) 
    deictics = get_child_constituents(mcg_node, mcg_graph, [DEICTIC, ])
    if deictics:
        return deictics[0]
    return None

def get_thing(mcg_node, mcg_graph):
    """ returns the thing of a noun phrase if any"""
#     assert is_noun_phrase(mcg_node, mcg_graph) or is_prepositional_phrase(mcg_node, mcg_graph), \
#                  "The MCG Node needs to be noun or prepositional phrase and not " + str(mcg_node) 
    things = get_child_constituents(mcg_node, mcg_graph, [THING, ])
    if things:
        return things[0]
    return None


def is_subject(mcg_node, mcg_graph):
    """ returns true if the node is subject """
    fcp = get_first_clausal_parent(mcg_node)
    if fcp: return SUBJECT == fcp.type()
    return False

def is_accusative(mcg_node, mcg_graph):
    """ return true if the node is direct object """
    fcp = get_first_clausal_parent(mcg_node)
    if fcp: return COMPLEMENT == fcp.type()
    return False

def is_indirect_or_oblique(mcg_node, mcg_graph):
    """ return true if node is complement_dative or prep_phrase"""
    fcp = get_first_clausal_parent(mcg_node)
    if fcp: return fcp.type() in [COMPLEMENT_DATIVE, COMPLEMENT_ADJUNCT, COMPLEMENT_AGENT]
    return False

def is_head_NP(mcg_node, mcg_graph):
    """ return true if the NP is not the qualifier of another np """
    return mcg_node.type() == THING and is_clausal_constituent(mcg_node.parent())

def is_non_adverbial_NP(mcg_node, mcg_graph):
    """ return true if the node is a prepositional phrase a acting as an 
    adverb, i.e. adjunct/circumstance.
    
    At the moment there are no means of determining it precisely, but tell whether
    it is a prepositional phrase or not. 
    """
    fcp = get_first_clausal_parent(mcg_node)
    if fcp: return COMPLEMENT_ADJUNCT != fcp.type()
    return False

def get_agreement_features(constit, verbose=False):
    """ returns tuple (person, number, sex, animacy).
        if the constituent does not have these features they will be taken from the Thing constituent"""
    thing = constit
    sex = None
    number = None
    person = None
    animacy = None
    # person
    if is_feature_selected(thing, ["speaker", "speaker-plus", ]):
        person = "1"
    elif is_feature_selected(thing, ["addressee"]):
        person = "2"
    elif is_feature_selected(thing, ["non-interactant"]):
        person = "3"
    elif is_feature_selected(thing, ["generalized"]):
        person = "0"
    else:
        if verbose: logging.warn("WARNING: could not detect person of " + str(constit))
    
    # number
    if is_feature_selected(thing, ["non-plural", "one-referent", "speaker", "addressee", "generalized"]):
        number = "singular"
    elif is_feature_selected(thing, ["plural", "speaker-plus", "plural-referent", ]):
        number = "plural"
    else:
        if verbose: logging.warn("WARNING: could not detect number(plurality) of " + str(constit))
    # gender
    if is_feature_selected(thing, ["male", ]):
        sex = "male"
    elif is_feature_selected(thing, ["female", ]):
        sex = "female"
    elif is_feature_selected(thing, ["unknown", "non-conscious"]):
        sex = None
    else: 
        if verbose: logging.warn("WARNING: could not detect sex(gender) of " + str(constit))
    
    # animacy
    if is_feature_selected(thing, ["animate", "male", "female", "conscious", "speaker", "addressee"]):
        animacy = "animate"
    elif is_feature_selected(thing, ["inanimate", "non-conscious"]):
        animacy = "ianimate"
    
    return person, number, sex, animacy

def smallest_constituent_with_words(mcg, id_word_token_list, include_confidence=False):
    """ 
        for a given token list of (id,word) find the smallest mcg constituent that 
        contains these words
        returns a mcg_constit, None or (mcg_constit,confidence)
    """
    const_list = []
    ti = map(lambda x: x[0], id_word_token_list)
    tw = map(lambda x: x[1], id_word_token_list)
    for const in mcg.nodes():
        cw = map(lambda x: x.word, const.nodes())
        ci = map(lambda x: x.id(), const.nodes())
        if los_in_los(tw, cw):
            if ti[0] in ci:
                const_list.append(const)
    if const_list:
        const_list = sorted(const_list, key=lambda x: len(x.nodes()))
        if include_confidence: 
            ci = map(lambda x: x.id(), const_list[0].nodes())
            if sorted(ci)[0] == sorted(ti)[0] and len(ci) == len(ti):
                conf = 1
            else: conf = 0.5
            return (const_list[0], conf)
        return const_list[0]
    return None

# def get_configurations_from_clause_graph0(clause_graph):
#     """ 
#         returns a list of instantiated configuration ((conig_type,clause_constit),[(role1,const1),(role2,const2)]) 
#         like in the example below,
#         [ (("two role action",constit0),[("ag",constit1),("af",constit2)]) ]
#     """
#     clause_root = root_node(clause_graph) 
#     
#     ents = get_complements_from_mcg_clause_graph(clause_graph)
#     if get_subject_from_mcg_clause_graph(clause_graph): ents.append(get_subject_from_mcg_clause_graph(clause_graph))
#     
#     if PROCESS_TYPE not in clause_root.feature_structure():
#         print "Clause has no process types associated: ", clause_root
#         return None
#     
#     result = []
#     for sem_id in range(len(clause_root.feature_structure()[PROCESS_TYPE])):
#         result_participant_list = []
#         for c in ents: result_participant_list.append((c.feature_structure()[PARTICIPANT_ROLE][sem_id], c))
#         result.append(((clause_root.feature_structure()[PROCESS_TYPE][sem_id], clause_root), result_participant_list))
#     return result

def get_configurations_from_clause_graph(clause_graph):
    """ 
        returns a list of instantiated configuration ((conig_type,clause_constit),[(role1,const1),(role2,const2)]) 
        like in the example below,
        [ (("two role action",constit0),[("ag",constit1),("af",constit2)]) ]
        IMPORTANT: the clause graph should not be a full MCG graph but a clause graph
    """
    clause_root = root_node(clause_graph)
    result = []
    if PROCESS_TYPE not in clause_root.feature_structure():
        logging.warn("Clause has no process types associated:" + str(clause_root))
        return result
    if CONFIGURATION not in clause_root.feature_structure():
        logging.warn("Clause has no configuration description (make sure the transitivity_patterns.py are up to date):" + str(clause_root))
        return result
    
    for config in clause_root.feature_structure()[CONFIGURATION]: 
        result_participant_dist = {}
        for prole in config[1]: 
            c = [x for x in clause_graph.nodes() if is_feature_selected(x, prole) and x != clause_root]
            result_participant_dist[prole] = c
        participant_instances = list(itertools.product(* filter(None, [[(x, y) for y in result_participant_dist[x]] for x in result_participant_dist])))
        result.extend([((config[0], clause_root), instance) for instance in participant_instances])
        #
    # pprint.pprint(result)
    return result

def feature_selection_generator(constituent, slot):
    """
         yelds features from the slot of a constituent if they are 
         * strings or 
         * lists of strings or 
         * strings separated by a slash
         
         parameter can be of type: Constituent, dict, FeatStruct or derivates of them 
    """
#     if not isinstance(constituent, (dict, FeatStruct,Constituent)): 
#         raise TypeError("Expecting a Constituent, FeatureStructure or dict, instead received %s"%type(constituent))
    fs = constituent.feature_structure() if not isinstance(constituent, (dict, FeatStruct)) else constituent
    if slot in fs:
        if isinstance(fs[slot], list):
            # proceed to generating feature selections
            for f in fs[slot]:
                if not isinstance(f, basestring): continue  # ignore any non string values
                for ff in f.split("/"): yield ff  # this is for cases of "Des/So" a disjunction of feature selections 
        elif isinstance(fs[slot], basestring):
            for ff in fs[slot].split("/"): yield ff  # same as above
        else: pass  # ignore any non string/list 2nd level values
    # else: logging.warn("could not find %s feature slot in the constituent %s" % (slot, constituent.name())) 

def constituents_with_feature_selection(mcg_graph, feature_selection):
    assert feature_selection, "Expecting a feature selection"
    selected = []
    if not mcg_graph: return selected
    feat_sel = list(feature_selection) if isinstance(feature_selection, (tuple, list, set)) else [feature_selection]
    ignored_slots = ["words", "lemmas", "possible_process_types", "dependecy_relation"]
    for c in mcg_graph.nodes():
        c_f_sel = flatten(filter_out_keys(c.feature_structure(), ignored_slots).values()) 
        if los_in_los(feat_sel, c_f_sel, False, True): selected.append(c)
    return selected

if __name__ == '__main__':
    pass
