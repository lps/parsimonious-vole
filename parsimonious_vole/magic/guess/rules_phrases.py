'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 11, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

def parse_noun_phrase(sentence, node, include_fs=False):
    """
    returns the text of complete noun phrase and the
    feature structure of that noun phrase
    return: text, fs
    """
    # check if node is /Noun, or /Pron and follow the chain of all outgoing links
    return str(node.word)

def parse_verb_phrase(sentence, node, include_fs=False):
    """
    returns the text of complete verb phrase and the
    feature structure of that verb phrase
    return: text, fs
    """
    return str(node.word)

if __name__ == '__main__':
    pass