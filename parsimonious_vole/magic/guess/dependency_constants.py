'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 7, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

CONJUNCTION_RELATION = ["conj"]
PARATAXIS_RELATION = ["parataxis"]

PARATACTIC_RELATIONS = CONJUNCTION_RELATION + PARATAXIS_RELATION
HYPOTACTIC_RELATIONS = ["advcl","ccomp", "cconj", "csubj", "csubjpass", 
                         "prepc", "purpcl", "rcmod", "infmod"] # "xcomp", "pcomp"


#FIRST_ROLE_BINDING_RELATIONS = ["nsubj", "nsubjpass"] # "agent"
#SECOND_ROLE_BINDING_RELATIONS = ["dobj", "acomp", "iobj", "dep", "agent"] # "pobj" , "nsubjpass", 
#THIRD_ROLE_BINDING_RELATIONS = ["prep"]

#FIRST_ROLE_BINDING_RELATIONS = ["nsubj", "agent"] 
#SECOND_ROLE_BINDING_RELATIONS = ["dobj", "acomp", "iobj", "dep", "nsubjpass"] # "pobj" , "nsubjpass", 
#THIRD_ROLE_BINDING_RELATIONS = ["prep"]

##########################
# Penn POS Tags (to be used in dg pattern matching algorithms)
VB = "/VB"
VBD = "/VBD"
VBG = "/VBG"
VBN = "/VBN"
VBP = "/VBP"
VBZ = "/VBZ"

MD = "/MD"
IN = "/IN"

RB = "/RB"
JJ = "/JJ"
NN = "/NN"

""" 
returns feature structure from the mood system: tense, polarity, voice

VB      verb, base form                     take
VBD     verb, past tense                     took
VBG     verb, gerund/present participle     taking
VBN     verb, past participle                 taken
VBP     verb, sing. present, non-3d             take
VBZ     verb, 3rd person sing. present         takes
"""
# tense POS - used in dg detection and extraction algorithms (the POs do not have "/" in front ) 
PRESENT_POS = ["VB", "VBZ", "VBP"]
PRESENT_PARTIC_POS = ["VBG"]

PAST_POS = ["VBD"]
PAST_PARTIC_POS = ["VBN"]

VERBS_POS = PAST_PARTIC_POS + PAST_POS + PRESENT_PARTIC_POS + PRESENT_POS


# DG edge types
dep = "dep"
neg = 'neg'
cop = "cop"
prt = "prt"

prepc = "prepc"
prep = "prep"

prep_as = "prep_as"
prep_in = "prep_in"
prep_at = "prep_at"
prep_on = "prep_on"
prep_to = "prep_to"
prep_from = "prep_from"
prep_via = "prep_via"
prep_through = "prep_through"
prep_past = "prep_past"
prep_away_from = "prep_away_from"
prep_out_of = "prep_out_of"
prep_with = "prep_with"
prep_into = "prep_into"


pcomp = "pcomp"
pobj = "pobj"

auxpass = "auxpass"
nsubjpass = "nsubjpass"
csubjpass = "csubjpass"
csubj = "csubj"

agent = "agent"

aux = "aux"
expl = "expl"

nsubj = "nsubj"
dobj = "dobj"
iobj = "iobj"

poss = "poss"
acomp = "acomp"
xcomp = "xcomp"
npadvmod = "npadvmod"
advmod = "advmod"
tmod = "tmod"

# passive voice detection 
DG_LINK_VOICE_PASSIVE = ["auxpass", "nsubjpass", "csubjpass"]

# polarity detection
DG_LINK_NEGATIVE_POLARITY = ["neg"]

# auxiliary verb dependecies
AUXILIARY_VERB_DEPENDECIES = ["aux", "auxpass"]

# auxiliaries
DO = "do"
BE = "be"
HAVE = "have"

# modals
WILL = "will"
CAN = "can"
MAY = "may"
NEED = "need"
WANT = "want"
WISH = "wish"


if __name__ == '__main__':
    pass