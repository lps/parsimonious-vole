'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 29, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

word sense disambiguation with WordNet 

----------------------------------------------------------------------------
Sugesstion from: http://stackoverflow.com/questions/15942406/word-sense-disambiguation-in-sentiwordnet-python
----------------------------------------------------------------------------

This is a word sense disambiguation problem, and getting your system to work reasonably 
well on any given multisense word will be very tough. You can try (a combination of) 
several methods to determine the right sense of a word:

* Pos tagging will reduce the number of candidate senses.

* Cosine similarity between the sentence and the gloss of each sense of the word in WordNet.

* Use SenseRelate: It measures the "WordNet similarity" between different senses of the 
target word and its surrounding words.

* Use WordNet Domains: the database contains domain labels assigned to each WordNet sense, 
such as "Music" for the music sense of "rock". Instead of comparing actual words that are 
found in the gloss and the sentence, you can compare the domain labels that are found in them.

* Represent the gloss and the sentence not by the words themselves that are found in them, 
but as an average co-occurrence vector of the words. Such vectors could be built using a 
large text corpus, preferably from the same application domain as the texts you are disambiguating. 
There are various techniques to refine such co-occurrence vectors (tf-idf, PCA, SVD), 
and you should read up on them separately.

* If your texts come from a very specialist domain (e.g., law), the accuracy will be higher. 
But, if you work with general language texts, then you can expect good accuracy only on words 
that are not highly polysemous (if they have no more than 3-4 senses in WordNet)

----------------------------------------------------------------------------
----------------------------------------------------------------------------


'''

import pywsd

# def getSenseSimilarity(worda, wordb):
#     """
#     find similarity betwwn word senses of two words
#     """
#     wordasynsets = wn.synsets(worda)
#     wordbsynsets = wn.synsets(wordb)
#     synsetnamea = [wn.synset(str(syns.name)) for syns in wordasynsets]
#     synsetnameb = [wn.synset(str(syns.name)) for syns in wordbsynsets]
# 
#     result = []
#     brown_ic = wordnet_ic.ic('ic-brown.dat')
#     for sseta, ssetb in [(sseta, ssetb) for sseta in synsetnamea\
#                                         for ssetb in synsetnameb]:
#         pathsim = sseta.path_similarity(ssetb) # [0,1], 
#         wupsim = sseta.wup_similarity(ssetb) #? [0,]
#         #ressim = sseta.res_similarity(ssetb,brown_ic) #? [0,]  # require same POS
#         #jcnsim = sseta.jcn_similarity(ssetb,brown_ic) #?[0,1]  # 1 / (IC(s1) + IC(s2) - 2 * IC(lcs)). # require same POS
#         #lchsim = sseta.lch_similarity(ssetb,) #? [0,] # log(p/2d) # require same pos
#         #linsim = sseta.lin_similarity(ssetb,brown_ic) # [0,1] # 2 * IC(lcs) / (IC(s1) + IC(s2)). # require same pos
#         
#         # create the CSV
#         if pathsim != None:
#             print "Path Sim Score: ", pathsim, " WUP Sim Score: ", wupsim, \
#             "\t", sseta.definition, "\t", ssetb.definition
#         # (sseta.definition,ssetb.definition,pathsim,wupsim,ressim,jcnsim,lchsim,linsim)
#         result.append((pathsim,wupsim, sseta.definition,ssetb.definition))
#          
#     with open(TEMPORARY_PATH+"/wsd.csv","wb+") as cfile:
#         spamwriter = csv.writer(cfile)
#         spamwriter.writerows(result)
#         cfile.close()


if __name__ == '__main__':
#     getSenseSimilarity("drive", "street")
    s = "The warriors probably sold all their weapons after the long lasting war."
    word = 'sold'
    print pywsd.lesk.cosine_lesk(s, 'sell')
    pass
