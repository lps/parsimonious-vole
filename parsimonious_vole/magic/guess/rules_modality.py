'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Apr 11, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

enriches the clause node with features from Anke's modality system

'''
import logging

from parsimonious_vole.magic.guess.fs_constants import FINITE, POLARITY, POSITIVE, NEGATIVE, ADJUNCT
from parsimonious_vole.magic.guess.mcg_utils import get_child_constituents, enrich_with_systemic_selections

from parsimonious_vole.magic.guess.system_network_mappings import VOCABULARY_MODALS_POSITIVE, SYSTEMIC_NETWORK_MOOD,\
    VOCABULARY_MODALS_NEGATIVE, MOOD_ADJUNCTS, MOOD_CONJUNCTIVE_ADJUNCTS


def enrich_with_modality_features(clause_node, constituency_graph):
    """ enriches the clause node with features from Anke's modality system
    1. if a modal verb is finite then select modality possibilities from the CVS
    """
    #1. get finites that is a modal verb and polarity and the match to vocabulary 
    finite = get_child_constituents(clause_node, constituency_graph, [FINITE])[0] \
                if get_child_constituents(clause_node, constituency_graph, [FINITE]) else None
    if not finite:
        return
    
    polarity = clause_node.feature_structure()[POLARITY] if POLARITY in clause_node.feature_structure() else None
    if not polarity:
        logging.error("No polarity detected, Modality features will not be parsed")
        return 
    if finite:
        finite_words = " ".join(finite.words()).lower()
        #print "Finite Words for Modality detection:"+finite_words
        if polarity == POSITIVE:
            if finite_words in VOCABULARY_MODALS_POSITIVE:
                enrich_with_systemic_selections(clause_node, SYSTEMIC_NETWORK_MOOD, VOCABULARY_MODALS_POSITIVE[finite_words])
        elif polarity == NEGATIVE:
            if finite_words in VOCABULARY_MODALS_NEGATIVE:
                enrich_with_systemic_selections(clause_node, SYSTEMIC_NETWORK_MOOD, VOCABULARY_MODALS_NEGATIVE[finite_words])
        else:
            logging.error("unknown polarity: "+str(polarity)+".")
            
def enrich_with_modal_assesment_features(clause_node, sentence_dependecy_graph, constituency_graph):
    """
    2. if adverb, adjunct from modality lexicon! and in Mood element (or end of clause) 
        then mark it as mood assesment modal/temporal/intensity
        TODO: create the lexicon in the vocabulary file
    3. if adjunct in comment-epistemic lexicon then mark it as epistemic-evaluative
        TODO: create the lexicon in the vocabulary file 
    """
    #2. find all adjuncts that are adverbs 
    adjuncts = get_child_constituents(clause_node, constituency_graph, [ADJUNCT])
    if not adjuncts: return
    
    mood_assesment_features = []
    for adjunct in adjuncts:
        awrd = " ".join(adjunct.words())
        if awrd in MOOD_ADJUNCTS:
            mood_assesment_features.extend(MOOD_ADJUNCTS[awrd])
        elif awrd in MOOD_CONJUNCTIVE_ADJUNCTS:
            mood_assesment_features.extend(MOOD_ADJUNCTS[awrd])
        
        
    enrich_with_systemic_selections(clause_node, SYSTEMIC_NETWORK_MOOD, mood_assesment_features)
    
if __name__ == '__main__':
    pass