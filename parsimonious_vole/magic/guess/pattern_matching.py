'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jan 11, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import gc
import logging
import re

import nltk
from parsimonious_vole.magic.guess.fs_constants import PARTICIPANT_ROLE
from parsimonious_vole.magic.utils import equal_dict_in_list, los_in_los, natsort
from networkx import DiGraph
from networkx.algorithms import isomorphism
from networkx.algorithms.cycles import simple_cycles

from parsimonious_vole.magic.parse.parse_constants import TYPE, POS, LEMMA, TOKEN, TEXT


class GraphMatcher():
    """
    The BASE Class for pattern matching and incremental feature structure creation.
    This class is not recommended for instantiation as such, but rather it's derivates.  
    """
    graph = None
    __root_node = None
    
    freature_dict = None
    
    constituency_freature_dicts = None
    
    def __init__(self, dx):
        self.graph = dx
        self.freature_dict = nltk.FeatDict()
        self.constituency_freature_dicts = []

    ######
    # functions for dependency graph , moved out of class
    ######
    def root_node(self):
        if not self.__root_node :
            self.__root_node = root_node(self.graph) 
        return self.__root_node

    def has_links(self, links, strict_filter=False):
        return has_links(self.graph, links, strict_filter)
            
    def get_dependent_tokens(self, token, ffilter=None, strict_filter=False):
        return get_dependent_tokens(self.graph, token, ffilter, strict_filter)

    def feature_structure(self):
        return self.freature_dict
    
    ###########
    # functions for dependency graph pattern matching
    ###########
    def contains_patterns_any(self, pattern_list):
        """ if any patterns are in the sentence then returns True """
        for ptrn in pattern_list:
            if contains_pattern(self.graph, ptrn):
                return True
            else: continue
        return False

    def contains_patterns_all(self, pattern_list):
        """ if all patterns are in the sentence then returns True """
        for ptrn in pattern_list:
            if not contains_pattern(self.graph, ptrn): return False
        return True
    
    def interpret_patterns_disjunctively(self, pattern_list, interpretation_fs, default_fs=None,):
        """
        checks whether ANY patterns are found in the sentence.
        if so then updates the feature structure with interpretation_fs,
        if not found the update fs with default_fs, 
        or do nothing if default_fs is None 
        """
        if self.contains_patterns_any(pattern_list):
            # print "contains_patterns_any!"
            self.freature_dict.update(interpretation_fs)
            return True
        else:
            # print "does not contain patterns any!"
            if default_fs: 
                self.freature_dict.update(default_fs)
        return False

    def interpret_patterns_conjunctively(self, pattern_list, interpretation_fs, default_fs=None,):
        """
        checks whether ALL patterns are found in the sentence.
        if so then updates the feature structure with interpretation_fs,
        if not found the update fs with default_fs, 
        or do nothing if default_fs is None 
        """
        if self.contains_patterns_all(pattern_list):
            self.freature_dict.update(interpretation_fs)
            return True
        else:
            if default_fs:
                self.freature_dict.update(default_fs)
        return False

    def add_constituency_parse_fs(self, feature_structure):
        # check if not already exist
        if not equal_dict_in_list(feature_structure, self.constituency_freature_dicts):
            self.constituency_freature_dicts.append(feature_structure)
        
    def get_constituency_fs(self):
        if self.constituency_freature_dicts: return self.constituency_freature_dicts[0]
        else: return self.constituency_freature_dicts

    def __str__(self):
        result = str(natsort(self.graph.nodes())) + "\n"
        if self.freature_dict: result += str(self.freature_dict) + "\n"
        if self.constituency_freature_dicts: 
            for i in self.constituency_freature_dicts:
                result += "*"+str(i) + "\n"
        return result 
    
    def __repr__(self):
        return self.__str__()
    

######
# functions for dependency graph 
######
def root_node(graph):
    for i in graph.nodes():
        if i and not graph.predecessors(i) and graph.successors(i):
            return i
    
    # if no root found then, either there are cycles or there are no connected nodes 
    if graph.nodes():
        if simple_cycles(graph): 
            logging.warn("there are cycles in the graph")
            return graph.nodes()[0]
        else: 
            return graph.nodes()[0]
    return None

def has_links(graph, links, strict_filter=False):
    """
        test for presence of "links" in the dependency graph 
    """
    for e in graph.edges(data=True):
        if strict_filter:
            if e[2][TYPE] in links: return True
        else:
            for fltr in links:
                    if e[2][TYPE].find(fltr) > -1: return True
    return False

def get_dependent_tokens(graph, token, ffilter=None, strict_filter=False):
    """ return all the tokens that are dependent on current token 
     filter is a list of edge types that are to be returned
     
     by default filter can be a substring of the edge. if the strict filtering
      is enabled then the edge type has to match exactly the filter.
       
     For example if the filter is "abc" and the option strict_filter=False then 
     edge types "abcd"/"qweabc"/"aabcddd" will be retuned as well. However if the 
     strict_filter=True then only edges that are "abc" will be returned 
    """
    if ffilter is None: return graph.out_edges(token, data=True)
    else:
        edges = []
        if strict_filter:
            for e in graph.out_edges(token, data=True):
                if e[2][TYPE] in ffilter: edges.append(e)
        else:
            for e in graph.out_edges(token, data=True):
                for fltr in ffilter:
                    if e[2][TYPE].find(fltr) > -1 and e not in edges: edges.append(e)
        return edges

def get_dominant_tokens(graph, token, ffilter=None, strict_filter=False):
    """ return all the tokens that are dominant to current token 
     filter is a list of edge types that are to be returned
     
     by default filter can be a substring of the edge. if the strict filtering
      is enabled then the edge type has to match exactly the filter.
       
     For example if the filter is "abc" and the option strict_filter=False then 
     edge types "abcd"/"qweabc"/"aabcddd" will be retuned as well. However if the 
     strict_filter=True then only edges that are "abc" will be returned 
    """
    if ffilter is None: return graph.in_edges(token, data=True)
    else:
        edges = []
        if strict_filter:
            for e in graph.in_edges(token, data=True):
                if e[2][TYPE] in ffilter: edges.append(e)
        else:
            for e in graph.in_edges(token, data=True):
                for fltr in ffilter:
                    if e[2][TYPE].find(fltr) > -1 and e not in edges: edges.append(e)
        return edges

####

def _isomorphism_node_compare(node1, node2):
    """
    A function that returns True iff node n1 in G1 and n2 in G2 
    should be considered equal during the isomorphism test. 
    The function will be called like:  node_match(G1.node[n1], G2.node[n2])
    That is, the function will receive the node attribute dictionaries 
    of the nodes under consideration. If None, then no attributes are 
    considered when testing for an isomorphism.
    """
    is_lemma, is_pos = True, True
#    if node2[LEMMA]: lemma = node2[LEMMA].lower() == node1[TOKEN].lemma.lower() 
#    if node2[POS]: pos = node2[POS].lower() in node1[TOKEN].pos.lower()
    #print node1, " n??n ", node2, lemma and pos #, " is not lemma ", (not node2[LEMMA]) , " node2 in node1",node2[TEXT].lower() in node1[TEXT].lower()
    #print node1, node2
    
    if node2[LEMMA]: is_lemma = los_in_los(node2[LEMMA], node1[TOKEN].lemma)
    if node2[POS]: is_pos = los_in_los(node2[POS],node1[TOKEN].pos)
    return is_lemma and is_pos

def _isomorphism_edge_compare(edge1, edge2):
    """
    A function that returns True iff the edge attribute dictionary 
    for the pair of nodes (u1, v1) in G1 and (u2, v2) in G2 should 
    be considered equal during the isomorphism test. The function 
    will be called like: edge_match(G1[u1][v1], G2[u2][v2])
    That is, the function will receive the edge attribute dictionaries 
    of the edges under consideration. If None, then no attributes are 
    considered when testing for an isomorphism.
    """ 
    # print edge1," e??e ",edge2
#    if edge1[TYPE] and edge2[TYPE]:
#        return edge2[TYPE] in edge1[TYPE]
#    else: 
#        return True
    return los_in_los(edge2[TYPE],edge1[TYPE])

def pattern_to_graph(pattern):
    """
    instantiates the pattern graph and adds automatically the 
    lemma and pos attributes if any found
     
    "@lemma, /POS, $ role"
    "@[lemma1, lemma2], /[POS1, pos2], $[role1, role2]"
    """
    # previous REG ex: "(@\w*)?(/\w*)?(\$[,\w\d\s\-\[\]]*)?"
    __repattern = re.compile("(@[,\w\d\s\-\[\]]*)?(/[,\w\d\s\-\[\]]*)?(\$[,\w\d\s\-\[\]]*)?", re.IGNORECASE) 
    dx = DiGraph(pattern)
    for n in dx.nodes():
        grps = __repattern.match(n).groups()
        dx.node[n][TEXT] = str(n)
        # Lemma
        if grps[0] and grps[0][1:]:
            if "[" not in grps[0][1:]: 
                dx.node[n][LEMMA] = [grps[0][1:].strip()]
            else: dx.node[n][LEMMA] = grps[0][1:].replace("[", "").replace("]", "").replace(" ","").split(",")
        else: dx.node[n][LEMMA] = []
        # POS
        if grps[1] and grps[1][1:]:
            if "[" not in grps[1][1:]:
                dx.node[n][POS] = [grps[1][1:].strip()]
            else: dx.node[n][POS] = grps[1][1:].replace("[", "").replace("]", "").replace(" ","").split(",")
        else: dx.node[n][POS] = []
        # Participants
        if grps[2] and grps[2][1:]:
            if "[" not in grps[2][1:]:
                dx.node[n][PARTICIPANT_ROLE] = [grps[2][1:].strip()]
            else: dx.node[n][PARTICIPANT_ROLE] = grps[2][1:].replace("[", "").replace("]", "").replace(" ","").split(",")
        else: dx.node[n][PARTICIPANT_ROLE] = []
    
    return dx

def contains_pattern(graph, mini_graph_pattern=None, return_matching_subraphs=False):
    """ 
    checks whether the two graphs are isomorphic
    """
    try:
        pattern_graph = pattern_to_graph(mini_graph_pattern)
    except Exception as e:
        raise Exception("The graph pattern is not correctly specified. \n\
        Specify the graph patter as a list of edges like that: [(n1,n2,{k:v1})\
        ...(nn,nn+1,{k:vn})] \n\
        The specified pattern is:" + str(mini_graph_pattern)) 
    
    dgm = isomorphism.DiGraphMatcher(graph, pattern_graph,
                                 node_match=_isomorphism_node_compare,
                                 edge_match=_isomorphism_edge_compare)
    is_isom = dgm.subgraph_is_isomorphic()
    
    if return_matching_subraphs:
        subgraph_isomorphisms = []   
        for n in dgm.subgraph_isomorphisms_iter():
            subgraph_isomorphisms.append(n)

        # cleaning up
        dgm.reset_recursion_limit()
        del dgm
        gc.collect()
        
        return is_isom, subgraph_isomorphisms
    else:
        # cleaning up
        dgm.reset_recursion_limit()
        del dgm
        gc.collect()
         
        return is_isom

def extract_marked_nodes(graph, mini_graph_pattern, group_nodes_by_possible_match=False):
    """ 
    returns a list of tuples (Participant Role,sentence_node) as a result of 
    matching the sentence to graph pattern
    
    Note:
    in the future an optimization would be to provide directly the instantiated pattern as graph  
    """
    try:
        pattern_graph = pattern_to_graph(mini_graph_pattern)
    except Exception as e:
        raise Exception("The graph pattern is not correctly specified. \n\
        Specify the graph patter as a list of edges like that: [(n1,n2,{k:v1})\
        ...(nn,nn+1,{k:vn})] \n\
        The specified pattern is:" + str(mini_graph_pattern)) 
    
    dgm = isomorphism.DiGraphMatcher(graph, pattern_graph,
                                 node_match=_isomorphism_node_compare,
                                 edge_match=_isomorphism_edge_compare)
    is_isom = dgm.subgraph_is_isomorphic()
    
    subgraph_isomorphisms = []   
    for n in dgm.subgraph_isomorphisms_iter():
        subgraph_isomorphisms.append(n)

    result = []
    if not is_isom: return result
    # get the subgraphs into a list of tuples
    for subgraph in subgraph_isomorphisms:
        temp = []
        for key in subgraph.keys():
            for pr in pattern_graph.node[subgraph[key]][PARTICIPANT_ROLE]:
                if group_nodes_by_possible_match:
                    temp.append((pr, key))
                else:
                    result.append((pr, key)) 
        if group_nodes_by_possible_match:
            result.append(temp)
    return result

if __name__ == '__main__':
    pass
