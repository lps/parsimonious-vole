'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 7, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

##########################################################################################
# MCG Constants
##########################################################################################
FINITE = "finite"
PREDICATOR_FINITE = "predicator_finite"
ADJUNCT = "adjunct"
COMPLEMENT = "complement"  # direct object
COMPLEMENT_AGENT = "complement_agent"  # agent
COMPLEMENT_DATIVE = "complement_dative"  # indirect object
COMPLEMENT_ADJUNCT = "complement_or_adjunct"  # complement or adjunct
PREDICATOR = "predicator"
SUBJECT = "subject"
EXPLETIVE_MARKER = "expletive_marker"
MARKER = "marker"
CLAUSE = "clause"
ROOT = "root"
DISCOURSE = "discourse"

###########################
DEPENDECY_CONTEXT = "dependecy_context"
DEPENDECY_RELATION = "dependecy_relation"
C_TYPE = "constituent_type"
###########################
MOOD = "mood"
DECLARATIVE = "declarative"
IMPERATIVE = "imperative"
INTERROGATIVE_YN = "interrogative_yes/no"
INTERROGATIVE_WH = "interrogative_Wh/"
MINOR = "minor"
FREE = "free"

###########################
# Noun phrase constituents
###########################
DEICTIC = "deictic"
PREDEICTIC = "predeictic"
CARDINAL_NUMERATIVE = "cardinal_numerative"
EPITHET_CLASSIFIER_OR_ORDINAL = "epithet_classifier_or_ordinal"
POSESSOR = "posessor"
APPOSITION = "apposition"
QUALIFIER = "qualifier"
THING = "thing"
###########################

## feature structures

#############################
# # Mood: Tense detection patterns+ polarity + voice
#############################

# tense
UNKNOWN = "unknown"
TENSE = "tense"
PAST = "past"
PRESENT = "present"
FUTURE = "future"

SIMPLE = "simple"
PROGRESSIVE = "progressive"
PERFECT = "perfect"

# voice
PASSIVE = "passive"
ACTIVE = "active"

# fetures
TIME = "time"
PERFECTIVITY = "perfectivity"
PROGRESSIVITY = "progressivity"
VOICE = "voice"

# polarity
POLARITY = "POLARITY-TYPE" 
POSITIVE = 'positive'
NEGATIVE = 'negative'
# modality
MODALITY = "modality"
MODAL_OPERATOR = "modal_operator"


##########################33
#Taxis constants
PARATAXIS = "parataxis"
HYPOTAXIS = "hypotaxis"
EMBEDDED = "embedded"

PROJECTION = "projection"
EXPANSION = "expansion"
INFLUENTIAL = "influential"
FULL ="full"

DEPENDENT = "dependent"
DOMINANT = "dominant"
#pattern spec
MANDATORY = "mandatory"
OPTIONAL = "optional"
PLUS = "plus"
MINUS = "minus"
#
FINITNESS = "finitness"
FINITE = "finite"
NON_FINITE = "non_finite"
NON_FINITE_PERFECTIVE = "non_finite_perfective"
NON_FINITE_IMPERFECTIVE = "non_finite_imperfective"

# transitivity
POSSIBLE_PROCESS_TYPES = "possible_process_types"
POSSIBLE_CONFIGURATIONS = "possible_configurations"
CONFIGURATION_TYPE = "configuration_type"
PARTICIPANT_ROLE = "participant_role"
CONFIGURATION = "configuration"
if __name__ == '__main__':
    pass