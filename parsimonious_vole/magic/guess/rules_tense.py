'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 11, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

#TODO: the whole thing is wrong because the features are being added to the feature
# structure of  the GraphMatcher, and they will be overridden for each clause
# instead make sure they are attached to each clause in constituency graph and 
# get rid of GraphMatcher class 

'''
import logging

from parsimonious_vole.magic.guess.fs_constants import MODAL_OPERATOR, MODALITY, PERFECT,\
    PERFECTIVITY, PROGRESSIVITY, SIMPLE, UNKNOWN, PROGRESSIVE, PAST, TENSE, TIME,\
    FUTURE, PRESENT
from parsimonious_vole.magic.guess.pattern_matching import GraphMatcher
from parsimonious_vole.magic.utils import eattr
from nltk import FeatDict

from parsimonious_vole.magic.guess.dependency_constants import *

####################### Tense #######################

tense_unknown = FeatDict({TENSE:{TIME :UNKNOWN, PERFECTIVITY: UNKNOWN, PROGRESSIVITY : UNKNOWN}})
#
# no auxiliary verb patterns
#
# present simple
verb_present_base_form = [(VB, "/", eattr(None))]
verb_present_3rd_pers_form = [(VBZ, "/", eattr(None))]
verb_present_non_3rd_pers_form = [(VBP, "/", eattr(None))]

verb_present_simple_form_patterns = [verb_present_base_form,
                                     verb_present_3rd_pers_form,
                                     verb_present_non_3rd_pers_form]
tense_present_simple_fs = FeatDict({TENSE:{TIME :PRESENT, PERFECTIVITY: SIMPLE, PROGRESSIVITY : SIMPLE}})

# past simple
verb_past_base_form = [(VBD, "/", eattr(None))]
tense_past_simple = FeatDict({TENSE:{TIME :PAST, PERFECTIVITY: SIMPLE, PROGRESSIVITY : SIMPLE}})

#
# one auxiliaries patterns
#
# present progressive - be(VBZ/VBP) + -ing(VBG)
present_progressive_form1 = [(VBG, "@be" + VBZ, eattr(aux))]
present_progressive_form2 = [(VBG, "@be" + VBP, eattr(aux))]
present_progressive_forms = [present_progressive_form1, present_progressive_form2]
tense_present_progressive = FeatDict({TENSE:{TIME :PRESENT, PERFECTIVITY: SIMPLE, PROGRESSIVITY : PROGRESSIVE}})

# present perfect - have(VBZ/VBP) + 3rd(VBN) 
present_perfect_form1 = [(VBN, "@have" + VBZ, eattr(aux))]
present_perfect_form2 = [(VBN, "@have" + VBP, eattr(aux))]
present_perfect_forms = [present_perfect_form1 , present_perfect_form2]
tense_present_perfect = FeatDict({TENSE:{TIME :PRESENT, PERFECTIVITY: PERFECT, PROGRESSIVITY : SIMPLE}})

# past progressive be(VBD) + -ing(VBG)
past_progressive_form = [(VBG, "@be" + VBD, eattr(aux))]
tense_past_progressive = FeatDict({TENSE:{TIME :PAST, PERFECTIVITY: SIMPLE, PROGRESSIVITY : PROGRESSIVE}})

# past perfect - have(VBZ/VBP) + 3rd(VBN) 
past_perfect_form = [(VBN, "@have" + VBD, eattr(aux))]
tense_past_perfect = FeatDict({TENSE:{TIME :PAST, PERFECTIVITY: PERFECT, PROGRESSIVITY : SIMPLE}})

# present simple +Do - do(VBP/VBZ) + VB/VBP
present_simple_do_form1 = [(VB, "@do" + VBP, eattr(aux))]
present_simple_do_form2 = [(VB, "@do" + VBZ, eattr(aux))]
present_simple_do_form3 = [(VBP, "@do" + VBP, eattr(aux))]
present_simple_do_form4 = [(VBP, "@do" + VBZ, eattr(aux))]
present_simple_do_forms = [present_simple_do_form1 , present_simple_do_form2 ,
                           present_simple_do_form3 , present_simple_do_form4]
# past simple +Do - do(VBD) + VB
past_simple_do_form1 = [(VB, "@do" + VBD, eattr(aux))]

# present simple passive voice be(VBZ/VBP) + VBN
present_simple_passive_form1 = [(VBN, "@be" + VBZ, eattr(aux))]
present_simple_passive_form2 = [(VBN, "@be" + VBP, eattr(aux))]
present_simple_passive_forms = [present_simple_passive_form1, present_simple_passive_form2]

# past simple passive voice be(VBD) + VBN
past_simple_passive_form1 = [(VBN, "@be" + VBD, eattr(aux))]
past_simple_passive_form2 = [(VBN, "@be" + VBD, eattr(auxpass))]

# Future simple active voice will + VB/VBP (I will take)
future_simple_form1 = [(VB, "@will" + MD, eattr(aux))] 
tense_future_simple = FeatDict({TENSE:{TIME :FUTURE, PERFECTIVITY: SIMPLE, PROGRESSIVITY : SIMPLE}})

# two auxiliaries patterns
#
# present perfect progressive  - have(VBP/VBZ) + be(VBN) + -ing(VBG/VBN)
present_perfect_progressive_form1 = [(VBG, "@be" + VBN, eattr(aux)), (VBG, "@have" + VBZ, eattr(aux)) ]
present_perfect_progressive_form2 = [(VBG, "@be" + VBN, eattr(aux)), (VBG, "@have" + VBP, eattr(aux)) ]
present_perfect_progressive_form3 = [(VBN, "@be" + VBN, eattr(aux)), (VBN, "@have" + VBZ, eattr(aux)) ]
present_perfect_progressive_form4 = [(VBN, "@be" + VBN, eattr(aux)), (VBN, "@have" + VBP, eattr(aux)) ]
present_perfect_progressive_forms = [present_perfect_progressive_form1, present_perfect_progressive_form2,
                                     present_perfect_progressive_form3, present_perfect_progressive_form4 ]

present_perfect_progressive_passive_form1 = [(VBG, "@be" + VBN, eattr(auxpass)), (VBG, "@have" + VBZ, eattr(aux)) ]
present_perfect_progressive_passive_form2 = [(VBG, "@be" + VBN, eattr(auxpass)), (VBG, "@have" + VBP, eattr(aux)) ]
present_perfect_progressive_passive_form3 = [(VBN, "@be" + VBN, eattr(auxpass)), (VBN, "@have" + VBZ, eattr(aux)) ]
present_perfect_progressive_passive_form4 = [(VBN, "@be" + VBN, eattr(auxpass)), (VBN, "@have" + VBP, eattr(aux)) ]
present_perfect_progressive_passive_forms = [present_perfect_progressive_passive_form1,
                                             present_perfect_progressive_passive_form2,
                                             present_perfect_progressive_passive_form3,
                                             present_perfect_progressive_passive_form4]
tense_present_perfect_progressive = FeatDict({TENSE:{TIME :PRESENT, PERFECTIVITY: PERFECT, PROGRESSIVITY : PROGRESSIVE}})

# past perfect progressive - have (VBD) + be (VBN) + -ing (VBG/VBN)
past_perfect_progressive_form1 = [(VBG, "@be" + VBN, eattr(aux)), (VBG, "@have" + VBD, eattr(aux)) ]
past_perfect_progressive_form2 = [(VBN, "@be" + VBN, eattr(aux)), (VBN, "@have" + VBD, eattr(aux)) ]
past_perfect_progressive_forms = [past_perfect_progressive_form1, past_perfect_progressive_form2]

past_perfect_progressive_passive_form1 = [(VBG, "@be" + VBN, eattr(auxpass)), (VBG, "@have" + VBD, eattr(aux)) ]
past_perfect_progressive_passive_form2 = [(VBN, "@be" + VBN, eattr(auxpass)), (VBN, "@have" + VBD, eattr(aux)) ]
past_perfect_progressive_passive_forms = [past_perfect_progressive_passive_form2, past_perfect_progressive_passive_form1]
tense_past_perfect_progressive = FeatDict({TENSE:{TIME :PAST, PERFECTIVITY: PERFECT, PROGRESSIVITY : PROGRESSIVE}})

# future simple passive voice will + be + VBN (I will be taken)
future_passive_form1 = [(VBN, "@will" + MD, eattr(aux)),(VBN, "@be", eattr(auxpass))] 
future_passive_form1_fs = FeatDict({TENSE:{TIME :FUTURE, PERFECTIVITY: SIMPLE, PROGRESSIVITY : SIMPLE}})
# future progressive active voice: will + be + VBG (I will be taking)
future_progressive_form1 = [(VBG, "@will" + MD, eattr(aux)),(VBG, "@be", eattr(aux))] 
future_progressive_form1_fs = FeatDict({TENSE:{TIME :FUTURE, PERFECTIVITY: SIMPLE, PROGRESSIVITY : PROGRESSIVE}})
# future perfect simple : will + have + VBN (I will have taken)
future_perfect_form1 = [(VBN, "@will" + MD, eattr(aux)),(VBN, "@have", eattr(aux))] 
future_perfect_form1_fs = FeatDict({TENSE:{TIME :FUTURE, PERFECTIVITY: PERFECT, PROGRESSIVITY : SIMPLE}})

# 3 auxiliaries patterns
#
# future perfect progressive : will + have + been + VBG (I will have been taking)
future_perfect_progressive_form1 = [(VBG, "@will" + MD, eattr(aux)),(VBG, "@have", eattr(aux)),(VBG, "@be", eattr(aux))] 
future_perfect_progressive_form1_fs = FeatDict({TENSE:{TIME :FUTURE, PERFECTIVITY: PERFECT, PROGRESSIVITY : PROGRESSIVE}})
# future perfect simple passive voice: will + have + been + VBN (I will have been taken)
future_perfect_simple_passive_form1 = [(VBN, "@will" + MD, eattr(aux)),(VBN, "@have", eattr(aux)),(VBN, "@be", eattr(auxpass))] 

#####   MODALS #####
# 1 aux modals
#modality_unknown = FeatDict({MODALITY:{MODAL_OPERATOR :UNKNOWN, PERFECTIVITY: UNKNOWN, PROGRESSIVITY : UNKNOWN}})

modal_simple_form = [(VB, MD+"$modal", eattr(aux))] #
modal_simple_form_fs = FeatDict({MODALITY:{MODAL_OPERATOR :UNKNOWN}})

# 2 aux modals
#
# modal progressive: MD+  be + VBG : (must be taking)
modal_2_aux_form_modal_progressive = [(VBG, MD+"$modal", eattr(aux)),(VBG, "@be", eattr(aux))] #
modal_2_aux_form_modal_progressive_fs = FeatDict({MODALITY:{MODAL_OPERATOR :UNKNOWN, PERFECTIVITY: SIMPLE, PROGRESSIVITY : PROGRESSIVE}})
#  modal perfect simple: MD + have + VBN (must have taken)
modal_2_aux_form_modal_perfect_simple = [(VBN, MD+"$modal", eattr(aux)),(VBN, "@have", eattr(aux))] #
modal_2_aux_form_modal_perfect_simple_fs = FeatDict({MODALITY:{MODAL_OPERATOR :UNKNOWN, PERFECTIVITY: PERFECT, PROGRESSIVITY : SIMPLE}})
# modal simple passive: MD + be + VBN: (must be taken)
modal_2_aux_form_modal_simple_passive = [(VBN, MD, eattr(aux)),(VBN, "@be", eattr(auxpass))] #

# 3 aux modals
# modal perfect progressive MD + have + be + VBG (must have been taking)
modal_3_aux_form_modal_perfect_progressive = [(VBG, MD+"$modal", eattr(aux)),(VBG, "@have", eattr(aux)),(VBG, "@be", eattr(aux))] #
modal_3_aux_form_modal_perfect_progressive_fs = FeatDict({MODALITY:{MODAL_OPERATOR :UNKNOWN, PERFECTIVITY: PERFECT, PROGRESSIVITY : SIMPLE}})
# modal perfect simple passive MD + have + be + VBN (must have been taken)
modal_3_aux_form_modal_perfect_simple_passive = [(VBN, MD+"$modal", eattr(aux)),(VBN, "@have", eattr(aux)),(VBN, "@be", eattr(auxpass))] #

###################################

def update_modal_particle_if_any(sentence,verb,modal_verb_list):
    """ inserts the modal verb if there is one"""
    if modal_verb_list:
        md = modal_verb_list[0]
        if MODALITY in sentence.feature_structure():
            sentence.feature_structure()[MODALITY][MODAL_OPERATOR] = md.word
        else: 
            #print "no modality feature structure found!"
            return False
    else:
        #print "no modal particle linked to the verb: "+ verb
        return False
    return True

def parse_tense_or_modality(sentence_grap_matcher, verb):
    """
    inserts a Tense or modality feature structure into the sentence_grap_matcher
    
    decide the DEICTICITY: temporal or modal [Halliday04, p350]
    """
    # getting the auxiliary verbs
    aux_deps = sentence_grap_matcher.get_dependent_tokens(verb, AUXILIARY_VERB_DEPENDECIES)
    
    aux_verbs = []
    modal_verbs = []
    for i in aux_deps:
        if i[1].pos in VERBS_POS: aux_verbs.append(i[1])
        elif i[1].pos == "MD": modal_verbs.append(i[1])
    # sort the aux verbs ascending, preserving their order in the original sentence_grap_matcher
    sorted(aux_verbs, key=lambda vb: vb.id)
    
    # print "Auxiliary verbs are ", map((lambda i: (i.lemma, i.pos)), aux_verbs)
    # by default the tense is unknown
    
    #sentence_grap_matcher.freature_dict.update(tense_unknown)
    
    # print len(aux_verbs)
    # checking for known tense patterns
    if not aux_verbs:
        if not modal_verbs:
            sentence_grap_matcher.interpret_patterns_disjunctively(verb_present_simple_form_patterns, tense_present_simple_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([verb_past_base_form], tense_past_simple)
        elif len(modal_verbs)==1:
            # future
            if not sentence_grap_matcher.interpret_patterns_disjunctively([future_simple_form1], tense_future_simple):
                # simple modal
                sentence_grap_matcher.interpret_patterns_disjunctively([modal_simple_form], modal_simple_form_fs)
                update_modal_particle_if_any(sentence_grap_matcher,verb,modal_verbs)
        else: logging.error("two modal verbs are impossible, something is terrible wrong")
    elif len(aux_verbs) == 1:
        if not modal_verbs:
            # print "one AUX ", aux_verbs
            # progressives
            sentence_grap_matcher.interpret_patterns_disjunctively(present_progressive_forms , tense_present_progressive)
            sentence_grap_matcher.interpret_patterns_disjunctively([past_progressive_form] , tense_past_progressive)
            # perfects
            sentence_grap_matcher.interpret_patterns_disjunctively(present_perfect_forms , tense_present_perfect)
            sentence_grap_matcher.interpret_patterns_disjunctively([past_perfect_form], tense_past_perfect)
            # pres/past simple + do
            sentence_grap_matcher.interpret_patterns_disjunctively(present_simple_do_forms, tense_present_simple_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([past_simple_do_form1], tense_past_simple)
            # passive simple pres/past
            sentence_grap_matcher.interpret_patterns_disjunctively([past_simple_passive_form1, past_simple_passive_form2], tense_past_simple)
            sentence_grap_matcher.interpret_patterns_disjunctively(present_simple_passive_forms, tense_present_simple_fs)
        elif len(modal_verbs)==1:
            # future 1 modal 1 aux
            sentence_grap_matcher.interpret_patterns_disjunctively([future_passive_form1],future_passive_form1_fs )
            sentence_grap_matcher.interpret_patterns_disjunctively([future_progressive_form1], future_progressive_form1_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([future_perfect_form1], future_perfect_form1_fs)
            
            # 2 modal AUX
            sentence_grap_matcher.interpret_patterns_disjunctively([modal_2_aux_form_modal_progressive], modal_2_aux_form_modal_progressive_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([modal_2_aux_form_modal_perfect_simple], modal_2_aux_form_modal_perfect_simple_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([modal_2_aux_form_modal_simple_passive], modal_simple_form_fs)
            
            update_modal_particle_if_any(sentence_grap_matcher,verb,modal_verbs)
        else: logging.error("two modal verbs are impossible, something is terrible wrong")
    elif len(aux_verbs) == 2:
        if not modal_verbs:
            # present perfect progressive
            sentence_grap_matcher.interpret_patterns_disjunctively(present_perfect_progressive_forms, tense_present_perfect_progressive)
            sentence_grap_matcher.interpret_patterns_disjunctively(present_perfect_progressive_passive_forms, tense_present_perfect_progressive)
            
            # past perfect progressive
            sentence_grap_matcher.interpret_patterns_disjunctively(past_perfect_progressive_forms, tense_past_perfect_progressive)
            sentence_grap_matcher.interpret_patterns_disjunctively(past_perfect_progressive_passive_forms, tense_past_perfect_progressive)
        elif len(modal_verbs)==1:
            #future 1 modal 2 aux 
            sentence_grap_matcher.interpret_patterns_disjunctively([future_perfect_progressive_form1], future_perfect_progressive_form1_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([future_perfect_simple_passive_form1], future_perfect_form1_fs)
            
            # 3 modal aux
            sentence_grap_matcher.interpret_patterns_disjunctively([modal_3_aux_form_modal_perfect_progressive], modal_3_aux_form_modal_perfect_progressive_fs)
            sentence_grap_matcher.interpret_patterns_disjunctively([modal_3_aux_form_modal_perfect_simple_passive], modal_2_aux_form_modal_perfect_simple_fs)
            update_modal_particle_if_any(sentence_grap_matcher,verb,modal_verbs)
        else: logging.warn("two modal verbs are impossible, something is terrible wrong") 
    else: 
        logging.warn("3 auxiliary verbs are not supported by the system"+str(aux_verbs))
    return True

def get_tense_or_modality_feature_selection(sentence, verb):
    """ 
        returns the feature selections of tense or modality
        selection is according to systemic network  
    """
    features_selected = []
    sgm = GraphMatcher(sentence)
    parse_tense_or_modality(sgm, verb)
    if MODALITY in sgm.feature_structure() and TENSE in sgm.feature_structure():
        logging.warn("WARNING: error in tense/modality detection algorithm")
    if MODALITY in sgm.feature_structure():
        features_selected.append("modal")
        # looking into specific modality selections
        
    elif TENSE in sgm.feature_structure():
        features_selected.append("temporal")
        features_selected.append(sgm.feature_structure()[TENSE][TIME])
        features_selected.append("non-progressive" if sgm.feature_structure()[TENSE][PROGRESSIVITY] is SIMPLE else "progressive"  )
        features_selected.append("non-perfect" if sgm.feature_structure()[TENSE][PERFECTIVITY] is SIMPLE else "perfect"  )
    return features_selected

if __name__ == '__main__':
    pass