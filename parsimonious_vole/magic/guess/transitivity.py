'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Mar 26, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

from __future__ import division

import logging
import os

from parsimonious_vole.magic.guess.fs_constants import PARTICIPANT_ROLE, CLAUSE
from parsimonious_vole.magic.guess.mcg_utils import list_of_possible_configurations, get_subject_from_mcg_clause_graph, get_complements_from_mcg_clause_graph, get_thing,\
    is_feature_selected, get_markers_of_mcg_constituent, get_list_of_clause_graphs, enrich_with_systemic_selections, get_child_constituents, feature_selection_generator, is_clause
from parsimonious_vole.magic.guess.mood import Constituent, ReferenceConstituent, parse_mood
from parsimonious_vole.magic.guess.mood_pattern_matching import enrich_from_pattern
from parsimonious_vole.magic.guess.system_network_mappings import SYSTEMIC_NETWORK_TRANSITIVITY
from parsimonious_vole.magic.guess.transitivity_pattens import pattern_set
from parsimonious_vole.magic.parse.VerbForm import PROCESS_TYPE
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.parse.parse_process import get_parse_result
from parsimonious_vole.magic.parse.stanford_parser import call_stanford_parser_file
from parsimonious_vole.magic.utils import root_node, natsort, los_in_los, strip
from networkx.classes.digraph import DiGraph
from nltk.featstruct import FeatDict

from parsimonious_vole.magic.guess.mood_binding_and_control_enricher import parse_mood_post_enrichment


def parse_transitivity_from_mood_graph(mcg,sentence_dependecy_graph):
    """
    TODO: the 2 role patterns are applied to 3 role clauses and they should not be.
          * The duke gave her a teapot
        
    TODO: in case of reflexive pronouns(complmenet) they will take together with the
         subject the same semantic role. hence they take only 1-2 distinct roles in 
         the clause even if there are 2-3 role receiving constituents.  
          * I find myself happy.
          * I said to myself what a wonderful world.
          * I asked myself whether bla bla. 
          * He though to himself when she would arrive.
          * I wash myself
    """
    logging.info("*** TRANSITIVITY-Parsing ***")
    clauses = get_list_of_clause_graphs(mcg)
    for cl in clauses:
        rn = root_node(cl)
        if rn and rn.head_dependecy_node():
            possible_configurations = list_of_possible_configurations(sentence_dependecy_graph, rn.head_dependecy_node())
            #pprint.pprint(possible_configurations) 
            #valid_possible_configurations = filter(lambda x: x[2] and x[3] and x[3][0] ,possible_configurations)
            valid_possible_configurations = possible_configurations 
            # apply the configurations
            #enrichement_results = []
            for config in valid_possible_configurations:
                for pattern in pattern_set[config[2]][config[3]]:
                    # condition bundle contains one function
#                     print "---"
#                     #pprint.pprint(pattern)
#                     print "---"
                    enrich_from_pattern(cl,pattern,condition_functions=[condition_for_roles_check],full_mcg_graph=mcg) 
                    #res = enrich_from_pattern(cl,pattern,condition_functions=[condition_for_roles_check],full_mcg_graph=mcg) # condition bundle contains one function
                    #enrichement_results.append(res)
            # reducing the configuration patterns to unique ones
#             if CONFIGURATION in rn.feature_structure():
#                 rn.feature_structure()[CONFIGURATION] = uniquify(rn.feature_structure()[CONFIGURATION])
        else:
            logging.warn("A Graph without root_node: "+str(natsort(mcg.nodes(True))))
        
        # TODO: this is a workaround, and network feature selection should be implemented directly into the patterns
        # proc_type from verb to clause + use systemic network
        # participants receive syst network feature, i.e. transform from Ag to agent from the systemic network
        subject = get_subject_from_mcg_clause_graph(cl)
        complements = get_complements_from_mcg_clause_graph(cl)

        for f in feature_selection_generator(rn,PROCESS_TYPE):
            enrich_with_systemic_selections(rn,SYSTEMIC_NETWORK_TRANSITIVITY,map_ptdb_to_transitivity_network(f))
         
        if subject:
            for f in feature_selection_generator(subject,PARTICIPANT_ROLE):
                enrich_with_systemic_selections(subject, SYSTEMIC_NETWORK_TRANSITIVITY,map_ptdb_to_transitivity_network(f))
                
        for compl in complements:
            for f in feature_selection_generator(compl,PARTICIPANT_ROLE):
                enrich_with_systemic_selections(compl, SYSTEMIC_NETWORK_TRANSITIVITY,map_ptdb_to_transitivity_network(f))
    logging.info("*** End TRANSITIVITY-Parsing ***")
    
    return mcg


require_animate = ["Cog","Em","Perc"]

role_preposition_map = {"Des":["to","towards","at","on","in"],
                        "Ben":["to","for"],
                        "Attr":["as"],
                        "Ra":["on","in"],
                        "So":["from",],
                        "Pa":["through","via"], 
                        "Loc":["in","at","into","behind","in front of","on",],  #"behind","in front of",
                        "Mtch":["with","to"], 
                        "Ag":["by"],
                        "Ph":["about"],
                        "Cog":["to"]}

def condition_for_roles_check(constituent, feature_dict, mcg_graph):
    """
        A Lambda function working as a filter.
        checks whether a particular role is not illegal for a particular constituent
        a condition function as required by the enrich_from_pattern function
    """
    # initial test
    if not isinstance(constituent, Constituent) or\
        not isinstance(feature_dict, (FeatDict,dict))or \
        not isinstance(mcg_graph, DiGraph): return True
    
    role = feature_dict[PARTICIPANT_ROLE] if PARTICIPANT_ROLE in feature_dict else None
    if not role: return True
    
    #if require animate then search for animate flag in constituent 
    if los_in_los(require_animate,role, True, True):
        if is_clause(constituent, mcg_graph) or isinstance(constituent, ReferenceConstituent): thing = constituent
        else:thing = get_thing(constituent, mcg_graph)
        
        if not (is_feature_selected(constituent, [u'animate']) or is_feature_selected(thing, [u'animate'])): return False
    
    # preposition check for role constraints in role_preposition_map
    preps = filter(None,[" ".join(x.words()) for x in get_markers_of_mcg_constituent(constituent,mcg_graph)])
    if role and preps and role in role_preposition_map: 
        if not los_in_los(role_preposition_map[role], preps, False, True): return False
    
    #if a constituent is a clause then it can take only Ph role
    # a list of children that are clause and their direct parent is current contituent
    clause_children = [x for x in get_child_constituents(constituent,mcg_graph,[CLAUSE]) if x.parent() is constituent]
    #if "ph" in role.lower(): print role 
    if clause_children and "ph" not in role.lower(): return False  
        
    return True

PTDB_TO_TRANSITIVITY_NETWORK_MAPPING = { "Ag":"agent",
                                        "Af": "affected",
                                        "Af-Cog": "affected-cognizant",
                                        "Af-Ca" : "affected-carrier",
                                        "Af-Pos" : "affected-possessed",
                                        "Ag-Perc": "agent-perceiver",
                                        "Ag-Ca" : "agent-perceiver",
                                        "Ag-Cog" : "agent-cognizant",
                                        "Af-Des" : "affected-destination",
                                        "Af-Em" :"affected-emoter",
                                        "Af-Pa" : "affected-path",
                                        "Af-Perc" : "affected-perceiver",
                                        "Af-So" : "affected-source",
                                        "Cre-Ph" : "created-phenomenon",
                                        "Af-Ph" : "affected-phenomenon",
                                        "Mtch" : "matchee",
                                        "Cre": "created",
                                        "Ra" : "range",
                                        "Ma" : "manner",
                                        "Ca" : "carrier",
                                        "At": "attribute",
                                        "Ph" :"phenomenon",
                                        "Em" : "emoter",
                                        "Perc" : "perceiver",
                                        "Dir": "destination", # TODO: a bug in PTDB, need to edit and replace direction role with destination
                                        "Cog" : "cognizant",
                                        "Pos" : "possessed",
                                        "Loc" : "location",
                                        "So" : "source",
                                        "Pa" : "path",
                                        "Des": "destination",
                                        "There" : "there",
                                        "it" : "it-empty",
                                        }

def map_ptdb_to_transitivity_network(ptdb_label):
    """ 
        returns a systemic network label for a PTDB label,
        re-maps only participant roles, process types should(are expected) be as in sys network after the version V 3.2
    """
    if strip(ptdb_label.replace("?","")): 
        if ptdb_label in PTDB_TO_TRANSITIVITY_NETWORK_MAPPING: return PTDB_TO_TRANSITIVITY_NETWORK_MAPPING[ptdb_label]
    return ptdb_label
    
def parse_transitivity_txt_file(txt_file, return_dep_graph = False):
    """ returns transitivity parsed MCG bundle for a .txt file"""
    logging.info("Parsing the transitivity for %s"%os.path.basename(txt_file))
    if not os.path.isfile(txt_file):
        raise IOError("The specified path of .txt file does not exist or it is not a file")
    if not os.path.isfile(txt_file+".stp"):
        logging.info("Pre-processing with Stanford Parser")
        call_stanford_parser_file(txt_file)
    if not os.path.isfile(txt_file+".stp"):
        raise Exception("The .stp file not found afetr running Stanford parser.")
    tp = parse_transitivity_stp_file(txt_file+".stp",return_dep_graph)
    logging.info("Done Parsing transitivity for %s"%os.path.basename(txt_file))
    return tp

def parse_transitivity_stp_file(stp_file, return_dep_graph = False):
    """ returns transitivity parsed MCG bundle for a .stp file """
    result = []
    
    parse_result = get_parse_result(stp_file)
    #print "Parse Result: ",[x/1024 for x in get_recursive_size(parse_result)]
    for sentence in parse_result.sentence_graphs:
            #pprint.pprint( get_recursive_items(sentence))
            #print "Sentence DG0: ",[x/1024 for x in get_recursive_size(sentence)]
            sentence = correct_dependecy_parse_errors(sentence)
            #print "Sentence DG1: ",[x/1024 for x in get_recursive_size(sentence)]
            mcg = parse_mood(sentence)
            #print "Mood MCG: ",[x/1024 for x in get_recursive_size(mcg)]
            parse_mood_post_enrichment(mcg,sentence)
            parse_transitivity_from_mood_graph(mcg,sentence)
            #print "Trans MCG: ",[x/1024 for x in get_recursive_size(mcg)]
            result.append(mcg)
            #print "Result Cum: ",[x/1024 for x in get_recursive_size(result)]
            
    #scanner.dump_all_objects("/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/evaluation/memory_dump.log")
    return result if not return_dep_graph else (result,parse_result.sentence_graphs)

###################################################################################################
#
###################################################################################################
# NP qualifiers interpretation
# Loc: in
# Dest: to
# So: from
# Pa: through
# whole, stuff: of,
# quality: with, 
# concern: on
# aim: for
# specification: of 
# function/role: as
# measure: of
##########################################################################################################
#
##########################################################################################################

if __name__ == '__main__':
    #mcg, dg = test_mood_graph(2, COOKING_HELA_EXAMPLE, True,True)
    #parse_transitivity_from_mood_graph(mcg,dg)
    pass
