'''

Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 20, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

this module implements binding and control theory with regards to splitting 
the sentence and the complex phrases into simple ones and enriching them
with the missing/implicit constituents.
'''
import logging

from parsimonious_vole.magic.guess.fs_constants import FINITE, FINITNESS, NON_FINITE, NON_FINITE_IMPERFECTIVE, NON_FINITE_PERFECTIVE
from parsimonious_vole.magic.guess.mood import C_TYPE, SUBJECT,\
    PREDICATOR, DEPENDECY_RELATION, CLAUSE,\
    COMPLEMENT, COMPLEMENT_ADJUNCT, COMPLEMENT_DATIVE, COMPLEMENT_AGENT,\
    QUALIFIER, THING

from parsimonious_vole.magic.guess.mood_pattern_matching import NODES, EDGES, insert_from_pattern

# cognitive
TWO_ROLE_COGNITION = "two role cognition"
THREE_ROLE_COGNITION = "three role cognition"

TWO_ROLE_PERCEPTION = "two role perception"
THREE_ROLE_PERCEPTION = "three role perception"

DESIDERATIVE = "desiderative"
EMOTIVE = "emotive"

#relational
ATTRIBUTIVE = "attributive"
DIRECTIONAL = "directional"

# influential
TENTATIVE = "tentative"
STARTING = "starting"
CONTINUING = "continuing"
CEASING = "ceasing"
SUCCEEDING = "succeeding"
FAILING = "failing"
CAUSATIVE = "causative" 
PERMISSIVE = "permiSsive"
ENABLING = "enabling"
PREVENTIVE = "preventive"
DELAYING = "delaying"



##########################################################################################
#
##########################################################################################



"""
---------------------------------Dependecy relations introducing clauses:-----------------------------------------------
advcl: adverbial clause modifier; a clause modifying the verb
ccomp: clausal complement; a clause that acts as VB or ADJ object (usually finite)
csubj: clausal subject; a clause acting as the sunject
csubjpass: passive clausal subject
parataxis: 
prepc: preposition clause modifying the meaning of VB, ADJ, or NN
puprpcl: a clause headed by "in order to", "to" (very similar with xcomp)
rcmod: a relative clause modifying the NP
partmod : participal modifier of NP or VB, is a participal verb modifying the meaning of a NP or VB/sentence 
xcomp: clausal complement without own subject; open clausal complement of a VP or ADJ

appos: is a clause missing the vrb to be  
poss: is a possesive clause without have
------------------

[A.Downing 2006] pp279

* Embedding 
    csubj, csubjpass, xcomp,
    ccomp # only if it can be assigned a semantic role, 
          # and dependent clause does not have "which" marker 

* Dependecy
 advcl, prepc, purpcl, ccomp (with no semantic role), 
 rcmod, partmod

#** relative clause (hypotactic elaboration supplimentative ) 
#    rcmod, ccomp (+which as nsubj or dobj preceding subj)
#    
#** nonfinite supplimentative clauses (hypotactic elaboration supplimentative i.e. specifying/commenting)

* Coordination (paratactic  expansion)
    conj (VB-conj-VB)

* Apposition (paratactic expansion)
    parataxis
"""

##########################################################################################
# Coordination Enrichement with Subject nodes
##########################################################################################
conj_fin_parent_subject_to_child ={NODES:{"cl1":({ C_TYPE:CLAUSE,FINITNESS:FINITE},None),
                                 "subj1":({C_TYPE:SUBJECT},None),
                                 "cl2":({C_TYPE:CLAUSE, FINITNESS:FINITE, DEPENDECY_RELATION:"conj"},None),
                                 "subj2!":({C_TYPE:SUBJECT},None),
                                 },
                         EDGES:[("cl1","subj1",None),("cl1","cl2",None), 
                                ("cl2","subj2",{"insert_reference":"subj1"}),  ]}

conj_fin_parent_complement_to_child ={NODES:{"cl1":({ C_TYPE:CLAUSE,FINITNESS:FINITE},None),
                                 "subj1":({C_TYPE:SUBJECT},None),
                                 "compl1":({C_TYPE:[COMPLEMENT]},None),
                                 "compl2!":({C_TYPE:[COMPLEMENT, COMPLEMENT_DATIVE,COMPLEMENT_ADJUNCT]},None),
                                 "cl2":({C_TYPE:CLAUSE,FINITNESS:FINITE, DEPENDECY_RELATION:"conj"},None),
                                 "compl3!":({C_TYPE:[COMPLEMENT, COMPLEMENT_ADJUNCT]},None),
                                 },
                         EDGES:[("cl1","subj1",None),("cl1","cl2",None), ("cl1","compl1",None), 
                                ("cl1","compl2",None), ("cl2","compl3",None),
                                ("cl2","compl3",{"insert_reference":"compl1"}),]}

def coordination_enrichment(mood_constituency_graph):
    """ add the empty subjects/objects for coordination cases """
    insert_from_pattern(mood_constituency_graph, conj_fin_parent_subject_to_child)
    insert_from_pattern(mood_constituency_graph, conj_fin_parent_complement_to_child)
    return 

##########################################################################################
# Complex Predicate split-enrichment, subject/object control 
##########################################################################################

#                               "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],
# #                                        POSSIBLE_PROCESS_TYPES:[TENTATIVE,STARTING, # either influential
# #                                                                CONTINUING, CEASING, 
# #                                                                SUCCEEDING, FAILING, 
# #                                                                CAUSATIVE,PERMISSIVE,
# #                                                                ENABLING, PREVENTIVE,
# #                                                                # or cognition/desiderative /emotive or directional
# #                                                                TWO_ROLE_COGNITION, EMOTIVE, DESIDERATIVE, DIRECTIONAL]


# Subject Control 
#SV + PRO(S)VO
compl_clause_SV_PROSVO = {NODES:{"cl":({C_TYPE:CLAUSE},None),
                              "subj":({C_TYPE:SUBJECT},None),
                              "compl":({C_TYPE:COMPLEMENT, 
                                        DEPENDECY_RELATION:["xcomp"]},None),
                               "compl1!":({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE], 
                                           DEPENDECY_RELATION:["dobj","iobj"],
                                           FINITNESS:[NON_FINITE,NON_FINITE_PERFECTIVE,NON_FINITE_IMPERFECTIVE],},None),
                              "cl2":({C_TYPE:CLAUSE, DEPENDECY_RELATION:["xcomp"] },None),
                              "pred2":({C_TYPE:PREDICATOR,},None), 
                              "subj2!" :({C_TYPE:SUBJECT},None),
                         },
                EDGES:[  ("cl","subj",None), ("cl","compl1",None),
                       ("cl","compl",None),("compl","cl2",None),
                       ("cl2","pred2",None), ("cl2","subj2",{"insert_reference":"subj"})]
                }

compl_clause_SVO_PROOVO = {NODES:{"cl":({C_TYPE:CLAUSE},None),
                              "subj":({C_TYPE:SUBJECT},None),
                              "compl":({C_TYPE:COMPLEMENT, 
                                        DEPENDECY_RELATION:["xcomp"]},None),
                              "compl1":({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE], 
                                        DEPENDECY_RELATION:["dobj","iobj"],
                                        },None),
                              "cl2":({C_TYPE:CLAUSE, 
                                      FINITNESS:[NON_FINITE,NON_FINITE_PERFECTIVE,NON_FINITE_IMPERFECTIVE],
                                      DEPENDECY_RELATION:["xcomp"], },None),
                              "pred2":({C_TYPE:PREDICATOR,},None), 
                              "subj2!" :({C_TYPE:SUBJECT},None),
                         },
                EDGES:[  ("cl","subj",None), ("cl","compl1",None),
                       ("cl","compl",None),("compl","cl2",None),
                       ("cl2","pred2",None), ("cl2","subj2",{"insert_reference":"compl1", "parameters":{C_TYPE:SUBJECT}})]
                }

compl_clause_SVO_PROSVO = None

# def enrich_with_possible_process_types(constituent_graph, sentence_dependecy_graph):
#     """
#         to each predicate in MCG add add feature: POSSIBLE_PROCESS_TYPES
#         #TODO: continue implementing patterns
#     """
#     clause_constituents = uniquify(filter(lambda x: x.type() is CLAUSE, constituent_graph.nodes()))
#     for i in clause_constituents:
#         predicate = filter(lambda x: x.type() is PREDICATOR or x.type() is PREDICATOR_FINITE, 
#                            map(lambda y: y[1], constituent_graph.out_edges(i)))
#         if not predicate:
#             print "WARNING: Clause without predicate?! something is terribly wrong"
#         else:    
#             verb_node = predicate[0].head_dependecy_node()
#             proc_type_list = uniquify(map(lambda x: x[2], list_of_possible_configurations(sentence_dependecy_graph, verb_node)))
#             predicate[0].append_feature_structure({POSSIBLE_PROCESS_TYPES:proc_type_list})
#     return constituent_graph

def complex_predicate_enrichment(mood_constituency_graph, dependecy_graph):
    #enrich_with_possible_process_types(mood_constituency_graph, dependecy_graph)
    insert_from_pattern(mood_constituency_graph,compl_clause_SV_PROSVO)
    insert_from_pattern(mood_constituency_graph,compl_clause_SVO_PROOVO)
    pass

##########################################################################################
#relative clause enrichment: partmod (only?) 
##########################################################################################

relative_clause_participalmod_subject_control = {NODES:{"constituent":({C_TYPE:[COMPLEMENT,
                                                                  SUBJECT,
                                                                  COMPLEMENT_ADJUNCT,
                                                                  COMPLEMENT_AGENT]},None),
                              "thing":({C_TYPE:THING},None),
                              "qualifier":({C_TYPE:QUALIFIER,DEPENDECY_RELATION:["partmod","infmod","vmod"]},None),
                              "clause":({C_TYPE:CLAUSE, DEPENDECY_RELATION:["partmod","infmod","vmod"]},
                                      None),
                              "subj!":({C_TYPE:SUBJECT},None),
                         },
                EDGES:[  ("constituent","qualifier",None), ("qualifier","clause",None),
                       ("constituent","thing",None),
                       ("clause","subj",{"insert_reference":"thing","parameters":{C_TYPE:SUBJECT}}),]
                }

def relative_clause_enrichment(mood_constituency_graph,dg):
    """ enrich the relative clauses with the constituent it modifies
    
        EG: Do you see the cat lying on the roof?
            I told you about the woman living next door.
    """
    insert_from_pattern(mood_constituency_graph, relative_clause_participalmod_subject_control)
    pass
##########################################################################################
# in case of prepc_ ... if dependent clause is nonfinite and has no subject then borrow subj/obj from parent
##########################################################################################

adverbial_clause_SV_PROSVO = {NODES:{"cl":({C_TYPE:CLAUSE},None),
                              "subj":({C_TYPE:SUBJECT},None),
                              "compl":({C_TYPE:COMPLEMENT_ADJUNCT, 
                                        DEPENDECY_RELATION:["prepc"]},None),
                               "compl1!":({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE], 
                                        DEPENDECY_RELATION:["dobj","iobj"]},None),
                              "cl2":({C_TYPE:CLAUSE,
                                      DEPENDECY_RELATION:["prepc"],
                                      FINITNESS:[NON_FINITE,NON_FINITE_PERFECTIVE,NON_FINITE_IMPERFECTIVE],
                                      },None),
                              "pred2":({C_TYPE:PREDICATOR,},None), 
                              "subj2!" :({C_TYPE:SUBJECT},None),
                         },
                EDGES:[  ("cl","subj",None), ("cl","compl1",None),
                       ("cl","compl",None),("compl","cl2",None),
                       ("cl2","pred2",None), ("cl2","subj2",{"insert_reference":"subj"})]
                }

adverbial_clause_SVO_PROOVO = {NODES:{"cl":({C_TYPE:CLAUSE},None),
                              "subj":({C_TYPE:SUBJECT},None),
                              "compl":({C_TYPE:COMPLEMENT_ADJUNCT, 
                                        DEPENDECY_RELATION:["prepc"]},None),
                               "compl1":({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE], 
                                        DEPENDECY_RELATION:["dobj","iobj"]},None),
                              "cl2":({C_TYPE:CLAUSE,
                                      DEPENDECY_RELATION:["prepc"],
                                      FINITNESS:[NON_FINITE,NON_FINITE_PERFECTIVE,NON_FINITE_IMPERFECTIVE],
                                      },None),
                              "pred2":({C_TYPE:PREDICATOR,},None), 
                              "subj2!" :({C_TYPE:SUBJECT},None),
                         },
                EDGES:[  ("cl","subj",None), ("cl","compl1",None),
                       ("cl","compl",None),("compl","cl2",None),
                       ("cl2","pred2",None), ("cl2","subj2",{"insert_reference":"compl1","parameters":{C_TYPE:SUBJECT}})]
                }

def adverbial_clause_introduced_by_prepc_enrichment(mood_constituency_graph,dg):
    insert_from_pattern(mood_constituency_graph, adverbial_clause_SV_PROSVO)

##########################################################################################
#
##########################################################################################

def parse_mood_post_enrichment(mood_constituency_graph,dg):
    """
    enrichment with new constituents is guided by subject/object control and consist of inserting
    reference to subject or object of the dominant clause 
    """
    try:
        #enrich_with_possible_process_types(mood_constituency_graph, dg)
        coordination_enrichment(mood_constituency_graph)
        complex_predicate_enrichment(mood_constituency_graph, dg)
        relative_clause_enrichment(mood_constituency_graph,dg)
        adverbial_clause_introduced_by_prepc_enrichment(mood_constituency_graph,dg)
    except: 
        logging.error("Post mood parse enrichment failed, the most likely MCG was faulty.")

if __name__ == '__main__':
    pass