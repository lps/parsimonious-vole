'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 11, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import logging

from parsimonious_vole.magic.guess.fs_constants import NEGATIVE, POLARITY, POSITIVE, ACTIVE, VOICE, PASSIVE, FINITE, FINITNESS,\
    NON_FINITE_IMPERFECTIVE, NON_FINITE_PERFECTIVE
from parsimonious_vole.magic.guess.pattern_matching import GraphMatcher, get_dependent_tokens,\
    get_dominant_tokens
from parsimonious_vole.magic.utils import eattr, natsort
from nltk.featstruct import FeatDict

from parsimonious_vole.magic.guess.dependency_constants import *

####################### Polarity #######################
negated_verb_link = [(VB, RB, eattr(neg))]
negated_copular_verb_link = [("/", VB, eattr(cop)), ("/", RB, eattr(neg))]  # negated copular verb, usually from JJ, but not sure
negative_polarity_patterns = [negated_copular_verb_link, negated_verb_link]
polarity_negative = FeatDict({POLARITY:NEGATIVE})
polarity_positive = FeatDict({POLARITY:POSITIVE})

####################### Voice ####################### 
auxpass_verb_link = [(VB, VB, eattr(auxpass))]
nsubjpass_link = [(VB, "/", eattr(nsubjpass))]  # noun or pronoun
csubjpass_link = [(VB, "VB", eattr(csubjpass))]
agent_link = [(VB, "/", eattr(agent))]

passive_voice_patterns = [auxpass_verb_link, nsubjpass_link, csubjpass_link, agent_link]
voice_active = FeatDict({VOICE:ACTIVE})
voice_passive = FeatDict({VOICE:PASSIVE})

##################### Finite check #######################
####################################################

def parse_voice_and_polarity(sentence, verb):
    sentence.interpret_patterns_disjunctively(negative_polarity_patterns, polarity_negative, polarity_positive) 
    sentence.interpret_patterns_disjunctively(passive_voice_patterns, voice_passive, voice_active)
    return True

def polarity_check(graph, verb_node):
    """ checks whether there is any dependecy link to a negation token or 
    whether the dominating copulative node has a link to a negation token"""
    if "VB" not in verb_node.pos: logging.error("During polarity checking, verb Node "+str(verb_node)+" NOT a verb?")
    fs, neg = None, None
    # check if it not a copular construction
    cop_node=get_dominant_tokens(graph, verb_node, ["cop"])
    if cop_node:
        neg = get_dependent_tokens(graph, cop_node[0][0], ["neg"])
    else:
        neg = get_dependent_tokens(graph, verb_node, ["neg"])
    if neg: fs = FeatDict({POLARITY:NEGATIVE})
    else: fs = FeatDict({POLARITY:POSITIVE})
    verb_node.unify_or_append(fs)
    return True

def get_finite_and_feature_selection(graph, verb_node):
    """ 
    append features to the node feature structure 
    if the verb is finite or has an auxiliary that is finite then it is finite
    otherwise it is non-finite, perfective or imperfective
    
    returns the finiteness deciding verb
    """
    if not verb_node: return None,None
    
    if "VB" not in verb_node.pos: 
        logging.error("verb Node "+str(verb_node)+" NOT a verb? ")
        return None,None
    #check if it is an aux verb
    if get_dominant_tokens(graph, verb_node, ["aux"]): 
        logging.warn("Waning, "+str(verb_node)+" is an auxiliary verb and is ignored") 
        return None,None
    #
    #fs = None
    finite_pos = ["VBD","VBP","VBZ","MD"]
    non_fin_perf = ["VB","VBN"]
    non_fin_imperf = ["VBG"]
    aux_verbs = None
    deciding_verb = verb_node
    # check if it not a copular construction # BUT copulas have been transformed into normal graphs
    cop_node=get_dominant_tokens(graph, verb_node, ["cop"])
    if cop_node:
        aux_verbs = filter(lambda x: "vb" in x.pos.lower() or "md" in x.pos.lower(),
                       map(lambda x: x[1], get_dependent_tokens(graph, cop_node[0][0], ["aux"])))
    else:
        aux_verbs = filter(lambda x: "vb" in x.pos.lower() or "md" in x.pos.lower() ,
                           map(lambda x: x[1], get_dependent_tokens(graph, verb_node, ["aux"])))
    if aux_verbs:
        deciding_verb = natsort( aux_verbs )[0]
    #print "verb is ",verb_node, "and deciding is ",deciding_verb
    feature_selection = []
    if deciding_verb.pos in finite_pos:
        fs = FeatDict({FINITNESS:FINITE})
        feature_selection = ["finite"]
    elif deciding_verb.pos in non_fin_imperf:
        fs = FeatDict({FINITNESS:NON_FINITE_IMPERFECTIVE})
        feature_selection = ["imperfective"]
    elif deciding_verb.pos in non_fin_perf:
        fs = FeatDict({FINITNESS:NON_FINITE_PERFECTIVE})
        feature_selection = ["perfective"]
    else: logging.error("Should never be here ..."+str(verb_node)+" "+ str(verb_node.pos))
    verb_node.unify_or_append(fs)
    return deciding_verb, feature_selection


def is_finite_dg_verb_node(dg_verb_node):
    """ check whether a dependecy graph node has the feature structure finiteness:finite"""
    try:
        if dg_verb_node.feature_structure()[FINITNESS] == FINITE:
            return True
        else: return False
    except KeyError:
        return None


##############################################################
# methods returning a set of feature selections instead of enriching
# the dependency graph node with feature structure
##############################################################
def get_voice_feature_selection(graph, verb):
    mtch = GraphMatcher(graph)
    if mtch.contains_patterns_any(passive_voice_patterns):
        return ["passive"]
    else: return ["active"]

def get_polarity_feature_selection_by_marker(graph, verb):
    mtch = GraphMatcher(graph)
    if mtch.contains_patterns_any(negative_polarity_patterns):
        return ["negative"]
    else: return ["positive"]

##############################################################

if __name__ == '__main__':
    pass