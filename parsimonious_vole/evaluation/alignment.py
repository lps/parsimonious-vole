'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jan 28, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

from __future__ import division

import logging
import os
import pprint

from parsimonious_vole.magic.guess.mcg_utils import feature_selection_generator, clause_less_boundaries, \
    is_feature_selected
from parsimonious_vole.magic.guess.mood import ReferenceConstituent
from parsimonious_vole.magic.guess.pattern_matching import root_node
from parsimonious_vole.magic.utils import flatten, list_minus, natsort, uniquify,  filter_out_keys, any_los_in_los
from parsimonious_vole.uam_corpus_tool_interface.io.analisys_annotation import read_text_UAM_style, SegmentBundle, index_substring_words, \
    Segment, serialize_segment_bundle, read_annotation_file
from parsimonious_vole.uam_corpus_tool_interface.io.network_scheme import feature_path_to_string_list
from parsimonious_vole.usage.bundle_to_html import output_analisys_of_parse_bundle_to_file

from parsimonious_vole.magic.guess.transitivity import parse_transitivity_txt_file


###################################################################################################
# Parses a Txt/stp/xml file and provides a bunch of segments for the MCG parse 
###################################################################################################
class TextAutoAnnotator(object): 
    """ 
        Generates Segments for a text file based on the MCG bundle.
        makes available, Sentence, word and constituent Segments 
    """
    __fileName = None
    __mcg_bundle = None
    sent_segments = None
    word_segments = None
    constituent_segments = None
    
    def __init__(self, fileName):
        self.__fileName = fileName
        self.raw_txt = read_text_UAM_style(self.__fileName)
        self.__parse_file_mcg()
        self.__index_mcg_words_and_sents()
        self.__index_constituents()
        self.generate_html_analisys()

    def __parse_file_mcg(self):
        """ parses the file and generates mcg bundle """
        self.__mcg_bundle = filter(None, parse_transitivity_txt_file(self.__fileName, False))
        if not self.__mcg_bundle: raise Exception("No MCG graphs generated from txt/stp file")
        
    def __index_mcg_words_and_sents(self):
        """ """
        self.word_segments = SegmentBundle()
        self.sent_segments = SegmentBundle()
        offset = 0
        for current_mcg in self.__mcg_bundle:
            mcg_words = filter(None, root_node(current_mcg).words())
            if not mcg_words: continue 
            word_segm, not_found = index_substring_words(self.raw_txt, mcg_words, offset, True)
            self.word_segments.extend(word_segm)
            sent_start, sent_end = word_segm[0].start, word_segm[-1].end 
            sent_txt = self.raw_txt[sent_start:sent_end]
            self.sent_segments.append(Segment(sent_start, sent_end, sent_start - 1, "senetence", sent_txt))
            if not_found : logging.warn("Not found words in sentence:" + str(not_found) + " : " + sent_txt)
            offset = sent_end
            
    def __index_constituents(self):
        
        ####
        self.constituent_segments = SegmentBundle()
        for midx in range(len(self.__mcg_bundle)):
            clause_word_segments = self.word_segments.contained(self.sent_segments[midx])
            root_constit_words = root_node(self.__mcg_bundle[midx]).nodes()
            if len(clause_word_segments) != len(root_constit_words):
                logging.warn("Mismatch of Words and segments: #word segments(%s) != #words(%s).\nList of words: %s.\nList of segments: %s"\
                              % (str(len(clause_word_segments)), \
                                 str(len(root_constit_words)), \
                                 str(root_constit_words), \
                                 pprint.pformat(clause_word_segments))) 
            # text index
            token_segment_index = { k:clause_word_segments[i] for i, k in enumerate(natsort(root_constit_words))}
            #
            for const in self.__mcg_bundle[midx].nodes():
                # getting boundaries without direct child clause and without ref const
                if isinstance(const, ReferenceConstituent) or is_feature_selected(const, "root") : continue  # just skip Ref Constts and ROOT constit
                widx_low, widx_hi = clause_less_boundaries(const, self.__mcg_bundle[midx], False)
                const_start, const_end = token_segment_index[widx_low].start, token_segment_index[widx_hi].end

                features = uniquify(flatten([feature_selection_generator(const, x) for x in \
                                             filter_out_keys(const.feature_structure())]))
                self.constituent_segments.append(Segment(const_start, const_end, \
                                                         const_start, features, self.raw_txt[const_start:const_end]))
    def generate_html_analisys(self):
        """ """
        if not os.path.exists(self.__fileName + ".html/parse_analysis.html"):
            output_analisys_of_parse_bundle_to_file(self.__fileName + ".html/parse_analysis.html", self.__fileName + ".stp")
    
    def serialize(self, output_file=None):
        with open(output_file if output_file else self.__fileName + ".mcg.xml", "wb") as fl:
            fl.write(serialize_segment_bundle(self.constituent_segments))
        with open(output_file if output_file else self.__fileName + ".words.xml", "wb") as fl:
            fl.write(serialize_segment_bundle(self.word_segments))
        with open(output_file if output_file else self.__fileName + ".sents.xml", "wb") as fl:
            fl.write(serialize_segment_bundle(self.sent_segments))

def text_auto_annotation_bundle(text_filename, bom_adjust=True):
    """ returns segment bundle for text file """
    if not os.path.exists(text_filename + ".mcg.xml"):
        logging.info("Annotating the Text File %s" % os.path.basename(text_filename))
        ta = TextAutoAnnotator(text_filename)
        ta.serialize()
        return ta.constituent_segments
    return read_annotation_file(text_filename + ".mcg.xml", text_filename, False)

###################################################################################################
# 
###################################################################################################   
class SegmentEvaluator(object):
    """
        for each segment in ...
    """
    __features_lambdas = None
    __text_pos_lambdas = None
    __distance_matrix_geometric = None
    __manual_set = None
    __automatic_set = None
    __feature_dictionnary = None
    __systemic_network_auto = None
    
    def __init__(self, manual_annotation_set, automatic_evaluation_set,
                 feature_dictionnary={},
                 text_pos_lambdas=[], features_lambdas=[], systemic_network_auto=None):
        """ 
            * feature_dictionnary making the link between the features used in man sys. network 
                and features in auto sys. network
            * lambdas are the comparison functions telling if two segments are same or not 
            * systemic_network_auto the system network that was used in auto annotation """
        assert manual_annotation_set and automatic_evaluation_set
        self.__manual_set = manual_annotation_set
        self.__automatic_set = automatic_evaluation_set
        self.__text_pos_lambdas = text_pos_lambdas
        self.__features_lambdas = features_lambdas
        self.__feature_dictionnary = feature_dictionnary  # filter out the segments according to this dict, also generate interagreement labels based on this dict
        self.__systemic_network_auto = systemic_network_auto
        
        self.__generate_matches()
    
    def __generate_matches(self):
        """
            TODO: when reducing auto-segment allow/copy multiple possible choices 
            (like in case of transitivity, or adjunct/complement) 
        """
        
        if not self.__feature_dictionnary:
            # evaluate against entire sys network, i.e. create the identity dict with all features from network
            if self.__systemic_network_auto:
                self.__feature_dictionnary = {x:x for x in uniquify(flatten([feature_path_to_string_list(x) for x in self.__systemic_network_auto.feature_paths()]))}
            # system unknown then identity dict over all used features in manual set 
            else:
                for i in self.__manual_set:
                    for j in i.features: self.__feature_dictionnary[j] = j
        
    def feature_based_statistics(self):
        """ 
        1. read a systemic network to figure out the grouppings of features based on system
        2. iteratively simiplify the dataset based on the systemic feature sets
        """
        # TODO: implement auto network detection
        res = [("Name", "Man N", "Man %", "Auto N", "Auto %", "Precision", "Recall", "F1")]
        if self.__systemic_network_auto is None: return res
        mmm, aaa = simplified_segments(self.__manual_set, self.__automatic_set,  self.__feature_dictionnary)
        #Total segments
        m,a = simplified_segments(self.__manual_set, self.__automatic_set, self.__feature_dictionnary)
        matches = stable_matching_segments(a, m, self.__text_pos_lambdas, self.__features_lambdas)
        res.append((None,None))
        res.append( ("TOTAL", len(m), None, len(a), None,
                               precission(matches), recall(matches), f1(matches)) )
        #
        for ss in list(self.__systemic_network_auto.system_iterator())[::-1]:
            # simplify the mapping dict reduced to each system
            dd = {k:v for k, v in self.__feature_dictionnary.iteritems() if any_los_in_los([x.name for x in ss.feature_list], v)}
            msb, asb = simplified_segments(self.__manual_set, self.__automatic_set, dd)
            if not msb or not asb: continue
            # system stat 
            matches = stable_matching_segments(asb, msb, self.__text_pos_lambdas, self.__features_lambdas)
            res.append((None,None))
            res.append((ss.name, len(msb), len(msb) / len(mmm) * 100, len(asb), len(asb) / len(aaa) * 100,
                           precission(matches), recall(matches), f1(matches)))
            # for each feature stats
            for f in ss.feature_list:
                dd = {k:v for k, v in self.__feature_dictionnary.iteritems() if any_los_in_los(f.name, v)}
                m,a = simplified_segments(self.__manual_set, self.__automatic_set, dd)
                matches = stable_matching_segments(a, m, self.__text_pos_lambdas, self.__features_lambdas)
                res.append( (f.name, len(m), len(m) / len(msb) * 100, len(a), len(a) / len(asb) * 100,
                               precission(matches), recall(matches), f1(matches)) )
        # adding the feature mappings
        res.append((None,None))
        res.append(("Mann <=> Auto Mapping",None))
        for k,v in self.__feature_dictionnary.iteritems():
            res.append((k,", ".join(v)))
        #
        return res
    
    def generate_csv_rows(self, include_standard_test_results=False):
        """
            generates csv rows where the manual and automatic annotations are sided in first columns. 
            in the next columns the text and the features are compared.
            
            *for text and feature comparison purposes, lambda functions are used.
            If no lambda function provided then the standard(==) identity is used for text
            and standard set inclusion is used for features. 
            Both, standard and lambda matches are provided.
            
        """
        
        headder_match = ["Man Id", "Dist. Geometric", "Man Interval", "Auto Interval", \
                         "Dist. Edit", "Man Text", "Auto Text", "Man Features", "Auto Features"]
        headder_non_match = ["Id", "Interval", "Text", "Features"]
        result = []
        # simplifying first
        manual_set, automatic_set = simplified_segments(self.__manual_set,
                                                                      self.__automatic_set,
                                                                      self.__feature_dictionnary)
        # generating matching pairs
        self.__segment_matches_strict = stable_matching_segments(automatic_set,
                                                          manual_set)
        
        self.__segment_matches_lambas = stable_matching_segments(automatic_set,
                                                          manual_set,
                                                          self.__text_pos_lambdas,
                                                          self.__features_lambdas)
        # ##
        result.append(("", "", "Relaxed/Lambda Match Statistics"))
        result.append(("", "Precission", precission(self.__segment_matches_lambas)))
        result.append(("", "Recall", recall(self.__segment_matches_lambas)))
        result.append(("", "F1", f1(self.__segment_matches_lambas)))
        result.append(("", ""))
        result.append(("", "Relaxed/Lambda Matches (True Positives)"))
        result.append(headder_match)
        for pair in [x for x in self.__segment_matches_lambas if x[0] and x[1]]:
            result.append((pair[1].id, pair[0].distance_geometric(pair[1]), \
                           "%i, %i" % (pair[1].start, pair[1].end), "%i, %i" % (pair[0].start, pair[0].end), \
                           pair[0].distance_edit(pair[1]), pair[1].text, pair[0].text, \
                           ", ".join(pair[1].features), ", ".join(pair[0].features)))
        result.append(("", ""))
        result.append(("", "Manual mismatching all parsed (False Negative)"))
        for pair in [x for x in self.__segment_matches_lambas if not x[0] and x[1]]:
            result.append((pair[1].id, "%i, %i" % (pair[1].start, pair[1].end), pair[1].text, ", ".join(pair[1].features)))
        result.append(("", ""))
        result.append(("", "Parsed mismatching all manual (False Positives)"))
        for pair in [x for x in self.__segment_matches_lambas if x[0] and not x[1]]:
            result.append((pair[0].id, "%i, %i" % (pair[0].start, pair[0].end), pair[0].text, ", ".join(pair[0].features)))
        ####

        if include_standard_test_results:
            result.append(("", ""))
            result.append(("", "", "Strict Match Statistics", "Relaxed/Lambda Match Statistics"))
            result.append(("", "Precission", precission(self.__segment_matches_strict),))
            result.append(("", "Recall", recall(self.__segment_matches_strict),))
            result.append(("", "F1", f1(self.__segment_matches_strict),))
            result.append(("", ""))
            result.append(("", "Strict Matches (True Positives)"))
            result.append(headder_match)
            for pair in [x for x in self.__segment_matches_strict if x[0] and x[1]]:
                result.append((pair[1].id, pair[0].distance_geometric(pair[1]), \
                           "%i, %i" % (pair[1].start, pair[1].end), "%i, %i" % (pair[0].start, pair[0].end), \
                           pair[0].distance_edit(pair[1]), pair[1].text, pair[0].text, \
                           ", ".join(pair[1].features), ", ".join(pair[0].features)))
            result.append(("", ""))
            result.append(("", "Manual mismatching all parsed (False Negative)"))
            result.append(headder_non_match)
            for pair in [x for x in self.__segment_matches_strict if not x[0] and x[1]]:
                result.append((pair[1].id, "%i, %i" % (pair[1].start, pair[1].end), pair[1].text, ", ".join(pair[1].features)))
            result.append(("", ""))
            result.append(("", "Parsed mismatching all manual (False Positives)"))
            result.append(headder_non_match)
            for pair in [x for x in self.__segment_matches_strict if x[0] and not x[1]]:
                result.append((pair[0].id, "%i, %i" % (pair[0].start, pair[0].end), pair[0].text, ", ".join(pair[0].features)))
        ####
        return result

# def generate_via_dictionarry_feat_match_lambda_function(dictionary):
#     """
#         @obsolete (maybe). it is compensated by simplification step in the Segment Evaluator  
#         
#         returns a function that takes only 2 arguments and incorporates the translation ddict
#         
#         attempts to match two feature sets via translation dictionary
#         Example:
#         d = {'config': ['figure', 'clause'],
#          'main': ['predicator', 'predicator-finite'],
#          'partic': ['complement', 'complment-direct', 'participant-role']}
#         
#         feat_man = ["main","verb","transitivity","bla bal"]
#         feat_auto = ["predicator","verb-group"]
#         
#         feat_man1 = ["main","config","transitivity","bla bal"]
#         
#         print feat_match(feat_man,feat_auto,d) # True
#         print feat_match(feat_man1,feat_auto,d) # False
#     """
#     def the_function(man_segment,auto_segment):
#         ddict = dictionary
#         man_feats, auto_feats = man_segment.features, auto_segment.features 
#         kk = [x for x in man_feats if x in ddict.keys()]
#         return all(any(dk in auto_feats for dk in ddict[k]) for k in kk)
#     return the_function


def reverse_feature_mapping_dict(fm_dict):
    """ reverses the feature mapping from values to keys """
    d = {}
    for k,v in fm_dict.iteritems():
        vv = v if isinstance(v, list) else [v] 
        for i in vv: d[i]=[k] if i not in d else d[i]+[k]
    return d

def simplified_segments(mann_bundle, auto_bundle, ddict):
    """ 
        returns a bundle of manual and a bundle of auto segments that have their feature structures simplified
        to one feature according to ddict.
        * if several features are detected, then created a new segment for each feature present
    """
    ms_result = SegmentBundle()
    as_result = SegmentBundle()
    for mann_segment in mann_bundle[:]:
        for k in uniquify([x for x in mann_segment.features if x in ddict.keys()]):
            m = mann_segment.copy()
            m.features = [k]
            ms_result.append(m)
           
    for auto_segment in auto_bundle[:]:
        for k in uniquify([key for key, value in ddict.iteritems() if \
                any_los_in_los(value, auto_segment.features)]):
            a = auto_segment.copy()
            a.features = [k]
            as_result.append(a)
    
    return uniquify(ms_result), uniquify(as_result)

def stable_matching_segments(_parsed, _correct, pos_text_lambdas=[], features_lambdas=[]):
    """
        * adapted Implementation of Gale-Shapley algorithm.; Solves Stable Marriage problem 
            but with partially expressed preferences 
        * returns a list of segment pairs [(b1_seg1,b2_seg2)].
        * If there are segments in _parsed or _correct that have not been matched then they are 
        provided as matching None of the segments in _parsed [(None,b2_seg3)] or _correct [(b1_seg2,None)]
    """
    # computing distances_p {s_p:[(dist1,s1),(dist2,s2)]}
    
    distances_p, distances_c, parsed, correct = {}, {}, _parsed[:], _correct[:]
    matched, parsed_non_matching, parsed_match_attempts = [], [], {x:[] for x in parsed}  # (parsed_seg,correct_seg)
    # correct_non_matching = []
    for p in parsed:  # calculating distances_p parsed-correct
        distances_p[p] = sorted([x for x in _correct if p.match_features(x, features_lambdas) and p.match_text_position(x, pos_text_lambdas)], key=lambda x: x.distance_geometric(p))
    for c in _correct:  # calculating distances_p correct-parsed
        distances_c[c] = sorted([x for x in _parsed if c.match_features(x, features_lambdas) and c.match_text_position(x, pos_text_lambdas)], key=lambda x: x.distance_geometric(c))

    while parsed:
        p = parsed[0]
        unatempted_c = list_minus(distances_p[p], parsed_match_attempts[p]) if p in parsed_match_attempts else distances_p[p] 
        if unatempted_c:
            c = unatempted_c[0]
            parsed_match_attempts[p].append(c)
            if c not in [x[1] for x in matched]:  # if c is free
                matched.append((p, c))
                parsed.remove(p)
            else:  # else if c is matched already
                # get the matched p. 
                pp = [x[0] for x in matched if x[1] == c][0]
                # if distances_c[c].index(p) == -1 or distances_c[c].index(pp) == -1: logging.warn("How can it be?!")
                if distances_c[c].index(p) < distances_c[c].index(pp): 
                    # break the previous match, and create new match
                    matched.remove((pp, c))
                    parsed.append(pp)
                    matched.append((p, c))  # eventually should reinitiate match attempts for pp
                    parsed.remove(p)
        else:
            parsed_non_matching.append(p)
            parsed.remove(p)
    # adding the rest of non matched elements
    matched_c = [y[1] for y in matched] 
    correct_non_matching = list_minus(correct, matched_c)
    matched.extend((None, x) for x in correct_non_matching)
    matched.extend((x, None) for x in parsed_non_matching)  
    #
    return matched 

######################################################################################
# Statistical metrics
######################################################################################
def precission(aligned_tuples):
    try:
        true_positives = len([x for x in aligned_tuples if x[0] and x[1]])
        false_positive = len([x for x in aligned_tuples if x[0] and not x[1]])
        return true_positives / (false_positive + true_positives)
    except:
        return -1

def recall(aligned_tuples):
    try:
        true_positives = len([x for x in aligned_tuples if x[0] and x[1]])
        false_negative = len([x for x in aligned_tuples if not x[0] and x[1]])
        return true_positives / (false_negative + true_positives)
    except:
        return -1

def f1(aligned_tuples):
    try:
        p = precission(aligned_tuples)
        r = recall(aligned_tuples)
        return 2 * p * r / (p + r)
    except:
        return -1

if __name__ == '__main__':
    pass
