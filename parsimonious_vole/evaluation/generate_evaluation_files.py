'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 14, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import csv
import logging
import os

import numpy
from parsimonious_vole.evaluation.alignment import SegmentEvaluator, text_auto_annotation_bundle
from parsimonious_vole.magic.guess.system_network_mappings import SYSTEMIC_NETWORK_CONSTITUENCY, SYSTEMIC_NETWORK_TRANSITIVITY,\
    SYSTEMIC_NETWORK_MOOD
from parsimonious_vole.magic.utils import write_to_csv, natsort, list_minus

from parsimonious_vole.configuration import UAM_BTC_PROJECT, UAM_OE_CLEAN_BIG_PROJECT, UAM_ELA_OCD_PROJECT, PATH_TO_PROJECT
from parsimonious_vole.uam_corpus_tool_interface.io.projects_ctpr import read_CTPR_project
from parsimonious_vole.uam_corpus_tool_interface.io.analisys_annotation import read_annotation_file, read_text_UAM_style,\
 adjust_segment_position_in_context, SegmentBundle


def evaluate_file_bundle(file_pairs, feature_matching_dict={},
                         text_lambdas=[], feature_lambdas=[],
                         annotation_offset=0, csv_suffix="", syst_network=None, overwrite=False):
    """
        generates the csv and the html analysis  
    """
    if not file_pairs: 
        logging.info("*** No file bundle to evaluate")
        return 
    logging.info("*** Evaluating the file bundle")
    for txt, xml in file_pairs.items():
        logging.info("*** Processing %s and %s" % (os.path.basename(txt), str([os.path.basename(x) for x in xml] if isinstance(xml, list) else os.path.basename(xml))))
        if not xml: continue
        if os.path.exists(txt + "_" + csv_suffix + ".csv") and not overwrite: continue
        # TODO: what is xml is a list of files?
        man = read_annotation_file(xml, txt, True)
        man.shift_index(annotation_offset)
        auto = text_auto_annotation_bundle(txt, True)
        
        se = SegmentEvaluator(man,
                              auto,
                              feature_matching_dict,
                              text_lambdas,
                              feature_lambdas, syst_network)
        write_to_csv(txt + "_" + csv_suffix + ".csv", se.generate_csv_rows(include_standard_test_results=False))
        write_to_csv(txt + "_" + csv_suffix + "_features.csv", se.feature_based_statistics())
        logging.info("*** Done processing and generating CSV results.")

def merge_feature_csvs(file_list, output):
    """ merges the csv files with detaile-feature-based 
        evaluations of different data sets and generates
         a summary(common overview) """
    def calculate_totals(row_name, all_files, file_order):
        """ """ 
        target_rows = []  # rows of TOTALs
        result_row = tuple()
        for fn in file_order:
            for row in all_files[fn]:
                if not row or not row[0]: continue
                if row_name in row[0]: 
                    target_rows.append(row)
                    result_row += tuple(row[1:])
                    break

        # return total stats and the rest of the row with all values
        return (row_name,
                  sum(map(float, [x[1] for x in target_rows if x and x[1] and float(x[1]) > 0 ])),  # Man N
                  numpy.mean(map(float, [x[2] for x in target_rows if x and x[2] and float(x[2]) > 0])),
                  numpy.std(map(float, [x[2] for x in target_rows if x and x[2] and float(x[2]) > 0])),
                  sum(map(float, [x[3] for x in target_rows if x and x[3] and float(x[3]) > 0])),  # Auto N
                  numpy.mean(map(float, [x[4] for x in target_rows if x and x[4] and float(x[4]) > 0])),
                  numpy.std(map(float, [x[4] for x in target_rows if x and x[4] and float(x[4]) > 0])),
                  numpy.mean(map(float, [x[5] for x in target_rows if x and x[5] and float(x[5]) > 0])),
                  numpy.std(map(float, [x[5] for x in target_rows if x and x[5] and float(x[5]) > 0])),
                  numpy.mean(map(float, [x[6] for x in target_rows if x and x[6] and float(x[6]) > 0])),
                  numpy.std(map(float, [x[6] for x in target_rows if x and x[6] and float(x[6]) > 0])),
                  numpy.mean(map(float, [x[7] for x in target_rows if x and x[7] and float(x[7]) > 0])),
                  numpy.std(map(float, [x[7] for x in target_rows if x and x[7] and float(x[7]) > 0])),
                  ) + result_row
    ###############
    result = []
    all_files = {}
    # reading all_files 
    for fl in file_list:
        if not os.path.exists(fl):
            logging.warn("File %s does not exist, skipping it." % os.path.basename(fl))
            continue
        with open(fl) as f:
            r = csv.reader(f)
            for row in r:
                if row and "<=>" in row[0]: break
                if fl in all_files: all_files[fl].append(row)
                else:  all_files[fl] = [row]

    # print all_files
    file_headder = tuple(all_files[all_files.keys()[0]][0])
    file_order = natsort(all_files.keys())
    totals_headder = ("Man N", "Avg Man %", "Std Dev",
                      "Auto N", "Avg Auto %", "Std Dev",
                              "Avg Precission", "Std Dev",
                              "Avg Recall", "Std Dev",
                              "Avg F1", "Std Dev",)
    result.append(("Name",) + totals_headder + file_headder[1:] * len(file_order))
    # result.append( (None,)+totals_headder+tuple(None*len(file_headder))*len(file_order) )
    
    # adding file names to the header
    t = (None,) + (None,) * (len(totals_headder))
    for x in file_order: t += (os.path.basename(x),) + (None,) * (len(file_headder) - 2)
    result.append(t)
    #
    for row in all_files[all_files.keys()[0]]:
        if row and row[0] and "Name" not in row[0]:
            result.append(calculate_totals(row[0], all_files, file_order))
        elif row and not row[0]:
            result.append((None, None))
    
    # write to file
    logging.info("Writing sumamry statistics to %s" % output)
    with open(output, "w") as f:
        wr = csv.writer(f)
        wr.writerows(result)
        
#############################################################################################
TRANSITIVITY_DICT_ANKE = {
        # participant roles
        u'affected' : [u'affected', u'affected-source', u'affected-perceiver', u'affected-emoter',
                       u'affected-possessed', u'affected-cognizant', u'affected-path', u'affected-carrier',
                       u'affected-destination'] ,
        u'affected-carrier' : [u'affected-carrier', u'affected', u'carrier'] ,
        u'affected-cognizant' : [u'affected', u'cognizant', u'affected-cognizant'] ,
        u'affected-destination' : [u'destination', u'affected', u'affected-destination'] ,
        u'affected-emoter' : [u'affected', u'affected-emoter', u'emoter'] ,
        u'affected-path' : [u'affected-path', u'affected', u'path'] ,
        u'affected-perceiver' : [u'affected-perceiver', u'perceiver', u'affected'] ,
        u'affected-possessed' : [u'possessed', u'affected', u'affected-possessed'] ,
        u'affected-source' : [u'source', u'affected', u'affected-source'] ,
        u'agent' : [u'agent-perceiver', u'agent-cognizant', u'agent-carrier', u'agent'] ,
        u'agent-carrier' : [u'carrier', u'agent-carrier', u'agent'] ,
        u'agent-cognizant' : [u'agent-cognizant', u'cognizant', u'agent'] ,
        u'agent-perceiver' : [u'perceiver', u'agent', u'agent-perceiver'] ,
        u'attribute' : [u'attribute'] ,
        u'carrier' : [u'carrier', u'affected-carrier', u'agent-carrier'] ,
        u'cognizant' : [u'agent-cognizant', u'affected-cognizant', u'cognizant'] ,
        u'created' : [u'created-phenomenon', u'created'] ,
        u'created-phenomenon' : [u'created-phenomenon', u'phenomenon', u'created'] ,
        u'destination' : [u'destination', u'affected-destination'] ,
        u'emoter' : [u'affected-emoter', u'emoter'] ,
        u'location' : [u'location'] ,
        u'manner' : [u'manner'] ,
        u'matchee' : [u'matchee'] ,
        u'path' : [u'path', u'affected-path'] ,
        u'perceiver' : [u'affected-perceiver', u'perceiver', u'agent-perceiver'] ,
        u'phenomenon' : [u'created-phenomenon', u'phenomenon'] ,
        u'possessed' : [u'possessed', u'affected-possessed'] ,
        u'range' : [u'range'] ,
        u'source' : [u'source', u'affected-source'] ,
        # configuration types 
        u'action': [u'action'],
        u'mental': [u'mental'],
        u'attributive': [u'attributive'],
        u'relational': [u'relational'],
        u'influential': [u'influential'],
        u'environmental': [u'environmental'],
        u'event-relating': [u'event-relating'],
        # predicate detection & clause
        u'main': [u'process'],
        u'process': [u'configuration'],
        # detailed configuration types
        u'attributive': [u'attributive'],
        u'causative': [u'causative'],
        u'ceasing': [u'ceasing'],
        u'continuing': [u'continuing'],
        u'delaying': [u'delaying'],
        u'desiderative': [u'desiderative'],
        u'directional': [u'directional'],
        u'emotive': [u'emotive'],
        u'enabling': [u'enabling'],
        u'environmental': [u'environmental'],
        u'event-relating': [u'event-relating'],
        u'failing': [u'failing'],
        u'locational': [u'locational'],
        u'matching': [u'matching'],
        u'one-role-action': [u'one-role-action'],
        u'permissive': [u'permissive'],
        u'possessive': [u'possessive'],
        u'preventive': [u'preventive'],
        u'starting': [u'starting'],
        u'succeeding': [u'succeeding'],
        u'tentative': [u'tentative'],
        u'three-role-action': [u'three-role-action'],
        u'three-role-cognition': [u'three-role-cognition'],
        u'three-role-perception': [u'three-role-perception'],
        u'two-role-action': [u'two-role-action'],
        u'two-role-cognition': [u'two-role-cognition'],
        u'two-role-perception': [u'two-role-perception']
        # 
        }

CONSTITUENCY_DICT_ANKE = {"main":["predicator"],
                          "configuration":["clause"],
                          "participant-role":["subject", "complement-direct", "complement-indirect"]}

ELA_CONSTITUENCY_DICT = {
                        "clause":"clause",
                        "marker":"marker",
                        "group":"group",
                        "finite":"finite",
                        "adjunct":"adjunct",
                        "subject":"subject",
                        "complement-direct":"complement-direct",
                        "predicator":"predicator",
                        "mood-adjunct":"mood-adjunct",
                        "nominal-group":"nominal-group",
                        "verbal-group":"verbal-group",
                        "prepositional-group":"prepositional-group",
                        "adverbial-group":"adverbial-group",
                        "adjectival-group":"adjectival-group",}
ELA_MOOD_DICT = {}
ELA_MODIFICATION_DICT = {}

#############################################################################################
def generate_Annotated_BTC_constituency_and_transitivity():
    """
        project: Annotated BTC English UAM 2_4
    """
    text_pos_lambdas, features_lambdas = [lambda x, y: x.overlap(y), ], []
    files = {k:v[0] for k, v in read_CTPR_project(UAM_BTC_PROJECT, "Transitivity").items() if v}
    
    evaluate_file_bundle(files, CONSTITUENCY_DICT_ANKE,
                         text_pos_lambdas, features_lambdas, 0, "const",
                         SYSTEMIC_NETWORK_CONSTITUENCY)
    
    evaluate_file_bundle(files, TRANSITIVITY_DICT_ANKE,
                         text_pos_lambdas, features_lambdas, 0, "trans",
                         SYSTEMIC_NETWORK_TRANSITIVITY)

def generate_Annotated_OE_CLEAN_constituency_and_transitivity(overwrire=False):
    """
        project: Annotation_OE_clean_gold_UAM_1_33_new_segments
    """
    text_pos_lambdas, features_lambdas = [lambda x, y: x.overlap(y), ], []
    files = {k:v[0] for k, v in read_CTPR_project(UAM_OE_CLEAN_BIG_PROJECT, "Transitivity").items() if v}
    
    evaluate_file_bundle(files, CONSTITUENCY_DICT_ANKE,
                         text_pos_lambdas, features_lambdas, 0, "const",
                         SYSTEMIC_NETWORK_CONSTITUENCY, overwrire)
    
    evaluate_file_bundle(files, TRANSITIVITY_DICT_ANKE,
                         text_pos_lambdas, features_lambdas, 0, "trans",
                         SYSTEMIC_NETWORK_TRANSITIVITY, overwrire)    

def generate_totals_for_Annotated_OE_CLEAN_constituency_and_transitivity():
    
    trans_files = [k + "_trans_features.csv" for k in read_CTPR_project(UAM_OE_CLEAN_BIG_PROJECT, "Transitivity").keys()]
    const_files = [k + "_const_features.csv" for k in read_CTPR_project(UAM_OE_CLEAN_BIG_PROJECT, "Transitivity").keys()]
    merge_feature_csvs(trans_files, UAM_OE_CLEAN_BIG_PROJECT + "_trans_feat_total_.csv")
    merge_feature_csvs(const_files, UAM_OE_CLEAN_BIG_PROJECT + "_const_feat_total_.csv")
    
def generate_OCD_annotation_Ela():
    """ """
    text_pos_lambdas, features_lambdas = [lambda x, y: x.overlap(y), ], []
    files = {k:v[0] for k, v in read_CTPR_project(UAM_ELA_OCD_PROJECT, "constituency").items() if v and "BeatOCD" in k}
    evaluate_file_bundle(files, None,
                         text_pos_lambdas, features_lambdas, 0, "const",
                         SYSTEMIC_NETWORK_CONSTITUENCY)


def parse_dirty_bid_ela_files1():
    """
        * parsing and comparing only the first quarter
        *segments need to be sorted properly (ascending by
         start index, and if same start index then longer segments first)
    """
    q1 = PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD1.txt"
    raw_big_txt = read_text_UAM_style(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt")
    
    bndl = read_annotation_file(q1 + ".mcg.xml", q1, True)
    
#    adjust_segment_position(raw_big_txt, bndl)
    
    # checking how many are matched
    
    bndl.sort(key = lambda x: x.id * 1000 - x.len()) 
    
    parents = {sg:sorted(bndl.segments([lambda x: x.contains(sg)]), key=lambda x:x.len()) for sg in bndl}
    nm,m =0,0
    tot_offset = 0
    for i in bndl:
        match = raw_big_txt[i.start:i.end] == i.text
#         print "S:",i.start, i.text
#         print "T:",i.start, raw_big_txt[i.start:i.end]
#         print "Match:", match
        if not match:
            offset = adjust_segment_position_in_context(i, raw_big_txt,parents[i])
            if offset:
                tot_offset+=offset
                bndl.segments([lambda x: x.start > i.start]).shift_index(offset)
#             print "Adjusted by:", offset
        match = raw_big_txt[i.start:i.end] == i.text
#         print "Match after adjustment: ", match
        if not match: 
            nm +=1
            print "S:",i.start, i.text
            print "T:",i.start, raw_big_txt[i.start:i.end]

        else: m +=1
#         print "---"
        #if raw_big_txt[i.start:i.end] == i.text: m +=1
    print "not matched: ",nm, "; matched:", m, "tot offset", tot_offset

    max_end_id = max(x.end for x in bndl)
    # reading the hand annotated file
    man_constit = read_annotation_file(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD/constituency.xml", 
                               PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt", False)

    man_mood = read_annotation_file(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD/mood.xml", 
                               PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt", False)
    
    man_1st_quarter_constit = man_constit.segments(lambda x: x.end<=max_end_id)
    man_1st_quarter_mood = man_mood.segments(lambda x: x.end<=max_end_id)
    
    # Constituency
    man_constit = read_annotation_file(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD/constituency.xml", 
                               PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt", False)
    text_pos_lambdas, features_lambdas = [lambda x, y: x.overlap(y), ], []
    csv_suffix = "constituency"
    se = SegmentEvaluator(man_1st_quarter_constit,
                              bndl,
                              ELA_CONSTITUENCY_DICT,
                              text_pos_lambdas,
                              features_lambdas, SYSTEMIC_NETWORK_CONSTITUENCY)
    
    write_to_csv(q1 + "_" + csv_suffix + ".csv", se.generate_csv_rows(include_standard_test_results=False))
    write_to_csv(q1 + "_" + csv_suffix + "_features.csv", se.feature_based_statistics())
    
    # Mood
    csv_suffix = "mood"
    se = SegmentEvaluator(man_1st_quarter_mood,
                              bndl,
                              None,
                              text_pos_lambdas,
                              features_lambdas, SYSTEMIC_NETWORK_MOOD)
    
    write_to_csv(q1 + "_" + csv_suffix + ".csv", se.generate_csv_rows(include_standard_test_results=False))
    write_to_csv(q1 + "_" + csv_suffix + "_features.csv", se.feature_based_statistics())
    
def parse_dirty_bid_ela_files():
    """ parsing and re-indexing the BeaOCD files"""
    
    def adjust_bundle(txt_filename,raw_big_txt,bundle_offset):
        """ returns the adjusted bundle """
        bndl = read_annotation_file(txt_filename + ".mcg.xml", txt_filename, True)
        if bundle_offset: bndl.shift_index(bundle_offset)
        bndl.sort(key = lambda x: x.id * 1000 - x.len()) 
        parents = {sg:sorted(bndl.segments([lambda x: x.contains(sg)]), key=lambda x:x.len()) for sg in bndl}
        nm,m =0,0
        tot_offset = 0
        nms = SegmentBundle()
        for i in bndl:
            match = raw_big_txt[i.start:i.end] == i.text
            if not match:
                offset = adjust_segment_position_in_context(i, raw_big_txt,parents[i])
                if offset:
                    tot_offset+=offset
                    bndl.segments([lambda x: x.start > i.start]).shift_index(offset)
            match = raw_big_txt[i.start:i.end] == i.text
            if not match: 
                nm +=1
                nms.append(i)
            else: m +=1
        print "bundle:", txt_filename
        print "not matched: ",nm, "; matched:", m, "tot offset", tot_offset
        return bndl, nms
    
    def first_line_in_text(txt):
        return txt.split("\n")[0]
    
    txt_files = [PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD1.txt",
                 PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD2.txt",
                 PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD3.txt",
                 PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD4.txt"]
    
    big_filename = PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt"
    raw_big_txt = read_text_UAM_style(big_filename)
    
    #txt_read_list = [read_text_UAM_style(txt) for txt in txt_files]
    auto_bndl_agregated = SegmentBundle()
    bundle_offset = 0
    for tf in txt_files:
        #bundle_offset = raw_big_txt.find(first_line_in_text(read_text_UAM_style(tf)))
        # 0, 4471, 7634, 11829 # indexes by file length
        bndl, nms = adjust_bundle(tf, raw_big_txt, bundle_offset)
        bundle_offset = max (x.end for x in bndl)
        # 4469, 7632, 11827, 16348# indexes by the last segment end
        print bundle_offset
        auto_bndl_agregated.extend( SegmentBundle(list_minus(bndl,nms)))
#         for i in nms:
#             print "S:",i.start, i.text
#             print "T:",i.start, raw_big_txt[i.start:i.end]

    # Constituency
    text_pos_lambdas, features_lambdas = [lambda x, y: x.overlap(y), ], []
    man_constit = read_annotation_file(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD/mood.xml", 
                               PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt", False)

    csv_suffix = "constituency"
    se = SegmentEvaluator(man_constit,
                              auto_bndl_agregated,
                              ELA_CONSTITUENCY_DICT,
                              text_pos_lambdas,
                              features_lambdas, SYSTEMIC_NETWORK_CONSTITUENCY)
    
    write_to_csv(big_filename + "_" + csv_suffix + ".csv", se.generate_csv_rows(include_standard_test_results=False))
    write_to_csv(big_filename + "_" + csv_suffix + "_features.csv", se.feature_based_statistics())
    
    # Mood
    man_mood = read_annotation_file(PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD/mood.xml", 
                               PATH_TO_PROJECT + "/resources/test/dirty_parsing_Ela_big_file/BeatOCD.txt", False)
    csv_suffix = "mood"
    se = SegmentEvaluator(man_mood,
                              auto_bndl_agregated,
                              None,
                              text_pos_lambdas,
                              features_lambdas, SYSTEMIC_NETWORK_MOOD)
    
    write_to_csv(big_filename + "_" + csv_suffix + ".csv", se.generate_csv_rows(include_standard_test_results=False))
    write_to_csv(big_filename + "_" + csv_suffix + "_features.csv", se.feature_based_statistics())
    
if __name__ == '__main__':
    # cProfile.run("generate_Annotated_OE_CLEAN_constituency_and_transitivity()","/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/evaluation/parse_bla.log")
    # generate_Annotated_BTC_constituency_and_transitivity()
    # ta = cProfile.run("""TextAutoAnnotator("/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE1clean/OE115tigger276.txt")""")
    # ta = TextAutoAnnotator("/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE1clean/OE115tigger276.txt")
    # generate_Annotated_OE_CLEAN_constituency_and_transitivity()
    # generate_totals_for_Annotated_OE_CLEAN_constituency_and_transitivity()
    generate_OCD_annotation_Ela()
    
    #parse_dirty_bid_ela_files()
    pass
