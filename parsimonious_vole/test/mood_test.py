'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jun 30, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import unittest

from nltk.featstruct import FeatStruct

from parsimonious_vole.magic.guess.mood import Constituent


class Test(unittest.TestCase):

    def testConstituentFS(self):
        c = Constituent("clause1", None, None)
        fs1 = FeatStruct({"process_type":"two-role-action","configuration":[("two-role-action",("Ag","Af"))]})
        fs2 = FeatStruct({"process_type":"influential","configuration":[("influential",("Ag","Af-Ph"))]})
        
        c.extend_feature_structure_values(fs1)
        c.extend_feature_structure_values(fs1)
        assert len(c.feature_structure()["process_type"]) == 2
        assert len(c.feature_structure()["configuration"]) == 2

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testConstituentFS']
    unittest.main()