'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jul 8, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

'''
import unittest

from parsimonious_vole.magic.guess.mood import C_TYPE, parse_mood,\
    PREDICATOR_FINITE
from parsimonious_vole.magic.guess.mood_pattern_matching import NODES, EDGES, pattern_to_graph,\
    contains_pattern, enrich_from_pattern, insert_from_pattern
from parsimonious_vole.magic.parse.parse_process import test_sentence_graph
from nltk.featstruct import FeatDict, conflicts

from parsimonious_vole.configuration import COGNITIVE_EXAMPLES
from parsimonious_vole.magic.guess.fs_constants import POSSIBLE_PROCESS_TYPES

#test_conflicting_feature_structures_pattern = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
#                         "pred":({C_TYPE:"predicator",POSSIBLE_PROCESS_TYPES:"emotion"},None),
#                         "subj":({C_TYPE:"subject"},None),
#                         "compl":({C_TYPE:"complement"},None),
#                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
#                         "pred2":({C_TYPE:"predicator",POSSIBLE_PROCESS_TYPES:["possessive","something else"]},None), 
#                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"}),
#                         #"subj2" :(None,None),
#                         },
#                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
#                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None)]
#                }

test_insert_pattern = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({C_TYPE:"predicator",},None),
                         "subj":({C_TYPE:"subject"},None),
                         "compl":({C_TYPE:"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2":({C_TYPE:"predicator",POSSIBLE_PROCESS_TYPES:["possessive","something else"]},None), 
                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"}),
                         },
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None), 
                         ("cl2","subj2",{"insert_reference":"subj"})]
                }

test_insert_pattern2 = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({C_TYPE:"predicator",},None),
                         "subj":({C_TYPE:"subject"},None),
                         "compl":({C_TYPE:"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2":({C_TYPE:"predicator",POSSIBLE_PROCESS_TYPES:["possessive","something else"]},None), 
                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"}),
                         },
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None), 
                         ("cl2","subj3",{"insert_new":{C_TYPE:PREDICATOR_FINITE,"FUNKY FEATURE":"Indeed funky"}})]
                }

test_pattern = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({C_TYPE:"predicator"},None),
                         "subj":({C_TYPE:"subject"},None),
                         "compl":({C_TYPE:"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2":({C_TYPE:"predicator"},None),
                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"})},
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None)]
                }

test_negated_nodes1 = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({C_TYPE:"predicator"},None),
                         "subj":({C_TYPE:"subject"},None),
                         "compl":({C_TYPE:"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2!":({C_TYPE:"predicator"},None),
                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"})},
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("compl","cl2",None),]
                }

test_negated_nodes2 = {NODES:{"cl":({"mood":"declarative", "voice":"active"},None),
                         "pred":({C_TYPE:"predicator"},None),
                         "subj":({C_TYPE:"subject"},None),
                         "compl":({C_TYPE:"complement"},None),
                         "cl2":({"finitness":'non_finite_perfective',"voice":"active"},{"special_feature":"matching_works"}),
                         "pred2":({C_TYPE:"predicator"},None),
                         "pred3!":({C_TYPE:"predicator"},None),
                         "pred4!":({C_TYPE:"predicator"},None),
                         "pred5!":({C_TYPE:"predicator"},None),
                         "compl2":({C_TYPE:"complement"},{"special_feature":"matching_works"})},
                EDGES:[("cl","subj",None),("cl","pred",None),("cl","compl",None),
                         ("cl2","compl2",None),("cl2","pred2",None),("cl2","pred3",None),("compl","cl2",None),
                         ("cl2","pred3",None),("cl2","pred4",None),("cl2","pred5",None)]
                }

class Test(unittest.TestCase):

    mood_graph = None

    def setUp(self):
        the_sentence = test_sentence_graph(19, COGNITIVE_EXAMPLES , True)
        self.mood_graph = parse_mood(the_sentence)

    def tearDown(self):
        pass

    def test_contains_pattern(self):
        print "test_contains_pattern"

#         assert contains_pattern(self.mood_graph,test_pattern,
#                                 return_matching_subraphs=False,return_graph_pattern=False)
        #conflicting fs
#        assert not contains_pattern(self.mood_graph,test_conflicting_feature_structures_pattern,
#                               return_matching_subraphs=False,return_graph_pattern=False)
        # inserting graphs
        assert contains_pattern(self.mood_graph, test_insert_pattern, return_matching_subraphs=False, return_graph_pattern=False)
        assert contains_pattern(self.mood_graph, test_insert_pattern2, return_matching_subraphs=False, return_graph_pattern=False)
        # negated nodes
        assert not contains_pattern(self.mood_graph, test_negated_nodes1, return_matching_subraphs=False, return_graph_pattern=False)
        # this test will fail, not implemented checking for distinguishing between twin nodes (i.e. same features different ids) 
        assert contains_pattern(self.mood_graph, test_negated_nodes2, return_matching_subraphs=False, return_graph_pattern=False)

    def test_pattern_to_graph(self):
        assert pattern_to_graph(test_insert_pattern,True)
        assert pattern_to_graph(test_insert_pattern,False)
        assert pattern_to_graph(test_pattern,True)
        assert pattern_to_graph(test_pattern,False)
        assert pattern_to_graph(test_negated_nodes1,  include_negations=True)

    def test_enrich_from_pattern(self):
        assert enrich_from_pattern(self.mood_graph, test_pattern)
        assert enrich_from_pattern(self.mood_graph, test_insert_pattern)
    
    def test_feature_structure(self):
        fs1 = FeatDict({"a":{"aa":{"aaa":["b","c","d"]}}})
        fs2 = FeatDict({"a":{"aa":{"aaa":"b"}}})
        print conflicts(fs1,fs2)

    def test_insert_from_pattern(self):
        assert insert_from_pattern(self.mood_graph, test_insert_pattern)
        assert insert_from_pattern(self.mood_graph, test_insert_pattern2)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_contains_pattern']
    unittest.main()