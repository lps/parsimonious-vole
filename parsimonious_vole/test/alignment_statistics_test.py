'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 10, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
from __future__ import division

import unittest

from parsimonious_vole.evaluation.alignment import stable_matching_segments, f1, precission, recall, SegmentEvaluator, \
    text_auto_annotation_bundle, simplified_segments

from parsimonious_vole.configuration import UAM_BTC_HANDS_ANALYSIS_TRANSITIVITY, UAM_BTC_HANDS_TEXT
from parsimonious_vole.uam_corpus_tool_interface.io.analisys_annotation import Segment, read_annotation_file, SegmentBundle


class Test(unittest.TestCase):

    ref_text_correct = "[Microsoft Corp.] CEO [Steve Ballmer] announced the release of [Windows 7] today"
    ref_text_parsed = "[Microsoft Corp.] [CEO] [Steve] Ballmer announced the release of Windows 7 [today]"
    
    bundle_correct = [Segment(0, 16, -1, ["company", "f1", "f2"], "Microsoft Corp."),
                     Segment(21, 34, -1, ["person", "f1", "f3"], "Steve Ballmer"),
                     Segment(60, 69, -1, ["product", "f4", "f2"], "Windows 7"), ]

    bundle_parsed = [Segment(0, 16, -1, ["company", "f1", "f2"], "Microsoft Corp."),
                     Segment(17, 20, -1, ["title", "f3", "f2"], "CEO"),
                     Segment(21, 26, -1, ["person", "f1", "f3"], "Steve"),
                     Segment(70, 75, -1, ["name", "f3", "f2"], "today"), ]

    bundle_parsed1 = [Segment(0, 16, -1, ["company", "f1", "f2"], "Microsoft Corp."),
                     Segment(17, 20, -1, ["title", "f3", "f2"], "CEO"),
                     Segment(21, 26, -1, ["person", "f1", "f3"], "Steve"),
                     Segment(21, 34, -1, ["person", "f1", "f3"], "Steve Ballmer"),
                     Segment(70, 75, -1, ["name", "f3", "f2"], "today"), ]
    
    CORRESPONDENT_FEATURE_DICTIONARY = {"company":["company"],
                                      "person":["person"],
                                      "title":["title"],
                                      "name":["name"]}
    
    def test_stable_matching_segments(self):
        # parsed vc correct
        a = stable_matching_segments(self.bundle_parsed, self.bundle_correct, [], [])
        assert (self.bundle_parsed[0], self.bundle_correct[0]) in a
        assert (self.bundle_parsed[1], None) in a
        assert (self.bundle_parsed[2], None) in a
        assert (self.bundle_parsed[3], None) in a
        assert (None, self.bundle_correct[1]) in a
        assert (None, self.bundle_correct[2]) in a
        a = stable_matching_segments(self.bundle_parsed1, self.bundle_correct, [], [])
        assert (self.bundle_parsed1[3], self.bundle_correct[1]) in a
        # parsed1 vs correct
        a = stable_matching_segments(self.bundle_parsed1, self.bundle_correct, [lambda x, y: x.overlap(y)], [])
        assert (self.bundle_parsed1[3], self.bundle_correct[1]) in a
        assert (self.bundle_parsed1[0], self.bundle_correct[0]) in a
        assert (self.bundle_parsed1[1], None) in a
        assert (self.bundle_parsed1[2], None) in a
        assert (self.bundle_parsed1[4], None) in a
        assert (None, self.bundle_correct[2]) in a
        
    def test_create_comparison_vectors(self):
        # identity test bundle_correct
        a = stable_matching_segments(self.bundle_correct, self.bundle_correct, [], [])
        assert precission(a) == recall(a) == f1(a) == 1

        # bundle_parsed test
        a = stable_matching_segments(self.bundle_parsed, self.bundle_correct, [], [])
        assert precission(a) == 1 / 4
        assert recall(a) == 1 / 3
        assert 0.28 < f1(a) < 0.29
        # relaxed mode: overlaps are allowed between segments
        a = stable_matching_segments(self.bundle_parsed, self.bundle_correct, [lambda x, y: x.overlap(y)], [])
        assert precission(a) == 1 / 2
        assert recall(a) == 2 / 3
        assert 0.57 < f1(a) < 0.58

        # reverse bundle_test
        a = stable_matching_segments(self.bundle_correct, self.bundle_parsed, [], [])
        assert precission(a) == 1 / 3
        assert recall(a) == 1 / 4
        assert 0.28 < f1(a) < 0.29

        # bundle_pared1 _tests
        a = stable_matching_segments(self.bundle_parsed1, self.bundle_correct, [], [])
        assert precission(a) == 2 / 5
        assert recall(a) == 2 / 3
        assert f1(a) == 1 / 2
        
        a = stable_matching_segments(self.bundle_parsed1, self.bundle_correct, [lambda x, y: x.overlap(y)], [])
        assert precission(a) == 2 / 5
        assert recall(a) == 2 / 3
        assert f1(a) == 1 / 2

    def test_stsistics_csv(self):
        feature_dictionnary_transitivity_BTC_OE1clean = {"main":["predicator_finite", "predicator"],
                                                     "process":["clause"],
                                                     "participant-role":["subject", "complement"]}
        text_pos_lambdas = [lambda x, y: x.overlap(y), ]
        features_lambdas = []
        # hands
        hands_man = read_annotation_file(UAM_BTC_HANDS_ANALYSIS_TRANSITIVITY, UAM_BTC_HANDS_TEXT)
        hands_auto = text_auto_annotation_bundle(UAM_BTC_HANDS_TEXT)
        se = SegmentEvaluator(hands_man,
                              hands_auto,
                              feature_dictionnary_transitivity_BTC_OE1clean,
                              text_pos_lambdas,
                              features_lambdas)
        # write_to_csv(UAM_BTC_HANDS_TEXT+".csv", se.generate_csv_rows(include_standard_test_results=True))
    
    def test_simplification(self):
        """ """
        deat_dict = {"verb":["verbal-group" ],
                                                     "main":["predicator","predicator-finite", ],
                                                     "process":["clause"],
                                                     "participant-role":["subject", "complement"]}
        msb = SegmentBundle([Segment(1, 2, -1, "transitivity;verb;main".split(";"), "m")])
        asb = SegmentBundle([Segment(1, 2, -1, "verbal-group;predicator;group;predicator;process;main".split(";"), "a"),
                    ])
        
        m,a = simplified_segments(msb,asb,deat_dict)
        assert len(m) ==2
        assert len(a) ==2
                
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
