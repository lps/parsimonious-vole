'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Feb 10, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import pprint
import unittest

from parsimonious_vole.magic.guess.system_network_mappings import SYSTEMIC_NETWORK_TRANSITIVITY
from parsimonious_vole.uam_corpus_tool_interface.io.network_scheme import feature_path_to_string_list

from parsimonious_vole.magic.guess.transitivity import PTDB_TO_TRANSITIVITY_NETWORK_MAPPING


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testSysNetMapping(self):
        pt = ['two-role-action',
             'one-role-action',
             'two-role-action',
             'possessive',
             'two-role-cognition',
             'locational',
             'matching',
             'three-role-cognition',
             'desiderative',
             'attributive',
             'two-role-perception',
             'directional',
             'emotive',
             '?',
             'permissive',
             'preventive',
             'tentative',
             'starting',
             'environmental',
             'causative',
             'succeeding',
             'ceasing',
             'three-role-perception',
             'continuing',
             'delaying',
             'enabling',
             'failing',
             'event-relating',
             'three-role-action']
        
        temp_paths = [feature_path_to_string_list(x)[-1] for x in SYSTEMIC_NETWORK_TRANSITIVITY.feature_paths()]
        pprint.pprint(temp_paths)
        for k in PTDB_TO_TRANSITIVITY_NETWORK_MAPPING.keys():
            if not PTDB_TO_TRANSITIVITY_NETWORK_MAPPING[k] in temp_paths:
                print k,PTDB_TO_TRANSITIVITY_NETWORK_MAPPING[k], PTDB_TO_TRANSITIVITY_NETWORK_MAPPING[k] in temp_paths
            assert PTDB_TO_TRANSITIVITY_NETWORK_MAPPING[k] in temp_paths

        for k in pt:
            if not  k in temp_paths:
                print k, k in temp_paths
            if k is not "?": assert k in temp_paths

            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSysNetMapping']
    unittest.main()