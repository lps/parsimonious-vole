'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 2, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import unittest

from parsimonious_vole.configuration import UAM_BTC_TIGER_ANALYSIS_TRANSITIVITY, UAM_BTC_TIGER_TEXT,\
    UAM_ELA_BEAT_OCD_ANALYSIS_CONSTITUENCY, UAM_ELA_BEAT_OCD_TEXT
from parsimonious_vole.uam_corpus_tool_interface.io.analisys_annotation import SegmentBundle, Segment,\
    index_substring_words, read_annotation_file


class Test(unittest.TestCase):


    tiger = read_annotation_file(UAM_BTC_TIGER_ANALYSIS_TRANSITIVITY, UAM_BTC_TIGER_TEXT)
    ela = read_annotation_file(UAM_ELA_BEAT_OCD_ANALYSIS_CONSTITUENCY, UAM_ELA_BEAT_OCD_TEXT)

    words = filter(None, "Bill Clinton , -LRB- The president of United States -RRB- , is n't from Washington -LSB- before going to Moscow -LCB- source : bla bla -RCB- .".split(" "))
    original = "Bill Clinton, (The president of United States), isn't  from Washington [before going to  Moscow  {source: bla bla}  ."
    tricky_original = """Bill Clinton, (The 
     
#     president of
#     OE115tigger276
# Do I have an eating disorder? 
# Hi everyone.
#              United States), isn't to Washington [before going from Moscow  {source: bla bla}  ."""
     
#     tiger = """OE115tigger276
# Do I have an eating disorder? 
# Hi everyone. Call me Tiger. I was a 6'0, 190, 32 w, athletic guy about 2 years ago. Then I don't know what happened. I lost all desire to workout. All I wanted to do after work was go home, eat and watch TV. I don't go to clubs or anything... did not have any desire to meet friends. A month ago, I got sick. Went to the doctor's office and she informed me that I gained 55 lbs in the last couple of years. She diagnosed me with high blood pressure and told me to get back in shape. I did. I watched what I ate... ate salads most of the time and went to the gym as often as I can - almost everyday. In a month, I dropped 20 lbs and started looking better. I lost the bloatedness and I felt a lot better. The problem started a week ago. I had a burger. After a month of eating nothing but salads and fruits... my tummy can't seem to handle meat anymore. I threw up and it felt much better. The next day. I had some salad with chicken - same thing happened. I can only seem to keep fruits and vegetable salad down. Last night scared me. I had soup and some salad... then I felt that strange sensation when you want to purge. I had to run to the bathroom 5 minutes after the meal. I'm scared. Is this the first signs of anorexia or bulimia? I looked at some of the websites and the more I look, the more scared I got. Please email me if you're in the same condition. I wish to talk to someone. 
# 
#  """

    def test_segments(self):
        """ """
        s1 = Segment(10, 20, -1, "a")
        s2 = Segment(15, 20, -1, "a")
        s3 = Segment(12, 19, -1, "a")
        self.assertEqual(s1.overlap_metric(s2), 5)
        self.assertEqual(s2.overlap_metric(s1), 5)
        
        self.assertTrue(s1.overlap(s2))
        self.assertTrue(s2.overlap(s3))
        self.assertTrue(s2.contains(s2))
        self.assertTrue(s1.contains(s3))
        self.assertFalse(s2.contains(s3))
        
        sb = SegmentBundle([s1,s2,s3])
        assert len(sb.contained(s1, True)) == 3
        assert len(sb.contained(s2, False)) == 1
        assert len(sb.contained(s2, True)) == 3

    def testReadAnnotationsTiger(self):
        assert self.tiger.segment(5).text == "I"
        assert self.tiger.segment(151).text == "Tiger"
        assert self.tiger.segment(152).text == "Call me Tiger"
        assert self.tiger.segment(116).text == "Do I have an eating disorder"
        assert self.tiger.segment(6).text == "was"
        
    def testReadAnnotationsEla(self):
        assert self.ela.segment(5).text == "In theory, it's great."
        assert self.ela.segment(11).text == "In theory"
        assert self.ela.segment(17).text == "actually"
        assert self.ela.segment(8).text == "it"

    def testStringIndexing(self):
        result_original, nfound = index_substring_words(self.original, self.words, return_not_found=True)
        assert not nfound
        assert result_original[-3].start != result_original[-4].start  # bla bla should be different words
        assert result_original[3].text != "-LRB-"
        assert result_original[3].text == "("
        assert not result_original.segment(9)

        result_original, nfound = index_substring_words(self.tricky_original, self.words, return_not_found=True)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testReadAnnotations']
    unittest.main()