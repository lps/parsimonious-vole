'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jun 30, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import unittest

from parsimonious_vole.magic.guess.fs_constants import CONFIGURATION
from parsimonious_vole.magic.guess.transitivity_pattens import pattern_set

from parsimonious_vole.magic.guess.mood_pattern_matching import NODES


class Test(unittest.TestCase):


    def test_one_config_per_pattern(self):
        i = 0
        for pt in pattern_set:
            for rd in pattern_set[pt]:
                for ptrn in pattern_set[pt][rd]:
                    for k in ptrn[NODES]:
                        if ptrn[NODES][k] is not None and len(ptrn[NODES][k])>1 and ptrn[NODES][k][1]:
                            if CONFIGURATION in ptrn[NODES][k][1]:
                                i+=1
                                print ptrn[NODES][k][1][CONFIGURATION]
        print "Total number of patterns is %i"%i
                    #print ptrn[NODES]['cl'][1][CONFIGURATION]
#                     assert 'cl' in ptrn[NODES]
#                     assert len(ptrn[NODES]['cl'][1][CONFIGURATION])==1, ptrn[NODES]['cl'][1][CONFIGURATION] 

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_one_config_per_pattern']
    unittest.main()