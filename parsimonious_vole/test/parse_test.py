'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 1, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import unittest

from parsimonious_vole.configuration import PATH_TO_PROJECT
from parsimonious_vole.magic.parse.parse_process import get_parse_result


class Test(unittest.TestCase):


    def setUp(self):
        self.stp_file = PATH_TO_PROJECT+"/resources/noun_phrase_structure.stp"


    def tearDown(self):
        pass


    def test_wsd(self):
        pr = get_parse_result(self.stp_file)
        for t in pr.graphs()[0]:
            if t.pos[:2] in ["NN","VB","JJ"]:
                assert t.synset is not None


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_wsd']
    unittest.main()