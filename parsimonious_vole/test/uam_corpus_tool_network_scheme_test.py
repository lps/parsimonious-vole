'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 9, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import unittest

from parsimonious_vole.magic.guess.system_network_mappings import SYSTEMIC_NETWORK_TRANSITIVITY

from parsimonious_vole.uam_corpus_tool_interface.io.network_scheme import SystemicFeature, SystemicSystem, SystemicNetwork


class Test(unittest.TestCase):

    def test_building_systemic_network(self):
        root = SystemicFeature("root")
        child1 = SystemicFeature("child1")
        root_system = SystemicSystem("children type", [SystemicFeature("child2"), SystemicFeature("child3")])
        child3_system = SystemicSystem("leaves type", [SystemicFeature("leaf1"), SystemicFeature("leaf3"), SystemicFeature("leaf2")])
        leaf3_system = SystemicSystem("ltype", [SystemicFeature("l1"), SystemicFeature("l2"), SystemicFeature("l3")])
        
        sn = SystemicNetwork(root, [root_system, child3_system, leaf3_system])

    def test_iterators(self):
        """ """
        l = list(SYSTEMIC_NETWORK_TRANSITIVITY.system_iterator())
        assert len(l) == 11
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()