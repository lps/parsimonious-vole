'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jan 14, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import pprint
import unittest

from parsimonious_vole.magic.guess.mood import parse_mood
from parsimonious_vole.magic.guess.mood_binding_and_control_enricher import parse_mood_post_enrichment
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.parse.parse_process import test_sentence_graph
from parsimonious_vole.magic.taxis.rules_taxis import get_clause_pairs_for_taxis_matching, TaxisDB, build_caluse_complex_graph,\
    get_clause_pairs_for_embediment_matching
from parsimonious_vole.magic.taxis.taxis_pattern import TaxisPattern
from parsimonious_vole.magic.utils import print_graph_taxis

from parsimonious_vole.configuration import COORDINATION_EXAMPLES, INFLUENTIAL_EXAMPLES
from parsimonious_vole.magic.guess.transitivity import parse_transitivity_from_mood_graph


class Test(unittest.TestCase):

    def setUp(self):
        self.sent1 = self._parse(2, COORDINATION_EXAMPLES)
        self.sent2 = self._parse(14, INFLUENTIAL_EXAMPLES)

    def tearDown(self):
        pass

    def _parse(self,sentence_id, test_bed):
        sentence = test_sentence_graph(sentence_id, test_bed, True)
        correct_dependecy_parse_errors(sentence)
        mcg = parse_mood(sentence)
        parse_mood_post_enrichment(mcg,sentence)
        mcg = parse_transitivity_from_mood_graph(mcg,sentence)
        return mcg

    def _testParatacticRelations(self):
        markers1 = ["and", "but also", "too", "in addition", "also", "moreover", "on the other hand", "and that", "but"]
        pattern1 = "@1 (,) (mrkr) @2[+positive]"
        #pattern1 = "@a[+finite] , (mrkr) @b[+positive]"
        pattern2 = '"both" @1 "and" @2'
        p1 = TaxisPattern(markers1,pattern1,['additive'])
        p2 = TaxisPattern(markers1,pattern2,['additive'])
        #
        for pair in get_clause_pairs_for_taxis_matching(self.sent1):
            print pair[0]
            print pair[1]
            pprint.pprint(TaxisDB().score_clause_pair(self.sent1, pair[0], pair[1]))

    def _testDrawGraph(self):
        build_caluse_complex_graph(self.sent1)
        pass
        
    def _testEmbededClauseExtraction(self):
        """ """
        m = get_clause_pairs_for_embediment_matching(self.sent2,truncate_embded=False)
        pprint.pprint(m)
        # taxis patterns on embedded full clauses
        if m[3][1]: print "taxis patterns on embedded full clauses:"
        for pair in m[3][1]:
            pprint.pprint(TaxisDB().score_clause_pair(self.sent2, pair[0], pair[1]))
    
    def testBuildTaxisGraph(self):
        gr = build_caluse_complex_graph(self.sent1)
        print_graph_taxis(gr)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testParatactic']
    unittest.main()