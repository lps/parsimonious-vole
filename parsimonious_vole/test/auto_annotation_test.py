'''
Created on May 5, 2014

@author: lps
'''
import unittest

from parsimonious_vole.evaluation.alignment import TextAutoAnnotator, SegmentEvaluator
from parsimonious_vole.magic.utils import write_to_csv

from parsimonious_vole.configuration import UAM_BTC_TIGER_TEXT, UAM_BTC_TIGER_ANALYSIS_TRANSITIVITY
from parsimonious_vole.uam_corpus_tool_interface.io.analisys_annotation import Segment, read_annotation_file


class Test(unittest.TestCase):
   
    ta = TextAutoAnnotator(UAM_BTC_TIGER_TEXT)
    ea = None #TextAutoAnnotator(UAM_ELA_BEAT_OCD_TEXT)

    def _testTextAutoAnnotator_ELA(self):
        # Ela corpus assertions
        # word tests
        ws = self.ea.word_segments
        assert ws[123].text==self.ea.raw_txt[ws[123].start:ws[123].end]
        assert ws[153].text==self.ea.raw_txt[ws[153].start:ws[153].end]
        assert ws[143].text==self.ea.raw_txt[ws[143].start:ws[143].end]
        assert ws[173].text==self.ea.raw_txt[ws[173].start:ws[173].end]
        assert ws[1123].text==self.ea.raw_txt[ws[1123].start:ws[1123].end]
        assert ws[1223].text==self.ea.raw_txt[ws[1223].start:ws[1223].end]
        assert ws[1323].text==self.ea.raw_txt[ws[1323].start:ws[1323].end]

        # sents test
        segs = self.ea.sent_segments.contained(Segment(65,87))
        assert segs and segs[0].text == self.ea.raw_txt[segs[0].start:segs[0].end] == "In theory, it's great."

        segs = self.ea.sent_segments.contained(Segment(670,711))
        assert segs and segs[0].text == self.ea.raw_txt[segs[0].start:segs[0].end] == "Just a few days over 90, most in the 80s."

        segs = self.ea.sent_segments.contained(Segment(5515,5541))
        assert segs and segs[0].text == self.ea.raw_txt[segs[0].start:segs[0].end] == "But a good thing happened."

        segs = self.ea.sent_segments.contained(Segment(16052,16085))
        assert segs and segs[0].text == self.ea.raw_txt[segs[0].start:segs[0].end] == "What if there's a flood in there."

        segs = self.ea.sent_segments.contained(Segment(16248,16348))
        assert segs and segs[0].text == self.ea.raw_txt[segs[0].start:segs[0].end] == "In the end, I knew it was checking, and I knew it was bad for me, and I walked away and I went home."
        
        #constituent test
        segs = self.ea.constituent_segments.contained(Segment(119,121))
        assert segs and any(['predicator_finite' in x.features for x in segs]) and segs[0].text == "'s" 

        segs = self.ea.constituent_segments.contained(Segment(1855,1880))
        assert segs and any(['clause' in x.features for x in segs]) and any([x.text == "we're working on together" for x in segs]) 

        segs = self.ea.constituent_segments.contained(Segment(1996,2027))
        assert segs and any(['clause' in x.features for x in segs]) and any([x.text == "that I knew it was OCD-related." for x in segs]) 

        segs = self.ea.constituent_segments.contained(Segment(4394,4396))
        assert segs and any(['marker' in x.features for x in segs]) and any([x.text == "of" for x in segs]) 

    def testTextAutoAnnotator_TIGER(self):
        ws = self.ta.word_segments
            
        assert ws[23].text==self.ta.raw_txt[ws[23].start:ws[23].end]  
        assert ws[53].text==self.ta.raw_txt[ws[53].start:ws[53].end]
        assert ws[43].text==self.ta.raw_txt[ws[43].start:ws[43].end]
        assert ws[73].text==self.ta.raw_txt[ws[73].start:ws[73].end]
        assert ws[123].text==self.ta.raw_txt[ws[123].start:ws[123].end]
        assert ws[223].text==self.ta.raw_txt[ws[223].start:ws[223].end]
        assert ws[323].text==self.ta.raw_txt[ws[323].start:ws[323].end]
        
        segs = self.ta.constituent_segments.contained(Segment(17,46,))
        assert segs
        assert segs[0].text == self.ta.raw_txt[segs[0].start:segs[0].end]
        
        segs = self.ta.sent_segments.contained(Segment(49,61))
        assert segs
        assert segs[0].text == self.ta.raw_txt[segs[0].start:segs[0].end]
        
    def testSegmentComparison_TIGER(self):
        tiger = read_annotation_file(UAM_BTC_TIGER_ANALYSIS_TRANSITIVITY, UAM_BTC_TIGER_TEXT)
        se = SegmentEvaluator(tiger ,self.ta.constituent_segments)
        rs = se.generate_csv_rows()
        write_to_csv(UAM_BTC_TIGER_TEXT, rs)
    
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testStringIndexing']
    unittest.main()
