#!/usr/bin/env bash
#
# Runs the English PCFG parser on one or more files, printing trees only

if [ ! $# -ge 1 ]; then
  echo Usage: `basename $0` 'file(s)'
  echo
  exit
fi

scriptdir=`dirname $0`

java -mx1G -cp "$scriptdir/*:" edu.stanford.nlp.parser.lexparser.LexicalizedParser \
 -outputFormat "words,wordsAndTags, typedDependenciesCollapsed " -writeOutputFiles -outputFormatOptions "stem" edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz $*

#penn, oneline, rootSymbolOnly, words, wordsAndTags, dependencies, typedDependencies, typedDependenciesCollapsed, 
# latexTree, xmlTree, collocations, semanticGraph, conllStyleDependencies, conll2007
