@echo off
:: Runs the English PCFG parser on one or more files, printing trees only
:: usage: lexparser fileToparse
java -mx1G -cp "*;" edu.stanford.nlp.parser.lexparser.LexicalizedParser -outputFormat "words,wordsAndTags, typedDependenciesCollapsed " -writeOutputFiles -outputFormatOptions "stem" edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz %1
