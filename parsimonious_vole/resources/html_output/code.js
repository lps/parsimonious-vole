$(document).ready(function() {

$('img').hover(function() {
    $(this).css("cursor", "pointer");
    $(this).animate({
        width: "50%",
        height: "50%",
    }, 'fast');

    
}, function() {
    $(this).animate({
        width: "5%"
    }, 'fast');
});

// showing tooltip
$('.name').each(function() {
	var iidd = jQuery(this).attr("id");
    $(this).qtip({
           content: {
           text: $('.tooltiptext#'+iidd)
           }
     });
});

})
