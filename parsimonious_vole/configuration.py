'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Dec 5, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

'''
import logging

""" Paths to various resource files """

import os
import sys
#set the sys_path to see the modules of this project
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

PATH_TO_PROJECT = os.path.dirname(__file__)

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

PICKLE_SOURCE = PATH_TO_PROJECT +"/resources/ptdb.pickle"
LOG_FILE = PATH_TO_PROJECT +"/log/magic_parser.log"

#CSV_SOURCE = PATH_TO_PROJECT+"/resources/Ptdb(ANeale).csv"
CSV_SOURCE = PATH_TO_PROJECT+"/resources/Ptdb(ANeale) 03.csv"

CSV_DESTINATION = PATH_TO_PROJECT+"/resources/ptdb.csv"
PREPOSITIONS_SOURCE = PATH_TO_PROJECT+"/resources/english_prepositions.txt"
PICKLE_DESTINATION = PICKLE_SOURCE
FILED_NAMES = ["FORM", "Occs", "COB class", "MEANING", "Occs", "Cardiff feature", "Levin feature", "participants role", "NOTES"]

PICKLE_DESTINATION_TAXIS_PATTERNS = PATH_TO_PROJECT + "/resources/taxis.pickle"
PICKLE_SOURCE_TAXIS_PATTERNS = PICKLE_DESTINATION_TAXIS_PATTERNS
CSV_SOURCE_TAXIS_PATTERNS = PATH_TO_PROJECT + "/resources/taxis_analisys_patterns1.2.csv"

### path to example bundles
PARSE_XML = PATH_TO_PROJECT+"/resources/potatoes-example-parse.xml"
TENSE_TEST_XML = PATH_TO_PROJECT+"/resources/tense-test-example-parse.xml"
MOOD_TEST_XML = PATH_TO_PROJECT+"/resources/mood_test_examples.xml"

DUKE_AUNT_TEAPOT_XML = PATH_TO_PROJECT+"/resources/duke-aunt-teapot-example.xml"
POLARITY_TEST = PATH_TO_PROJECT+"/resources/polarity_text.xml"
POTATOES_TEST_EXAMPLE = PATH_TO_PROJECT + "/resources/potatoes-example-parse.xml"
PHRASAL_TWO = PATH_TO_PROJECT + "/resources/phrasal-verb-two-part.xml"
PHRASAL_THREE = PATH_TO_PROJECT + "/resources/phrasal-verb-three-part.xml"
TWO_ROLE_EXAMPLES = PATH_TO_PROJECT + "/resources/two-role-action-examples.xml"
ONE_ROLE_EXAMPLES = PATH_TO_PROJECT + "/resources/one-role-actiona-examples.xml"

REL_DIRECTIONAL_EXAMPLES = PATH_TO_PROJECT + "/resources/rel-directional-examples.xml"
REL_POSSESIVE_EXAMPLES = PATH_TO_PROJECT + "/resources/rel-possesive-examples.xml"
REL_LOCATIONAL_EXAMPLES = PATH_TO_PROJECT + "/resources/rel-locational-examples.xml"
REL_ATTRIBUTIVE_EXAMPLES = PATH_TO_PROJECT + "/resources/rel-attributive-examples.xml"
REL_MATCHING_EXAMPLES = PATH_TO_PROJECT + "/resources/rel-matching-examples.xml"

INFLUENTIAL_EXAMPLES = PATH_TO_PROJECT + "/resources/influential-examples.xml"
COMPLEX_VPHR_EXAMPLES = PATH_TO_PROJECT + "/resources/complex-verb-phrase-example.xml"
COGNITIVE_EXAMPLES = PATH_TO_PROJECT + "/resources/cognitive-examples.xml"
EVENT_RELATING2 = PATH_TO_PROJECT + "/resources/event-relating-examples2.xml"
EVENT_RELATING1 = PATH_TO_PROJECT + "/resources/event-relating-examples1.xml"
COMPLEX_CLAUSES = PATH_TO_PROJECT + "/resources/complex_clauses.xml"
COORDINATION_EXAMPLES = PATH_TO_PROJECT + "/resources/coordination_examples.stp"
CONTROL_AND_BINDING = PATH_TO_PROJECT + "/resources/control_and_binding.txt.stp"
NOUN_PHRASE_STRUCTURE = PATH_TO_PROJECT + "/resources/noun_phrase_structure.stp"

TEMPORAL_SENTENCES = PATH_TO_PROJECT + "/resources/temporal_expressions.stp"

LITTLE_RIDING_HOOD = PATH_TO_PROJECT + "/resources/little_riding_hood.txt.stp"
###
TRANSITIVITY_PATTERNS_FILE = PATH_TO_PROJECT + "/magic/guess/transitivity_pattens.py"

### Network shemes
CONSTITUENT_SYSTEM =  PATH_TO_PROJECT + "/resources/uam_sys_networks/mood_elements.xml"
MOOD_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/mood_features.xml"
PERSON_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/person_system.xml"
DETERMINANT_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/determination.xml"
TEST_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/test_sn.xml"
AGREEMENT_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/agreement_features.xml"
TRANSITIVITY_SYSTEM = PATH_TO_PROJECT + "/resources/uam_sys_networks/Transitivity.xml"
# Vocabulary
FEMALE_NOUN_LIST = PATH_TO_PROJECT + "/resources/vocabulary/female_stanford_dcoref.txt"
MALE_NOUN_LIST = PATH_TO_PROJECT + "/resources/vocabulary/male_stanford_dcoref.txt"
FEMALE_FIRST_NAME_LIST = PATH_TO_PROJECT + "/resources/vocabulary/female_first.txt"
MALE_FIRST_NAME_LIST = PATH_TO_PROJECT + "/resources/vocabulary/male_first.txt"
PERSON_TITLE_NOUN_LIST = PATH_TO_PROJECT + "/resources/vocabulary/person_title.txt"

ADVERB_INTENSITY_LIST = PATH_TO_PROJECT + "/resources/vocabulary/adverb_adjunct_intensity.txt"
ADVERB_TEMPORALITY_LIST = PATH_TO_PROJECT + "/resources/vocabulary/adverb_adjunct_temporality.txt"
ADVERB_MODALITY_LIST = PATH_TO_PROJECT + "/resources/vocabulary/adverb_adjunct_modality.txt"


#TODO: Need to make it pretty
PATH_TO_STANFORD_PARSER = None
try:
    PATH_TO_STANFORD_PARSER = os.environ['STANFORD_PARSER_HOME']
except KeyError:
    logging.error("STANFORD_PARSER_HOME environment variable is not specified.")
    PATH_TO_STANFORD_PARSER = PATH_TO_PROJECT+ "/third-party/stanford-parser-full-2014-06-16"

#TODO: Need to make it pretty
PATH_TO_TARQUI_CODE = None
# try:
#     PATH_TO_TARQUI_CODE = os.environ['TARQUI_CODE_HOME']
# except KeyError:
#     logging.warn("TARQUI_CODE_HOME is not specified")
#     # PATH_TO_TARQUI_CODE = PATH_TO_TARQUI_CODE = "/home/lps/work/nlp/temporal_parser/tarsqui/ttk-1.0/code"


TEMPORARY_PATH = PATH_TO_PROJECT + "/temp"

# UAM annotation paths
UAM_BTC_PROJECT = PATH_TO_PROJECT+"/resources/test/Annotated_BTC_English_UAM_2_4/Annotation_OE1clean.ctpr"
UAM_OE_CLEAN_BIG_PROJECT = PATH_TO_PROJECT+"/resources/test/Annotation_OE_clean_gold_UAM_1_33_new_segments/Annotation_OE1clean.ctpr"
UAM_ELA_OCD_PROJECT = PATH_TO_PROJECT + "/resources/test/OCD_Annotation_Ela/OCD_Annotation.ctpr"
#@obsolete paths to project files
UAM_CHURCH_MOUSE_TEXT = PATH_TO_PROJECT + "/resources/test/Church_mouse_Schulz/Corpus/Texts/The_Diary_of_a_Church_Mouse.txt"
UAM_CHURCH_MOUSE_ANALYSIS_TRANSITIVITY = PATH_TO_PROJECT + "/resources/test/Church_mouse_Schulz/Analyses/Texts/The_Diary_of_a_Church_Mouse.txt/Transitivity.xml"

UAM_BTC_TIGER_TEXT = PATH_TO_PROJECT +  "/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE1clean/OE115tigger276.txt"
UAM_BTC_TIGER_ANALYSIS_TRANSITIVITY = PATH_TO_PROJECT +  "/resources/test/Annotated_BTC_English_UAM_2_4/Analyses/OE1clean/OE115tigger276.txt/Transitivity.xml"

UAM_BTC_BLISS_TEXT = PATH_TO_PROJECT +  "/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE1clean/OE119bliss256.txt"
UAM_BTC_BLISS_ANALYSIS_TRANSITIVITY = PATH_TO_PROJECT +  "/resources/test/Annotated_BTC_English_UAM_2_4/Analyses/OE1clean/OE119bliss256.txt/Transitivity.xml"

UAM_BTC_LURKING_TEXT = PATH_TO_PROJECT + "/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE2clean/OE203Lurking276.txt"
UAM_BTC_LURKING_ANALYSIS_TRANSITIVITY = PATH_TO_PROJECT + "/resources/test/Annotated_BTC_English_UAM_2_4/Analyses/OE2clean/OE203Lurking276.txt/Transitivity.xml"

UAM_BTC_HANDS_TEXT = PATH_TO_PROJECT + "/resources/test/Annotated_BTC_English_UAM_2_4/Corpus/OE2clean/OE217hands248.txt"
UAM_BTC_HANDS_ANALYSIS_TRANSITIVITY = PATH_TO_PROJECT + "/resources/test/Annotated_BTC_English_UAM_2_4/Analyses/OE2clean/OE217hands248.txt/Transitivity.xml" 

UAM_ELA_BEAT_OCD_TEXT = PATH_TO_PROJECT + "/resources/test/OCD_Annotation_Ela/Corpus/Texts/BeatOCD.txt"
UAM_ELA_BEAT_OCD_ANALYSIS_CONSTITUENCY = PATH_TO_PROJECT + "/resources/test/OCD_Annotation_Ela/Analyses/Texts/BeatOCD.txt/constituency.xml"