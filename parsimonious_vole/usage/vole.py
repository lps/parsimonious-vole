'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jul 24, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

Command line program with various parsing options.
'''
import json
import os

import click
import networkx as nx
from parsimonious_vole.magic.guess.mood import parse_mood
from parsimonious_vole.magic.guess.mood_binding_and_control_enricher import parse_mood_post_enrichment
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.parse.parse_process import get_parse_result
from parsimonious_vole.magic.parse.stanford_parser import call_stanford_parser_file
from parsimonious_vole.usage.bundle_to_html import text_to_html_analisys
from parsimonious_vole.usage.serialiser import mood_graph_to_json

from parsimonious_vole.magic.guess.transitivity import parse_transitivity_from_mood_graph


class Config(object):
    """util class to pass the arguments from cli group to cli commands"""

    def __init__(self):
        self.verbose = False


# making the decorator that instantiates the config object at the group
# level and passes it as argument to the commands decorated with it
pass_config = click.make_pass_decorator(Config, ensure=True)


@click.group()
# @click.option("--verbose", is_flag=True)
@click.option("--format", default="HTML",
              help="The output format of the parse results: HTML, JSON, YAML, PICKLE or ADJ_LIST")
# @click.option("--stanfordHome", help="The path to stanford parser home folder.")
@pass_config
def cli(config, format):  # verbose
    """ A Systemic Functional Parser for English language """
    formats = ["HTML", "JSON", "ADJ_LIST", "YAML", "PICKLE", ]
    config.format = format if format in formats else formats[0]


@cli.command("parse", short_help='parse the text file')
@click.argument("input", type=click.Path(exists=True), required=True, )
@click.argument("output", type=click.File("w"), default="-", required=True,)
@pass_config
def parse(config, input, output):
    """ This operation takes an input file and generates the systemic parse for it.
    The output can be written into a file if specified.
    """
    if config.format == "HTML":
        text_to_html_analisys(input)
        return

    call_stanford_parser_file(os.path.abspath(input))
    p = get_parse_result(os.path.abspath(input) + ".stp")
    mcgs = []
    for sentence in p.sentence_graphs:
        correct_dependecy_parse_errors(sentence)
        mcg = parse_mood(sentence)
        parse_mood_post_enrichment(mcg, sentence)
        mcg = parse_transitivity_from_mood_graph(mcg, sentence)
        # mcgs.append(mcg)
        # output.write(json.dumps(json_graph.node_link_data(mcg), indent=4))
        if config.format == "YAML":
            nx.write_yaml(mcg, output)
        elif config.format == "ADJ_LIST":
            nx.write_multiline_adjlist(mcg, output)
        elif config.format == "PICKLE":
            nx.write_gpickle(mcg, output)
        elif config.format == "JSON":
            mcgs.append(mood_graph_to_json(mcg))

    # check if it is JSON then write all at once rather than one by one
    if config.format == "JSON":
        click.echo(json.dumps(mcgs, sort_keys=True,
                              indent=4, separators=(',', ': ')), output)
