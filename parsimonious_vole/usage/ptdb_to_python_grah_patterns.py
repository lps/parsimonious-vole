'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Sep 16, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

generates the graph patterns for the transitivity parse. the graph patterns 
are generated based on possible configurations extracted PTDB configurations

initial version covered indicative mood in active and passive forms
latter was added imperative mood   
'''
import itertools
import time

from parsimonious_vole.magic.utils import strip

from parsimonious_vole.configuration import TRANSITIVITY_PATTERNS_FILE
from parsimonious_vole.magic.parse.reader import db, entry_canonic_form_with_variations


# pattern graph generator from the PTDB
def generate_pattern_list():
    f = open(TRANSITIVITY_PATTERNS_FILE,"wb") 
    headder = """\"\"\"
This file was automatically created by 'ptdb_to_python_grah_patterns.py' script
create on """ +str(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))+ """

- the pattern set has the structure: pattern_set = {process_type:{(r1,r2):[pattern1,pattern2]}}
- the pattern has the structure as described in mood_pattern_mattching.pattern_to_graph

\"\"\"

from magic.guess.mood_pattern_matching import NODES, EDGES
from magic.guess.mood import C_TYPE, SUBJECT, COMPLEMENT, COMPLEMENT_ADJUNCT, COMPLEMENT_DATIVE, \
    COMPLEMENT_AGENT, PREDICATOR, PREDICATOR_FINITE, MARKER
from magic.guess.fs_constants import  VOICE, ACTIVE, PASSIVE, EXPLETIVE_MARKER, PARTICIPANT_ROLE, CONFIGURATION
from magic.parse.VerbForm import PROCESS_TYPE

"""
    # constant sets:
    _exclude_passive_form = [('Af-Ca', 'Pos', 'Ag'), ('Af-Cog', 'Ph', 'Ag')]
    # 
    f.write(headder)
    possible_configuration_set = set()
    entries = db.get_all_entries()
    for key in entries.keys():
        for i in entries[key]:
            # entry_canonic_form_with_variations(i)[2:]
            for i in entry_canonic_form_with_variations(i):
                possible_configuration_set.add(i[2:])
    
    possible_configuration_set = filter(lambda x: x[0] and x[1] and x[1][0], possible_configuration_set)
    
    f.write("""pattern_set = { """)
    process_type_set = set(map(lambda x: x[0],possible_configuration_set))
    for process_type in process_type_set: 
        if process_type and "?" not in process_type:
            f.write("\t'"+strip(process_type)+"':{\n")
            configuration_set = filter(lambda x: x[0]==process_type,possible_configuration_set)
            for configuration in configuration_set:
                f.write("\t\t"+str(configuration[1])+":[")
                if not _contains_emty_role(configuration): # if normal pattern 
                    f.write(_generate_indicative_pattern(configuration[0], configuration[1], True,True)) # active voice
                    if configuration not in _exclude_passive_form:
                        f.write(_generate_indicative_pattern(configuration[0], configuration[1], False)) # passive voice
                    if _accept_imperative_form(configuration):
                        f.write(_generate_imperative_pattern(configuration[0], configuration[1]))
                else:# else no need to bother with voice and imperative variation
                    f.write(_generate_indicative_pattern(configuration[0], configuration[1], True))
                f.write("],\n")
            f.write("\t\t},\n")
    f.write("""}""")
    f.close()
    
def _accept_imperative_form(config):
    """
        check whether the configuration accepts a imperative form:
        i.e. the first participant shall be an active animate role: Ag, Em, Cog, Ca, Perc (and derivates)
        otherwise the configuration cannot be imperative.
        It appears to be correlated to passive voice forms as well.  
    """
    accepted_imperative_empty_roles = ["Ag","Ag-Ca","Cog","Ag-Cog","Af-Cog","Em","Af-Em","Perc","Af-Perc","Ag-Perc"]
    return config[1][0] in accepted_imperative_empty_roles

def _accept_expletive_there(config):
    """ """
    return config[0].lower() in ["locational"] or config[1][0].lower() in ['there']

def _accept_pleonastic_it(config):
    return config[0].lower() in ["two-role-action","attributive","emotive", "desiderative",
                         "two-role-cognition", "causative","permissive","enabling", 
                         "preventive","delaying","starting","ceasing","continuing",
                         "happening","environmental",]

def _contains_emty_role(config):
    """ """
    return config[1][0].lower() in ["there","it"]

def _generate_indicative_pattern(proc_type, participants, active, invent_expletives=False):
    """
        generates patterns for indicative mood with active or passive voice
    
        invent-expletives, if true, generates an expletive form even if the current configuration
         does not specifies and it/there form. 
    """
    voice = "ACTIVE" if active else "PASSIVE" 
    participant_nodes_pattern = ""
    participant_edges_pattern = ""
    big_pattern = ""
    
    # indicates whether there is an empty role specified
    #is_IT_pattern = "it" == participants[0].lower() or "there" == participants[0].lower() 
    if not _contains_emty_role([proc_type,participants]):
        if active:
            if len(participants) == 1:
                participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},None),"""
                participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None)"""
            elif len(participants) == 2:
                participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,COMPLEMENT_DATIVE],},{ PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None),"""
                participant_edges_pattern = """ ('cl','subj',None), ('cl',"compl1",None),('cl',"compl2",None),"""
            elif len(participants) == 3:
                participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[2] + """'})"""
                participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),"""
            elif len(participants) == 4: # for some directional processes
                participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                    'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[3] + """'})"""
                participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),('cl','compl3',None),"""
            elif len(participants) == 5: # for some directional processes
                participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                    'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[3] + """'}),
                    'compl4':({C_TYPE:[COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[4] + """'})"""
                participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),('cl','compl3',None),('cl','compl4',None),"""
            
        else:  # PASSIVE, the config should contain a role of agency 
            if _accept_imperative_form((None,participants)): # config
                if len(participants) == 1: 
                    # does not make sense to have one role passives, unless the Agent is covert
                    participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                        'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,COMPLEMENT_AGENT],},None),"""
                    participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),"""
                elif len(participants) == 2:
                    participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                        'compl1':({C_TYPE:[COMPLEMENT_AGENT],},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                        'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None)"""
                    participant_edges_pattern = """ ('cl','subj',None), ('cl',"compl1",None),('cl',"compl2",None),"""
                elif len(participants) == 3:
                    participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                        'compl1':({C_TYPE:[COMPLEMENT_AGENT],},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                        'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'})"""
                    participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),"""
                elif len(participants) == 4: # some directional processes
                    participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                        'compl1':({C_TYPE:[COMPLEMENT_AGENT],},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                        'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                        'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[3] + """'})"""
                    participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),('cl','compl3',None),"""
                elif len(participants) == 5: # some directional processes
                    participant_nodes_pattern = """'subj':({C_TYPE:SUBJECT,},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                        'compl1':({C_TYPE:[COMPLEMENT_AGENT],},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                        'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                        'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[3] + """'}),
                        'compl4':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[4] + """'})"""
                    participant_edges_pattern = """ ('cl','subj',None),('cl','compl1',None),('cl','compl2',None),('cl','compl3',None),('cl','compl4',None),"""
            else: participant_nodes_pattern, participant_edges_pattern = None, None 

        if participant_edges_pattern and participant_nodes_pattern:
            big_pattern += """\n\t\t\t{NODES:{"cl":({C_TYPE:'clause', VOICE:""" + voice + """},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                        "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                        """ + participant_nodes_pattern + """},
                     EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""
                 
    else:
        # the it/there pattern 
        # subj/PRP/it - subject , expl/EX/there - marker
        if not _accept_expletive_there([proc_type,participants]): # only if not a locational process
            if len(participants) == 1:
                participant_nodes_pattern = """'it':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},None),"""
                participant_edges_pattern = """ ('cl','it',None),('cl','compl1',None)"""
            elif len(participants) == 2:
                participant_nodes_pattern = """'it':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,COMPLEMENT_DATIVE],},{ PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None),"""
                participant_edges_pattern = """ ('cl','it',None), ('cl',"compl1",None),('cl',"compl2",None),"""
            elif len(participants) == 3:
                participant_nodes_pattern = """'it':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'})"""
                participant_edges_pattern = """ ('cl','it',None),('cl','compl1',None),('cl','compl2',None),"""
            elif len(participants) == 4: # for some directional proc
                participant_nodes_pattern = """'it':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                    'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[3] + """'})"""
                participant_edges_pattern = """ ('cl','it',None),('cl','compl1',None),('cl','compl2',None),('cl','compl3',None),"""

            if participant_edges_pattern and participant_nodes_pattern:
                big_pattern += """\n\t\t\t{NODES:{"cl":({C_TYPE:'clause',},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                        "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                        """ + participant_nodes_pattern + """},
                     EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""
                     
        else: # if it is a locational/existential process
            if len(participants) == 1:
                participant_nodes_pattern = """'there':({C_TYPE:[EXPLETIVE_MARKER],'lemmas':['there',]},{ PARTICIPANT_ROLE:'there'}),
                    'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,SUBJECT,COMPLEMENT_AGENT],},None),"""
                participant_edges_pattern = """ ('cl','there',None),('cl','compl1',None)"""
            elif len(participants) == 2:
                participant_nodes_pattern = """'there':({C_TYPE:[EXPLETIVE_MARKER],'lemmas':['there',] },{ PARTICIPANT_ROLE:'there'}),
                    'compl1':({C_TYPE:[SUBJECT],},{ PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None),"""
                participant_edges_pattern = """ ('cl','there',None), ('cl',"compl1",None),('cl',"compl2",None),"""
            elif len(participants) == 3:
                participant_nodes_pattern = """'there':({C_TYPE:[EXPLETIVE_MARKER],'lemmas':['there',] },{ PARTICIPANT_ROLE:'there'}),
                    'compl1':({C_TYPE:[SUBJECT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,COMPLEMENT_ADJUNCT,],},{PARTICIPANT_ROLE:'""" + participants[2] + """'})"""
                participant_edges_pattern = """ ('cl','there',None),('cl','compl1',None),('cl','compl2',None),"""
            
            if participant_edges_pattern and participant_nodes_pattern:
                big_pattern += """\n\t\t\t{NODES:{"cl":({C_TYPE:'clause',},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                        "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                        """ + participant_nodes_pattern + """},
                     EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""     
            
    # on request (invent_expletives=True) -> inventing expletives even if the pattern does not cover it
    if invent_expletives and not _contains_emty_role([proc_type,participants]) and \
        (_accept_expletive_there([proc_type,participants]) or _accept_pleonastic_it([proc_type,participants])):
        
        if not _accept_expletive_there([proc_type,participants]): # only if not a locational process
            if len(participants) == 1:
                participant_nodes_pattern = """'it_invented':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT,COMPLEMENT_DATIVE],},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None),"""
                participant_edges_pattern = """ ('cl','it_invented',None),('cl','compl1',None), ('cl',"compl2",None)"""
            elif len(participants) == 2:
                participant_nodes_pattern = """'it_invented':({C_TYPE:[SUBJECT],'lemmas':['it',]},{ PARTICIPANT_ROLE:'it'}),
                    'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl2':({C_TYPE:[COMPLEMENT,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[1] + """'})"""
                participant_edges_pattern = """ ('cl','it_invented',None), ('cl',"compl1",None),('cl',"compl2",None),"""
            
            big_pattern += """\n\t\t\t{NODES:{"cl":({C_TYPE:'clause',},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                    "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                    """ + participant_nodes_pattern + """},
                 EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""     
        else:
            if len(participants) == 1:  # existential 
                participant_nodes_pattern = """'there_invented':({C_TYPE:[EXPLETIVE_MARKER],'lemmas':['there',], },{ PARTICIPANT_ROLE:'there'}),
                    'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,SUBJECT,COMPLEMENT_AGENT],},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),"""
                participant_edges_pattern = """ ('cl','there_invented',None),('cl','compl1',None)"""
            elif len(participants) == 2:
                participant_nodes_pattern = """'there_invented':({C_TYPE:[EXPLETIVE_MARKER],'lemmas':['there',], },{ PARTICIPANT_ROLE:'there'}),
                    'compl1':({C_TYPE:[SUBJECT,],},{ PARTICIPANT_ROLE:'""" + participants[0] + """'}),
                    'compl2!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},{ PARTICIPANT_ROLE:'""" + participants[1] + """'}),"""
                participant_edges_pattern = """ ('cl','there_invented',None), ('cl',"compl1",None),('cl',"compl2",None),"""

            if participant_edges_pattern and participant_nodes_pattern:
                big_pattern += """\n\t\t\t{NODES:{"cl":({C_TYPE:'clause',},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                        "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                        """ + participant_nodes_pattern + """},
                     EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""     
  
    # if directional proc then invent variations of So/Pa/Des even if the pattern does not cover it
    #TODO:
    return big_pattern

def _generate_imperative_pattern(proc_type, participants):
    """ """
    
    participant_nodes_pattern = ""
    participant_edges_pattern = ""
    big_pattern = ""
    
    if len(participants) == 1:
        # then no complement shall be present
        participant_nodes_pattern += """'subj!':({C_TYPE:SUBJECT,},None),
                'compl1!':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,],},None),"""
        participant_edges_pattern = """ ('cl','subj',None), ('cl',"compl1",None),"""
    elif len(participants) == 2:
        participant_nodes_pattern += """'subj!':({C_TYPE:SUBJECT,},None),
                'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE,COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                'compl2!':({C_TYPE:[COMPLEMENT,],},None),"""
        participant_edges_pattern = """('cl','subj',None), ('cl',"compl1",None),('cl',"compl2",None),"""
    elif len(participants) == 3:
        participant_nodes_pattern += """'subj!':({C_TYPE:SUBJECT,},None),
                'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                'compl2':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'})"""
        participant_edges_pattern = """('cl','subj',None),('cl',"compl1",None),('cl',"compl2",None),"""
    elif len(participants) == 4: # for some directional processes
        participant_nodes_pattern += """'subj!':({C_TYPE:SUBJECT,},None),
                'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                'compl2':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[3] + """'})"""
        participant_edges_pattern = """('cl','subj',None),('cl',"compl1",None),('cl',"compl2",None),('cl',"compl3",None),"""
    elif len(participants) == 5: # for some directional processes
        participant_nodes_pattern += """'subj!':({C_TYPE:SUBJECT,},None),
                'compl1':({C_TYPE:[COMPLEMENT,COMPLEMENT_DATIVE],},{PARTICIPANT_ROLE:'""" + participants[1] + """'}),
                'compl2':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[2] + """'}),
                'compl3':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[3] + """'}),
                'compl4':({C_TYPE:[COMPLEMENT_ADJUNCT],},{PARTICIPANT_ROLE:'""" + participants[4] + """'})"""
        participant_edges_pattern = """('cl','subj',None),('cl',"compl1",None),('cl',"compl2",None),('cl',"compl3",None),('cl',"compl4",None),"""

    if participant_edges_pattern and participant_nodes_pattern:
        big_pattern += """\n\t\t\t{NODES:{"cl":({'MOOD-TYPE':'imperative'},{PROCESS_TYPE:'""" + proc_type + """',CONFIGURATION: [('"""+ proc_type+ """',"""+ str(participants) +""")],}),
                    "pred":({C_TYPE:[PREDICATOR,PREDICATOR_FINITE],},{"VERB-TYPE":"main"}),
                    """ + participant_nodes_pattern + """},
                     EDGES:[('cl','pred',None),""" + participant_edges_pattern + """]},"""
    return big_pattern

def get_synthesis_from_patterns(pattern_set):
    """
        util function
    """
    ps = {k:v.keys() for k,v in pattern_set.iteritems()}
    r  = []
    [r.extend(list(itertools.product([k],v))) for k,v in ps.iteritems()]
    return r

if __name__ == '__main__':
    print "generatting pattern list" 
    generate_pattern_list()
    print "writting it to file "+ TRANSITIVITY_PATTERNS_FILE
