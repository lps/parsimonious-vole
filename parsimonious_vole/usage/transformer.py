'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 26, 2012

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

transforms the Ptdb(ANeale).csv CVS into a python pickle.
The picle structure is a list of VerbForm class instances  

'''
import cPickle as pickle
import csv
import re

from parsimonious_vole.magic.utils import strip

from parsimonious_vole.configuration import CSV_SOURCE, PICKLE_DESTINATION
from parsimonious_vole.magic.parse.VerbForm import *


def process_verb_process_type(maj, min):
    """
    @ObsoleTE
    TODO: Schedule delete 
    returns the major and minor process types
    """
    minor = map(lambda i: strip(i), min.split(","))
    minor = filter(None, minor)
    return {MAJOR_PROCESS_TYPE: maj, MINOR_PROCESS_TYPES: minor}

def process_example(ttext):
    """ 
    extract the synonym expression if any, and  
    the example in the parentheses and / alternation
    
    Ex: compensate (any escapism in the magazine is balanced by more practical items)
    (balance the books)
    ignore
    be present at / go to (I stopped off in London to attend a conference / I attended Cardiff University for 8 years)

    """
    return ttext

def rows2verb_form(rows):
    """ 
    transforms a set of rows into a VerbForm with 
    multiple entries of sense, participants, prepositions, etc.
    
    the columns are according to Version v3.2 of PTDB 
    """
    if not rows:
        return None
    
    entries = []
    for row in rows:
        entry = {}
        entry[SENSE] = row[3]
        entry[CARDIFF_FEATURE] = row[5]
        entry[LEVIN_FEATURE] = row[2]
        entry[PARTIC] = process_verb_participants(row[10])
        entry[PROCESS_TYPE] = row[5]
        entry[EXAMPLE_AND_SYNONYM] = row[3]
        entries.append(entry)

    verb, mandatory, optional = process_verb_form(rows[0][0])
    
    return VerbForm(verb, mandatory, optional, entries)
    

def process_verb_participants(textt):
    """ 
     * participant name: word
     * composite participant name: word-word
     * + additional participant
     * ( ) optional
     
        (...) this element is occasionally unrealized
        ((...)) this element is frequently unrealized
        (((...))) this element is almost always unrealized
        (... oblig) this element is obligatorily unrealized
        ... oblig this element is obligatorily realized
    
    Ex: Ag + Af-Ca (+ Des)
        Ag + Af-Ca ((+ Des))
        Ag (+ Af-Cog)oblig + Ph
        Ag + Af-Cog(oblig) + Ph
    
     """
    text = textt.replace("?", "")
    participants = []
    # TODO: reduce to CANONICAL 
    # print text.split("+")
    for pidx in text.split("+"):
        if pidx.find(")") > -1:
            if pidx.find("oblig") > -1:
                participants.append({PARTICIPANT_MANDATORY:pidx.replace(")", "").replace("(", "").replace("oblig", "").strip()})
            else:
                parenthesis_count = pidx.count(")")
                participants.append({PARTICIPANT_OPTIONAL:pidx.replace(")", "").replace("(", "").strip() ,
                                     PARTICIPANT_MISSING_FREQUENCY:parenthesis_count})
        else:
            participants.append({PARTICIPANT_MANDATORY:pidx.replace("(", "").strip()})
    # TODO: remove the previous distinctions
    text = textt.replace("?", "").replace("(", "").replace(")", "").replace("oblig", "").replace(" ", "")
    participants.append({ PARTICIPANT_CANONICAL:tuple(text.split("+"))})
    return participants


def process_verb_form(text):
    """
    verb form can be:
    *verb
    *verb + preposition 
    ** / - alternatives delimiter
    ** () - optional marker
    returns 
     * the verb infinitiv, 
     * mandatory (alternative) prepositions, 
     * optional (alternative) prepositions
    """
    verb = text.split(' ')[0]
    remaining = text.replace(verb, "")
    optionals = []
    mandatory = []
    # search for parentesises
    if re.search('\(|\)', remaining):
        remaining = remaining.replace("(", "")
        remaining = remaining.replace(")", "")
        optionals = remaining.split("/")
    else:
        mandatory = remaining.split("/")
    
    m, o = [], []
    for i in mandatory: m.append(strip(i))
    for i in optionals: o.append(strip(i))
    
    return strip(verb) , filter(None, map(strip, m)), filter(None, map(strip, o))
    
def read_and_transform():
    """ Reads the CSV file and transforms and writes it as a pickle """
    form = None
    rows = []
    verbs = []
    print "reading and processing CSV file: ", CSV_SOURCE
    with open(CSV_SOURCE, 'rb') as csvfile:
        # reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        
        for row in reader:
            if form is None: form = row[0]  # I know the first row contains a form
            if len(row[0].strip()) == 0: 
                rows.append(row)
            else:
                verbs.append(rows2verb_form(rows))
                rows = []
                form = row[0] 
                rows.append(row)
    csvfile.close()
    # pickle verb Forms
    print "writing Pickle file file: ", PICKLE_DESTINATION
    pickle.dump(verbs, open(PICKLE_DESTINATION, "wb"))
    print "End."



if __name__ == '__main__':
    read_and_transform()
