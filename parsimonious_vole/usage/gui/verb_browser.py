'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Mar 25, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

import sys

import  wx
import  wx.lib.mixins.listctrl  as  listmix
import wx.richtext as rt
from magic.parse.reader import db

import images
from parsimonious_vole.magic.parse.VerbForm import PARTICIPANT_CANONICAL, PREP_MANDT, PREP_OPT,\
    PARTIC, MAJOR_PROCESS_TYPE, MINOR_PROCESS_TYPES, PROCESS_TYPE, SENSE


class VerbListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)

#---------------------------------------------------------------------------
class VerbBrowser(wx.Frame):
    verb_list = None 
    
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.filterBar = wx.SearchCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)
        self.list = VerbListCtrl(self, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_SORT_DESCENDING)  # wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        
        self.richText = rt.RichTextCtrl(self, style=wx.VSCROLL | wx.HSCROLL | wx.NO_BORDER, size=(300, 500))

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnItemSelected, self.list)
        
        self.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.OnSearch, self.filterBar)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnSearch, self.filterBar)
        
        self.__set_properties()
        self.__do_layout()
        self.__populate_list()
        # end wxGlade

    def __set_properties(self):
        self.verb_list = sorted(db.get_all_entries(None).keys())

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        sizer_3.Add(self.filterBar, 0, wx.EXPAND, 0)
        sizer_3.Add(self.list, 1, wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 50, wx.EXPAND, 0)
        sizer_2.Add(self.richText, 50, wx.EXPAND, 0)
        self.SetSizer(sizer_2)
        sizer_2.Fit(self)
        self.Layout()
        # end wxGlade
        
    def __populate_list(self,filtered=None):
        self.il = wx.ImageList(16, 16)
        self.idx1 = self.il.Add(images.Pencil.GetBitmap())
        
        info = wx.ListItem()
        info.m_mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_IMAGE | wx.LIST_MASK_FORMAT
        info.m_image = -1
        info.m_format = 0
        info.m_text = "Verb Base Form"
        self.list.InsertColumnInfo(0, info)

        info.m_format = wx.LIST_FORMAT_RIGHT
        info.m_text = "Entries"
        self.list.InsertColumnInfo(1, info)

        presented_list = self.verb_list
        if filtered:
            presented_list = filter(lambda x: x.startswith(filtered) , self.verb_list)
        
        for key in presented_list:
            index = self.list.InsertImageStringItem(sys.maxint, key, self.idx1)
            self.list.SetStringItem(index, 1, str(len(db.get_all_entries(key))))

        self.list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
        self.list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
    
    def OnItemSelected(self, event):
        self.richText.Clear()
        self.currentItem = event.m_itemIndex
        entries = db.get_all_entries(event.Item.Text)
        for entry in entries: 
            self.writeToRichtext(event.Item.Text,entry)
        event.Skip()
        
    def OnSearch(self, evt):
        self.list.ClearAll()
        self.__populate_list(evt.String)
            
    def writeToRichtext(self,verb, entry):
        self.richText.Freeze()
        self.richText.BeginSuppressUndo()
        
        self.richText.BeginAlignment(rt.TEXT_ALIGNMENT_LEFT)
        self.richText.BeginFontSize(11)
        #self.richText.BeginBold()
        self.richText.WriteText("Verb: "+verb+" ")
        if entry[PREP_MANDT] or entry[PREP_OPT]:
            self.richText.WriteText("+ "+str(entry[PREP_MANDT]).replace("[","").replace("]","")+" "  )
            if entry[PREP_OPT]:
                self.richText.WriteText(str(entry[PREP_OPT]))
        self.richText.Newline()
        self.richText.WriteText("Process type: "+str(entry[PROCESS_TYPE][MAJOR_PROCESS_TYPE])+" - "+str(entry[PROCESS_TYPE][MINOR_PROCESS_TYPES]))
        self.richText.Newline()
        self.richText.WriteText("Configuration :"+str( filter(lambda x: PARTICIPANT_CANONICAL in x, entry[PARTIC])[0][PARTICIPANT_CANONICAL] ))
        self.richText.Newline()
        self.richText.WriteText("Sense: "+str(entry[SENSE]))
        self.richText.Newline()
        self.richText.WriteText("-"*40)
        self.richText.EndFontSize()
        self.richText.Newline()
        
        self.richText.EndSuppressUndo()
        self.richText.Thaw()

#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------
#---------------------------------------------------------------------------


def runMainFrame():
    app = wx.App()
    frame = VerbBrowser(None, -1, 'Verb Browser')
    frame.Center()
    frame.Show()
    app.MainLoop()

if __name__ == '__main__':
    runMainFrame()
    pass
