'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jul 11, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com


'''
from PyQt4 import QtGui, QtCore
import sys
import os


class FileBrowser(QtGui.QTreeView):
    """
        added 2 more actions and key bindings:
        set root directory: Return/Enter Key
        go one level up: Backspace 
        
        also filters displayed files: "*.txt","*.stp","*.mcg.xml","*.sents.xml","*.words.xml" 
    """
    enterKeyPressed = QtCore.pyqtSignal()
    backspaceKeyPressed = QtCore.pyqtSignal()
    climbedUP = QtCore.pyqtSignal(basestring)
    climbedDown = QtCore.pyqtSignal(basestring)
    
    def __init__(self,*args, **kwargs):
        super(FileBrowser,self).__init__(*args, **kwargs)
        self.setup()
    
    def setup(self):
        self.model = QtGui.QFileSystemModel()
        self.model.setNameFilters(["*.txt","*.stp","*.mcg.xml","*.sents.xml","*.words.xml"])
        self.model.setNameFilterDisables(False) # True to see all other files
#         self.setRootFolder(self.model.rootPath())
        self.setModel(self.model)
        self.setRootFolder(os.path.expanduser("~"))
        
        self.setAnimated(True)
        self.setSortingEnabled(True)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

        #signal connections
        self.selectionModel().selectionChanged.connect(self.renderView) # signal connection
        self.expanded.connect(self.renderView)
        self.collapsed.connect(self.renderView)
        self.backspaceKeyPressed.connect(self.climbUp)
        self.enterKeyPressed.connect(self.climbDown)
        self.model.directoryLoaded.connect(self.renderView)
        self.model.rootPathChanged.connect(self.renderView)
        
    def setRootFolder(self, path):
        self.setRootIndex(self.model.index(path))
        self.model.setRootPath(path)
        self.renderView()
    
    def keyPressEvent(self, event):
        super(FileBrowser,self).keyPressEvent(event)
        if event.type() == QtCore.QEvent.KeyPress:
            if event.key() in [QtCore.Qt.Key_Return]:
                self.enterKeyPressed.emit()
            elif event.key() == QtCore.Qt.Key_Backspace:
                self.backspaceKeyPressed.emit()

    def climbUp(self):
         
        path = os.path.dirname(str(self.model.rootDirectory().absolutePath()))
        self.setRootFolder(path)
        self.climbedUP.emit(str(path))
        
    def curentPath(self):
        if self.selectedIndexes() and self.selectedIndexes()[0]:
            path = (str(self.model.filePath(self.selectedIndexes()[0])))
            print path
            return path
        else:
            return
            
    def climbDown(self):
        if self.selectedIndexes() and self.selectedIndexes()[0]:
            if self.model.isDir(self.selectedIndexes()[0]):
                fp = self.model.filePath(self.selectedIndexes()[0])
                self.setRootFolder(fp)
                self.climbedDown.emit(str(fp))
            
    def renderView(self):
        self.resizeColumnToContents(0)
        self.resizeColumnToContents(1)
        self.resizeColumnToContents(2)
        self.resizeColumnToContents(3)
        self.model.sort(0,QtCore.Qt.AscendingOrder)
        self.sortByColumn(0,QtCore.Qt.AscendingOrder)
        

def app_exec():
    app = QtGui.QApplication(sys.argv)
    fb = FileBrowser()
    fb.setWindowTitle("Dir View")
    fb.resize(640, 480)
    fb.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    #app_exec()
    pass