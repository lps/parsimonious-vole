'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Jul 13, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import logging
import os
import sys

from PyQt4 import QtGui, QtCore
from usage.bundle_to_html import text_to_html_analisys
from usage.gui.vole_gui_main_window import Ui_MainWindow


##########################################################################################
# Some Logging capturing
##########################################################################################

class QtLoggingHandler(logging.StreamHandler, QtCore.QObject):
    """a simple logging handler """
    messageWritten = QtCore.pyqtSignal(str)
    def __init__(self):
        super(QtCore.QObject, self).__init__()
        super(QtLoggingHandler,self).__init__()
        
    def emit(self, record):
        record = self.format(record)
        self.messageWritten.emit(unicode("{}\n".format(record)))

class QtWriteStream(QtCore.QObject):
    """ a simple stream """
    messageWritten = QtCore.pyqtSignal(str)
    def __init__(self, stream):
        super(QtCore.QObject, self).__init__()
        self.stream = stream
    
    def write(self, txt):
        #print "Writting from wrapper"
        self.stream.write(">"+txt)
        self.messageWritten.emit(txt)

##########################################################################################
# Main App functionality
##########################################################################################

class VoleGui(QtGui.QMainWindow, Ui_MainWindow):
    """ """
    def __init__(self,*args, **kwargs):
        super(VoleGui,self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.setupMenu()
        self.setupConsoleLogger()
        self.setupFunctionality()
    
    def setupMenu(self):
        """ sets up the context menu for the file browser """
        self.fbContextMenu = QtGui.QMenu()   
        self.actionFullParse = self.fbContextMenu.addAction("Parse")
        self.actionFullParse.triggered.connect(self.fullParse)
        self.fileBrowserTreeView.customContextMenuRequested.connect(self.showFbContextMenu)

    def setupFunctionality(self):
        """ connextions between signals and slots """
        self.curPath.setText(self.fileBrowserTreeView.model.rootPath())
        self.fileBrowserTreeView.activated.connect(self.filePreview)
        self.fileBrowserTreeView.selectionModel().currentChanged.connect(self.currentChanged)
        self.folderUp.pressed.connect(self.fileBrowserTreeView.climbUp)
        self.folderDown.pressed.connect(self.fileBrowserTreeView.climbDown)
        self.fileBrowserTreeView.expanded.connect(self.currentExpanded)
        self.fileBrowserTreeView.climbedUP.connect(self.currentExpanded)
        self.fileBrowserTreeView.climbedDown.connect(self.currentExpanded)
    
    def setupConsoleLogger(self):
        """ setup logging levels and logging handlers
            TODO: incomplete
        """
        self.loggingHandler = QtLoggingHandler()
        self.loggingHandler.setLevel(logging.DEBUG)
        self.loggingHandler.setFormatter(logging.Formatter("%(levelname)s: %(message)s"))
         
        logging.getLogger("").addHandler(self.loggingHandler)
        logging.getLogger("").setLevel(logging.DEBUG)
        
        self.loggingHandler.messageWritten.connect(self.consoleTextBrowser.insertPlainText)
        # std err and out
        #self.old_stderr, self.old_stdout = sys.stderr, sys.stdout
        self.myErr = QtWriteStream(sys.stderr)
        self.myOut = QtWriteStream(sys.stdout)
        sys.stderr, sys.stdout = self.myErr, self.myOut
        
        self.myErr.messageWritten.connect(self.consoleTextBrowser.insertPlainText)
        self.myOut.messageWritten.connect(self.consoleTextBrowser.insertPlainText)
        
    ###########################################################333333
    
    def currentChanged(self, new_item, old_item):
        """ when selection changed emit the new item as being activated """
        self.fileBrowserTreeView.activated.emit(new_item)
        
    def currentExpanded(self,item):
        if isinstance(item, basestring):
            self.curPath.setText(item)
        else: self.curPath.setText(str(self.fileBrowserTreeView.model.filePath(item)))
        
    def filePreview(self, new_item):
        """ displays the file content in the central text box"""
        if not self.fileBrowserTreeView.model.isDir(new_item):
            fp = self.fileBrowserTreeView.model.filePath(new_item)
            self.centralTextBrowser.setSource(QtCore.QUrl(fp))
        else: self.centralTextBrowser.clear()
    
    def showFbContextMenu(self, point):
        """ display the context menu """
        #logging.debug("Test")
        self.fbContextMenu.exec_(self.fileBrowserTreeView.mapToGlobal(point))
    
    def fullParse(self):
        """ parse a .txt file """
        if not(self.fileBrowserTreeView.selectedIndexes() and self.fileBrowserTreeView.selectedIndexes()[0]):
            return
        item = self.fileBrowserTreeView.selectedIndexes()[0]
        
        if not self.fileBrowserTreeView.model.isDir(item):
            fp = str(self.fileBrowserTreeView.model.filePath(item))
            if fp.endswith(".txt"):
                logging.info("Running Parsimonoius Vole Parser on %s"%(os.path.basename(fp)))
                text_to_html_analisys(fp)
                logging.info("Done Parsing.")
    
if __name__ == '__main__':
#     e,o = QtWriteStream(sys.stderr), QtWriteStream(sys.stdout) 
#     sys.stderr = e
#     sys.stdout = o
    app = QtGui.QApplication(sys.argv)
    g = VoleGui()
    g.show()
    sys.exit(app.exec_())
