#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 4, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

magic.mood_graph_to_csv -- shortdesc

magic.mood_graph_to_csv is a command line tool to transform a mood graph (including feature structures) into a CSV format.

usage paratemetrs: /PATH_TO/dependegy_graph.xml, /PATH_TO/output.csv 

'''

import os
import sys

from parsimonious_vole.magic.guess.pattern_matching import get_dependent_tokens
from parsimonious_vole.magic.parse.parse_process import get_parse_result

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from parsimonious_vole.magic.guess.mcg_utils import is_pronoun
from parsimonious_vole.magic.parse.stanford_parser import call_stanford_parser_file

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from parsimonious_vole.magic.guess.mood import parse_mood, CLAUSE, Constituent, PREDICATOR, \
    PREDICATOR_FINITE, SUBJECT, ROOT
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.utils import uniquify, root_node, flatten
import csv
from parsimonious_vole.magic.guess.fs_constants import FINITE, THING, COMPLEMENT_AGENT
import glob

__all__ = []
__version__ = 0.2
__date__ = '2013-07-29'
__updated__ = '2014-05-02'

DEBUG = 1
TESTRUN = 0
PROFILE = 0


class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''

    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg

    def __str__(self):
        return self.msg

    def __unicode__(self):
        return self.msg


undetected_pronouns = ["one", "nobody", "everyone", "everybody", "someone", "somebody",
                       "nothing", "everything", "something", "anything",
                       "whatever", "whoever", "whomever", "whichever", ""
                                                                       "other", "others", "none", "all", "some", "any",
                       "such"]


def check_pronoun_and_determiner_in_subject(clause, sentence_constituency_graph, dependecy_graph):
    """ returns subject POS, pronoun if any and determiner if any """
    # TODO: check if there is pronoun in the subject
    # get subject
    subjects = filter(lambda x: x.type() in [SUBJECT], sentence_constituency_graph.successors(clause))
    subject = None
    if subjects:
        subject = subjects[0]
        head_word = subject.head_dependecy_node().word
        head_pos = subject.head_dependecy_node().pos
        pronoun = None
        determiner = None
        try:
            if head_pos in ["PRP", "PRP$"]:
                pronoun = subject.head_dependecy_node().word

            det_edges = get_dependent_tokens(dependecy_graph, subject.head_dependecy_node(), ["det"])
            if det_edges:
                determiner = ", ".join(map(lambda x: x[1].word, det_edges))
                # determiner = det_edges[0][1].word
        except Exception:
            pass
        # check for undetected pronouns
        if head_word in undetected_pronouns:
            pronoun = head_word

        return head_pos, head_word, pronoun, determiner
    else:
        return None, None, None, None


def get_participant_OCD_relevant_features(participant_node, mcg):
    """
        return the OCD relevant features as header array and string-values array.
        headder array:
            * Thing (S)
            * is Animate (B)
            * Is pronoun (B)
            * Person System Selection (S)
        ["Thing","animacy","is pronoun","person system"],
    """
    if not participant_node: return [None, None, None, None]
    thing = filter(lambda x: x.type() in [THING], mcg.successors(participant_node))
    thing = thing[0] if thing else None
    thing_words = None
    animacy = None
    pppp = None
    is_pron = "X" if is_pronoun(participant_node) else None
    if thing:
        thing_words = " ".join(thing.words())
        animacy = thing.feature_structure()["ANIMACY"] if "ANIMACY" in thing.feature_structure() else None
        # person system:
        person = thing.feature_structure()["PERSON"] if "PERSON" in thing.feature_structure() else ""
        int_type = thing.feature_structure()[
            "INTERACTANT-TYPE"] if "INTERACTANT-TYPE" in thing.feature_structure() else ""
        non_int_type = thing.feature_structure()[
            "NON-INTERACTANT-TYPE"] if "NON-INTERACTANT-TYPE" in thing.feature_structure() else ""
        one_ref = thing.feature_structure()[
            "ONE-REFERENT-TYPE"] if "ONE-REFERENT-TYPE" in thing.feature_structure() else ""
        sex = thing.feature_structure()["SEX"] if "SEX" in thing.feature_structure() else ""
        pppp = ", ".join([x for x in flatten([person, int_type, non_int_type, one_ref, sex]) if x])
    return [thing_words, animacy, is_pron, pppp]


def write_headders(csv_writer):
    """ """
    csv_writer.writerow(["Full Sentence",
                         "Clause", "Is Main Clause?",
                         "Predicator",
                         "Finiteness",
                         "Voice",  #
                         "Polarity",
                         "Modal Verb",  # if any
                         "Modality",  # if any
                         "Subject Thing",  #
                         "Subject animacy",  #
                         "Subject pronoun",  # if any
                         "Subject person system selection",
                         # speaker/speaker-plus/addresse/one-refent/plural-referent/generalized
                         "Compl_Agent Thing",  #
                         "Compl_Agent animacy",  #
                         "Compl_Agent pronoun"  # if any
                         "Compl_Agent person system selection",
                         # speaker/speaker-plus/addresse/one-refent/plural-referent/generalized
                         ])


def clean_str(clause):
    """ """
    return " ".join(clause.words())


def constituency_graph_to_cvs_rows(sentence_constituency_graph, dependecy_graph):
    """ writing a sentence graph """
    result_rows = []
    clause_nodes = uniquify(filter(lambda x: x.type() is CLAUSE, sentence_constituency_graph.nodes()))
    result_rows.append([clean_str(root_node(sentence_constituency_graph))])
    for clause in clause_nodes:
        if isinstance(clause, Constituent):
            # collecting the fields
            # mood = clause.feature_structure()[MOOD] if MOOD in clause.feature_structure() else None
            # if mood == UNKNOWN: mood = None
            voice = clause.feature_structure()["VOICE-TYPE"] if "VOICE-TYPE" in clause.feature_structure() else None
            polarity = clause.feature_structure()[
                "POLARITY-TYPE"] if "POLARITY-TYPE" in clause.feature_structure() else None

            deicticity = clause.feature_structure()[
                "DEICTICITY"] if "DEICTICITY" in clause.feature_structure() else None
            finites = filter(lambda x: x.type() in [FINITE], sentence_constituency_graph.successors(clause))
            modal_verb, modal_selections = None, None
            if deicticity == "modal":
                modal_verb = " ".join(finites[0].words()) if finites else None
                et = clause.feature_structure()[
                    "EPISTEMIC-TYPE"] if "EPISTEMIC-TYPE" in clause.feature_structure() else []
                rt = clause.feature_structure()["ROOT-TYPE"] if "ROOT-TYPE" in clause.feature_structure() else []
                modal_selections = ", ".join(et + rt)

            is_main_clause = "X" if filter(lambda x: x[0].type() == ROOT,
                                           sentence_constituency_graph.in_edges(clause)) else None

            finitness = clause.feature_structure()['FINITNESS'] if "FINITNESS" in clause.feature_structure() else None
            finitness = finitness[0] if isinstance(finitness, list) else finitness

            predicator = filter(lambda x: x.type() in [PREDICATOR, PREDICATOR_FINITE],
                                sentence_constituency_graph.successors(clause))
            predicator = predicator[0].head_dependecy_node().lemma if predicator else None

            # subject_head_pos,subject_head_word, pronoun, determiner = check_pronoun_and_determiner_in_subject(clause, sentence_constituency_graph,dependecy_graph)
            subject = filter(lambda x: x.type() in [SUBJECT], sentence_constituency_graph.successors(clause))
            subject = subject[0] if subject else None
            agent_complement = filter(lambda x: x.type() in [COMPLEMENT_AGENT],
                                      sentence_constituency_graph.successors(clause))
            agent_complement = agent_complement[0] if agent_complement else None

            # writting to file
            result_rows.append([None, clean_str(clause), is_main_clause,
                                predicator, finitness, voice, polarity,
                                modal_verb, modal_selections, ] +
                               get_participant_OCD_relevant_features(subject, sentence_constituency_graph) +
                               get_participant_OCD_relevant_features(agent_complement, sentence_constituency_graph))
    return result_rows


def dependecy_to_csv(dependecy_file_name):
    """ """
    # creating a new csv file
    with open(dependecy_file_name + '.csv', 'w') as csvfile:
        # csvfile = open(dependecy_file_name+'.csv', 'wb')
        csv_writer = csv.writer(csvfile, dialect=csv.excel)
        write_headders(csv_writer)

        # reading the XML into a DG
        parse_result = get_parse_result(dependecy_file_name)
        print len(parse_result.sentence_data)

        for sentence in parse_result.sentence_graphs:
            sentence = correct_dependecy_parse_errors(sentence)
            # creating a mood graph
            constituency_graph = parse_mood(sentence)
            if constituency_graph:  # sometimes there are minor clauses result in empty MCG's
                csv_writer.writerows(constituency_graph_to_cvs_rows(constituency_graph, sentence))
        # clean file close
        csvfile.close()
    return True


def main(argv=None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Costetchi Eugeniu.
  Copyright 2013-2014 Henri Tudor. All rights reserved.
   
  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc)

    # Setup argument parser
    parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("path",
                        help="path to the folder containing the XML files produced by the Stanford dependecy parser")

    # Process arguments
    args = parser.parse_args()
    dependecy_folder = args.path
    print "Searching for .TXT .XML and .STP files in ", dependecy_folder, " (.XML are the old dependency representation, equivalent of .STP)"
    stp_file_list = glob.glob(dependecy_folder + "/*.stp")
    txt_file_list = [x for x in glob.glob(dependecy_folder + "/*.txt") if x + ".stp" not in stp_file_list]
    print "Reducing the work to the difference .TXT->.STP->.CVS In order to execute full work delete .STP and/or .CSV files"

    if txt_file_list:
        print "Running Stanford Parser (.TXT->.STP)"
    else:
        "All text files are already parser and corresponding .STP exists."
    # transform
    for fle in txt_file_list:
        print "-" * 30 + " processing ", str(fle)
        call_stanford_parser_file(fle)

    csv_file_list = glob.glob(dependecy_folder + "/*.csv")
    xml_file_list = [x for x in glob.glob(dependecy_folder + "/*.xml") if x + ".csv" not in csv_file_list]  # legacy
    stp_file_list = [x for x in glob.glob(dependecy_folder + "/*.stp") if x + ".csv" not in csv_file_list]

    if stp_file_list:
        print "Running Parsimonious Vole  (.STP->.CSV)"
    else:
        "All text files are already parser and corresponding .STP exists."

    for f in stp_file_list:
        print "-" * 30 + " processing ", str(f)
        dependecy_to_csv(f)
    for f in xml_file_list:  # legacy
        print "-" * 30 + " processing ", str(f)
        dependecy_to_csv(f)


if __name__ == "__main__":
    pass
