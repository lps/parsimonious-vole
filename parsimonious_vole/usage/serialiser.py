'''
Created on 25 Jun 2015

@author: Alain Pfeiffer [alain.pfeiffer@list.lu]
'''
import json

NODES = "nodes"
EDGES = "edges"
SENTENCE = "sentence"
WORDS = "words"
LEMMAS = "lemmas"
POS = "pos"


def to_file(string, filename):
    with open(filename, 'w') as text_file:
        text_file.write(string)


def mood_graph_to_json(mcg):
    """ transform from a mood constituencz graph into a json string """
    res = {NODES: {}, EDGES: []}
    for n in mcg.nodes():
        n_value = n.feature_structure()
        n_value.update({WORDS: map(lambda x: x.word, n.nodes()),
                        LEMMAS: map(lambda x: x.lemma, n.nodes()),
                        POS: map(lambda x: x.pos, n.nodes()), }
                       )
        res[NODES][n.name()] = n_value

    for edge in mcg.edges():
        res[EDGES].append([edge[0].name(), edge[1].name()])
    return res


def mood_graph_bundle_to_json(mcg_bundle):
    """ transform from a mood constituencz graph into a json string """
    res = []
    for mcg in mcg_bundle:
        res.append(mood_graph_to_json(mcg))
    return res


def serialise_mcg_bundle(filename, mcg_bundle):
    """ """
    mcgb = mood_graph_bundle_to_json(mcg_bundle)
    with open(filename, "wb") as f:
        f.write(json.dumps(mcgb, sort_keys=True, indent=4, separators=(",", ":")))


if __name__ == '__main__':
    # mcgb = parse_mood_txt_file("C:/Users/pfeiffer/Desktop/temp/mte1.txt")
    # serialise_mcg_bundle("C:/Users/pfeiffer/Desktop/temp/mte1.txt.json", mcgb)
    # print "Done."
    pass
