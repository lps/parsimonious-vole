'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Nov 27, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''

import itertools
import logging
import os
import pprint
import traceback
from shutil import copyfile

from parsimonious_vole.magic.guess.mood import CLAUSE, parse_mood
from parsimonious_vole.magic.guess.mood_binding_and_control_enricher import parse_mood_post_enrichment
from parsimonious_vole.magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
from parsimonious_vole.magic.parse.parse_constants import TYPE
from parsimonious_vole.magic.parse.parse_process import get_parse_result
from parsimonious_vole.magic.parse.stanford_parser import call_stanford_parser_file
from parsimonious_vole.magic.taxis.rules_taxis import build_caluse_complex_graph
from parsimonious_vole.magic.utils import root_node, draw_graph, filter_out_keys
from networkx.algorithms.shortest_paths.generic import shortest_path_length
from networkx.algorithms.traversal.breadth_first_search import bfs_successors
from nltk.featstruct import FeatDict

from parsimonious_vole.configuration import PATH_TO_PROJECT
from parsimonious_vole.magic.guess.transitivity import parse_transitivity_from_mood_graph


###################################################################################
# functions generating html
###################################################################################

def __generate_tooltip_with_id(id,fs):
    """ returns a tooltip """
    
    def __reduce_fs():
        return {k:v if not isinstance(v, list) else v if len(v)>1 else v[0] for k,v in fs.items()}
        
    html_result = "<div class=tooltiptext id="+id+">"
    r = []
    pretty_items(r, __reduce_fs())
    html_result += "\n".join(r)
    html_result += "</div>"
    return html_result 

def create_mood_analisys_of_sentence(mood_graph):
    """ """
    def items_up_to_token(index):
        max_tokes = root_node(mood_graph).nodes()
        for i in range(0, len(max_tokes)):
            if max_tokes[i] == index:
                return i
        return -1
    
    def gap_between_components(component1, component2):
        """ """
        c1_c2 = component2.boundaries()[0].id() - component1.boundaries()[1].id()
        c2_c1 = component1.boundaries()[0].id() - component2.boundaries()[1].id() 
        if c1_c2 > 0:return c1_c2 - 1
        else: return c2_c1 - 1
    
    page_html_content = ""
    r = root_node(mood_graph)
    #small sanity check
    if not (r and mood_graph): return "<p> Oops, could not process the text. Perhaps no root node could be found in MCG. </p>" 
    successors = bfs_successors(mood_graph, r)
    successors = list(itertools.chain.from_iterable(successors.values()))
    successors = map(lambda x: (shortest_path_length(mood_graph, r, x), x) , successors)
    successors = sorted(successors, key=lambda x: (x[0], x[1].lowest_token_index()))
    
    nodes_per_lvl = {}
    # create dict of nodes per lvl
    for i in successors:
        if i[0] not in nodes_per_lvl:
            nodes_per_lvl[i[0]] = []
        nodes_per_lvl[i[0]].append(i[1])
    # sort nodes at each lvl
    for i in nodes_per_lvl.keys():
        nodes_per_lvl[i] = sorted(nodes_per_lvl[i], key=lambda x: x.lowest_token_index())
    # draw them
    page_html_content += '<table> <caption><b>Mood Constituency</b></caption>'
    
    for level in nodes_per_lvl.keys():
        page_html_content += '<tr>'
        previous_component = None
        for current_comp in nodes_per_lvl[level]:
            if not previous_component:
                # print items_up_to_token(current_comp.lowest_token_index())
                if items_up_to_token(current_comp.lowest_token_index()) > 0:
                    page_html_content += "<td colspan='" + str(items_up_to_token(current_comp.lowest_token_index())) + "'></td>"
            else:
                #print gap_between_components(previous_component, current_comp)
                if gap_between_components(previous_component, current_comp) > 0:
                    page_html_content += "<td colspan='" + str(gap_between_components(previous_component, current_comp)) + "'></td>"
                    
            page_html_content += "<td colspan='" + str(len(current_comp.nodes())) + "'>" + \
                                    " <span class='name' id="+current_comp.name()+">"+current_comp.name() + "</span><br/>" + \
                                    __generate_tooltip_with_id(current_comp.name(), filter_out_keys(current_comp.feature_structure())) + \
                                    "<span class='featureStructure'>" + \
                                    pprint.pformat(filter_out_keys(current_comp.feature_structure())) + "</span></td>" # .replace('\n', '<br />')
            previous_component = current_comp
        page_html_content += '</tr>'
    # writing words
    page_html_content += '<tr>'
    for i in r.nodes():
        page_html_content += '<td> <span class="sentenceWords">' + i.word + '</span> </td>'
    page_html_content += '</tr>'

    page_html_content += '</table>'
    return page_html_content
    
def create_clause_listing(mcg):
    page_html_content = '<table> <caption><b>Clauses(+transitivity,+control)</b></caption>'
    
    def  get_clause_nodes_ordered():
        traverse = itertools.chain.from_iterable(bfs_successors(mcg, root_node(mcg)).values())
        return filter(lambda x: x.type() == CLAUSE, traverse)
    
    clauses = get_clause_nodes_ordered()
    for clause in clauses:
        constituents = sorted(filter(lambda x: x.type() != CLAUSE, mcg.successors(clause)), key=lambda x: x.lowest_token_index())
        page_html_content += "<tr><td colspan='" + str(len(constituents)) + "' >"
#         pprint_lines = []
#         pretty_items(pprint_lines, clause.feature_structure())
        page_html_content +=  "<span class='name' id="+clause.name()+">" +clause.name()+ " </span><br/>"
        page_html_content +=  "<span class='sentenceWords'>" + " ".join(clause.words()) + " </span>\
         <br/> <span class='featureStructure'>" + pprint.pformat(filter_out_keys(clause.feature_structure())) + "</span>" # .replace('\n', '<br/>')
        page_html_content += '</td></tr>'
        page_html_content += '<tr>'
        for constituent in constituents:
            page_html_content += '<td>'
            page_html_content += "<span class='name' id='"+constituent.name()+"'> " + constituent.name() + "</span> <br/>" +\
            __generate_tooltip_with_id(constituent.name(),filter_out_keys(constituent.feature_structure())) + \
            "<span class='sentenceWords'>" + " ".join(constituent.words()) + "</span><br/>"\
                                + "<span class='featureStructure'> "+ pprint.pformat(filter_out_keys(constituent.feature_structure())) + "</span>" # .replace('\n', '<br />') 
            page_html_content += '</td>'
        page_html_content += '</tr>'
    # providing the links between clauses
#    for clause in clauses:
#        clause_parents = filter(lambda x: x.type()==CLAUSE, mcg.predecessors(clause))
#        if clause_parents:
#            page_html_content += '<tr>'
#            page_html_content += '</tr>'
    page_html_content += '</table>'
    return page_html_content

def pretty_items(r, d, nametag="<strong>%s: </strong>", itemtag='<li>%s</li>',
             valuetag="%s", blocktag=('<ul>', '</ul>')):
    """r - result, d - dictionary """
    if isinstance(d, dict):
        r.append(blocktag[0])
        for k, v in d.iteritems():
            name = nametag % k 
            if isinstance(v, dict) or isinstance(v, list):
                r.append(itemtag % name)
                pretty_items(r, v)
            else:
                value = valuetag % str(v)
                r.append(itemtag % (name + value))
        r.append(blocktag[1])
    elif isinstance(d, list):
        r.append(blocktag[0])
        for i in d:
            if isinstance(i, dict) or isinstance(i, list):
                r.append(itemtag % " - ")
                pretty_items(r, i)
            else:
                r.append(itemtag % i)
        r.append(blocktag[1])

##########################################################################

def create_dir_structure(filename):
    """ """
    dir_name = os.path.join(os.path.dirname(filename),"resources")
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    ###
    copyfile(PATH_TO_PROJECT+"/resources/html_output/code.js",dir_name+"/code.js")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery-1.10.2.min.js",dir_name+"/jquery-1.10.2.min.js")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/defaultStyle.css",dir_name+"/defaultStyle.css")

    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery.qtip.css",dir_name+"/jquery.qtip.css")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery.qtip.js",dir_name+"/jquery.qtip.js")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery.qtip.min.js",dir_name+"/jquery.qtip.min.js")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery.qtip.min.css",dir_name+"/jquery.qtip.min.css")
    copyfile(PATH_TO_PROJECT+"/resources/html_output/jquery.qtip.min.map",dir_name+"/jquery.qtip.min.map")
    
    return dir_name

def create_taxis_graph(mcg):
    """ """
    txg = build_caluse_complex_graph(mcg)
    if not txg: return "<p>No clause complexes, just a simple clause.</p> "
    output = "<table> <caption><b>Taxis (Clause Complex analysis)</b></caption>"
    for edge in txg.edges(data=True):
        #pprint.pprint(edge)
        output += "<tr>"
        output += "<td><span class='name' id = '"+edge[0].name()+"'>"+edge[0].name()+ "</span></br>"
        output += "<span class='sentenceWords'>"+ " ".join(edge[0].words())+"</span></td>"
        output += "<td> <span class='name' id = '"+edge[1].name()+"'>" +edge[1].name() + "</span></br>"
        output += "<span class='sentenceWords'>"+" ".join(edge[1].words())+"</span></td>"
        output += "<td> <span class='featureStructure'>" + str(FeatDict(edge[2][TYPE])) if TYPE in edge[2] else str(FeatDict(edge[2])) + "</span> </td>"
        output += "</tr>"
    #print output + "</table>"
    return output + "</table>"

HEADDER = """<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd"> 
<html> <head>   <title> Sentence Analysis by Magic Parser</title> </head>

<link rel="stylesheet" href="resources/defaultStyle.css">
<link rel="stylesheet" href="resources/jquery.qtip.css">
 
<script src="resources/jquery-1.10.2.min.js"></script>
<script src="resources/jquery.qtip.js"></script>
<script src="resources/code.js"></script>

<body> """

FOOTER = """</body></html>"""

def output_analisys_of_parse_bundle_to_file(output_filename, parse_bundle_file):
    """
        creates the HTML file with syntactic analysis of all sentences in parse_bundle_file
        
        output_filename: path to html file
        parse_bundle_file: path to parse bundle file which contains Stanford parser outputs
                             in XML or text format 
    """
    res_dir = create_dir_structure(output_filename)
    p = get_parse_result(parse_bundle_file)
    f = open(output_filename, "wb")
    f.write(HEADDER)
    counter = 0
    try:
        for sentence in p.sentence_graphs:
            counter+=1
            correct_dependecy_parse_errors(sentence)
            draw_graph(sentence, os.path.join(res_dir,"dgimage_"+str(counter)))
            mcg = parse_mood(sentence)
            #
            first_content = create_mood_analisys_of_sentence(mcg)
            parse_mood_post_enrichment(mcg,sentence)
            mcg = parse_transitivity_from_mood_graph(mcg,sentence)
            #
            second_content = ""
            try: 
                second_content = create_clause_listing(mcg)
            except:
                second_content = "<p> Failed to create the clause listing enriched with transitivity and controlled constituents.</p>"
            try:
                third_content = create_taxis_graph(mcg)
            except:
                traceback.print_exc()
                third_content = "<p> Failed to create the clause complex analysis comprising the intrer-clausal relations.</p>"
            
            f.write("<div class='sentenceAnalisys'>")
            f.write('<span class="sentenceId"> '+str(counter-1)+'</span> <img src="resources/dgimage_'+str(counter)+'.png" alt="Sentence Dependecy Tree" width="5%" >')
            f.write(first_content)
            f.write(second_content)
            f.write(third_content)
            f.write("</div>")
    except Exception,e:
        print traceback.format_exc()
        f.write(FOOTER)
        f.close()

    if not f.closed:
        f.write(FOOTER)
        f.close()

def text_to_html_analisys(text_filename):
    call_stanford_parser_file(os.path.abspath(text_filename) )
    dirname = os.path.abspath(text_filename+".html")
    logging.info("Making folder %s"%dirname)
    os.mkdir(dirname)
    output_analisys_of_parse_bundle_to_file(os.path.join(dirname,"index.html"), text_filename+".stp")
 
if __name__ == '__main__':
    # text_to_html_analisys("../resources/control_and_binding.txt")
    pass