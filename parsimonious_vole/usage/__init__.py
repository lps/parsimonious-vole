'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
#
# from magic.parse.stanford_parser import parse_result_file
# from magic.parse.correct_dep_parse_errors import correct_dependecy_parse_errors
# from magic.guess.mood import parse_mood
# from magic.guess.transitivity import parse_transitivity_from_mood_graph

# def complete_parse(txt_file_name,return_dg_bundle=False):
#     dg_bundle = parse_result_file(txt_file_name).sentence_graphs
#     mcg_bundle = []
#     for dg in dg_bundle:
#         dg = correct_dependecy_parse_errors(dg)
#         mcg = parse_mood(dg)
#         mcg = parse_transitivity_from_mood_graph(mcg, dg)
#         mcg_bundle.append(mcg)
#     if return_dg_bundle: return (mcg_bundle,dg_bundle)
#     return mcg_bundle