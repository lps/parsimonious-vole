'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Oct 15, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

reads/writes UAM corpus Tool Scheme files
'''
import logging

import bs4.element
from bs4 import BeautifulSoup
from nltk.featstruct import FeatStruct


class SystemicFeature(object):
    """ """
    name = None
    state = "active"
    comment = None
    system_list = []
    
    def __init__(self, _name, _state="active", _comment=""):
        self.system_list = []
        self.name = _name.lower()
        self.state = _state
        self.comment = _comment
        self.host_system = None
    
    def __str__(self, *args, **kwargs):
        return self.name + (" ~ " + str(map(lambda x: str(x), self.system_list)) + "" if self.system_list else "") 
    
    def __repr__(self, *args, **kwargs):
        return "" + self.name + ":" + str(map(lambda x: x.name, self.system_list))
    
    def add_system(self, system):
        if system and system not in self.system_list: 
            # print "...adding system ", repr(system), " to feature", repr(self)
            self.system_list.append(system)
            system.add_extended_feature(self)
#             if self not in system.extended_feature_list: system.extended_feature_list.append(self) 
        
class SystemicSystem(object):
    """
    
    """
    name = None
    feature_list = None
    extended_feature_list = None
    extended_condition_operation = "and"
    extended_feature_names = None
    
    def __init__(self, _name, _feature_list, _ec_feat_names=[], _ec_operation="and"):
        self.name = _name.upper()
        # telling features to what system they belong
        self.feature_list = _feature_list
        for f in self.feature_list:
            f.host_system = self
            
        self.extended_feature_names = _ec_feat_names 
        self.extended_condition_operation = _ec_operation
        self.extended_feature_list = []
    
    def __str__(self, *args, **kwargs):
        return "" + self.name + "{ " + ", ".join(map(lambda x: str(x), self.feature_list)) + " }"
    
    def __repr__(self, *args, **kwargs):
        return "" + self.name + ":" + str(self.extended_feature_names) + \
                    ":" + str(map(lambda x: x.name, self.extended_feature_list)) + \
                    ":" + str(map(lambda x: x.name, self.feature_list))
    
    def add_extended_feature(self, feature):
        if feature and feature not in self.extended_feature_list:
            # print "...adding EC", repr(feature), " to ", repr(self)
            self.extended_feature_list.append(feature)
            feature.add_system(self)

class SystemicNetwork(object):
    """
     a systemic network that starts with a root feature 
    """
    root_feature = None

    def __init__(self, _root_feature,list_of_systems):
        self.root_feature = _root_feature
        #""" a set of tuples. each tuple is a path to selected feature in the network """
        self.__paths = None
        if not self.root_feature:
            self.root_feature = SystemicFeature("root")
            self.root_feature = SystemicSystem("ROOT", [])
        self.__link_systems(list_of_systems)
    
    def __link_systems(self, list_of_systems):
        """  construct the systemic network by linking the systems to features """
        # building feature index, by name, {f_name:f_object}
        feature_index = {self.root_feature.name:self.root_feature}
        for i in list_of_systems:
            for j in i.feature_list:
                feature_index[j.name] = j
        # linking system to feature
        for ssys in list_of_systems:
            for ecfn in ssys.extended_feature_names:
                feature_index[ecfn].add_system(ssys)
             
    def feature_paths(self):
        """ return all possible paths in the systemic network """
        if not self.__paths:
            self.__paths = set() 
            for i in self.__path_generator(self.root_feature, (self.root_feature,)):
                self.__paths.add(i)
        return self.__paths
        
    def select_feature(self, feature_name, all_paths=False):
        """ returns the path to feature """
        
        def get_sub_path_to_feature(pth):
            for feature in pth:
                if feature.name == feature_name:
                    idx = pth.index(feature)
                    return tuple(pth[:idx + 1])
            return None
        
        relevant_paths = set()
        for path in self.feature_paths():
            p = get_sub_path_to_feature(list(path))
            if not all_paths:
                # return the first found
                if p: return p
            else:
                # build the set of paths
                relevant_paths.add(p)
        # return result 
        if relevant_paths:
            return relevant_paths
        else:
            logging.error("Could not find feature '%s' in the systemic network." % feature_name)
            return None
    
    def __path_generator(self, _current_feature, cur=()):
        if not _current_feature:
            yield cur
        else:
            if not _current_feature.system_list:
                yield cur
            else:
                for sys in _current_feature.system_list:
                    if not sys or not sys.feature_list:
                        yield cur
                    else:
                        for f in sys.feature_list:
                            for path in self.__path_generator(f, cur + (f,)):
                                yield path
    
    def __system_iterator(self, _current_feature):
        """ """
        if _current_feature:
            for s in _current_feature.system_list:
                for f in s.feature_list:
                    for i in self.__system_iterator(f):
                        yield i 
                yield s
    
    def system_iterator(self):
        return self.__system_iterator(self.root_feature)
    
    def __str__(self, *args, **kwargs):
        return self.root_feature.__str__()
    
    def __repr__(self, *args, **kwargs):
        return self.__str__()
 
def feature_path_to_string(path):
    return "; ".join(map(lambda y: y.name, path))
 
def feature_path_to_string_list(path):
    return [x.name for x in path] #map(lambda y: y.name, path)

def feature_path_to_feature_structure(path):
    """ transforms the path or the set of paths into the feature structure """
    if not path:return FeatStruct()
    
    _path_list = None
    if isinstance(path, set): 
        _path_list = map(lambda x: list(x), filter(None, list(path)))
    elif isinstance(path, tuple): 
        _path_list = [list(path)]
        
    def _single_path_to_fs(pth):
        """ transform a path to fs"""    
        f = FeatStruct({pth[-1].host_system.name:pth[-1].name})
        for feature in pth[:-1][::-1]:
            if feature.host_system:
                f = FeatStruct({feature.host_system.name : feature.name}).unify(f)
        return f
    # transforming
    result = None 
    for p in _path_list:
        if result == None:
            result = _single_path_to_fs(p)
        else:
            result = result.unify(_single_path_to_fs(p))
            
    return result

# def filter_features_structure_by_systemic_network(feature_structure,systemic_network):
#     """ returns a subset of the feature structure with only features that are contained within the systemic network"""
    

def read_systemic_network_from_file(filename):
    soup = BeautifulSoup(open(filename), "xml")
    root_feature = SystemicFeature(soup.NETWORK.ROOT.FEATURE.NAME.contents[0],
                                         soup.NETWORK.ROOT.FEATURE.STATE.contents[0])
    list_of_systems = []
    
    for system in filter(lambda x: isinstance(x, bs4.element.Tag), soup.NETWORK.SYSTEMS):
        _name = system.NAME.string
        _feature_list = []
        # reading entry conditions EC
        _ef_names = []
        _ef_operation = system.EC.OP.contents[0] if system.EC.OP else None
        if not _ef_operation: _ef_names = [system.EC.string]
        else:
            for _ec_feat in filter(lambda x: isinstance(x, bs4.element.Tag) and x.name == "FEATURE", system.EC):
                _ef_names.append(_ec_feat.NAME.contents[0])
        # reading features
        for feature in filter(lambda x: isinstance(x, bs4.element.Tag), system.FEATURES):
            f = SystemicFeature(feature.NAME.contents[0], feature.STATE.contents[0])
            if feature.GLOSS and feature.GLOSS.contents[0]:
                f.comment = feature.GLOSS.contents[0]
            _feature_list.append(f)
        #
        ss = SystemicSystem(_name, _feature_list, _ef_names, _ef_operation)
        list_of_systems.append(ss)
    sn = SystemicNetwork(root_feature,list_of_systems)
    return sn

if __name__ == '__main__':
    #sn = read_systemic_network_from_file("/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/resources/uam_sys_networks/mood_features.xml")
    #sn = read_systemic_network_from_file("/home/lps/Desktop/Church mouse_Schulz/Schemes/determination.xml")
    #print sn
    #pprint.pprint(sn.feature_paths())
    #print sn.select_feature("male")
    #print feature_path_to_feature_structure( list(sn.feature_paths())[8])
    #print feature_path_to_feature_structure(sn.select_feature("near", True))
    # print sn.feature_paths()
    
    #pprint.pprint( generate_dictionary_from_feature_glosses("/home/lps/Desktop/Church mouse_Schulz/Schemes/person_system.xml"))
     
    # d = generate_word_features_from_network_scheme("/home/lps/Desktop/Church mouse_Schulz/Schemes/determination.xml")
    # pprint.pprint(d)
    #print {x:x for x in uniquify(flatten([feature_path_to_string_list(x) for x in read_systemic_network_from_file(CONSTITUENT_SYSTEM).feature_paths()]))}
    pass
