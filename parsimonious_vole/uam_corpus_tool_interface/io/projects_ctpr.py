'''
Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on May 23, 2014

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com
'''
import logging
import os

from parsimonious_vole.magic.utils import find_files


def read_CTPR_project(ctpt_file_name, target_layer_name):
    """
        returns the dictionary of text files and corresponding analysis files 
    """
    logging.info("*** Reading Project (Corpus - Annotation file pairs) %s"%os.path.basename(ctpt_file_name))
    based_dir = os.path.dirname(ctpt_file_name)
    txt_files = list(find_files(os.path.join(based_dir, "Corpus"), "*.txt"))
    xml_files = list(find_files(os.path.join(based_dir, "Analyses"), "*%s*.xml" % target_layer_name))
    if not txt_files or not xml_files: return {}
    return {k:filter(lambda x: os.path.basename(k) in x, xml_files) for k in txt_files}

if __name__ == '__main__':
    pass