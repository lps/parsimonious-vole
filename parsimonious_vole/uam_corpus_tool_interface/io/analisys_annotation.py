'''

Copyright (C) 2013 CRP Henri Tudor All Rights Reserved
@license: GNU General Public License, version 2

Created on Oct 15, 2013

@author:  Eugeniu Costetchi
@contact: costezki.eugen@gmail.com

reads UAM corpus Tool Analysis files, and can generate evaluation metrics as CSV files

1. read the segments 

'''
from __future__ import division

import codecs
import copy
import logging
import math
import os
import re

import chardet
from bs4 import BeautifulSoup
from parsimonious_vole.magic.utils import multi_filter , uniquify, index_words, los_in_los
from nltk.metrics.distance import edit_distance

from parsimonious_vole.magic.parse.stanford_parser import STP_word_translation


class Segment(object):
    """ General Class for annotation segment """
    start = None
    end = None
    id = None
    features = None
    text = None
    
    def __init__(self, _start, _end, _id=-1, _features=None, _text=""):
        assert int(_start) < int(_end)
        self.start = int(_start)
        self.end = int(_end)
        self.id = int(_id)
        self.features = _features
        self.text = _text
        
    def __str__(self, *args, **kwargs):
        return "id:" + str(self.id) + " offset(" + str(self.start) + "," + str(self.end) + \
                ") features:" + str(self.features) + "'" + self.text + "'"
    
    def __repr__(self, *args, **kwargs):
        return self.__str__(*args, **kwargs)

    def overlap_metric(self, other, include_perc=False):
        """  
            * returns the length of the common part
            * if include_perc, then will return the overlap length as percent 
                of the 1st and 2nd segment length 
        """
        overlap = max(0, min(self.end, other.end) - max(self.start, other.start))
        if include_perc and overlap == 0: return 0, 0, 0
        if include_perc:
            return overlap, overlap / self.len()  , overlap / other.len()
        return overlap
    
    def distance_geometric(self, other):
        """ calculates geometric distance between points formed by the segments s1(start,end) ~ s2(start,end) """
        return math.hypot(self.start - other.start, self.end - other.end)
    
    def distance_edit(self, other):
        """ returns Levenshtein edit-distance """
        return edit_distance(self.text, other.text)
    
    def overlap(self, other):
        """ return true if the segments have any parts in common"""
        return self.overlap_metric(other, False) > 0
    
    def len(self):
        """ return the length of the segment"""
        return self.end - self.start
    
    def contains(self, other):
        """ return true if the segment contains the Otehr segment """
        return (self.overlap_metric(other, False) == other.len())
    
    def __eq__(self, other):
        """ """
        if not other: return False
        return self.start == other.start and\
                self.end == other.end and \
                self.id == other.id and \
                set(self.features) == set(other.features) and\
                self.text == other.text
    
    def is_feature_selected(self, features, all_selected=False):
        """
            checks if features from feature list are selected in the segment.
            by default if any of the features from the feature list matched then
            returns true. If the flag all_selected is true, then the feature_list
            shall be a subset of features of segment features.
        """
        f = features if isinstance(features, (list, set)) else [features]
        return los_in_los(f, self.features, False, True) if all_selected else set(f) <= set(self.features)
    
    def match_text_position(self, other, lambda_functions=[]):
        """ 
            returns true if the two segments are considered to have 
            same position and False if the two segments are in unrelated
             positions.
            *  perfect match will be returned if no lambda function provided. 
               if list of lambda function are provided then returns true if any 
              lambda function returns a true.
        """
        _lambda_functions = lambda_functions if isinstance(lambda_functions, list) else [lambda_functions] 
        if not _lambda_functions:
            return (self.text == other.text) and (self.distance_geometric(other) == 0)
        return any(x(self, other) for x in _lambda_functions)
    
    def match_features(self, other, lambda_functions=[]):
        """ returns true if the two segments are considered to have 
            same features.
            *  perfect set inclusion(s1 included in s2) test will be evaluated if no lambda function provided. 
               if list of lambda function are provided then returns true if any 
              lambda function returns a true.
        """
        _lambda_functions = lambda_functions if isinstance(lambda_functions, list) else [lambda_functions]
        # the comparison lambda magic
        if not _lambda_functions:
            return all([x in other.features for x in self.features])
        return any(x(self, other) for x in _lambda_functions)

    def shift_index(self, offset=0):
        """ changes the index of segments by adding offset"""
        pos_add = lambda x, o: x + o if x + o >= 0 else 0
        self.start, self.end = pos_add(self.start, offset), pos_add(self.end, offset)
        
    def copy(self):
        return copy.deepcopy(self)

class SegmentBundle(list):
    """ a list of segments that responds to segment related queries """
    
    def __init__(self, segment_list=[]):
        super(SegmentBundle, self).__init__(segment_list)
    
    def shift_index(self, offset=0, _lamnda_condition_list=[lambda x: True]):
        """ changes the index of segments by adding offset"""
        lamnda_condition_list = _lamnda_condition_list if isinstance(_lamnda_condition_list, list) \
                                                        else [_lamnda_condition_list]
        for i in self:
            if any(x(i) for x in lamnda_condition_list): 
                i.shift_index(offset)
    
    def segment(self, segment_id):
        """ return a segment by it's Id """
        for s in self:
            if s.id == segment_id: return s

    def segments(self, lambda_filters=[]):
        """ returns a list of segments filtered by a list of lambda filters"""
        if not lambda_filters: return self
        return SegmentBundle(multi_filter(self, lambda_filters)) 
    
    def contained(self, host, include_intersecting=False):
        """ returns the segments that are within the limits of the host segment """
        if include_intersecting: return uniquify([x for x in self if host.overlap(x)])
        return uniquify([x for x in self if host.contains(x)])
    
def read_annotation_file(annotation_fileName, text_fileName="", adjust_bom=True):
    """ reads the annotation file and enriches the segments with text if any provided"""
    segmentList, raw_text = SegmentBundle(), None
    # reading the annotation file
    soup = BeautifulSoup(open(annotation_fileName), "xml")
    segments = soup.find_all('segment')
    nas = 0
    for s in segments:
        start = str(s.get("start"))
        stop = str(s.get("end"))
        _id = str(s.get("id"))
        features = str(s.get("features")).split(";")  # [-1] # last feature
        segmentEl = Segment(start, stop, _id, features)
        if s.get("state") == "active":
            segmentList.append(segmentEl)
        else: nas = +1
    # filling in the text if any
    if text_fileName and os.path.exists(text_fileName):
        if adjust_bom: bom_adjust(segmentList, text_fileName)
        # enriching with text 
        raw_text = read_text_UAM_style(text_fileName)
        for segment in segmentList:
            segment.text = raw_text[segment.start:segment.end]
    return segmentList
    
def serialize_segment_bundle(segment_bundle):
    """ 
        writes a segment bundle to file 
    """ 
    if not segment_bundle: raise ValueError("No segment bundle provided for serialization")
    
    soup = BeautifulSoup(features='xml')
    document = soup.new_tag("document")
    soup.append(document)
    headder = soup.new_tag("header")
    text_file = soup.new_tag("textfile")
    document.append(headder)
    headder.append(text_file)
    headder.append(soup.new_tag("lang"))
    headder.append(soup.new_tag("complete"))
    segments = soup.new_tag("segments")
    document.append(segments)
    for seg in segment_bundle:
        segments.append(soup.new_tag("segment", id="%i" % seg.id,
                 start="%i" % seg.start,
                 end="%i" % seg.end,
                 features=";".join(seg.features) if isinstance(seg.features, (list, set, tuple)) else str(seg.features),
                 state="active"))
    return soup.prettify()

###################################################################################################
# Util functions
###################################################################################################

def bom_adjust(segment_bundle, fileName):
    """ readjusts segments in a bundle according to the BOM """
    if bom_offset(fileName): segment_bundle.shift_index(+1)
    
def bom_offset(file_path):
    """ returns the length of BOM marker if any available"""
    with open(file_path, "rb") as test_bom_file:
            r = test_bom_file.read(8)
            for bom in (codecs.BOM, codecs.BOM_BE, codecs.BOM_LE, codecs.BOM_UTF8,
                        codecs.BOM_UTF16, codecs.BOM_UTF16_BE, codecs.BOM_UTF16_LE,
                        codecs.BOM_UTF32, codecs.BOM_UTF32_BE, codecs.BOM_UTF32_LE):
                if r[0:len(bom)] == bom:
                    return len(bom)
    return 0

def read_text_UAM_style(text_file_path, enc='utf-8'):
        """
            * reads a file with a certain encoding. 
            * also and performs some character transformations for compatibility with Stanford parser 
        """
        def translate_some_characters(raw_text):
            iso885915_utf_map = {   
                                u"\u2019":  u"'",
                                u"\u2018":  u"'",
                                u"\u201c":  u'"',
                                u"\u201d":  u'"',
                                u"\u00B4":  u"'",
                                u"\u00A0":  u" ",
                            }
            return raw_text.translate({ord(k):v for k, v in iso885915_utf_map.iteritems()})  # recreating the char table
        # 
        __encoding = enc
        if not os.path.exists(text_file_path):
            raise  IOError("Text file missing : " + text_file_path)
        
        try:
            __raw_text = codecs.open(text_file_path, encoding=__encoding).read()
        except:
            logging.warn("Failed to open the text file due to encoding error, \
                    retrying to open it again with auto encoding detection.")
            sample = open(text_file_path, 'rb').read(min(32, os.path.getsize(text_file_path)))
            __encoding = chardet.detect(sample)['encoding']
            __raw_text = codecs.open(text_file_path, encoding=__encoding, errors="replace").read()
        return translate_some_characters(__raw_text)

def index_substring_words(raw_text, consecutive_word_list, start_offset=0, return_not_found=True):
    """
        returns a list of (word, segment) tuple as they were indexed within the raw text 
    """
    _consecutive_word_list = consecutive_word_list
    widx, not_found = index_words(raw_text, STP_word_translation(_consecutive_word_list), start_offset, return_not_found)
    word_segments = SegmentBundle([Segment(x[1], x[2], x[1], "word", x[0]) for x in widx])
    return word_segments if not return_not_found else word_segments, not_found

# def adjust_segment_position(text, bndl_context):
#     """ """
#     def offset_recursively(segm, ct_dict, offset, seen=set()):
#         """ recursively apply offset to all segments in chain"""
# 
#         # if segm not in ct_dict or not ct_dict[segm]: return
# #         segment = segm
# #         if offset == None:
# #             prec = int(segm.start-segm.len()*factor) if int(segm.start-segm.len()*factor) >= 0 else 0
# #             antec = int(segm.end+segm.len()*factor) if int(segm.end+segm.len()*factor) <= len(text) else len(text) 
# #             window = text[prec : antec]
# #             start_offset = sorted([x.start() for x in re.finditer(ur'('+re.escape(segm.text)+')', window, re.UNICODE)])
# #             if start_offset: offset = start_offset[0]-(segm.start-prec)
#         
#         for s in ct_dict[segm]:
#             if s not in seen:
#                 seen.add(s)
#                 offset_recursively(s, ct_dict, offset, seen)
#         #
#         #
#         segm.shift_index(offset)
#     # ##
#     containement_dict = {x:bndl_context.segments([lambda q:x.contains(q)]) for x in bndl_context}
#     for k, v in containement_dict.iteritems(): v.remove(k)
#          
#     # pprint.pprint(containement_dict)
#     # print len(top_segments)
#     all_subordinate_segment = flatten(containement_dict.values())
#     top_segments = set(bndl_context) - set(all_subordinate_segment)
#     
#     factor = 4
#     for segment in containement_dict.keys():
#         prec = int(segment.start - segment.len() * factor) if int(segment.start - segment.len() * factor) >= 0 else 0
#         antec = int(segment.end + segment.len() * factor) if int(segment.end + segment.len() * factor) <= len(text) else len(text) 
#         window = text[prec : antec]
#         start_offset = sorted([x.start() for x in re.finditer(ur'(' + re.escape(segment.text) + ')', window, re.UNICODE)])
#         if start_offset: 
#             offset = start_offset[0] - (segment.start - prec)
#             offset_recursively(segment, containement_dict, offset)
#     
# #     for k in top_segments: del containement_dict[k]
# #     all_subordinate_segment = flatten(containement_dict.values())
# #     top_segments = set(bndl_context)-set(all_subordinate_segment)
# # 
# #     for segment in containement_dict.keys():
# #         prec = int(segment.start-segment.len()*factor) if int(segment.start-segment.len()*factor) >= 0 else 0
# #         antec = int(segment.end+segment.len()*factor) if int(segment.end+segment.len()*factor) <= len(text) else len(text) 
# #         window = text[prec : antec]
# #         start_offset = sorted([x.start() for x in re.finditer(ur'('+re.escape(segment.text)+')', window, re.UNICODE)])
# #         if start_offset: 
# #             offset = start_offset[0]-(segment.start-prec)
# #             offset_recursively(segment, containement_dict, offset)


def adjust_segment_position_in_context(segment, text,parents):
    """
        re-index segments to match the text and takes into consideration 
        their position relative to parents
    """
    factor = 4
    for prnt in sorted(parents, key=lambda x:x.len()):
     
        prec = int(prnt.start - prnt.len() * factor) if int(prnt.start - prnt.len() * factor) >= 0 else 0
        antec = int(prnt.end + prnt.len() * factor) if int(prnt.end + prnt.len() * factor) <= len(text) else len(text) 
        window = text[prec : antec]
        
        start_offset = sorted([x.start() for x in re.finditer(ur'(' + re.escape(segment.text) + ')', 
                                                              window, re.UNICODE)])
        if start_offset:
            so = start_offset[0] - (segment.start - prec)
            if text[segment.start+so:segment.end+so] == segment.text:
                segment.shift_index(so)
                return so
    return None
        
# def adjust_segment_position1(segment, text):
#     """
#         * if the segment indexes do not match the text index, then search for the segment.text the a radius and 
#         adjust the segment position to match the text.
#         * returns true if segment found and adjusted else if segment has no associated text or not found in radius
#          then returns false else .
#     """
#     if not segment.text or not text or not segment: return False
#     factor = 4
#      
#     prec = int(segment.start - segment.len() * factor) if int(segment.start - segment.len() * factor) >= 0 else 0
#     antec = int(segment.end + segment.len() * factor) if int(segment.end + segment.len() * factor) <= len(text) else len(text) 
#     # radius = segment.len()*4
#      
#     window = text[prec : antec]
#     start_offset = sorted([x.start() for x in re.finditer(ur'(' + re.escape(segment.text) + ')', window, re.UNICODE)])
# #     print window
# #     print prec, antec
#     if start_offset:
#         segment.shift_index(start_offset[0] - (segment.start - prec))
#         return start_offset[0] - (segment.start - prec)
#     return False
    
if __name__ == '__main__':
    # clean_text_file_of_annyoing_unicode_chars("/home/lps/work/git-parsimonious-vole/git-vole/parsimonious-vole/resources/test/Annotation_OE_clean_gold_UAM_1_33_new_segments/Corpus/OE1clean/OE102Sandy260.txt")
    pass
